function staticDataset = build_staticDataset(constraints)
% builds a dataset holding the static prefernces of the user in the format
% required by 'bond_ttl_multi.m'. 
% 
%Input
% =====
% required: constraints.sectors.sectors_to_include      - CELLARRAY list of sectors to include in filter  
%                         constraints.sectors.sectors_to_include_id - DOUBLE NSectorsX1 list of sectors ID to include in filter
% 
% Optional: constraints.static.types_to_include - can have one of two options:
%                                                                                                                        - BOOLEAN vector of NTypes X 1 stating which of the types should be 
%                                                                                                                              included in the static filter
%                                                                                                                        - dataset with the correct format for the bond_ttl_multi 
% 
%                         constraints.static.corpgovToInclude - can have one of two options:
%                                                                                                                         - BOOLEAN vector of 2 X 1 stating which of the corp\gov should be 
%                                                                                                                             included in the static filter
%                                                                                                                         - dataset with the correct format for the bond_ttl_multi 
import bll.filtering_functions.*

if isfield(constraints.sectors,'sectors_to_include')&&...
        ~isempty(constraints.sectors.sectors_to_include)
    SectorsToInclude = constraints.sectors.sectors_to_include;        
else
    SectorsToInclude={};
end
% AFTER YIGAL WILL CHANGE BOND_TTL TO GET ID'S, SHOULD CHANGE TO SOMETHING LIKE THIS:
% if ~isempty(constraints.sectors.sectors_to_include_id)
%     stat_param.sector = constraints.sectors.sectors_to_include_id;
% end

if isfield(constraints,'static')
    if isfield(constraints.static,'types_to_include')&&~isempty(constraints.static.types_to_include)...
            &&( isa(constraints.static.types_to_include,'dataset') || ...
                            ( isnumeric(constraints.static.types_to_include) && sum(constraints.static.types_to_include)~=0 ) )
                %this long condition is ment to enable the creation of type
                %structure only in the case where 'types_to_include' is
                %non-empty and is either a dataset (pre-prepared preferences)
                %or a vector with the 0 and 1's for the selected types with
                %AT LEAST one selected type.                
        if isa(constraints.static.types_to_include,'dataset')
            staticDataset=constraints.static.types_to_include;            
        else
            staticDataset=build_type_struct(SectorsToInclude,constraints.static.types_to_include);
        end
    elseif isfield(constraints.static,'corpgovToInclude')&&~isempty(constraints.static.corpgovToInclude)...
              &&( isa(constraints.static.types_to_include,'dataset') || ...
                           ( isnumeric(constraints.static.corpgovToInclude) && sum(constraints.static.corpgovToInclude)~=0 ) )
        if isa(constraints.static.corpgovToInclude,'dataset')                          
            staticDataset=constraints.static.corpgovToInclude;
        else
            staticDataset=build_corpgov_struct(SectorsToInclude,constraints.static.corpgovToInclude);
        end
    else %meaning there are static constraints but there are no 'type' preferences
        staticDataset=build_corpgov_struct(SectorsToInclude);
    end
else %meaning there are no static preferences
    staticDataset=build_corpgov_struct(SectorsToInclude);
end