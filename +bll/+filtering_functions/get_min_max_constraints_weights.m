function [sectorsMinWeight staticMinWeight sectorsMaxWeight staticMaxWeight] =  get_min_max_constraints_weights(constraints,conSector,conTypes)
% get_min_max_constraints_weights assigns the correct min\max values for constraints
%   [sectorsMinWeight staticMinWeight sectorsMaxWeight staticMaxWeight] =  get_min_max_constraints_weights(constraints,conSector,conTypes)
%   receives the belonging constraints-belonging conSector and conTypes and
%   produces the constraints' min\max weights of the same length
%   Input:
%       required
%           constraints - structure with the details of the constraints, as
%           specified in filter_by_constraints and constraints_struct
%           conSector - (NConstraintsSector X NAssets) the 'belonging' matrix of the
%           sectors.
%           conTypes  -   (NConstraintsType X NAssets) the 'belonging' matrix of the
%           sectors.
%
%       Optional
%           constraints.sectors.minWeight\maxWeight - the minimal\maximal
%           weight of constraint of the different sectors  under
%           constraint. If it is a vector - it should be of length of the
%           number of constraint sectors. if it is a scalar - it is
%           assumed to be constant for all constraint sectors. if it doesnt
%           exist - default value is used.
%           constraints.static.minWeight\maxWeight - the minimal\maximal
%           weight of constraint of the different types  under
%           constraint. If it is a vector - it should be of length of the
%           number of constraint types. if it is a scalar - it is
%           assumed to be constant for all constraint types. if it doesnt
%           exist - default value is used.
%           
% Written by: Amit Godel, 04-Feb-2013
% Copyright 2013, BondIT Ltd.

DEFAULT_MIN=0;
DEFAULT_MAX=1;

if isfield(constraints,'sectors') && isfield(constraints.sectors,'minWeight')
    if size(conSector,1)==size(constraints.sectors.minWeight,1)
        % The case where constraints.sectors.minWeight is in the correct
        % size - like the sectors constraints length
        sectorsMinWeight=constraints.sectors.minWeight;
    else
        % We assuming  constraints.sectors.minWeight is a scalar.
        % if not - we'll use only it's first element. enough is enough!
        sectorsMinWeight=constraints.sectors.minWeight(1)*ones(size(conSector,1),1);
    end
else
    % No specfied minWeight - we shall use default values
    sectorsMinWeight=DEFAULT_MIN*ones(size(conSector,1),1);
end
% the rest of the code is just a repetition of the above, so remarks are
% ommited.
if isfield(constraints,'sectors') && isfield(constraints.sectors,'maxWeight')
    if size(conSector,1)==size(constraints.sectors.maxWeight,1)
        sectorsMaxWeight=constraints.sectors.maxWeight;
    else
        sectorsMaxWeight=constraints.sectors.maxWeight(1)*ones(size(conSector,1),1);
    end
else
    sectorsMaxWeight=DEFAULT_MAX*ones(size(conSector,1),1);
end

if isfield(constraints,'static') && isfield(constraints.static,'minWeight')
    if size(conTypes,1)==size(constraints.static.minWeight,1)
        staticMinWeight=constraints.static.minWeight;
    else
        staticMinWeight=constraints.static.minWeight(1)*ones(size(conTypes,1),1);
    end
else
    staticMinWeight=DEFAULT_MIN*ones(size(conTypes,1),1);
end

if isfield(constraints,'static') && isfield(constraints.static,'maxWeight')
    if size(conTypes,1)==size(constraints.static.maxWeight,1)
        staticMaxWeight=constraints.static.maxWeight;
    else
        staticMaxWeight=constraints.static.maxWeight(1)*ones(size(conTypes,1),1);
    end
else
    staticMaxWeight=DEFAULT_MAX*ones(size(conTypes,1),1);
end
