function [solver_props] = create_solver_props()
% 
% creates data structure with necessary fields for solver_props.

%% Set all model flags = 0 initially
solver_props.Markowitz = 0;
solver_props.SingleIndex = 0;
solver_props.MaxOmega = 0;
solver_props.MultiIndex = 0;
solver_props.Spread = 0;

solver_props.ExpInflation = 0.03; % when dynamic data available, change
solver_props.TypeFilterParams = cell(7,1); %if types change - need to update
solver_props.UserBondTypes = zeros(1, 7); %no types initially...
solver_props.BondsPerType = zeros(7, 1);%  handles.UserBondTypes';

solver_props.UseTermStructure = 0; %Flag whether or not to use Term structure
solver_props.Sigma_eiMethod = 1; %Flag whether or not to use sigma e_i method

solver_props.MaxCorpGovWeight = [1; 1]; %Max and min for the corporate/government types total weights
solver_props.MinCorpGovWeight = [0; 0];
solver_props.MaxSectorWeight= []; %Max and min weights for the sectors - if specific sectors have 
                                  %other max/min weights, will be updated in the sectors choice section
solver_props.MinSectorWeight= [];

solver_props.TargetBetaFlag = 0; %Flag whether a target beta is wanted
solver_props.TargetBeta = 0; %If Target beta is wanted, holds the value

solver_props.MaxBondWeight = .1; %Initial max bond weight, default value
solver_props.MinBondWeight = eps; %Initial min bond weight, default value

solver_props.ConSeterror = []; %Initialized, built in sovler
%% Algorithmicc flags here
solver_props.MaxOmega = 0; 
solver_props.MinDownsideRisk = 0;
solver_props.Mar  = 0;
solver_props.TarRet = 0;
solver_props.FminConFlag = 0;
solver_props.frontier_point = [];% (NEW) which point on the frontier to solve for
solver_props.NumPorts = 2;
solver_props.HigherMoments = 0;
% solver_props.ConMinNumOfBonds = 0; % Not used
% solver_props.ConMaxNumOfBonds = 0;
% solver_props.ConMinWeight2AlloBonds = 0;
% solver_props.ConMaxWeight2NonAlloBonds = 0;

solver_props.MinNumOfBonds = ceil(1/solver_props.MaxBondWeight); %Minimum number of bonds allowed
solver_props.MaxNumOfBonds = 8000; %Max number for default purposes
solver_props.MinWeight2AllocBonds = .0010000001; %Min weight for allocated bonds in solution initially
solver_props.MaxWeight2NonAllocBonds = .0015; %Cut off for bonds put into solution

solver_props.indices_of_used_sectors  = []; %index of which sectors the user wants

solver_props.sector_weight_flag = 0; % (NEW) if there is a sector weight that is not 0/1
solver_props.CorpGovTypes = [0 0]; %nonzero only if one of them is not the usual 0/1

solver_props.solved = 0; %(NEW) flag marking that solver has been used once and hence bond data has been pulled and stored in nsin_data

solver_props.number_nans_allowed = 5; %Number of nans allowed in bond data before bond is tossed from list
solver_props.max_weight_changed_flag = 0; %(NEW) for use in case max weight was changed outside solver - redo conset. 

%% Default values for the choose functions
solver_props.AddTCConstraint = 0;

solver_props.sell_cost = 0; %For transaction cost, default values. 
solver_props.buy_cost = 0;

solver_props.benchmarkSolutionFlag = 0; %

