function [MatOut, nsins2throw, dates_spots_erased] = GetRidOfNan(ReturnMat, flagMarkowitz, NumNanAllowed)
%
%
%    input: ReturnMat - mDAYS X nASSETS matrix of prices of bonds
%           
%           flagMarkowitz - 0/1 flag stating whether or not Markowitz is
%               the model - 1 = yes.
%
%           NumNanAllowed - scalar dictating number of Nans allowed right now.
%
%    output: MatOut - (m-dates_spots_erased)DAYS X (n-nsins2throw)ASSETS
%             matrix of prices without NaNs
%
%            nsins2throw - DOUBLE - array of nsins removed for having more
%             than NumNanAllowed NaNs in their prices
%
%            dates_spots_erased - DOUBLE - array of dates erased due to
%             having NaNs but containing less than NumNanAllowed nan's and were erased.



import bll.simulation.*;
import dml.*;
import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;

dates_spots_erased = [];

nsins2throw = [];
MatOut = ReturnMat;                 %return mat
NansInMat = sum(isnan(MatOut)); %count number of NaN's
locationNans = find(NansInMat~=0);  %spots of nsins which have NaN's
NumNans = sum(NansInMat>0);             %number of entries which are not NaN's

if flagMarkowitz %In Markowitz, we need the number of observation to be larger than the number of assets
    if NumNans~=0
        for nsin_num_w_nan = NumNans:-1:1
            value = locationNans(nsin_num_w_nan);
            nans_in_vector_of_nsin = isnan(MatOut(:, value)); %for what days is it NaN
            u = find(nans_in_vector_of_nsin~=0);
            if NansInMat(value)> NumNanAllowed || size(MatOut, 1)<=size(MatOut, 2)+length(u)
                %if there are more than 5 instances of NaN or there are not
                %enough days with obs (Markowitz, must have num nsins = num
                %obs
                
                MatOut(:, value) = []; %erase value with NaN
                nsins2throw(end+1) = value; %remember spot of nsin
            else
                nans_in_vector_of_nsin = isnan(MatOut(:, value));
                u = find(nans_in_vector_of_nsin~=0);
                for b = 1:length(u)
                    MatOut(u(length(u)-b+1), :) = []; %otherwise simply erase all rows where there was NaN for this nsin
                end
                dates_spots_erased = [dates_spots_erased u];
                
            end
        end
    end
else  %%Not Markowitz
    if NumNans~=0
        for nsin_num_w_nan = NumNans:-1:1
            value = locationNans(nsin_num_w_nan);
            nans_in_vector_of_nsin = isnan(MatOut(:, value));
            u = find(nans_in_vector_of_nsin~=0);
            if NansInMat(value)> NumNanAllowed
                MatOut(:, value) = [];
                nsins2throw(end+1) = value; %remember spot of nsin
            else
                nans_in_vector_of_nsin = isnan(MatOut(:, value));
                u = find(nans_in_vector_of_nsin~=0);
                for b = 1:length(u)
                    MatOut(u(length(u)-b+1), :) = [];
                end
                dates_spots_erased = [dates_spots_erased u];
            end
        end
    end
end

dates_spots_erased = unique(dates_spots_erased);
