function [Flag, selected_indx] =  keep_only_nsin_with_large_weights(solution,  MinNumOfBonds, MaxWeight2NonAllocBonds)
% function takes a solution in terms of weights and the minimum number of bonds allowed and removes bonds that have less than acceptable weight, returns the bonds removed and a flag set to 1 if bonds were removed.  
% 
% input: solution - structure containing the solution received from the optimization algorithm including the nsin list and the corresponding weights
% 
%        MinNumOfBonds - value equaling the minimum number of bonds allowed in a solutin
%        
%        MaxWeight2NonAllocBonds - DOUBLE - number determining max weight
%           that can be assigned to bonds so that they are taken into
%           account even if not in portfolio.
%        
% output: Flag - value 0/1 answer.  if set to 1, then bonds were removed.
% 
%         selected_indx - Nassets X 1 vector containing the index locations of the bonds removed due to low weight
%         
%         
% Written by Idan Oren, BondIT 2012
% Code review by Hillel Raz, BondIT 2013



Wts = solution.PortWts; 
nsin = solution.nsin_sol;

selected_indx=find(Wts>MaxWeight2NonAllocBonds*sum(Wts));

NumBondsofNonZeroWeights = sum(Wts>1000*eps);

NumGoodBonds = length(selected_indx);
if size(Wts, 2)~=1 %%%Make sure this is a column vector rather than row vector
    Wts = Wts';
end
if size(nsin, 2)~=1 %%%Make sure this is a column vector rather than row vector
    nsin = nsin';
end
Flag = 0;

if NumGoodBonds~= NumBondsofNonZeroWeights
    Wts_nsinMat = [Wts nsin];
    SortedWtsnsin = sortrows(Wts_nsinMat);
    SortedWtsnsin = flipud(SortedWtsnsin); %matrix now organized with largest weight on top, descending in order to bottom
    if NumGoodBonds<MinNumOfBonds
        SmallestAcceptableWeight = SortedWtsnsin(ceil(1.5*MinNumOfBonds), 1); %take a bit more than the minimum allowed number of bonds and re-optimize
        selected_indx = find(Wts>=SmallestAcceptableWeight);
        %nsin_out = SortedWtsnsin(1: NumGoodBonds+10, 2);
        %selected_indx = find(nsin_out== Wts_nsinMat(:,2));
        %Wts_out = SortedWtsnsin(1: NumGoodBonds+10, 1);
    %else
         %nsin_out = SortedWtsnsin(1: NumGoodBonds, 2);
         %Wts_out = SortedWtsnsin(1: NumGoodBonds, 1);
    end
    Flag = 1;
end

if sum(selected_indx)
    Flag = 1;
end
    


% if NumGoodBonds~= NumBondsofNonZeroWeights
%     Wts_nsinMat = [Wts nsin];
%     SortedWtsnsin = sortrows(Wts_nsinMat);
%     SortedWtsnsin = flipud(SortedWtsnsin);
%     NumGoodBonds = sum(Wts>0.0075*sum(Wts));
%     if NumGoodBonds<10 %~NumMinBonds
%         nsin_out = SortedWtsnsin(1: NumGoodBonds+10, 2);
%         Wts_out = SortedWtsnsin(1: NumGoodBonds+10, 1);
%     else
%         nsin_out = SortedWtsnsin(1: NumGoodBonds, 2);
%          Wts_out = SortedWtsnsin(1: NumGoodBonds, 1);
%     end
% end




