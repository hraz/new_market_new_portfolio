function [name bond_type linkage_index coupon_type Duration YTM sector BondInfo]=...
    GetLastKnownData(Dataset,YtmFTS,ModDurFTS,PriceFTS,RankDataSet, solver_props, DateNum)
%this function extracts some of the fields of the bonds that passed the filters
import bll.term_structure.z_spread.*;
import bll.term_structure.bootstrap_mdl.*;
import dal.data_acess.cash_flow.*;
import bll.inflation_mdl.*;
import gui.*;
% dbstop if error
nsin=Dataset.security_id;      %this is identical to handles.nsin (which is already sorted), so there is no need to output it from this function
name=Dataset.name;
bond_type=Dataset.bond_type;
linkage_index=Dataset.linkage_index;
nsin_non_linked_index=find(strcmp(linkage_index,'NON LINKED'));
nsin_non_linked=nsin(nsin_non_linked_index);
coupon_type=Dataset.coupon_type;
Maalot=RankDataSet.rating_maalot;
Midroog=RankDataSet.rating_midroog;
YTM=(YtmFTS(end, :)).';
YTM=AddExpInflation(YTM,linkage_index,solver_props.ExpInflation);  %add expected inflation to YTM. 
if solver_props.UseTermStructure   %using term structre to calc a more accurate YTM
    %% Build zero spot curve .
    %can use what ever model, here we use bootstrap
    settle = DateNum; %datenum('2008-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.
    param.compounding = 1;
    param.basis = 10;
    param.interpmethod = 'cubic';
    
    %% Cash flow build:
    [ tbill_cf ] = cfbuilder(settle, 0);
    [ shahar_cf ] = cfbuilder(settle, 1);
    gov_cf = merge(tbill_cf, shahar_cf);
    
    %% Build zero-coupon curve:
    
    [ gov_ircurve ] = bootstrap( 'zero', gov_cf, param );
    % figure; plot(gov_ircurve.Data, '.-r')
    % hold on
    %% calculate z-spread
    MeanPortDuration = mean([handles.MaxPortDuration handles.MaxPortDuration]); %find the mean portfolio duration of the user, according to what he entered as an input. This is in years.
    LastInvestmentDay =  handles.DateNum + MeanPortDuration*365; %rdate at which z-spread is wanted. translate the current date and the mean portfolio duration of the user to a future date for which the yield of the bonds will be calculated. convert year to days and add to DateNum - the numeric day of today    
    [zSpread ExpInt ] = z_spread(gov_ircurve.Data, nsin_non_linked, LastInvestmentDay);   %ExpInt - is what the term structure gives us. 
    for i=1:length(zSpread) %calc the bond rate for each bond by itself, since the zSpread is given as a vector (zSpread per bond). By adding the Zspread of bond i to the gov yield curve, we receive the term structure of the bond
     bond_rate(:,i) = fts2mat(gov_ircurve.Data) + zSpread(i); %bond_rate is a matrix, whose number of columns is equal to the number of bonds, and the number of rows is equal to the number of point in the gov term structure
    end
    bond_dates = gov_ircurve.Data.dates;
    if 0  %change to 1 if you wish to see the term structure
        plot(bond_dates, bond_rate, '--b');   
    end
    YTM(nsin_non_linked_index(~isnan(ExpInt)))= ExpInt(~isnan(ExpInt)); %replace the YTMs with the new Expected intereset that was estimated using the term structre
end
Duration=(ModDurFTS(end, :)).';
sector=Dataset.sector;
price=(PriceFTS(end, :)).';
%convexity = fts2mat(ConvexityFTS(end)).';
BondInfo=[num2cell(YTM*100) num2cell(Duration) bond_type linkage_index coupon_type...
    Maalot Midroog num2cell(price) num2cell(nsin)]; %This is all the data together, in order to write the Xls file instantly.
