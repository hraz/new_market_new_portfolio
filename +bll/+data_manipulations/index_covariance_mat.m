function [IndexRisk] = index_covariance_mat(IndexPriceYld, HistLen)
% Hillel Raz, June 11th, BABY DAY!!!
% Copyright 2013, Bond IT Ltd

import bll.data_manipulations.*;
import dal.market.get.market_indx.*;

IndexReturnMat=IndexPriceYld';
cutSpot = min(HistLen, length(IndexReturnMat));
IndexReturnMat = IndexReturnMat(end-cutSpot+1:end);

IndexReturnMat(isnan(IndexReturnMat)) = [];
IndexRisk = std(IndexReturnMat);


