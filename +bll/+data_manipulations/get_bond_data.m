function [nsin_data] = get_bond_data(filtered, solver_props, constraints, DateNum)
% Get both static and dynamic data for each bond that passed the filters.
% The static data is given in a single dataset, with differetn fields.
% The dynamic data is saved in seperated FTSs.
import dal.market.get.dynamic.*;
import dal.market.get.static.*;
import dal.market.get.risk.*;
import bll.inflation_mdl.*;
import utility.*;

global GLOBALDSLINKAGEINDEX;

%with amit's ConSet
 data_list = {'linkage_index', 'redemption_type'}; %{'nsin', 'name', 'bond_type','linkage_index','coupon_type','sector'};

%without
%data_list = {'nsin', 'name', 'bond_type','linkage_index','coupon_type','sector'};

%% With Caching
tic; nsinAll = GLOBALDSLINKAGEINDEX.security_id;
nsinFiltered = filtered.nsin_list;
index = [];
for iNsinFiltered = 1:length(nsinFiltered)
    spotID = find(nsinAll == nsinFiltered(iNsinFiltered));
    index = [index spotID];
end
nsin_data.Dataset = GLOBALDSLINKAGEINDEX(index, :);
 y = repmat('multi', length(nsin_data.Dataset), 1);
nsin_data.Dataset.redemption_type = cellstr(y);
toc

%%


% nsin_data.Dataset = get_bond_stat( filtered.nsin_list, DateNum, data_list);

% without Amit's
    nsin_data.RankDataSet = get_bond_risk(filtered.nsin_list, DateNum);

nsin_data.nsin=nsin_data.Dataset.security_id;      %this is identical to handles.nsin (which is already sorted), so there is no need to output it from this function
nsin_data.redemption_type = nsin_data.Dataset.redemption_type;

nsin_data.Maalot=nsin_data.RankDataSet.rating_maalot;
nsin_data.Midroog=nsin_data.RankDataSet.rating_midroog;
% nsin_data.sector=nsin_data.Dataset.sector;


%with Amit's
nsin_data.linkage_index=nsin_data.Dataset.linkage_index;

% %%With Caching
% 
% load(fullfile('ftsCleanPriceAllBonds.mat'));
% mtxCache = fts2mat(FTSPRICECLEAN.price_clean_yld);
% fields = strid2numid(fieldnames(FTSPRICECLEAN.price_clean_yld));
% fields = fields(4: end);
% dates = FTSPRICECLEAN.price_clean_yld.dates;
% index = [];
% for nNewID = 1:length(filtered.nsin_list)
%     spotID = find(fields == filtered.nsin_list(nNewID));
%     index = [index spotID];
% end
% dateIndexFirst = find(FTSPRICECLEAN.price_clean_yld.dates <= DateNum - 2*constraints.dynamic.hist_len, 1, 'last');
% dateIndexLast = find(FTSPRICECLEAN.price_clean_yld.dates <= DateNum, 1, 'last');
% [price_fts1] = fints(dates(dateIndexFirst:dateIndexLast), mtxCache(dateIndexFirst:dateIndexLast, index), numid2strid(filtered.nsin_list));
% 
% %%


tbl_name = 'bond';
data_name = {'price_clean_yld'}; %'conv',%need history length worth of values
[price_fts] = get_multi_dyn_between_dates( filtered.nsin_list , data_name , tbl_name, DateNum - 2*constraints.dynamic.hist_len, DateNum);

%%

% %%With Caching
% 
% load(fullfile('structYldAndTtmAllBonds.mat'));
% mtxCacheYld = fts2mat(YIELDTTMSTRUCT.yield);
% mtxCacheTtm = fts2mat(YIELDTTMSTRUCT.ttm);
% 
% fields = strid2numid(fieldnames(YIELDTTMSTRUCT.yield));
% fields = fields(4: end);
% dates = YIELDTTMSTRUCT.yield.dates;
% index = [];
% for nNewID = 1:length(filtered.nsin_list)
%     spotID = find(fields == filtered.nsin_list(nNewID));
%     index = [index spotID];
% end
% dateIndexYld = find(YIELDTTMSTRUCT.yield.dates <= DateNum, 1, 'last');
% [ftsYld] = fints(dates(dateIndexYld), mtxCacheYld(dateIndexYld, index), numid2strid(filtered.nsin_list));
% dateIndexTtm = find(YIELDTTMSTRUCT.ttm.dates <= DateNum, 1, 'last');
% [ftsTtm] = fints(dates(dateIndexTtm), mtxCacheTtm(dateIndexTtm, index), numid2strid(filtered.nsin_list));
% res_fts1 = struct();
% res_fts1.yield = ftsYld;
% res_fts1.ttm = ftsTtm;
% 
% %%

data_name =  {'yield', 'ttm'}; %need only final date's data

[res_fts] = get_multi_dyn_between_dates( filtered.nsin_list , data_name , tbl_name, DateNum, DateNum);

%    [res_fts] = get_multi_dyn( handles.nsin , data_name , tbl_name,  handles.DateNum);

nsin_data.YTM = fts2mat(res_fts.yield)'; %changed ytm to yield per Yigal's request/demand. da_bond_dyn( handles.nsin, 'ytm', handles.DateNum );

YTM=AddExpInflation(nsin_data.YTM,nsin_data.linkage_index,solver_props.ExpInflation);

nsin_data.Duration = fts2mat(res_fts.ttm)'; %da_bond_dyn( handles.nsin, 'mod_dur', handles.DateNum );

nsin_data.price_clean_yld=fts2mat(price_fts.price_clean_yld)';



%     nsin_data.PriceFTS = fts2mat(res_fts.price_dirty); %da_bond_dyn( handles.nsin, 'price_dirty', handles.DateNum);
%     handles_PriceFTS = nsin_data.PriceFTS;
%     nsin_data.PriceYld = fts2mat(res_fts.price_yld); %da_bond_dyn( handles.nsin, 'price_yld', handles.DateNum );
%     handles_PriceYld = nsin_data.PriceYld;
%


%     nsin_data.ConvexityFTS = res_fts.conv; %da_bond_dyn( handles.nsin, 'conv', handles.DateNum );
%     handles_ConvexityFTS = nsin_data.ConvexityFTS;

%     nsin_data.RankDataSet = get_bond_risk(filtered.nsin, DateNum);
%     handles_RankDataSet = nsin_data.RankDataSet;

%     [nsin_data.Name nsin_data.Type nsin_data.Linkage nsin_data.InterestType ...
%         nsin_data.Duration nsin_data.YTM nsin_data.Sector nsin_data.BondInfo]=...
%         GetLastKnownDataSim(handles_Dataset,handles_YtmFTS,handles_ModDurFTS,...
%         handles_PriceFTS, handles_RankDataSet, solver_props, DateNum);  %get the most recent observations in the FTS and the static data, and assign to variables which will be used to identify all bonds
%     handles_Name = nsin_data.Name;
%     handles_Type = nsin_data.Type;
%     handles_Linkage = nsin_data.Linkage;
%     handles_InterestType = nsin_data.InterestType;
%     handles_Duration = nsin_data.Duration;
%     handles_YTM = nsin_data.YTM;
%     handles_Sector = nsin_data.Sector;
%     handles_BondInfo = nsin_data.BondInfo;
%