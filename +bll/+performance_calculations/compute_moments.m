function [portfolioReturn portfolioStd portfolioSkewness portfolioKurtosis] = compute_moments(portfolioValue)


%   COMPUTE_MOMENTS computes the first four moments of the portfolio returns.
%			
%   [portfolioReturn portfolioStd portfolioSkewness portfolioKurtosis] = compute_moments(portfolioValue)
%   receives the portfolio values over time, and returns the first four
%   moments of the portfolio value random variable. The four moment are:
%   the expected value, the standard deviation (second moment squared root), the skewness and the kurtosis.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days. 
%   Output:
%       portfolioReturn � annual return that the portfolio has yielded, in
%       percentages (expressed as a fractional number)
%       portfolioStd - annual standard deviation of the portfolio, in
%       percentages (expressed as a fractional number).
%       portfolioSkewness - annual Skewness of the portfolio, in percentages. Annualized?
%       portfolioKurtosis - annual Kurtosis of the portfolio, in percentages. Annualized?
%   Sample:
%		Some example of the function using.
% 	
%		See Also:
%		compure_alpha, compute_beta, compute_omega,
%		compute_ratios, compute_downside_volatility, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

global NBUSINESSDAYS;
import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0                                              
   error('compute_omega:wrongInputSize','portfolioValue is expected to be a vector.')
end

import gui.*

%% compute moments 
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;   % Daily return series of the portfolio in percentages.
portfolioDailyReturn = mean(portfolioReturnSeries);                          % Average daily return over the investment period
portfolioReturn = Day2Year(portfolioDailyReturn);                            % Annual portfolio return is given by calculating the equivalent annual return to the estimated daily return. 
portfolioStd = std(portfolioReturnSeries)*sqrt(NBUSINESSDAYS);                          % Annual stardard deviation
portfolioSkewness = skewness(portfolioReturnSeries) / sqrt(NBUSINESSDAYS);
portfolioKurtosis = ( kurtosis(portfolioReturnSeries)-3 ) / NBUSINESSDAYS;


