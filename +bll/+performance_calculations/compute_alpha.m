function alpha = compute_alpha(portfolioValue, marketReturn, beta)


%   COMPUTE_ALPHA computes the alpha of the portfolio based on the portfolio returns,the
%   market index returns, and the portfolio beta.
%
%   alpha = compute_alpha(portfolioValue, marketReturn, beta)
%   receives the portfolio values over time, the market returns vector,
%   the beta of the portfolio, and returns the alpha of the portfolio.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days.
%       marketReturn - a vector of market returns, given in the same
%       times frequency as portfolioValue.
%       beta - the beta of the portfolio with the market, as defined in
%       compute_beta.
%   Output:
%       alpha � excess return of the portfolio over the market, as is defined by
%       the single index model: alpha = (portfolio return) - beta * (market return).
%       Alpha is returned in annualized terms.
%   Sample:
%		Some example of the function using.
%
%		See Also:
%		compute_beta, compute_omega, compute_moments,
%		compute_ratios, compute_downside_volatility, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
s2=size(marketReturn);
s3=size(beta);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif ~isa(marketReturn,'numeric')
    error('compute_omega:wrongInputType','marketReturn is expected to be a numeric argument');
elseif ~isa(beta,'numeric')
    error('compute_omega:wrongInputType','beta is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0
    error('compute_omega:wrongInputSize','portfolioValue is expected to be a vector.')
elseif sum(s2>1)==2 || sum(s2>1) == 0
    error('compute_omega:wrongInputSize','marketReturn is expected to be a vector.')
elseif sum(s3)~=2
    error('compute_omega:wrongInputSize','beta is expected to be a scaler')
elseif s1(1)>s1(2) %a column vector
    if s1(1)~=s2(1)+1 %number of rows in the portfolioValue vector must be greater than the number of rows in the marketReturn vector
        error('compute_omega:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
elseif s1(1)<s1(2) %same for the case of row vector
    if s1(2)~=s2(2)+1
        error('compute_omega:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
end

%% compute alpha
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
portfolioMeanDailyReturn = mean(portfolioReturnSeries);                            % Average daily return over the investment period
marketMeanDailyReturn = mean(marketReturn);                                        % Average daily return of the market index over the investment period
alphaDaily = portfolioMeanDailyReturn - beta * marketMeanDailyReturn ;                      % Difference between the measured returns and the returns expected by the single index model
alpha = Day2Year(alphaDaily);