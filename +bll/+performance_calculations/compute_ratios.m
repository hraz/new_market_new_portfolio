function  [sharpeRatio informationRatio] = compute_ratios(portfolioValue, marketReturnDaily, risklessRate)

%   COMPUTE_RATIOS computes Shapre ratio and Information Ratio
%
%   [sharpeRatio informationRatio] = compute_ratios(portfolioValue, marketReturn, risklessRate)
%   receives the portfolio values over time, the market return series, the
%   riskless rate of return, and returns Sharpe ratio and Information ratio.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received coupons) in
%       different days.
%       marketReturnDaily - a vector of market returns, given in the same
%       times frequency as portfolioValue.
%       risklessRate - annual risk less rate, i.e., the annual interest of
%       a 1 year band deposite (usually taken as the interest given by the
%       longest Makam).
%   Output:
%       sharpeRatio � the shape ratio, defined as the maximal slope between the riskless return rate
%       and all points of the efficient frontier:
%       (sharpe return - risk free rate) / shapre risk
%       informationRatio - the ratio of relative return to relative risk. It has the form of:
%       mean(portfoli - market) / Sigma (portfolio - market)
%   Sample:
%		Some example of the function using.
%
%		See Also:
%		compure_alpha, compute_beta, compute_omega, compute_moments,
%		compute_downside_volatility, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
s2=size(marketReturnDaily);
s3=size(risklessRate);
if ~isa(portfolioValue,'numeric')
    error('compute_ratios:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif ~isa(marketReturnDaily,'numeric')
    error('compute_ratios:wrongInputType','marketReturn is expected to be a numeric argument');
elseif ~isa(risklessRate,'numeric')
    error('compute_ratios:wrongInputType','risklessRate is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0
    error('compute_ratios:wrongInputSize','portfolioValue is expected to be a vector.')
elseif sum(s2>1)==2 || sum(s2>1) == 0
    error('compute_ratios:wrongInputSize','marketReturn is expected to be a vector.') 
elseif s1(2)~=1 || s2(2)~=1
    error('compute_ratios:wrongInputSize','portfolioValue and marketReturn are expected to be column vectors.')
elseif risklessRate<0
    error('compute_ratios:wrongValue','risklessRate is expected to be a positive scalar')
elseif risklessRate>1
    error('compute_ratios:wrongValue','risklessRate is expected to be a fractional scalar, e.g., for risklessRate = 2 percent, enter 0.02 instead of 2');
elseif s1(1)>s1(2) %a column vector
    if s1(1)~=s2(1)+1 %number of rows in the portfolioValue vector must be greater than the number of rows in the marketReturn vector
        error('compute_ratios:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
elseif s1(1)<s1(2) %same for the case of row vector
    if s1(2)~=s2(2)+1
        error('compute_ratios:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end    
end
%% compute Sharpe and Information ratios

portfolioReturnSeriesDaily = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
risklessRateDay = Year2Day(risklessRate);
sharpeRatio = sharpe(portfolioReturnSeriesDaily , risklessRateDay );
informationRatio = inforatio(portfolioReturnSeriesDaily, marketReturnDaily);

