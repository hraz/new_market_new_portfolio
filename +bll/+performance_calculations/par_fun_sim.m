function    par_fun_sim(handles, sim_num, filtered_nsin, reinv_low_bnd, reinv_up_bnd, date_vec, duration_vec );
act_name=[];
import dal.portfolio.crud.*;


try
    errorDS  = dataset({}, 'VarNames', 'err_msg');
    
    import bll.simulation.*;
    import dal.market.filter.market_indx.*;
    import dal.market.get.dynamic.*;
    import dal.portfolio.data_access.*;
    import dal.portfolio.crud.*;
    import dml.*;
    import simulations.*;
    
    %% Initiate handles
    handles = rmfield(handles,{'MarkowitzG', 'SingleIndexG', 'MultiIndexG'});        
    handles.MaxBondWeight = 0.2 + 1000*eps;
    handles.MinNumOfBonds = 5;

    %%  Optimization choice
    handles = rmfield(handles, {'MinVar', 'TarRetType', 'Opt', 'OptTarRet', 'TarRet'});        
    mod_num = ceil(sim_num/216000);   
    mod_sim = mod(sim_num, 216000);
    
    %% Get bonds from filteration vectors
    handles = update_filtered_handles(handles,mod_sim, date_vec, duration_vec);      
    t_0 = handles.DateNum; %update date according to filter vector.
    
    %% Solve for optimal portfolio
    handles.nsin = filtered_nsin{:};
    handles.nsin0 = filtered_nsin{:};%update nsin list to filtered list received from amit
    handles.NumBonds = length(handles.nsin);
    handles.BondsPerType0 =  zeros(7,1);%handles.UserBondTypes';
    handles.BondsPerType =  zeros(7, 1);%  handles.UserBondTypes';

    %% Initiate model
    handles.MarkowitzG = 0;
    handles.SingleIndexG = 0;
    handles.MultiIndexG = 0;
    handles.Spread = 0;
    handles.MinVar = 0;
    handles.TarRetType = 0;
    handles.Opt = 0;
    handles.OptTarRet = 0;
    handles.TarRet = 0;    
mod_num =2;
    switch mod_num
    
        case 1
            handles.MarkowitzG = 1;
            handles.MinVar = 1;
            mod_name ='Markowitz';
            opt_name = 'MinVar';
            act_name = 'MMV';

        case  2
            handles.SingleIndexG = 1;
            handles.Opt = 1;
            mod_name = 'SingleIndex';
            opt_name = 'OptSharpe';
            act_name = 'SIO';

        case  3
            handles.SingleIndexG = 1;
            handles.MinVar = 1;
            mod_name = 'SingleIndex';
            opt_name = 'MinVar';
            act_name = 'SIMV';

        case 4
            handles.SingleIndexG = 1;
            handles.OptTarRet = 1;
            handles.TarRet = 4; %target return of 4%
            mod_name = 'SingleIndex';
            opt_name= 'OptTarRet';
            act_name = 'SIOTo4-';

        case 5
            handles.SingleIndexG = 1;
            handles.OptTarRet = 1;
            handles.TarRet = 8; %target return of 8%
            mod_name = 'SingleIndex';
            opt_name= 'OptTarRet';
            act_name = 'SIOTo8-';

        case 6
            handles.SingleIndexG = 1;
            handles.OptTarRet = 1;
            handles.TarRet = 12; %target return of 12%
            mod_name = 'SingleIndex';
            opt_name = 'OptTarRet';
            act_name = 'SIOTo12-';
    end        
            
    if handles.NumBonds > 4 %otherwise solution is not feasible with constraint of at most 10% per bond.
        if ((mod(ceil(sim_num/60)-1, 4) +1)) == 1 %if bond duration is 2, then there is no point in
           %building a portfolio for more than 2 years (this is the same as
           %the first location in duration_vec = [2 4 6 10]
            port_duration_max = 3;
            [errors1 errors2 handles] = SolverNoGui(handles, port_duration_max);
            
            if ~isempty(errors1)
                errors1 = [errors1; act_name];
                errorDS = [errorDS; dataset(cellstr(errors1), 'VarNames', 'err_msg')];
            end
            if ~isempty(errors2)
                errors2 = [errors2; act_name];                
                errorDS = [errorDS; dataset(cellstr(errors2), 'VarNames', 'err_msg')];
            end
            
            handles.Wts(handles.Wts <= handles.MaxWeight2NonAllocBonds * 1.5) = 0;
            %  Print zeros where the weighs are smaller than MaxWeight2NonAllocBonds -- the maximum weight that a bond can have
            %  while stil be considered as a bond which was not included in the portfolio
            nsin_i = handles.nsin;
               
                [rows_n col_n] = size(nsin_i);
                if rows_n <col_n
                    nsin_i = nsin_i';
                end
            
            PortRisk = handles.PortRisk;
            PortRet = handles.PortReturn;
            if isempty(handles.PortReturn)
                handles.PortReturn = NaN;
            end
            if isempty(handles.PortRisk)
                handles.PortRisk = NaN;
            end
            wts = handles.PortWts(1,:)';
            wts(wts <= handles.MaxWeight2NonAllocBonds * 1.5) = 0;
            wts0 = (wts == 0);
            wts_no_0 = wts( ~wts0 );
            nsin = nsin_i( ~wts0 );
                
            %% Figure out count
            prices = get_bond_dyn_single_date(nsin, 'price_dirty', t_0);
            count_rough = 15000 * wts_no_0 ./ fts2mat(prices)';
%             masheu = weights2holdings(1, wts_no_0', fts2mat(prices));

            count = round(count_rough); %TO CHECK with YOAV - initial weights = counts
            %%%% pull prices for each bond and calculate how many bonds are bought
            %%%% with wts of bond in portfolio (wt in terms of money)
            
            %% Create Portfolio
            % t_0 = datenum('2006-01-02'); %fill in initial date (creation_date)
            benchmark_id = 601;
            account_id = [];
            sim_num_save = mod_sim * 10 + mod_num; %simulation number to be saved
            
            required_holding_period = (port_duration_max+1)/2; % Yigal: "This value used below in the number of situations."
            account_name = ['JessicaBiel' act_name num2str(required_holding_period) 'PD' num2str(sim_num_save)]; %fill in sim_num_name as a number which changes with every run, must be unique
            name_file{mod_num, mod_sim} = account_name;
            owner_id = 2;
            manager_id = 1;
            initial_amount = 100000;
            underlying_data_name = 'ytm';
            filter_id = 23; %get from amit, number of vector

            initial_allocation = dataset(nsin, count); %create initial allocation

            %% Portfolio model declaration:
            % Yigal: Previuose version:
%             port_model = dataset(mod_sim, {mod_name}, {opt_name}, ...
%                                                     PortRet, PortRisk, -0.2, 0.8,  ...
%                                                     required_holding_period, handles.PortDuration, handles.HistLen, ...
%                                                     'VarNames', {'sim_id', 'model', 'optimization_type', ...
%                                                                             'port_ret', 'port_risk', 'reinv_low_bnd', 'reinv_up_bnd', ...
%                                                                             'port_duration', 'calc_duration', 'hist_length'});

            port_model = dataset(mod_sim, {mod_name}, {opt_name}, ...
                                                    PortRet, PortRisk, reinv_low_bnd, reinv_up_bnd,  ...
                                                    required_holding_period, handles.PortDuration, handles.HistLen, ...
                                                    'VarNames', {'sim_id', 'model', 'optimization_type', ...
                                                                            'port_ret', 'port_risk', 'reinv_low_bnd', 'reinv_up_bnd', ...
                                                                            'port_duration', 'calc_duration', 'hist_length'});
                                                                        
                                                                        
            %% Creation of the portfolio object:
            %note that account_id you get back from DB after first time saving.
            port = portfolio(account_id, account_name, owner_id, manager_id, t_0, t_0, initial_amount, benchmark_id, filter_id, initial_allocation, port_model);

            %% Save current portfolio into database:
            [~, err_msg] = port.save(); % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
            if isempty(err_msg)
                err_msg =['no error, portfolio initiation:' act_name];
            end
            errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

            %% t_f
            %account_name - remains same, true for below as well
            %owner_id = 2;
            %manager_id = 1;

            %this automatically executes
            %calc( port_current, underlying_data_name, t_f );
            %- which saves automatically into the object

                
            %% Simulation:
            if (t_0 < 733408) && (required_holding_period == 4)


                t_f1 = datenum(t_0) + 1 * 365;
                port_current = da_portfolio( account_name, t_f1, manager_id );


                [~, err_msg] = save(port_current); %bool returns yes no if saved or not. % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                
                t_f2 = datenum(t_0) + 2 * 365;
                port_current = da_portfolio( account_name, t_f2, manager_id );

                [~, err_msg] = save(port_current); % bool returns yes no if saved or not. % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                t_f3 = datenum(t_0) + 3 * 365;
                port_current = da_portfolio( account_name, t_f3, manager_id );

                [~, err_msg] = save(port_current); % bool returns yes no if saved or not. % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                t_f = datenum(t_0) + 4 * 365; %date on which portfolio ends

            elseif (t_0 <  733773) && (required_holding_period > 2) %31/12/2008, run first for 1 year, then for 2 years then run for three years
                t_f1 = datenum(t_0) + 1 * 365;
                port_current = da_portfolio( account_name, t_f1, manager_id );


                [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.

                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                t_f2 = datenum(t_0) + 2 * 365;
                port_current = da_portfolio( account_name, t_f2, manager_id );

                [~, err_msg] = save(port_current);%bool returns yes no if saved or not. % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                t_f = datenum(t_0) + 3 *  365; %date on which portfolio ends
                
            elseif t_0 <734138 %31/12/2009, run first for one year, then for two years

                t_f1 = datenum(t_0) + 1 * 365;
                port_current = da_portfolio( account_name, t_f1, manager_id );


                [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.

                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                t_f = datenum(t_0) + 2 * 365;

            else
                t_f = datenum(t_0) + 1 * 365; %simulations from 2010, run for one year.
            end

            %%  Get old portfolio from database, with current data for t_f


            port_current = da_portfolio( account_name, t_f, manager_id );    
            [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.


            err_msg = [err_msg, act_name];
            errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];            
        
        else
            for port_dur=1:3
                port_duration_max = 2*port_dur + 1;

                [errors1 errors2 handles] = SolverNoGui(handles, port_duration_max);

                if ~isempty(errors1)
                    errors1 = [errors1; act_name];
                    errorDS = [errorDS; dataset(cellstr(errors1), 'VarNames', 'err_msg')];
                end
                if ~isempty(errors2)
                    errors2 = [errors2; act_name];
                    errorDS = [errorDS; dataset(cellstr(errors2), 'VarNames', 'err_msg')];
                end

                nsin_i = handles.nsin;
                
                [rows_n col_n] = size(nsin_i);
                if rows_n <col_n
                    nsin_i = nsin_i';
                end
                
                PortRisk=handles.PortRisk;
                PortRet = handles.PortReturn;
                if isempty(handles.PortReturn)
                    handles.PortReturn = NaN;
                end
                if isempty(handles.PortRisk)
                    handles.PortRisk = NaN;
                end
                wts = handles.PortWts(1,:)';
                wts(wts <= handles.MaxWeight2NonAllocBonds * 1.5) = 0;
                %  print zeros where the weighs are smaller than MaxWeight2NonAllocBonds -- the maximum weight that a bond can have
                %  while stil be considered as a bond which was not included in the portfolio
                wts0 = (wts == 0);
                wts_no_0 = wts( ~wts0 );
                nsin = nsin_i( ~wts0 );

                %% figure out count
                prices = get_bond_dyn_single_date(nsin, 'price_dirty', t_0);
                count_rough = 15000 * wts_no_0 ./ fts2mat(prices)';
%                 masheu = weights2holdings(1, wts_no_0', fts2mat(prices));

                count = round(count_rough); %TO CHECK with YOAV - initial weights = counts
                %%%% pull prices for each bond and calculate how many bonds are bought
                %%%% with wts of bond in portfolio (wt in terms of money)
                
                %% Create Portfolio
                %t_0= datenum('2006-01-02');%fill in initial date (creation_date)
                benchmark_id = 601;
                account_id = [];
                sim_num_save = mod_sim*10 + mod_num;%simulation number to be saved

                required_holding_period = (port_duration_max+1)/2;
                account_name = ['JessicaBiel' act_name num2str(required_holding_period) 'PD' num2str(sim_num_save)];%fill in sim_num_name as a number which changes with every run, must be unique

                name_file{mod_num, mod_sim} = account_name;
                owner_id = 2;
                manager_id = 1;
                initial_amount = 100000;
                underlying_data_name = 'ytm';
                filter_id = 23; %get from amit, number of vector

                initial_allocation = dataset(nsin, count); %create initial allocation

                %% Portfolio model declaration:
                % Yigal: Previuose version:
    %             port_model = dataset(mod_sim, {mod_name}, {opt_name}, ...
    %                                                     PortRet, PortRisk, -0.2, 0.8,  ...
    %                                                     required_holding_period, handles.PortDuration, handles.HistLen, ...
    %                                                     'VarNames', {'sim_id', 'model', 'optimization_type', ...
    %                                                                             'port_ret', 'port_risk', 'reinv_low_bnd', 'reinv_up_bnd', ...
    %                                                                             'port_duration', 'calc_duration', 'hist_length'});

                port_model = dataset(mod_sim, {mod_name}, {opt_name}, ...
                                                        PortRet, PortRisk, reinv_low_bnd, reinv_up_bnd,  ...
                                                        required_holding_period, handles.PortDuration, handles.HistLen, ...
                                                        'VarNames', {'sim_id', 'model', 'optimization_type', ...
                                                                                'port_ret', 'port_risk', 'reinv_low_bnd', 'reinv_up_bnd', ...
                                                                                'port_duration', 'calc_duration', 'hist_length'});

                %% Creation of the portfolio object:
                %note that account_id you get back from DB after first time saving.
                port = portfolio(account_id, account_name, owner_id, manager_id, t_0, t_0, initial_amount, benchmark_id, filter_id, initial_allocation, port_model);

                %% Save current portfolio into database:
                [bool, err_msg] = port.save();
                if isempty(err_msg)
                    err_msg =['no error, portfolio initiation:' act_name];
                end
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                %% t_f
                %account_name - remains same, true for below as well
                %owner_id = 2;
                %manager_id = 1;

                %this automatically executes
                %calc( port_current, underlying_data_name, t_f );
                %- which saves automatically into the object



                %% Simulation:
                if (t_0 < 733408) && (required_holding_period == 4)


                    t_f1 = datenum(t_0) + 1 * 365;
                    port_current = da_portfolio( account_name, t_f1, manager_id );
                    
                                  [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.

                    err_msg = [err_msg, act_name];
                    errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];


                    t_f2 = datenum(t_0) + 2 * 365;
                    port_current = da_portfolio( account_name, t_f2, manager_id );

                    [~, err_msg] = save(port_current);%bool returns yes no if saved or not.
                    err_msg = [err_msg, act_name];
                    errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];


                    t_f3 = datenum(t_0) + 3 * 365;
                    port_current = da_portfolio( account_name, t_f3, manager_id );

                    [~, err_msg] = save(port_current);%bool returns yes no if saved or not.
                    err_msg = [err_msg, act_name];
                    errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                    t_f = datenum(t_0) + 4 * 365; %date on which portfolio ends

                elseif (t_0 <  733773) && (required_holding_period > 2) %31/12/2008, run first for 1 year, then for 2 years then run for three years
                    
                    t_f1 = datenum(t_0) + 1 * 365;
                    port_current = da_portfolio( account_name, t_f1, manager_id );

                    [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.

                    err_msg = [err_msg, act_name];
                    errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];


                    t_f2 = datenum(t_0) + 2 * 365;
                    port_current = da_portfolio( account_name, t_f2, manager_id );

                    [~, err_msg] = save(port_current);%bool returns yes no if saved or not.
                    err_msg = [err_msg, act_name];
                    errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                    t_f = datenum(t_0) + 3 * 365; %date on which portfolio ends
                    
                elseif (t_0 < 734138) %31/12/2009, run first for one year, then for two years

                    t_f1 = datenum(t_0) + 1 * 365;
                    port_current = da_portfolio( account_name, t_f1, manager_id );

                    [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.

                    err_msg = [err_msg, act_name];
                    errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                    t_f = datenum(t_0) + 2 * 365;

                else
                    
                    t_f = datenum(t_0) + 1 * 365; %simulations from 2010, run for one year.
                    
                end

                %%  Get old portfolio from database, with current data for t_f
            
                port_current = da_portfolio( account_name, t_f, manager_id );
                [bool, err_msg] = save(port_current);%bool returns yes no if saved or not.


                err_msg = [err_msg, act_name];
                errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];

                %%%%%%% SAVE ERRORS INTO SEPARATE FILE
            end            
        end        
    elseif handles.NumBonds < 5
       act_name = ['NoBonds'];
        err_msg = 'Not enough bonds through filter, no solution';
        err_msg = [err_msg];
        errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
        
        benchmark_id = 601;
        account_id = [];
        sim_num_save = 10*mod_sim + 9;%simulation number to be saved, 9 means error.
        account_name = ['NoBonds'  num2str(sim_num_save)];%fill in sim_num_name as a number which changes with every run, must be unique
        %name_file{mod_num, sim} = account_name;
        owner_id = 2;
        manager_id = 1;
        initial_amount = 100000;
        
        underlying_data_name = 'ytm';
        filter_id = 23;     
    
        clear handles;
        set_sim_err( t_0, mod_sim , errorDS); % expected errors    
    end
    
catch ME    
    err_msg = [ME.message, act_name];
    set_sim_err( t_0, sim_num , dataset(cellstr(err_msg), 'VarNames', 'err_msg')); % unexpected errors    
    %            save('name_file.mat');    
end
