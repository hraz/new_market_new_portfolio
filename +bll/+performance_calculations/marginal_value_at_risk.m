function [ marginalValueAtRisk componentValueAtRisk] = marginal_value_at_risk( percentileRisk, ftsHoldingValuePerBond, histLength )
%MARGINAL_VALUE_AT_RSIK function computes marginal value at risk and component value at risk.
%
% function computes marginal VaR for all assets in portfolio.  The
% computation is done by computing the VaR for the portfolio without a given
% asset and dividing this quantity by the portfolio VaR. Marginal value at
% risk is measured on a per bond basis where the VaR is calculated for the
% portfolio with the bond and then without.  The percentage change is the
% mVaR. Negative means the VaR improved without the bond. The component VaR 
% is the amount of money that percentage is equivalent to in terms of the value 
% of the portfolio.
%
%   input:  percentileRisk - double - scalar between 1-10% saying the percentile wanted
%
%           ftsHoldingValuePerBond - nDAYS X mASSETS matrix of returns of bonds in portfolio
%              given as value of each
%
%           histLength - double - scalar telling the number of days back for the function to check, default is 30
%
%
%
%    output:  marginalValueAtRisk - N-ASSETS X 1 vector with marginal value at risk for each bond
%
%             componentValueAtRisk - N-ASSETS X 1 vector with component
%               value at risk for each bond
%
% written by Hillel Raz, January 2013
% Copyright 2013, BondIT Ltd.

import bll.performance_calculations.*;

if ~isa(percentileRisk, 'double')||~isa(ftsHoldingValuePerBond, 'fints');
    error('value_at_risk:wrongInput', ...
        'One or more input arguments is not of the right type');
end

defaultLength = 30; %number of days on which to base VaR computation
if nargin == 2
    histLength = defaultLength;
end

%% Set up variables

cumulativeValuesPerBond = fts2mat(ftsHoldingValuePerBond);
dates = ftsHoldingValuePerBond.dates;

numBonds = size(cumulativeValuesPerBond, 2);

marginalValueAtRisk = zeros(numBonds, 1); % To store marginal value at risk per bond
componentValueAtRisk = zeros(numBonds, 1); % To store component value at risk per bond

nansInMatrix = isnan(cumulativeValuesPerBond);

cumulativeValuesPerBond(nansInMatrix) = 0; % Set all nans to 0's.
totalValueVector = sum(cumulativeValuesPerBond, 2);
ftsTotalValueVector = fints(dates, totalValueVector); % Convert back to fts

[valueAtRisk] = value_at_risk(percentileRisk, ftsTotalValueVector, histLength); %get VaR

%% Compute mVaR
if valueAtRisk~=0
    %in the case that VaR is non zero, proceed as follows. otherwise the
    %marginals will be zero as defualt value. MORE SENSIBLE TREATMENT
    %SHOULD BE CONSIDERED.
    
    for i = 1:numBonds
        cumulativeReturnsWithout_i = cumulativeValuesPerBond;
        cumulativeReturnsWithout_i(:,i) = 0; % Remove i-th returns
        cumulativeReturnsWithout_i = sum(cumulativeReturnsWithout_i, 2);
        ftsCumulativeReturnsWithout_i = fints(dates, cumulativeReturnsWithout_i); % Convert back to fts
        
        [valueAtRisk_i] = value_at_risk(percentileRisk, ftsCumulativeReturnsWithout_i, histLength);% Get VaR for portfolio without asset i
        marginalValueAtRisk(i) = valueAtRisk_i/valueAtRisk - 1; % Marginal VaR for i-th asset
    end
end

%% Compute component VaR
componentValueAtRisk = abs(marginalValueAtRisk.*totalValueVector(end)); % Value at the end is worth of portfolio

end

