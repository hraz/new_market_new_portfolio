function varargout = calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM, benchmarkID, varargin)
%CALC_EXPECTED_RETURN calculates expected return
%
%   varargout = calc_expected_return(varargin)calculates the expected
%   return of a portfolio based on the ytm of its assets
%
%   Input:
%     Possibilities: -for returnMethod = 1, reinvestmentStrategy == 1 and 7
%                           calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM,benchmarkID, reqDate) - need to pull out data
%                               includes ytm (varargin{1}), and ttm (varagin{2})
%                           calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM, benchmarkID, vecYTM, vecTTM)
%                       for reinvestmentStrategy == 2
%                           calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM, benchmarkID, vecYTM, vecTTM, riskfreeRates, riskfreeTTM)
%                           calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM, benchmarkID, vecYTM, vecTTM, reqDate) � need to pull risk-free data
%                       for reinvestmentStrategy == 4
%                           calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM,
%                               benchmarkID, reqDate) - need to pull ytm, ttm
%                           calc_expected_return(returnMethod, reinvestmentStrategy, allocation, portTTM, benchmarkID, vecYTM, vecTTM, reqDate)
%                    -for returnMethod = 2
%                           TBD
%     ============
%               returnMethod - INT - values: 1 - calculate using ytm, 2 -
%                   calculate using prices
%               reinvestmentStrategy - char - what type of reinvestment strategy to calculate, possibilities:
%               1. 'Specific-Bond-Reinvestment' - we invest in the same bond from which we got a coupon.
%               2. 'risk-free': we invest in a risk free asset (government bond).
%               3. 'portfolio': we invest in the entire portfolio.
%               4. 'benchmark': we invest in the appropriate benchmark.
%               5. 'bank': we put the money in the bank to earn interest.
%               6. 'none':
%               7. 'real-specific-bond-reinvestment': seek the best bond
%                   based on closest YTM to original bond's YTM
%
%     vecYTM - nNSIN X 1 DOUBLE- vector of YTMs for the bonds, if some are
%        0 or non-existent then those nsin's weights are removed.
%     allocation - dataset with at least the following fields:
%         nsin
%         weight
%     reqDate - DATENUM
%     method - INTEGER - 1 = regular calculation
%     portTTM - double - time left till portfolio maturity in years
%     benchmarkID - Integer - nsin for relevant benchmark
%     Possibility 2:
%     =============
%
%   Output:
%     expReturn - DOUBLE, annual return in decimal (not percentage)
%
%   See also: calc
%
% Written by Hillel Raz, Jan 2013
% Updated by Amit Godel, Apr 2013
%     changed varargin{2} variable type to dataset, instead of original
%     struct (needed for correspondenceto rest of the code)
%     also changed the isfield(..) to ismember(fieldnames(..)
% Updated again by Hillel Raz, July 2013
%     Reorganized function so that it calls calc_expected_return_w_
%     reinvestment when needed.
% Copyright, BondIT Ltd
 
import dal.market.get.dynamic.*;
import utility.*;
import bll.performance_calculations.*;

    %% Input check-point Charlie.
    if returnMethod == 1 % Calculate using YTM
        if  ~isa(reinvestmentStrategy, 'char') || ~isa(allocation.nsin, 'double') || ~ismember('weight',fieldnames(allocation))|| ~isa(portTTM, 'double')...
                || ~isa(benchmarkID, 'double')
            error('calc_expected_return:wrongInput', ...
                'One or more input arguments is not of the right type');
        end

        %% Remove 0's from list of nsins for DB pulls
        nsins = allocation.nsin;
        nsins = setdiff(nsins, 0);
        weight = allocation.weight;
        if strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment') || strcmp(reinvestmentStrategy, 'risk-free')... % Specific-Bond YTM, risk-free reinvestment
                || strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment')
            if nargin == 6 % No data inputted
                reqDate = varargin{1};
                tbl_name = 'bond';
                data_name =  {'yield', 'ttm'};
                [ftsYTMttm] = get_multi_dyn_between_dates( nsins , data_name , tbl_name, reqDate, reqDate);
                vecYTM = fts2mat(ftsYTMttm.yield);
                vecTTM = fts2mat(ftsYTMttm.ttm);
            elseif nargin== 7
                vecYTM = varargin{1};
                vecTTM = varargin{2};
            elseif nargin == 9
                if ~isa(varargin{3}, 'double') || ~isa(varargin{4}, 'double')
                    error('calc_expected_return:wrongInput', ...
                        'One or more input arguments is not of the right type');
                end
                vecYTM = varargin{1};
                vecTTM = varargin{2};
                riskfreeRates = varargin{3};
                riskfreeTTM = varargin{4};
            end
        elseif strcmp(reinvestmentStrategy, 'benchmark') % Benchmark
            if nargin == 6
                reqDate = varargin{1};
                tbl_name = 'bond';
                data_name =  {'yield', 'ttm'};
                [ftsYTMttm] = get_multi_dyn_between_dates( nsins , data_name , tbl_name, reqDate, reqDate);
                vecYTM = fts2mat(ftsYTMttm.yield);
                vecTTM = fts2mat(ftsYTMttm.ttm);
            elseif nargin == 8
                vecYTM = varargin{1};
                vecTTM = varargin{2};
                reqDate = varargin{3};
            end
        end

        %% Prepare needed data
        % Remove NaNs
        nNsins = length(allocation.nsin);
        nansYTM = isnan(vecYTM);
        if any(nansYTM) % For the case when there are dead bonds - so their YTMs are NaNs, zero out those bonds weights
            % so as to not calculate them in the expected return
            vecYTM(nansYTM) = 0;
            nsinFields = fieldnames(ftsYTMttm.yield);
            nsinList = strid2numid(nsinFields(4:end));
            [~, indexNsins, ~] = intersect(allocation.nsin, nsinList);
            weight(indexNsins(nansYTM)) = 0;
        end

        if nNsins ~= length(vecYTM) % In case some of the nsins died, the number of live 'YTM's will be less than
            % the number of nsins, set those YTM's = 0 and same with TTM's.
            nsins  = fieldnames(ftsYTMttm.yield);
            [~, indexAllocation, ~] = intersect(allocation.nsin, strid2numid(nsins));
            newVecYTM = zeros(nNsins, 1);
            newVecYTM(indexAllocation) = vecYTM;
            vecYTM = newVecYTM;
            newVecTTM  = zeros(nNsins, 1);
            newVecTTM(indexAllocation) = vecTTM;
            vecTTM = newVecTTM;
        end

        if sum(weight)>0
            weight = weight/sum(weight); %If sum weights =1, this changes nothing. Otherwise -reconfigures it
        else
            error('calc_expected_return:sum of weights cannot be 0');
        end

        %% Calculate Expected Return = weights (decimal) * YTM
        if strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment')|| strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment') % 'Specific-Bond-Reinvestment'
            [expReturn] = calc_expected_return_w_reinvestment_strategy(reinvestmentStrategy, weight, vecYTM);
            varargout{1} = expReturn;
        elseif strcmp(reinvestmentStrategy, 'risk-free')
            if exist('riskfreeRates', 'var')
                [expReturn] = calc_expected_return_w_reinvestment_strategy(reinvestmentStrategy, weight, vecYTM, portTTM, vecTTM, riskfreeRates, riskfreeTTM);
            else
                [expReturn] = calc_expected_return_w_reinvestment_strategy(reinvestmentStrategy, weight, vecYTM, portTTM, vecTTM, reqDate);
            end
            varargout{1} = expReturn;
        elseif strcmp(reinvestmentStrategy, 'benchmark')
            [expReturn] = calc_expected_return_w_reinvestment_strategy(reinvestmentStrategy, weight, vecYTM, portTTM, vecTTM, benchmarkID, reqDate);
            varargout{1} = expReturn;
        else
            varargout{1} = 0; %In case that all bonds had NaN's for YTM, return simply YTM = 0.
        end
    end
    if returnMethod == 2 % Calculate using prices
    end
end