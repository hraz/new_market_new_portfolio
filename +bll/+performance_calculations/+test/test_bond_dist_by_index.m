clear; clc;

bnd_list_in = [
6390157
1115385
6390249
1115401
6390207
1121326
1980234
6490312
1115823
3900206
1098656
4110151
2260131];

bnd_wts_in = [0.1 
    0.05
    0.04
    0.01
    0.1
    0.03
    0.02
    0.05
    0.32
    0.08
    0.05
    0.03
    0.12
   ];

t_0 = '2010-08-14';
%% Import external packages:
import bll.simulation.*

%% Find distribution of bonds according to index

 [ indx_uniq indx_wts ] = bond_dist_by_index( bnd_list_in, bnd_wts_in, t_0 )