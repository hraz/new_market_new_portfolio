clc; clear;
import bll.performance_calculations.*;
vecNsin = [1087915;1940089;1940147;1940345;6004113;7410129;9267139];
reqDate = 733204;
 
vecCount = [   82
    91
    13
    92
    64
    10
    28];
   vecWeight=[ 0.1085
    0.1893
    0.1913
    0.0316
    0.1933
    0.1893
    0.0966];
idModel = 1; %Markowitz
idBenchmark = 601; %AllBonds
% expRisk = calc_expected_risk(ftsPrices, allocation, reqDate, idModel, idBenchmark)
histLength = 60;
allocation=dataset(vecNsin,vecCount,vecWeight,'VarNames', {'nsin', 'count','weight'});
expRisk = calc_expected_risk([], allocation, histLength, reqDate, idModel, idBenchmark)
 
mtxCovariance = 1.0e-003 * [0.0086   -0.0082   -0.0004    0.0008   -0.0012   -0.0004    0.0002
   -0.0082    0.1230    0.0036    0.0012   -0.0003    0.0011    0.0026
   -0.0004    0.0036    0.0039   -0.0002   -0.0009   -0.0006    0.0009
    0.0008    0.0012   -0.0002    0.0039   -0.0003    0.0015    0.0002
   -0.0012   -0.0003   -0.0009   -0.0003    0.0063   -0.0009   -0.0000
   -0.0004    0.0011   -0.0006    0.0015   -0.0009    0.0048   -0.0002
    0.0002    0.0026    0.0009    0.0002   -0.0000   -0.0002    0.0008]
expRisk = calc_expected_risk(mtxCovariance, allocation)
% expRisk = 0.0353