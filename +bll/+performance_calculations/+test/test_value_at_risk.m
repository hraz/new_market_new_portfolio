clear; clc;

load 'D:\GitRepo\simulation_code\bll\performance_calculations\test\ftsCumulativeValues.mat'

import bll.performance_calculations.*;

percentile_risk = 0.1;

% cumulative_returns = [1500 1512 1516 1522 1518 1519 1516 1520 1525 1522 1527 1527 1526 1527 1528 1529 1533 1531 1530 1529 1532 1531 1530 1532 1531 1534 1530 1532 1532 1534 1536 1534 1532 1534 1533];

hist_length = 35; %default is 30

tic
[valueAtRisk conditionalValueAtRisk] = value_at_risk(percentile_risk, ftsTotalHoldingValue, hist_length)
toc

% Expected values:
% valueAtRisk =
% 
%    64.7945
% 
% 
% conditionalValueAtRisk =
% 
%   101.8620