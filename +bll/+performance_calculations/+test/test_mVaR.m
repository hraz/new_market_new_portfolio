clear; clc;
dbstop if error;
import simulations.*;

percentile_risk = 0.1;
cumulative_return= zeros(3, 2, 35);

cumulative_returns(1, 1, 1:35) = [1500 1512 1516 1522 1518 1519 1516 1520 1525 1522 1527 1527 1526 1527 1528 1529 1533 1531 1530 1529 1532 1531 1530 1532 1531 1534 1530 1532 1532 1534 1536 1534 1532 1534 1533];
cumulative_returns(1, 2, 1:35) = 11*ones(1,35);

cumulative_returns(2, 1, 1:35) = [1500 1502 1503 1499 1497 1495 1496 1496 1493 1492 1495 1496 1497 1492 1491 1490 1490 1491 1492 1494 1492 1492 1494 1495 1496 1498 1500 1499 1498 1501 1502 1503 1504 1503 1505];
cumulative_returns(2, 2, 1:35) = [5 5 5 5 5 5 5 5 5 5 5 5 5 5 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1 5.1]; 

cumulative_returns(3, 1, 1:35) = 1500+ceil(10*(rand(1, 35)));
cumulative_returns(3, 2, 1:35) = [6 6 6 6 6 6 6 6 6 6 6.2 6.2 6.2 6.2 6.2 6.2 6.2 6.2 6.5 6.5 6.5 6.5 6.5 6.5 6.5 6.5 6.5 6.4 6.4 6.4 6.4 6.4 6.4 6.4 6.4];
hist_length = 35; %default is 30

tic
[marginal_value_at_risk component_value_at_risk] = mVaR(percentile_risk, cumulative_returns, hist_length); 

%[value_at_risk] = VaR(percentile_risk, cumulative_returns); 
toc
