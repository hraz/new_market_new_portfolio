function [ SharpeRatB SharpeRatP ] = PortSharpe( BondList, riskless_rate, t_0, t_f )

%     function calculates Sharpe ratio for given bond list.  in addition, function calculates Sharpe ratio for 
%             entire portfolio based on composition of bonds in the portfolio (the weights).  
% 
% 
% Input:  BondList - n x 2 matrix made of list of nsins of bonds in portfolio and weights (to be divided by 100) for each bond
% 
%                 riskless_rate - decimal scalar - rate plugged in by user in GUI.  vice
%                 versa could be rate of a particular bond (governmental
%                 makam for instance) to test against.  
%
%                 t_0 - starting time for portfolio
%                 
%                 t_f - finishing time for portfolio
%                 
%  Output:  SharpeRatB - vector of Sharpe ratios for given assets in
%                                               BondList
%
%                   SharpeRatP - scalar of Sharpe ratio for portfolio

import dal.market.get.dynamic.*
import bll.simulation.*
import dml.*;

bond_nsin_in= BondList( :, 1); %nsins of bonds
[bond_nsin Ind_n] = sort(bond_nsin_in);
num_bonds = length(bond_nsin);%number of bonds
bond_weights_in = BondList(:, 2); %weights of bonds in the portfolio
bond_weights = bond_weights_in(Ind_n);

%% Step 1 - Get returns for all bonds

[returns] = BondRetHist( bond_nsin, t_0, t_f ); %get bond returns and yields for each market day between t_0 and t_f

Asset = returns(:, 2); %the yields for the bonds

%% Step 2 - Compute Sharpe ratio for each of the bonds
for num=1:1:num_bonds
    
Denom = nanstd(Asset{num}(:, 2), 1); %get yields for each bond, use only non NaN data
zDenom = max(Asset{num}(:, 2)) == min(Asset{num}(:, 2)); %test to see if data of one of the bonds is made of a single price only.  if so - make it redundant
SharpeRatB(zDenom) = NaN;
SharpeRatB(~zDenom) = 1 ./ Denom(~zDenom); %compute sharpe ratio for bonds
SharpeRatB = SharpeRatB .* (nanmean(Asset{num}(:, 2)) - riskless_rate .* ones(1,num_bonds));%subtract riskless rate from each mean
end

SharpeRatP = SharpeRatB.*bond_weights'/100;
SharpeRatP = sum(SharpeRatP); %sharpe ratio for portfolio based on weights, excluding values where sharpe ratio was NaN


end

