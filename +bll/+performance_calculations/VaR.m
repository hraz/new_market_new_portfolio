function [value_at_risk conditional_value_at_risk ] = VaR(percentile_risk, cumulative_returns, hist_length)
%
% function computes the value at risk of a portfolio based on its value changes over specified period
%
% input:
%        percentile_risk - double - scalar between 1-10% saying the percentile wanted
%
%        cumulative_returns - NDays X 1 vector of returns of portfolio
%        given as value of portfolio
%
%        hist_length - double - scalar telling the number of days back for the function to check, default is 60
%
%
%   output: value_at_risk - double - amount of money which is at risk for the percentile given
%
%           conditional_value_at_risk - double - average amount of money at the percentile given
%
%
% written by Hillel Raz, BondIT January, 2013

default_length = 30; %number of days on which to base VaR computation
if nargin == 2
    hist_length = default_length;
end

cumulative_returns = cumulative_returns(end-hist_length+1:end); %take only last 'hist_length' number of days

cumulative_returns_without_last_entry = cumulative_returns;
cumulative_returns_without_last_entry(end) =[]; %remove last entry

percent_returns = diff(cumulative_returns)./cumulative_returns_without_last_entry;
%will be used to get percentage change from previous return - hence don't
%need the last return
sorted_returns = sort(percent_returns);

percentile_loc = percentile_risk*length(sorted_returns);

percentile_int = floor(percentile_loc); %exact location in vector of returns - integer
percentile_remainder = percentile_loc-percentile_int; %non-integer part
extra_amount_due_to_remainder = sorted_returns(percentile_int+1)-sorted_returns(percentile_int);

initial_var = sorted_returns(percentile_int);
percent_value_change_at_risk = initial_var + extra_amount_due_to_remainder*(percentile_remainder);

if percent_value_change_at_risk >0 %if portfolio only goes up then VaR and cVaR should be 0
    value_at_risk = 0;
    conditional_value_at_risk = 0;
else
    value_at_risk = cumulative_returns(end)*abs(percent_value_change_at_risk);
    
    if percentile_loc<1 %if not percentile asked is smaller than minimum bin size, take part of worst return
        conditional_value_at_risk = percentile_loc*sorted_returns(1);
    else
        sum_percent_value_change_till_percentile = sum(sorted_returns(1:percentile_int)); 
        conditional_value_at_risk = cumulative_returns(end)*abs(sum_percent_value_change_till_percentile + extra_amount_due_to_remainder*(percentile_remainder) )/percentile_loc;
        %averaged amount of money upto the given percentile, with linear
        %progression upto the exact location
    end
end