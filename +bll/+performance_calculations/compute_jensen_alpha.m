function jensenAlpha = compute_jensen_alpha(portfolioValue, marketReturn, risklessRate)


%   COMPUTE_JENSEN_ALPHA computes jensen alpha
%
%   jensenAlpha = compute_jensen_alpha(portfolioValue, marketReturn, risklessRate)
%   receives the portfolio values over time, the market return series, the
%   risk less rate of return, and returns jensen alpha.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days.
%       marketReturn - a vector of market returns, given in the same
%       times frequency as portfolioValue.
%       risklessRate - annual risk less rate, i.e., the annual interest of
%       a 1 year band deposite (usually taken as the interest given by the
%       longest Makam).
%   Output:
%       jensenAlpha � jensen alpha, according to the capm model:
%       jensen's alpha = Portfolio Return + [Risk Free Rate + Portfolio Beta * (Market Return - Risk Free Rate)]
%   Sample:
%		Some example of the function using.
%
%		See Also:
%		compure_alpha, compute_beta, compute_omega, compute_moments,
%		compute_ratios, compute_downside_volatility, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
s2=size(marketReturn);
s3=size(risklessRate);
if ~isa(portfolioValue,'numeric')
    error('compute_jensen_alpha:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif ~isa(marketReturn,'numeric')
    error('compute_jensen_alpha:wrongInputType','marketReturn is expected to be a numeric argument');
elseif ~isa(risklessRate,'numeric')
    error('compute_jensen_alpha:wrongInputType','risklessRate is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0
    error('compute_jensen_alpha:wrongInputSize','portfolioValue is expected to be a vector.')
elseif sum(s2>1)==2 || sum(s2>1) == 0
    error('compute_jensen_alpha:wrongInputSize','marketReturn is expected to be a vector.')
    % elseif sum(s3)~=2
    %     error('compute_jensen_alpha:wrongInputSize','risklessRate is expected to be a scaler')
elseif risklessRate<0
    error('compute_jensen_alpha:wrongValue','risklessRate is expected to be a positive scaler')
elseif risklessRate>1
    error('compute_jensen_alpha:wrongValue','risklessRate is expected to be a fractional scaler, e.g., for risklessRate = 2 percent, enter 0.02 instead of 2');
elseif s1(1)>s1(2) %a column vector
    if s1(1)~=s2(1)+1 %number of rows in the portfolioValue vector must be greater than the number of rows in the marketReturn vector
        error('compute_jensen_alpha:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
elseif s1(1)<s1(2) %same for the case of row vector
    if s1(2)~=s2(2)+1
        error('compute_jensen_alpha:wrongInputSize','portfolioValue vector must be longer than marketReturn vector by one observation')
    end
end

%% compute jensen alpha
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
risklessRateDay = Year2Day(risklessRate);                                      % Daily riskless rate
if size(risklessRateDay, 1)~= size(portfolioReturnSeries, 1)
    nDays = min(size(risklessRateDay, 1), size(portfolioReturnSeries, 1));
    risklessRateDay = risklessRateDay(end-nDays+1:end);
    portfolioReturnSeries = portfolioReturnSeries(end-nDays+1:end);
    marketReturn = marketReturn(end-nDays+1:end);
end

[jensenAlpha]=portalpha(portfolioReturnSeries, marketReturn, risklessRateDay,'capm');  % Compute Jensen's alpha based on CAPM model