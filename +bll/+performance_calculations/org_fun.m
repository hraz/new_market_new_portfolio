function  org_fun( num_sims, file_name )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

import bll.simulation.*;

load(fullfile('D:\core\', file_name));
load 'D:\core\Results1.mat';
load 'D:\core\Results2.mat';
load 'D:\core\Results3.mat';
load 'D:\core\Results4.mat';


for i = 1:num_sims
    if mod(i, 4) == 1
        Results(i, :) = Result_val1(i, :);

    elseif mod(i, 4) ==2
        Results(i, :) = Result_val2(i, :);

    elseif mod(i, 4) == 3
        Results(i, :) = Result_val3(i, :);

    elseif mod(i, 4) == 0
        Results(i, :) = Result_val4(i, :);

    end
end

save(file_name, 'Results', 'Result_val1','Result_val2','Result_val3', 'Result_val4'  );

end

