classdef ir_curve < handle
%IR_CURVE Base abstract class for interest rate curves
%   =================================   %
%   The class built on base IRCurve MATLAB class.   %
%   =================================   %
%
%   IR_CURVE is an abstract class, and you cannot create instances of
%   it directly.  You can create IRFUNCTIONCURVE and IRDATACURVE objects
%   that are derived from this class.
%
%   The class has the following properties:
%
%   Type: Forward, Zero, Discount -- type of curve
%   Settle: Settle Date
%   Compounding: -1,1,2,3,4,6,12 -- compounding for ir_curve
%   Basis: 0-13, day count convention for ir_curve
%
%   Classes that inherit from IR_CURVE must implement the following methods:
%
%   GETFORWARDRATES: Returns forward rates for input dates
%   GETZERORATES: Returns zero rates for input dates
%   GETDISCOUNTFACTORS: Returns discount factors for input dates
%   GETPARYIELDS: Returns par yields for input dates
%   TORATESPEC: Converts to be a RateSpec object, like the ones produced by INTSENVSET, for input dates
%
% See also IRFUNCTIONCURVE, IRDATACURVE, INTENVSET

    properties (SetAccess = protected, GetAccess = public)
        Type
        Settle
        % israeli domestic defaults:
        Compounding = 1;  %yearly
        Basis = 10; % Actual/365 fix (ISMA)
    end

    methods (Abstract)
        get_fwd_rates(obj,dates,optional)
        get_zero_rates(obj,dates, optional)
        get_disc_factors(obj,dates)
        
        rate_specification(obj)
    end
    methods (Static = true)
        [ req_rate ] = rate_convert( cur_rate, cur_comp_freq, cur_basis, req_comp_freq, req_basis, first_date, last_date )
    end
end