% clear; clc;

import utility.cash_flow.*;
import utility.fts.*;
import utility.*;
import dal.market.get.dynamic.*;
import bll.term_structure.nss_mdl.*;
import bll.term_structure.*;

%%
settle = datenum('2005-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.

%%
ir_type = 'forward';
coupon_type = 1; % 0 (Zero coupon) or 1 (Non Zero Coupon)

%% Cash flow build:
[ cf, nsin ] = cfbuilder(settle, coupon_type);
price = get_bond_dyn_single_date(nsin, 'price_dirty', settle);

%%
param = struct('compounding', 1, 'basis', 10);

%%
[ s_obj ] = svensson(ir_type, price, cf, settle, param);
  
%%
time = [price.dates+1: 1: cf.dates(end)]';
s_fr = s_obj.get_fwd_rates(time);
% s_zr = ir_func_curve.get_zero_rates(s_obj, time);
% s_df = ir_func_curve.get_disc_factors(s_obj, time);

%%
start_date = price.dates;
end_date = cf.dates(end);
% [ ribit ] = da_shortTB_yield( ytm_flag, start_date, end_date );
% ribit = ribit/100;

%%
figure(2);
plot(s_fr, '*b');
hold on;
% plot(s_zr, '-r');
% hold on;
% plot(ribit, '.')
