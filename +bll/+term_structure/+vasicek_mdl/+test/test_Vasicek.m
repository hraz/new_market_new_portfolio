% clear; clc;

import bll.term_structure.temp.amit.*

%%  loading the data from Bank of Israel

lower_date = '2005-01-02';
upper_date = '2011-12-31';
months=3;

[market_rates dates]=GetDailyData(months,lower_date,upper_date);
 
%% interpolating 

 di=(dates(1):dates(end))';
 ri=interp1(dates,market_rates,di,'cubic');

 
 %%
 import bll.term_structure.vasicek_mdl.*
% plotting the market data
%   dplot(di,ri)
  
 fprintf('Vasicek parameters for %s - %s  are:\n',lower_date,upper_date);
 para=VasParaMLEstimate(ri,1)  
 
 %plotting the Vasicek analytic expression for mean rate
 [r std_of_r t] = VasMeanAnalytic(market_rates(1),para,dates(1),dates(end),1e3);
 hold on
%  dplot(t,r,'r',t,r+std_of_r,'k',t,r-std_of_r,'k')
 
 % plotting the mean over 30 Vasicek simulations
  [rM t] = VasExactSim(market_rates(1),para,dates(1),dates(end),1e3,30);
  hold on
%   dplot(t,mean(rM),'g')
hold off