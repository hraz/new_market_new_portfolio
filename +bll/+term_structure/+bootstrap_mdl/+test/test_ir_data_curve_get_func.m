clear; clc;

%% Import external packages:
import bll.term_structure.bootstrap_mdl.*;
import dal.market.get.cash_flow.*;
import dal.market.get.dynamic.*;
import utility.graphs.*;
import utility.cash_flow.*;

%% Build start parameters:
settle = datenum('2005-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.

param.compounding = 1;
param.basis = 10;
param.interpmethod = 'cubic';

%% Cash flow build:
[ tbill_cf, tbill_nsin ] = cfbuilder(settle, 0);
tbill_price = get_bond_dyn_single_date(tbill_nsin, 'price_dirty', settle);

[ shahar_cf, shahar_nsin ] = cfbuilder(settle, 1);
shahar_price = get_bond_dyn_single_date(shahar_nsin, 'price_dirty', settle);

gov_cf = merge(tbill_cf, shahar_cf);
gov_price = merge(tbill_price, shahar_price);

%% Build zero-coupon curve:
[ tbill_ircurve ] = bootstrap( 'forward', tbill_price, tbill_cf, settle, param );
[ shahar_ircurve ] = bootstrap( 'forward', shahar_price, shahar_cf, settle, param );
[ gov_ircurve ] = bootstrap( 'forward', gov_price, gov_cf, settle, param );

% figure(1); plot( ircurve.Data, '.-r' )
                        
%% Check and show results of all gt functions of ir_data_curve object:
% df = get_disc_factors( ircurve );
% figure(2); plot(df)
% 
% zr = get_zero_rates( ircurve );
% figure(3); plot(zr)

% tbill_fr = get_fwd_rates( tbill_ircurve );
% shahar_fr = get_fwd_rates(shahar_ircurve);
% gov_fr = get_fwd_rates( gov_ircurve );

tbill_fr = get_zero_rates( tbill_ircurve );
shahar_fr = get_zero_rates(shahar_ircurve);
gov_fr = get_zero_rates( gov_ircurve );

figure(4); 
plot(tbill_fr, '-k'); hold on;
plot(shahar_fr, '-b'); hold on;
plot(gov_fr, '-g'); hold on;
