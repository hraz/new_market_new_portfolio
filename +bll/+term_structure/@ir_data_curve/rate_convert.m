function [ req_rate ] = rate_convert( cur_rate, cur_comp_freq, cur_basis, req_comp_freq, req_basis, first_date,  last_date )
%RATE_CONVERT Converts compounding and day count conventions.
%
%   [ req_rate ] = rate_convert( cur_rate, cur_comp_freq, cur_basis, req_comp_freq, req_basis, first_date,  last_date )
%   
%
%   Input:
%       cur_rate - is an NRATESx1 vector specifying the current rate values.
%
%       cur_comp_freq - is a scalar value specifying the the count of coupon payments per year. Acceptable values are
%                                   -1,1(default),2,3,4,6,12.
%
%       cur_basis - is a scalar value specifying the day-count basis. Possible values include:
%                           0	- actual/actual (default)
%                           1	- 30/360 SIA
%                           2	- actual/360
%                           3	- actual/365
%                           4	- 30/360 PSA
%                           5	- 30/360 ISDA
%                           6	- 30/360 European
%                           7	- actual/365 Japanese
%                           8	- actual/actual ISMA
%                           9	- actual/360 ISMA
%                           10 - actual/365 ISMA
%                           11 - 30/360 ISMA
%                           12 - actual/365 ISDA
%                           13 - bus/252
%
%       req_comp_freq - is a scalar value specifying the required count of coupon payments per year.
%                                   Possible values are the same as curr_comp_freq.
%
%       req_basis - is a scalar value specifying the required day-count basis. Possible values are
%                                   the same as cur_basis.
%
%       first_date - is a scalar specifying the first date (in all possible MATLAB date formats) of
%                                   the required time period, basicly it is a settlement date.
%
%       last_date - is a scalar or NDATESx1 vector specifying the last date (in all possible MATLAB
%                                   date formats) of the required time period, basicly it is a maturity date.
%
%   Output:
%       req_rate - is an MRATESx1 vector specifying the rates in the required format.
%

% Yigal Ben Tal
% Copyright 2012.

    %% Compute accrual factors
    cur_time = yearfrac(first_date,last_date,cur_basis);
    req_time = yearfrac(first_date,last_date,req_basis);

    req_rate = zeros(size(cur_rate));

    %% Simple interest is signified with a 0 -- replace with a 1
    if cur_comp_freq == 0 && req_comp_freq == 0
        req_rate = (cur_time./req_time).*cur_rate;
    elseif cur_comp_freq == 0 && req_comp_freq == -1
        req_rate = log(1 + cur_time.*cur_rate)./req_time;
    elseif cur_comp_freq == 0 && req_comp_freq > 0
        req_rate = ((1 + cur_time.*cur_rate).^(1./(req_time*req_comp_freq)) - 1)*req_comp_freq;
    elseif cur_comp_freq > 0 && req_comp_freq == 0
        req_rate = ((1 + cur_rate/cur_comp_freq).^(cur_time*cur_comp_freq) - 1)./req_time;
    elseif cur_comp_freq > 0 && req_comp_freq == -1
        req_rate = log((1 + cur_rate/cur_comp_freq).^(cur_time*cur_comp_freq))./req_time;
    elseif cur_comp_freq > 0 && req_comp_freq > 0
        req_rate = ((1 + cur_rate/cur_comp_freq).^(cur_time*cur_comp_freq./(req_time*req_comp_freq)) - 1)*req_comp_freq;
    elseif cur_comp_freq == -1 && req_comp_freq == 0
        req_rate = (exp(cur_time.*cur_rate) - 1)./req_time;
    elseif cur_comp_freq == -1 && req_comp_freq == -1
        req_rate = (cur_time./req_time).*cur_rate;
    elseif cur_comp_freq == -1 && req_comp_freq > 0
        req_rate = ((exp(cur_time.*cur_rate).^(1/(req_time*req_comp_freq))) - 1)*req_comp_freq;
    end

    % Handle the case when the input date is a settle
    req_rate(cur_time == 0) = cur_rate(cur_time == 0);

end