function [ res_dates ] = ex2pmtperiod(ds)

    %% Input validation:
    error(nargchk(1,1,nargin));
    
    %% Extract required dates:
    unsin = unique(ds.nsin);
    nassets = length(unsin);
    res_dates = cell(nassets,1);
%     periods = cell();
    for i = 1: nassets
        % Build periods between ex days to payment days of each coupon:
        periods = arrayfun(@(x,y) [x:y]', datenum(ds.date_ex(ds.nsin == unsin(i))), datenum(ds.date_pay(ds.nsin == unsin(i))), 'UniformOutput', false);
       
        % Union all ex-periods for each asset:
        res_dates{i} = cell2mat(periods);
    end
    
    %% Build output structure in case of multiple input:
    
end