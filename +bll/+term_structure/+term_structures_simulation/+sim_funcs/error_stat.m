function [ time_stat, sim_stat, crv_stat ] = error_stat( fit_err )
%ERROR_STAT 

    %% Import external packages:
    
    %% Input validation:
    error(nargchk(1, 1, nargin));
    
    %% Extract data:
    l = size(fit_err, 4);
    for ll = 1: l
        
        % Step #1 (Intra-window time statistics - mean of all simulations per asset):
        time_stat.mean(:,:,ll) = nanmean(fit_err(:,:,:,ll), 3);
        time_stat.std(:,:,ll) = nanstd(fit_err(:,:,:,ll), [], 3);
        time_stat.min(:,:,ll) = nanmin(fit_err(:,:,:,ll), [], 3);
        time_stat.max(:,:,ll) = nanmax(fit_err(:,:,:,ll), [], 3);
        time_stat.skew(:,:,ll) = skewness(fit_err(:,:,:,ll), 0, 3);
        time_stat.kurt(:,:,ll) = kurtosis(fit_err(:,:,:,ll), 0, 3);
    
        % Step #2 (Simulation statistics - per curve and per asset)
        sim_stat.mean(ll,:,:) = nanmean(fit_err(:,:,:,ll), 1);
        sim_stat.std(ll,:,:) = nanstd(fit_err(:,:,:,ll), [], 1);
        sim_stat.min(ll,:,:) = nanmin(fit_err(:,:,:,ll), [], 1);
        sim_stat.max(ll,:,:) = nanmax(fit_err(:,:,:,ll), [], 1);
        sim_stat.skew(ll,:,:) = skewness(fit_err(:,:,:,ll), 0, 1);
        sim_stat.kurt(ll,:,:) = kurtosis(fit_err(:,:,:,ll), 0, 1);
        
        crv_stat.mean(:,ll,:) = nanmean(fit_err(:,:,:,ll), 2);
        crv_stat.std(:,ll,:) = nanstd(fit_err(:,:,:,ll), [], 2);
        crv_stat.min(:,ll,:) = nanmin(fit_err(:,:,:,ll), [], 2);
        crv_stat.max(:,ll,:) = nanmax(fit_err(:,:,:,ll), [], 2);
        crv_stat.skew(:,ll,:) = skewness(fit_err(:,:,:,ll), 0, 2);
        crv_stat.kurt(:,ll,:) = kurtosis(fit_err(:,:,:,ll), 0, 2);
    end

    
    if size(fit_err,2) ==1
        sim_stat.mean = permute(sim_stat.mean, [1 3 2]);
        sim_stat.std = permute(sim_stat.std, [1 3 2]);
        sim_stat.min = permute(sim_stat.min, [1 3 2]);
        sim_stat.max = permute(sim_stat.max, [1 3 2]);
        sim_stat.skew = permute(sim_stat.skew, [1 3 2]);
        sim_stat.kurt = permute(sim_stat.kurt, [1 3 2]);
    end
    
    
        
end

