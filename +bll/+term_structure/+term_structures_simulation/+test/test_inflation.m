clear; clc;
 
%% Import external packages:
import bll.term_structure.bootstrap_mdl.*;
import bll.term_structure.ns_mdl.*;
import bll.term_structure.nss_mdl.*;
 
import utility.cash_flow.*;
import dal.market.get.dynamic.*;
 
%% Build start parameters:
settle = datenum('2011-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.
 
param.compounding = 1;
param.basis = 10;
param.interpmethod = 'cubic';
 
ir_type = 'forward';

 
%% Nominal data:
[ tbill_cf, tbill_nsin ] = cfbuilder(settle, 0);
tbill_price = get_bond_dyn_single_date(tbill_nsin, 'price_dirty', settle);
 
[ shahar_cf, shahar_nsin ] = cfbuilder(settle, 1);
shahar_price = get_bond_dyn_single_date(shahar_nsin, 'price_dirty', settle);
 
nom_cf = merge(tbill_cf, shahar_cf);
nom_price = merge(tbill_price, shahar_price);

% nom_cf = shahar_cf;
% nom_price = shahar_price;

%% Real data:
[ tips_cf, tips_nsin ] = cfbuilder(settle, 2);
tips_price = get_bond_dyn_single_date(tips_nsin, 'price_dirty', settle);
 
%% Nominal Model Curves:
[ nom_ircurve_bt ] = bootstrap( ir_type, nom_price, nom_cf, settle, param );
[ nom_ircurve_ns ] = nelson_siegel(ir_type, nom_price, nom_cf, settle, param);
[ nom_ircurve_sv ] = svensson(ir_type, nom_price, nom_cf, settle, param);
 
%%  Real Model Curves:
[ tips_ircurve_bt ] = bootstrap( ir_type, tips_price, tips_cf, settle, param );
[ tips_ircurve_ns ] = nelson_siegel(ir_type, tips_price, tips_cf, settle, param);
[ tips_ircurve_sv ] = svensson(ir_type, tips_price, tips_cf, settle, param);
 
%% Inflation Curves by Models:
PlotDates = settle+30:30:settle+365*10;
inf_curve_bt = (1 + nom_ircurve_bt.get_zero_rates(PlotDates)) ./ (1 + tips_ircurve_bt.get_zero_rates(PlotDates)) - 1;
plot(inf_curve_bt, 'b'); hold on;
inf_curve_ns = (1 + nom_ircurve_ns.get_zero_rates(PlotDates)) ./ (1 + tips_ircurve_ns.get_zero_rates(PlotDates)) - 1;
plot(inf_curve_ns, 'g'); hold on;
inf_curve_sv = (1 + nom_ircurve_sv.get_zero_rates(PlotDates)) ./ (1 + tips_ircurve_sv.get_zero_rates(PlotDates)) - 1;
plot(inf_curve_sv, 'r'); hold on;
plot(PlotDates, 0.01*ones(size(PlotDates)), '--k'); hold on;
plot(PlotDates, 0.03*ones(size(PlotDates)), '--k');
