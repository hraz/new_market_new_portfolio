function [ obj, exitflag ] = fit_func(ir_type, price, cf, settle, func_handle, fit_option, fit_bench, varargin)
%FIT_FUNC Fits function to market data
%
%   [ obj ] = fit_func(ir_type, price, cf, func_handle, fit_option, fit_bench, varargin)
%   fits an interest rate function curve to market data.
%
%   Input:
%       ir_type - is a string value specifying the type of interest rate curve (Forward, Zero).
%
%       price - is an 1xNBONDS fts specifying the dirty price at settlement date.
%
%       cf - is a NPAYMENTSxNBONDS fts specifying the cash flow of the given portfolio.
%
%       func_handle - is a function handle to be fit.
%
%       fit_option - is a scalar object of an IRFitOption class.
%
%       fit_bench - is an 1xNBONDS fts specifying the values of the yield or modified duration at
%                           settlement date.
%
%   Optional input:
%       basis - is a scalar value specifying the day-count basis. Possible values include:
%                           0	- actual/actual
%                           1	- 30/360 SIA
%                           2	- actual/360
%                           3	- actual/365
%                           4	- 30/360 PSA
%                           5	- 30/360 ISDA
%                           6	- 30/360 European
%                           7	- actual/365 Japanese
%                           8	- actual/actual ISMA
%                           9	- actual/360 ISMA
%                           10 - actual/365 ISMA  (default)
%                           11 - 30/360 ISMA
%                           12 - actual/365 ISDA
%                           13 - bus/252
%
%       compounding -  is a scalar value specifying the the count of coupon payments per year. Acceptable values are
%                                   -1,1(default),2,3,4,6,12.
%
%   Note:   The additional parameters (basis, compounding) may be passed in via some
%               structure with field_name = parameter_name and its value is a parameter_value. 
%
%   Output:
%       obj - is a scalar of ir_func_curve class object.

% Yigal Ben Tal
% Copyright 2012.

    %% Import external packages:
%     global f;
    import utility.calendar.*;
    import bll.term_structure.*;

    %% Input validation:
    error(nargchk(7, 8, nargin));
    
    %% Parse inputs
    p = inputParser;

    p.addRequired('ir_type',@ischar);
    p.addRequired('price', @(x)(isa(x, 'fints')));
    p.addRequired('cf', @(x)(isa(x, 'fints')));
    p.addRequired('func_handle',@(x)(isa(x,'function_handle')));
    p.addRequired('fit_option',@(x)(isa(x,'IRFitOptions')));
    p.addRequired('fit_bench',@(x)(isa(x,'fints')));

    p.addParamValue('compounding', 1, @(x)(all(ismember(x,[-1 1 2 3 4 6 12]))));
    p.addParamValue('basis',10);              % act/365   
    
    % Parse method to parse and validate the inputs:
    p.StructExpand = true; % for input as a structure
    p.KeepUnmatched = true;
    
    try
        p.parse(ir_type, price, cf, func_handle, fit_option, fit_bench, varargin{:});
    catch ME
        newME = MException('term_structure:ir_func_curve:optionalInputError',...
            'Error in optional parameter value inputs');
        newME = addCause(newME,ME);
        throw(newME)
    end

    %% Step #1 (Building of the basic parameters):
    param.compounding = p.Results.compounding;
    param.basis = p.Results.basis;

    %% Step #2 (Preparing the benchmark data for fitting):
    f_bench = fit_bench.desc;
    if strcmpi(fit_option.FitType,'yield') && strcmpi(f_bench(1), 'y')
        observed_yld =  fts2mat(fit_bench)';
    elseif strcmpi(fit_option.FitType,'durationweightedprice') && any(strfind(f_bench, 'dur'))
        dur = fts2mat(fit_bench)';
    end

    %% Step #3 (Preparing the price and cash flow data for using in the model):
    dirty_price = fts2mat(price)';
    
    cf_amount = fts2mat(cf)';
    cf_amount(isnan(cf_amount)) = 0;

    cf_date = cf.dates;

    % This addresses the case where payment dates are on the settle
    if cf_date(1) == settle
        cf_date(1) = [];
        doAddSettle = 1;
    else
        doAddSettle = 0;
    end
    
    %% Step #4 (Find the time to maturity for each of the payment dates):
    cf_times = yearfrac(settle, cf_date, param.basis);

    %% Step #5 (Calculating of required rates):
    function f = costfun(x)
%         global f; % tmp variable
        rates = func_handle(cf_times,x);
        switch lower(ir_type)
            case 'zero'
                zr = rates;
                df = zero2disc(zr,cf_date,settle,param.compounding,param.basis);
            case 'discount'
                df = rates;
            case 'forward'
                zr = zeros(size(cf_times));
                for jdx=1:length(cf_times)
                    zr(jdx) = quadl(@(cf_times) func_handle(cf_times,x),0,cf_times(jdx));
                end
                zr = zr./cf_times;
                df = zero2disc(zr,cf_date,settle,param.compounding,param.basis);
        end

        if doAddSettle
            df = [1; df]; 
        end
        pv = cf_amount * df;

        switch lower(fit_option.FitType)
            case 'price'
                f = dirty_price - pv;
            case 'yield'
%                 exp_yld = bnd_yield(); % its need to add bond yield calculation;
%                 f = observed_yld - exp_yld;
            case 'durationweightedprice'
                f = (dirty_price - pv)./dur;
        end
    end

    %% Step #6 (Definition of the start parameters' values for optimization process):
    x0 = fit_option.InitialGuess;
    ub = fit_option.UpperBound;
    lb = fit_option.LowerBound;
    options = fit_option.OptOptions;
    
    %% Step #7 (Make the optimization process for defining of the model parameters):
    [params,~,~,exitflag] = lsqnonlin(@costfun,x0,lb,ub,options);
    
    if (exitflag == 0)
        warning('term_structure:ir_func_curve:LimitExceeded', ...
            'Function evaluation or iteration limit exceeded.');
    elseif (exitflag < 0)
        error('term_structure:ir_func_curve:NoSolution', ...
            'Error in optimization, check curve function');
    end
    
    %% Step #8 (Construct an object with the inputs):
    obj = ir_func_curve(ir_type,settle,@(t)(func_handle(t,params)), param);
%      params
       
end