classdef ir_func_curve < bll.term_structure.ir_curve
%IR_FUNC_CURVE Interest rate curve represented with function
%
%   =========================================
%   The class based on IRFunctionCurve MATLAB's class.              
%   ========================================= 
%
%   An ir_func_curve is a representation of an interest rate curve with a function.  This can be
%   constructed directly by specifying a function handle or a function can be fit to market data
%   using methods of the object. Once a curve has been constructed, forward and zero rates and
%   discount factors can be extracted and it can be converted to a RateSpec structure -- and used
%   with functions in the Financial Derivatives Toolbox. 
%
%   An interest rate function curve has the following properties:
%   -------------------------------------------------------
%
%   Type - is a type of curve, either Forward or Zero.
%
%   Settle - is a settle date for curve (scalar).
%
%   Compounding - is a compounding frequency for curve, acceptable values are -1,1(default),2,3,4,6,12.
%
%   Basis - is a day-count basis.
%       Possible values include:
%           0 - actual/actual (default)
%           1 - 30/360 SIA
%           2 - actual/360
%           3 - actual/365
%           4 - 30/360 PSA
%           5 - 30/360 ISDA
%           6 - 30/360 European
%           7 - actual/365 Japanese
%           8 - actual/actual ISMA
%           9 - actual/360 ISMA
%           10 - actual/365 ISMA
%           11 - 30/360 ISMA
%           12 - actual/365 ISDA
%
%   FuncHandle - is a function that defines the interest rate curve.
%
%   An interest rate data curve has the following methods:
%   -----------------------------------------------------
%   get_fwd_rates: Returns forward rates for input dates.
%
%   get_zero_rates: Returns zero rates for input dates.
%
%   get_disc_factors: Returns discount factors for input dates.
%
%   Optional inputs to the above methods are basis and compounding
%   ------------------------------------------------------------------
%   rate_specification: Converts to be a RateSpec object, like the ones produced by INTSENVSET, for
%                               input dates.
%
% See also IRCURVE, IRFITOPTIONS

% Yigal Ben Tal
% Copyright 2012

    %% Properties and methods declaration:
    properties (SetAccess = protected, GetAccess = public)
        FuncHandle = [];
    end
    methods (Access = private)
        function [ compounding, basis ] = parse_optin( in_param )
        %PARSE_OPTINP Parses inputs to get_fwd_rates, get_zero_rates, get_disc_factors functions
        %
        %   [ compounding, basis ] = parse_optin( in_param )
        %   parses optional input for three methods.
        %
        %   Input:
        %       in_param - is a structure of optional input for some methods of the class.
        %
        %   Output:
        %       compounding - it is a scalar value specifying the compounding frequency.
        %
        %       basis - it is a scalar value specifying the day-count basis.
        %
        % Yigal Ben Tal
        % Copyright 2012
        
            %% Declaration of the parser:
            p = inputParser;

            %%  Building the optional parameters default values and type checking rules:
            p.addParamValue('compounding',[],@(x) ismember(x,[-1 1 2 3 4 6 12]));
            p.addParamValue('basis',[],@(x) ismember(x,0:12));

            %% Parse method to parse and validate the inputs:
            p.StructExpand = true; % for input as a structure
            p.KeepUnmatched = true;
            
            %% Parsing input:
            try
                p.parse( in_param );
            catch ME
                newME = MException('ir_func_curve:parse_optin:optionalInputError',...
                    'Error in optional parameter value inputs');
                newME = addCause(newME,ME);
                throw(newME)
            end

            compounding = p.Results.compounding;
            basis = p.Results.basis;

        end

    end
    methods
        function [ obj ] = ir_func_curve( type, settle, func_handle, varargin )
        %IR_FUNC_CURVE Create an interest rate function curve.
        %   [ obj ] = ir_func_curve( type, settle ) creates a curve with specified
        %   type and settle date.
        %
        %   [ obj ] = ir_func_curve(type, settle, func_handle) creates
        %   a function curve with a function handle specified
        %
        %   Optional inputs, specified as fields of some structure, are
        %   COMPOUNDING and BASIS.
        %
        %   Example:  Create a curve
        %
        %           c = ir_func_curve('Forward',today);
        %
        %   Example: Create a curve with dates and data
        %
        %           c = ir_func_curve('Forward',today, @(t) polyval([-0.0001 0.003 0.02],t));
        %
        % Yigal Ben Tal
        % Copyright 2012
        
            %% Input validation:
            error(nargchk(2, 4, nargin));
            
            % Parse required input
            if all(~strcmpi(type,{'forward','zero','discount'}))
                error(message('ir_func_curve:ir_func_curve:invalidType'))
            end
            
            if length(settle) > 1
                error(message('ir_func_curve:ir_func_curve:invalidSettleSize'))
            end
            
            try
                settle = finargdate(settle);
            catch E
                error('ir_data_curve:ir_data_curve:invalidSettle','Settle must be a Date')
            end
            
            %% Parse optional inputs:
            if nargin > 2
                if ~isempty(func_handle) && ~isa(func_handle,'function_handle')
                        error(message('ir_func_curve:ir_func_curve:invalidFuncHandle'))
                end
                if (nargin == 4)
                    p = inputParser;

                    p.addParamValue('compounding', 1, @(x) ismember(x,[-1 1 2 3 4 6 12]));
                    p.addParamValue('basis', 10, @isvalidbasis);

                    % Parse method to parse and validate the inputs:
                    p.StructExpand = true; % for input as a structure
                    p.KeepUnmatched = true;
                    try
                        p.parse(varargin{:});
                    catch ME
                        newME = MException('ir_func_curve:ir_func_curve:optionalInputError',...
                            'Error in optional parameter value inputs');
                        newME = addCause(newME,ME);
                        throw(newME)
                    end
                end
            end
            
            %% Initialization of the output parameters:
            obj.Type = type;
            obj.Settle = settle;
            
            if (nargin > 2)
                obj.FuncHandle = func_handle;
                if (nargin == 4)
                    obj.Basis = p.Results.basis;
                    obj.Compounding = p.Results.compounding;
                end
            end
            
        end
        function [ ir_spec ] = rate_specification( obj, req_dates )
        %IR_FUNC_CURVE/RATE_SPECIFICATION Converts to RateSpec
        %
        %   [ ir_spec ] = rate_specification( obj, req_dates ) returns a ir_spec
        %   object, like the one created by INTENVSET -- where REQ_DATES
        %   is an N X 1 vector of MATLAB date numbers or a string or cell array of strings of dates
        %   after the Curve settle. 
        %
        %   Input:
        %       obj - is an ir_func_curve object.
        %
        %       req_dates - is an N X 1 vector of MATLAB date numbers or a string or
        %                           cell array of strings of dates after the curve settle.
        %
        %   Output:
        %       ir_spec - is an object, like the one created by INTENVSET.
        %
        % Yigal Ben Tal
        % Copyright 2012
            
            %% Reformat required dates:
            req_dates = finargdate(req_dates);
            req_dates = req_dates(:);
            
            %% Get zero rates according to required dates:
            [ zero_rates ] = fts2mat(get_zero_rates(obj, req_dates));
            
            %%  Build rate structure:
            ir_spec = intenvset( 'Rates', zero_rates(:), 'EndDates', req_dates(:), 'StartDates', obj.Settle,...
                                          'Basis', obj.Basis, 'Compounding', obj.Compounding);
            
        end
        function disp(obj)
        %IR_DATA_CURVE/DISP displays an object.
        %   
        %   DISP(IRFC) displays the ir_data_curve at the command line.
        %
        %   Input:
        %       obj - is an ir_data_curve object.
        %
        %   Output:
        %       none.
        %
        % Yigal Ben Tal
        % Copyright 2012.

            href = '<a href="matlab:help ir_func_curve ">ir_func_curve</a>';
            fprintf(1, ['\t' href '\n\n']);
            
            BasisStrings = {'actual/actual','30/360 (SIA)',...
                'actual/360','actual/365','30/360 (PSA)','30/360 (ISDA)',...
                '30/360 (European)','actual/365 (Japanese)',...
                'actual/actual (ISMA)','actual/360 (ISMA)',...
                'actual/365 (ISMA)','30/360E (ISMA)','actual/365 (ISDA)'};
            fprintf(1,['\tCurve Properties: \n']);
            fprintf(1,['\t\t\t Type\t\t\t: ' obj.Type '\n']);
            fprintf(1,['\t\t\t Settle\t\t\t: ' num2str(obj.Settle) ' (' datestr(obj.Settle) ')' '\n']);
            fprintf(1,['\t\t\t Compounding\t: ' num2str(obj.Compounding) '\n']);
            fprintf(1,['\t\t\t Basis\t\t\t: ' num2str(obj.Basis) ' (' BasisStrings{obj.Basis+1}  ')' '\n']);
            fprintf('\n');
            
            href1 = '<a href="matlab:methods(''ir_func_curve'') ">Methods</a>';
            fprintf(1, ['\t' href1 '\n']);
            
            isLoose = strcmp(get(0,'FormatSpacing'),'loose');
            if (isLoose), fprintf('\n'); end
        end
        [ zero_rate] = get_zero_rates( obj, req_dates, varargin )
        [ fwd_rate ] = get_fwd_rates( obj, req_dates, varargin )
        [ df ] = get_disc_factors( obj, req_dates, settle )        
    end
    methods(Static = true)
        [ obj, exitflag ] = fit_func(ir_type, price, cf, func_handle, fit_option, fit_bench, varargin)    
    end
    
end