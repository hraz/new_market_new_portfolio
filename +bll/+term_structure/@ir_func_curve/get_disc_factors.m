function [ df ] = get_disc_factors(obj, req_dates, settle)
%IR_FUNC_CURVE/GET_DISC_FACTORS Get Discount Factors for input dates
%
%   [ df ] = get_disc_factors( obj, req_dates) 
%   [ df ] = get_disc_factors(obj, req_dates, settle)
%   [ df ] = get_disc_factors(obj, req_dates, settle, varargin)
%   returns discount factors for the input dates.
%
%   Input:
%       obj - is an ir_func_curve object represented the given curve.
%
%       req_dates - is an NDATESx1 cell-array of strings or numeric vector of required dates.
%
%   Optional input:
%       settle - is a scalar value specifying some settlement date different from the settlement of
%                   the curve.
%
%       compounding - is a compounding frequency for curve, acceptable values are -1,1(default),2,3,4,6,12.
%
%       basis - is a day-count basis.
%                   Possible values include:
%                       0 - actual/actual
%                       1 - 30/360 SIA
%                       2 - actual/360
%                       3 - actual/365
%                       4 - 30/360 PSA
%                       5 - 30/360 ISDA
%                       6 - 30/360 European
%                       7 - actual/365 Japanese
%                       8 - actual/actual ISMA
%                       9 - actual/360 ISMA
%                       10 - actual/365 ISMA (default)
%                       11 - 30/360 ISMA
%                       12 - actual/365 ISDA
%                       13 - bus/252
%
%   Output:
%       df - is an NREQDATESx1 financial time-series object represented discount factors for
%                               the input dates. 

% Yigal Ben Tal
% Copyright 2012

    %% Import external variables:
    import bll.term_structure.*;
    
    %% Input validation:
    error(nargchk(2, 3, nargin));
    
    % Check the object class:
    if ~isa(obj, 'bll.term_structure.ir_func_curve')
        error('ir_func_curve:get_disc_factors:invalidObject', 'Object must be one of ir_func_curve class.');
    end
    
    % Reformat required dates:
     if (nargin <2) || isempty(req_dates)
        error('ir_func_curve:get_disc_factors:invalidRequiredDates',...
            'Req_dates cannot be an empty.');
    else
        req_dates = finargdate(req_dates);
        req_dates = req_dates(:);
     end
    
     % Validation of optional input:
    if (nargin < 3) || isempty(settle)
        settle = obj.Settle;
    end
    
    %% Step #1 (Create time to maturity periods using reguired dates):
    ttm = yearfrac(settle, req_dates, obj.Basis);
    
    %% Step #2 (Create discount factors on base existed interest rate curve):
    switch lower(obj.Type)
        case 'forward'
            zr = zeros(size(ttm));
            for jdx=1:length(ttm)
                zr(jdx) = quadl(obj.FuncHandle,0,ttm(jdx));
            end
            zr = zr./ttm;
            [ df ] = zero2disc(zr, req_dates, settle, obj.Compounding, obj.Basis);
        case 'zero'
            [ zr ] = feval(obj.FuncHandle, ttm);
            [ df ] = zero2disc(zr, req_dates, settle, obj.Compounding, obj.Basis);
        case 'discount'
            [ df ] = feval(obj.FuncHandle, ttm);
    end
    
    %% Step #3 (Build fts):
    [ df ] = fints(req_dates, df, 'discount_factor');
    
end