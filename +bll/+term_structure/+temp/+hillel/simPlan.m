%%% Summary of functions/method of simulation

% Calculate bond value - BondInvAtYieldRate(Bond, t_0, t_f) - calculates value of bond
% after time, where Bond is some data set which will give BondPrice0, BondYield,
%BondMaturity, BondPriceT, CoupVal and CoupDates. last two are vectors which give all moneys given back
% to bond holder along with date when given.  calculation is based on
%reinvestment of bonds at interest rate of coupon yield.  

%calculate value for portfolio - add value for all bonds

% calculate yield for portfolio - PortYield (V_0, V_F) - based on initial
% value V_0, final value, V_F

% calculate beta - use function FindBeta defined by The Machine in previous program

% calculate alpha - use function FindAlpha defined by The Machine in the
% previous program

% calculate total value for portfolio - TotValPort(Bond, t_0, time) where Bond
% is bond list (will depend on type of data structure we use) t_0 is starting time and time is
% length of time portfolio is alive

% calculate Information Ratio - InfoRatio( Index, BondList, t_0, t_f ) where Index - list of active indices (vector),
%BondList - list of bonds in portfolio (vector), (also needed BondList(i, t).indexValue
%- index value at time t of bond i - will depend on type of data structure).  t_0 is starting time for portfolio and
%t_f is final time.  function uses BondInvAtYieldRate(Bond, t_0, t_f) where
%Bond comes from BondList...  

% PortRet( BondList, t_0, t_f ) gives portfolio returns for every time t between t_0 and t_f given BondList in portfolio, starting
% time t_0 and final time t_f.  

% calculate skewness and kurtosis of portfolio returns - PortSkew(PortRet).
% function does not call PortRet but instead uses its output value,
% PortVal.  