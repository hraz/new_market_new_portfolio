function [ returns mat_price_yld prices ] = BondRetHist( nsin, t_0, t_f )
%
% function calculates bond return including interest yielded on coupons received.  Result_val
% Once bond is dead, function returns NaN for all values. 
%
%
% Input:
%  nsin = a vector of nsin for bonds
% 
% t_0 = time at which investment is to begin
% 
% t_f = time at which to sell bonds
% 
%
% Output:
% 
% returns = number of nsins x 2 cell, where first cell is nsin and second
% cell is made of a 2 by number of active trades dates matrix (num_dates)
% and the first column is the daily cash returns of the bonds, and the second
% column is the yield (in percentage)
%
% mat_yield - number of nsins +1 x t_f-t_0  matrix, first column is nsins
% and rest yields.
%
%prices - number of nsins vector of prices of last day (t_f)
%
% Notes: 
% 
% At this point, the originl prices paid for the bonds are not discounted.  That is, this function 
% gives total money returned but doesn't discount for expenses.  In addition, the yield right now
% is calculated according to the initial yield at t_0.  


import dal.market.get.static.*;
import dal.market.get.cash_flow.*;
import dal.market.get.dynamic.*;
import utility.fts.*;
import utility.*;

all_nsin = length(nsin); %number of nsins that are to be calculated for

t_0 = datenum(t_0); 
t_f = datenum(t_f);

%% Check that bonds are alive during time

BondData = get_bond_stat(nsin, t_0) ; %get bond data - in particular bond maturity date

nsin = BondData.nsin; %as nsins may have been re-ordered

nsin_good_dates = []; %list of bonds that are alive between t_0 and t_f
nsin_bad_dates = []; %list of bonds that are not alive between t_0 and t_f

for num=1:1:all_nsin
    
    bond_beg = datenum(BondData.first_trading_date(num)); %get bond first trading date
    bond_end =  datenum(BondData.maturity_date(num));%get maturity date for bond
    
    if (bond_beg < t_0 && bond_end > t_0) || (bond_end > t_f && bond_beg < t_f) || (bond_beg > t_0 && bond_end <t_f)
        
        nsin_good_dates = [nsin_good_dates nsin(num) ]; %nsin is alive during period, add to good list
        
    else
        
        nsin_bad_dates = [nsin_bad_dates nsin(num) ];
        
    end
    
   
end

num_nsin = length(nsin_good_dates); %use only good nsins

cell_nsin = cell(num_nsin, 8); %will store all info on bonds: cell 1 is made of nsin
 %cell 2 is made of maturity date
 %cell 3 is made of type (G or NG)
 %cell 4 is made of cash flow payments and dates
 %cell 5 is made of number of coupons
 %cell 6 is made of ytms on dates coupons were given
 %cell 7 is made of number of dates of redemption funds and the
 %redemption values


%% Bonds' maturities

for num = 1:1:num_nsin

    
    cell_nsin{num, 1} = nsin_good_dates(num); %store nsins in first cell of each cell group

  cell_nsin{num, 2} = datenum(BondData.maturity_date(num));%store maturities of all bonds in corresponding cell (#2)
   BondClass = BondData.bond_class(num); 
   cell_nsin{num, 3} = 'G'; %check if bond is government or non-government.  this will be used later if the coupons fall on non-business days
                                                    %store in 3 corresponding cell

   if strcmp(BondClass, 'NON-GOVERNMENT BOND')
       cell_nsin{num, 3} = 'NG';
   end
   
end
  
   
%% Get cash flow for all bonds

 [ coupon_income,redemption_income ] = get_cash_flow_income(t_0, t_f, nsin_good_dates);
 
 vec_coup_ids = tsnames(coupon_income);
vec_red_ids = tsnames(redemption_income);

 red_vector =  ismember(nsin, strid2numid(vec_red_ids));%find nsins common to both
 coup_vector =  ismember(nsin, strid2numid(vec_coup_ids));%find nsins common to both
 
num_live_bnd = length(vec_coup_ids);%number of bonds that actually have cash flow (i.e. are alive) during time period
num_red_bnd = length(vec_red_ids);

coupnum = 0;%counter for number of coupon yielding bonds in loop  

    for num = 1:1:num_live_bnd
    
        if coup_vector(num)
            coupnum = coupnum  + 1; 
            
        coup_flow_init = fts2mat(coupon_income.(vec_coup_ids(coupnum)));
        coup_flow_dates = coupon_income.(vec_coup_ids(coupnum)).dates;
        coup_flow_nan = isnan(coup_flow_init);
        
        coup_flow = [coup_flow_dates coup_flow_init];
        coup_flow(find(coup_flow_nan),:) = [];
        

    cell_nsin{num, 4} = [coup_flow]; %cash flow dates for given bond with non NaN's

    num_coups= size(cell_nsin{num, 4}); %number of coupons paid for a given bond
    cell_nsin{num, 5} = num_coups(1);
        end    
   %zero out all arrays

coup_flow_init = [];
coup_flow_dates = [];
coup_flow_nan = [];
coup_flow = [];
   
    end
    red = 0; %will count number of times for redemption coupons loop

    for num = 1:1:num_nsin

   if red_vector(num)
       red = red + 1;
       
        red_flow_init = fts2mat(redemption_income.(vec_red_ids(red)));
        red_flow_dates = redemption_income.(vec_red_ids(red)).dates;
        red_flow_nan = isnan(red_flow_init);
        
        red_flow = [red_flow_dates red_flow_init];
        red_flow(find(red_flow_nan),:) = [];
   
        cell_nsin{num, 7} = [red_flow];
   end 
   %zero out all arrays
    red_flow_init =[];
red_flow_dates = [];
 red_flow_nan = [];
red_flow = [];

    end
%% get prices and ytms for time interval

    data_name = {'price_dirty', 'ytm', 'price_yld'};   
    [fts_price] = get_multi_dyn_between_dates(nsin_good_dates, data_name, 'bond', t_0, t_f ); %pull out prices between t_0 and t_f
    mat_price = fts2mat(fts_price.price_dirty, 1); %convert to matrix
%     
%     for num = 1:1:num_nsin 
%         if cell_nsin{num, 2} < t_f
%             cell_nsin{num, 7} = max( find( mat_price(:,1) <= cell_nsin{num, 2}) ); 
%             %get number of dates in
%             %which there is activity in TASE between t_0 and BondMaturity
%     
%         else 
%             cell_nsin{num, 7} = length(mat_price); %otherwise get full length of mat_price as BondMaturity is beyond range
%         end
%         
%     end

prices_temp = mat_price(end, :); %prices on last day

prices = prices_temp(2:end);

%     data_name = {'ytm'}; 
%       [fts_ytm] = get_multi_dyn_between_dates(nsin_good_dates, data_name, 'bond', t_0, t_f ); %pull out prices between t_0 and t_f
 mat_yield = fts2mat(fts_price.ytm, 1); %get matrix of yields
    
 
%      data_name = {'price_yld'}; 
%       [fts_pri] = get_multi_dyn_between_dates(nsin_good_dates, data_name, 'bond', t_0, t_f ); %pull out prices between t_0 and t_f
 mat_price_yld = fts2mat(fts_price.price_yld, 1);

 for num=1:1:num_nsin
     
     num_coupons = cell_nsin{num, 5}; %number of coupons for corresponding nsin
     coup_dates_ytm = zeros(num_coupons, 1); %will hold the ytms on the dates the coupons are given - in order to calculate the reinvestment according to them

 for c=1:1:num_coupons

     date_found = find(mat_yield(:, 1) == cell_nsin{num, 4}(c,1));
     
     if date_found %if date  of coupon payment is a business day
  
    coup_dates_ytm(c) = mat_yield(date_found, 2); %find the matching date in the coupon flow chart and take the yield
     
     elseif strcmp(cell_nsin{num, 3}, 'G') %if date of coupon payment is not a business and the bond is a government bond, take the next business day
     date_found = min(find(mat_yield(:, 1) >= cell_nsin{num, 4}(c,1) ) );    
      if isempty(date_found)
            coup_dates_ytm(c) = 0; %incase there is an issue - take ytm = 0
      else
     coup_dates_ytm(c) = mat_yield(date_found, 2); %find the matching date in the coupon flow chart and take the yield
      end
      
     elseif strcmp(cell_nsin{num, 3}, 'NG')  %if date of coupon payment is not a business and the bond is a non-government bond, take the previous business day
     date_found = max(find(mat_yield(:, 1) <= cell_nsin{num, 4}(c,1)) );    
     if isempty(date_found)
            coup_dates_ytm(c) = 0; %incase there is an issue - take ytm = 0
     else
     coup_dates_ytm(c) = mat_yield(date_found, 2); %find the matching date in the coupon flow chart and take the yield
     end
     
     end
     
 end

 cell_nsin{num, 6} = coup_dates_ytm; %store corresponding ytms in cell

 end
%% Calculate bond return for each time step

returns = cell( num_nsin, 2); %to store returns and yields for each nsin
num_dates = max( find( mat_price(:,1) <= t_f) );  %number of days of activity in TASE for bond during given period (t_0, t_f)

for num=1:1:num_nsin

    %num_dates = cell_nsin{num, 2}; %number of days of activity in TASE for bond during given period (t_0, t_f)
    bond_ret = zeros(num_dates, 2); %to store returns of each bond for each day (column 1) and the percentage return (column 2)
    num_coupons = cell_nsin{num, 5}; %number of coupons for corresponding bond
    principle = 0;
    
for time = 1:1:num_dates %time indicating where we are in terms of value of bond
    
        for j=1:1:num_coupons
           
            if mat_price(time, 1)  > cell_nsin{num, 4}(j) %if time is beyond coupon dates, get the coupon
            
                 bond_ret(time, 1) = cell_nsin{num, 4}(j, 2) * (1+cell_nsin{num,6}(j) )^( (mat_price(time, 1) - cell_nsin{num, 4}(j) )/365 );
                 %calculate worth of coupon including its interest where
                 %the interest is the ytm at the time the coupon was issued
                 %note that till first coupon comes, the yield is zero
            end
        end

    

                
 bond_ret(time, 1) = bond_ret(time, 1) + mat_price(time, num+1); %amount of money returned
     if red_vector(num)

     
            if cell_nsin{num, 7}(end, 1) <= mat_price(time, 1) %if time is beyond redemption date, get the redemption
                red_mat = cell_nsin{num, 7};
                        vec_payments = sum(red_mat(:, 2)); %get principle
                       principle = sum( vec_payments);
        
             bond_ret(time, 1) = bond_ret(time, 1) + principle; %principle paid
    end
     end

 bond_ret(time, 2) = ( bond_ret(time, 1)/mat_price(time, num+1) )^(365/time)-1; %yield returnedResult_val
 bond_ret(time, 1) = bond_ret(time, 1) - principle; %to make sure its paid only once
 
         if mat_price(time, 1) >cell_nsin{num, 2} %if we are past maturity date, bond is dead, return is set to NaN
            
            bond_ret(time, 1) = NaN;
            bond_ret(time, 2) = NaN;
            
        end
    
end

returns{num, 1} = nsin_good_dates(num); %store bond nsin in first column
returns{num, 2} = bond_ret; %store bond returns and yield in second column


        
end
