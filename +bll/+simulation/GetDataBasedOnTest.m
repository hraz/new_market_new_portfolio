clear; clc;

import dal.market.filter.*;
import dal.market.get.*;

% type = {'Structured Bond'};
% type = {'Corporate Bond' , 'Structured Bond'};
%type = {'Convertable', 'Corporate Bond', 'Structured Bond', 'Government Bond  Galil', 'Government Bond  Shachar'}
type = da_bond_base_cmn( 'type' );


% sector = {'BIOMED'};
% sector = {'BIOMED' , 'COMMERCE'};
%sector = {'INSURANCE', 'SERVICES', 'OIL AND GAS EXPLORATION','COMMERCIAL BANKS'};
sector =  da_bond_base_cmn( 'sector' );

linkage = {'CPI','NIS'};
% linkage = {'NIS' , 'UKP'};
% linkage = {'UKP', 'USD', 'EURO'}

coupon_type = 'FIX';
% coupon_type = 'VAR';

%param.rdate = 734757; %uint32(datenum('2011-01-01'));
param.rdate = datenum('17.02.2008','dd.mm.yyyy');
param.length = {[] []};
param.ytm_bruto = {[] []};
param.mod_duration = {[] []};
param.turnover = {[] []};

param.maalot = {'NR', 'AAA'};
param.midroog = {'NR', 'Aaa'};
param.log_opr = '';

% tic
[ isin ] = bond_ttl( type, sector, linkage, coupon_type, param);
% toc

%% Get static data

GetStaticData;

%%limit list so that there is a single mentioning of each bond
ds = ds (ds.ytm_bruto <1,:);
[dummy i]=unique(ds.isin);
dsu = ds(i,:);

isinu = dsu.isin;  % list of unique bonds, no repeats
NumOfBonds = length(isinu);
%% Get Dynamic data

GetDynamicData;

fts1 = fts;
%%
YTM = Year2Week(dsu.ytm_bruto);

%Hillel - Note that nancov can be applied directly on an fts, and we
%there's no need to convert the fts into a matrix using A=fts2mat(fts) and
%then using ExpCovMat= nancov(A);
ExpCovMat=nancov(fts); %TODO: think about adding weights as in ewstats - 
B =fts2mat(fts);



%% creating matrix without NaN

LenB = size(B);
j = 0;
for Row = LenB(1):-1:1

   if sum(isnan(B(Row, 1:size(B,2) ) ) )  < 0.5 
       j = Row;
   end
    
   if j ~=0
       break;
   end
end

%% plotting data and comparing ewstats vs nancov
BNew = B(1:j, :);
[ExpReturnB, ExpCovMatB, NumB] = ewstats(BNew, .94) %decay factor of .94, as is used by jpmorgan
      
NumPorts=20;
ConSet=portcons('default',NumOfBonds);
[PortRisk, PortReturn, PortWts, error, warning] = portoptError(YTM,ExpCovMat,NumPorts, [], ConSet); %Optimization
subplot(2,1,1) %first plot done with nancov
plot(sqrt(52)*PortRisk,Week2Year(PortReturn))
ConSet=portcons('default',NumOfBonds);
[PortRiskB, PortReturnB, PortWts, error, warning] = portoptError(YTM,ExpCovMatB,NumPorts, [], ConSet); %Optimization
subplot(2,1,2) %second plot done with ewstats - remains to see which one is "better"
plot(sqrt(52)*PortRiskB,Week2Year(PortReturnB))
norm(ExpCovMat-ExpCovMatB)
% done to this point - got data, plotted efficiency curve.  NTD: choose
% model, get prediction, compare to actual market performance.  

%% Getting data for rest of time

%type = da_bond_base_cmn( 'type' );


% sector = {'BIOMED'};
% sector = {'BIOMED' , 'COMMERCE'};
%sector = {'INSURANCE', 'SERVICES', 'OIL AND GAS EXPLORATION','COMMERCIAL BANKS'};
%sector =  da_bond_base_cmn( 'sector' );

%linkage = {'CPI','NIS'};
% linkage = {'NIS' , 'UKP'};
% linkage = {'UKP', 'USD', 'EURO'}

%coupon_type = 'FIX';
% coupon_type = 'VAR';

%param.rdate = 734757; %uint32(datenum('2011-01-01'));
param.rdate = datenum('2011-01-01'); %Note that this is the final available current date
%param.length = {[] []};
%param.ytm_bruto = {[] []};
%param.mod_duration = {[] []};
%param.turnover = {[] []};

%param.maalot = {'NR', 'AAA'};
%param.midroog = {'NR', 'Aaa'};
%param.log_opr = '';


%% Get static data

GetStaticData

%%limit list so that there is a single mentioning of each bond
ds = ds (ds.ytm_bruto <1,:);
[dummy i]=unique(ds.isin);
dsuFinal = ds(i,:);

isinuFinal = dsuFinal.isin;  % list of unique bonds, no repeats
NumOfBonds = length(isinuFinal);
%% Get Dynamic data

GetDynamicData

