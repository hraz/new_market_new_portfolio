function [Price_t_0 RetBreak ] = BondVal( nsin, t_0, t_f )
%calculates total value given back to customer for given Bond list, given starting time
%of investment t_0 and time of sell, t_f
%
% 
% Input:
%  nsin = a vector of nsin for bonds
% 
% t_0 = time at which investment is to begin
% 
% t_f = time at which to sell bonds
% 
% 
% Output:
% TotVal = vector of total value for each bond in list including its coupon payments and possibly
% its final payment or if date hasn't reached, then the sell of the bond at the current price
% 
% Notes: 
% 
% At this point, the originl prices paid for the bonds are not discounted.  That is, this function 
% gives total money returned but doesn't discount for expenses.  In addition, the yield right now
% is calculated according to the initial yield at t_0.  
import utility.*;
import utility.fts.*;
import dal.market.get.static.*;
import dal.market.get.cash_flow.*;
import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import dal.market.filter.static.*;
import dml.*;

t_0 = datenum(t_0); 
t_f = datenum(t_f);

%num_red_bonds =1; %will count number of bonds that go to redemption - i.e. expire

num_nsin = length(nsin); %number of nsins that are to be calculated for

%% Bonds' maturities
data_list={'last_trading_date'};
BondData = get_bond_stat( nsin, t_0, data_list );
%     BondData = get_bond_stat(nsin) ; %get bond data - in particular bond maturity date
BondMaturity= datenum(BondData.last_trading_date);%store maturities of all bonds
%ttmbonds = datenum(BondMaturity) - t_0; %time left till bond maturity

%% Get cash flow for all bonds
%  [ coupon_income, redemption_income ] = get_cash_flow_income(t_0, t_f, nsin);
%  coupon_nsin =  strid2numid(tsnames(coupon_income));%get list of nsins that was returned from coupon_income
% [ coup_indic_in_nsin, indx_coup_id_in_nsin ] = ismember(nsin, coupon_nsin); %get nsins which have coupon return
% num_coup_ids = sum(coup_indic_in_nsin); %number of bonds with coupons
% redemption_nsin =  strid2numid(tsnames(redemption_income));%get list of nsins that was returned from coupon_income
% [ red_indic_in_nsin, indx_red_id_in_nsin ] = ismember(nsin, redemption_nsin); %get nsins which have coupon return 
% red_bonds_indx = find(red_indic_in_nsin > 0); %index of bonds yielding coupons.
% red_mat  = fts2mat(redemption_income); 
% no_nan =  isnan(red_mat);
% red_mat(no_nan) = 0;
% CoupDates =   coupon_income.dates; %dates on which coupons are paid
% coup_flow =  [CoupDates fts2mat(coupon_income)]; %matrix with first column coupon dates, second column amount given
% red_dates = redemption_income.dates;
% red_flow = [red_dates red_mat]

[cash_fts] = get_cash_flow(t_0, t_f, nsin); %get cash flow, including redemptions
ids = tsnames(cash_fts);

nsin_ids = strid2numid(ids);

[ coups_nsins, indx_coups_nsins ] = ismember(nsin, nsin_ids); %get nsins which have coupon return 
num_nsin_coups = sum(coups_nsins);


CoupDates = cash_fts.dates; %get coupon dates
coup_flow = [CoupDates fts2mat(cash_fts)];

TotVal = zeros(num_nsin, 1); %total value of all coupons including interest
CFMoney = zeros(num_nsin, 1); %will count cash flow money
IIRR = zeros(num_nsin, 1); %will count interim interior rate of return (cash flow plus interest from cash flow)
IntPaid = zeros(num_nsin, 1); %will count money earned purley via interest
MakInt = zeros(num_nsin, 1); %interest paid of buying makam
NAV = zeros(num_nsin, 1); %money made from selling of bonds (capital gains)

num_coup_bonds = 0; %count for number of bonds carrying coupons

  data_name = {'ytm', 'price_dirty'};   
  tbl_name = 'bond';
 [ fts_ytm ] = get_multi_dyn_between_dates(nsin, data_name, tbl_name , t_0, t_f );
%  
%      data_name = 'price_dirty'; %get price of bond
%  [ fts_price ] = get_bond_dyn_single_date( nsin, data_name , t_f );
 ids_fts_price = tsnames(fts_ytm.price_dirty);
 ids_price = strid2numid(ids_fts_price);
[ price_nsins, indx_price_nsins ] = ismember(nsin, ids_price); %get nsins which have coupon return 

interest_period = [(1:1:11)/12, 1, 2, 3, 5, 7, 10];

[ riskless_fts ] = da_nominal_yield_between_dates( interest_period, t_0, t_f ); %get interest rates for different gov bonds 
riskless_periods = tsnames(riskless_fts);
riskless_dates = riskless_fts.dates;
riskless_int = fts2mat(riskless_fts);
riskless_mat = [riskless_dates riskless_int]; %matrix of dates and riskless interest on those dates

for k = 1:1:num_nsin
    ytm_vec = [];
    if coups_nsins(k)

         num_coup_bonds = num_coup_bonds + 1;
        bond_num =k;
% if coup_indic_in_nsin(k) %indicator for whether bond has coupons or not
 ytm_vec = fts_ytm.ytm.(ids(num_coup_bonds));%ytms for relevant bond
 
   
    
[nan_ind1 nan_ind2] = find(~isnan(coup_flow(:, num_coup_bonds+1) ) ); %get coupon flow for _th bond

coup_flow_new = coup_flow(nan_ind1, :); %cash flow dates for given bond with no NaN's

num_coupons = size(coup_flow_new); %number of coupons paid for a given bond
num_coupons = num_coupons(1);



   %get bond yield from time t_0 to time t_f
% BondYield = fts2mat(fts_ytm); %currently bond yield is determined according to t_0 could be equally determined for any given time
 
for coup=1:1:num_coupons

    if coup_flow_new(coup, 1) <= t_f %as long as we are before final time 
       
        date = datestr(coup_flow_new(coup, 1), 'yyyy-mm-dd'); %convert to right date when ytm should be calculated
        
        if  BondMaturity(bond_num)  - coup_flow_new(coup, 1)< 10 %very close to end of bond, assume no yield for additional coupon/fund given 
            BondYield = 0;
        else
        i = 0;
        
    
          if datenum(date) <ytm_vec.dates(end)
           while isempty(ytm_vec(date)) %in case coupon comes in on date that TASE is not open, take nearest next date
            i = i +1;
            date = datestr(coup_flow_new(coup, 1) +i, 'yyyy-mm-dd');
            
            if date> ytm_vec.dates(end)
                date = ytm_vec.dates(end);%in case there are no more dates, take last available ytm
                break;
            end
        end
        
        else
             date = datestr(ytm_vec.dates(end), 'yyyy-mm-dd');%in case there are no more dates, take last available ytm
        end
            
        BondYield = fts2mat(ytm_vec(date)); %get yield on date coupon was given
        end
    CFMoney(bond_num) =  CFMoney(bond_num) + coup_flow_new(coup, num_coup_bonds+1);
    IIRR(bond_num) = IIRR(bond_num) + coup_flow_new(coup, num_coup_bonds+1)*(1 + BondYield)^( (t_f - coup_flow_new(coup, 1) )/365 );
    IntPaid(bond_num) = IntPaid(bond_num) + (coup_flow_new(coup, num_coup_bonds+1)*(1 + BondYield)^( (t_f - coup_flow_new(coup, 1) )/365 ) -coup_flow_new(coup, num_coup_bonds+1) );
    
    TotVal(k) = TotVal(k) + coup_flow_new(coup, num_coup_bonds+1)*(1 + BondYield)^( (BondMaturity(bond_num) - coup_flow_new(coup, 1) )/365 );%add to total the value from the coupon at the interest from the yield
    end
    
end
Price = 0; %zero out variable
if t_f <= BondMaturity(bond_num) %as long as we are not at bond maturity, sell bond

 
     if ~price_nsins(bond_num) %loop hole in case no price exists
         Price = 0;
         nsin(k)
     else
          Price_vec = fts_ytm.price_dirty.(ids_fts_price(bond_num));
           Price = fts2mat(Price_vec(end));
if isnan(Price) %bond died - default of some sort
    Price = 0;
           fprintf('Bond  %u has no price, defaulted? \n',ids_price(bond_num))

end

    end
 
 TotVal(bond_num) = TotVal(bond_num) + Price; %sell bond at time t_f
 NAV(bond_num) = Price;
 
%end

% if red_indic_in_nsin(k) %indicator for a bond that has a redemption
% num_red_bonds = num_red_bonds + 1;
% 
%     [nan_ind1 nan_ind2] = find(~isnan(red_flow(:, num_red_bonds+1) ) ); %get coupon flow for _th bond
% 
%  
%     red_payments = sum(red_mat(:, num_red_bonds));
%     
%     bond_num = indx_red_id_in_nsin(k);
% end
end

if t_f > BondMaturity(bond_num)
%     
% 
%        principle = red_payments;
%        
  time = (t_f - BondMaturity(bond_num))/365;
  
  poss_bonds = time<interest_period;
  indx_time_period = min(find(poss_bonds==1));
poss_time = BondMaturity(bond_num)<riskless_mat(:, 1);
  poss_date = min(find(poss_time==1));
  
  non_risk_yield = riskless_mat(poss_date, indx_time_period +1);
  
  if isnan(non_risk_yield) %incase date falls on time when gov bond is not in existence, take nearest yield
      riskless_vec = riskless_mat(:, indx_time_period +1);
       riskless_vec = riskless_vec(1:poss_date);
      vec_risk =riskless_vec(~isnan(riskless_vec));
      non_risk_yield = vec_risk(end);
  end
  
      
      %  
% 
%  TotVal(bond_num) = IIRR(bond_num) + principle; %as we didn't sell bond (it expired)
%  
% %  while time > 0
% %   
% %      % Get makam information in case bonds mature before t_f 
% %      
% %  in such a case, money will be invested in yield of makam equivalent to remaining time
% % filter_name = 'type';
% % filter_cond = {'Convertable'};
% % filter_cond = {'Short Term Treasury Bill'};
% % 
% % if floor(time/365) > 0
% %     t = 365; %if more than one year remains, jump one year ahead
% % else
% %     t = time; %if less than one year remains, take the time remaining
% % end
% % 
% % [ nsin_makam ] = bond_static_partial( filter_name, filter_cond, BondMaturity(bond_num)+t);
% % 
% % nsin_makam_order = sort(nsin_makam); %get ttm's for all nsins, then sort
% % 
% % data_name = 'ttm';
% %  [ makam_ttm_fts ] = get_bond_dyn_single_date( nsin_makam_order , data_name , BondMaturity(bond_num)+t);
% %  makam_ttm = fts2mat(makam_ttm_fts);
% %  makam_ttm = sort(makam_ttm);
% %  
% % Ind_ttm = find(makam_ttm <=t/365); %find ttm closest but not larger than time remaining, so if time remaining is > 1 year, 
% % buy makam that's closest to year and re-enter loop
% %  
% % %%% need to add loop for time left greater than 1 year
% %  
% % [~, ind_max] = max(makam_ttm(Ind_ttm)); %found closest ttm
% % 
% % data_name = 'ytm'; %get ytm for relevant makam
% % if ~isempty(ind_max)
% %  [ makam_ytm_fts ] = get_bond_dyn_single_date(  nsin_makam_order(ind_max), data_name , BondMaturity(bond_num)+t );
% %  makam_ytm = fts2mat(makam_ytm_fts);
% % else
% %     makam_ytm = 0; %not enough time left to actually buy a makam
% %     ind_max = 1;
% %     makam_ttm(ind_max) = 0;
% % end
% % 
% % % calculate money made so far from bond in interest from corresponding makam

TotVal(bond_num) = TotVal(bond_num)*(1+ non_risk_yield)^(time); 
 
 %time = time - 365;
 end
 
 MakInt(bond_num) = TotVal(bond_num) - IIRR(bond_num); %money earned soley from makam
NAV(bond_num) = 100; %assume full payment of fund at maturity
end
    

    end
    Price_t_0 = fts2mat(fts_ytm.price_dirty(1));
RetBreak = [TotVal CFMoney IIRR IntPaid MakInt NAV];

end

    
    


