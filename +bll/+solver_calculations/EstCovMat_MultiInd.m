function  handles = EstCovMat_MultiInd(handles)
% Estimate the covaraince matrix according to the multi index model. The
% chosen indices are: 
%% find the first index - Galil yield
import dal.market.filter.total.*;
import dal.market.get.dynamic.*;
import gui.*;
DynParam.ytm = {nan nan};           %just put nan in ytm filed, in order to have something in thy dynamic params
nsin_Galil = bond_ttl(handles.DateNum, handles.TypeFilterParams{6}, DynParam );
Durations = fts2mat(get_bond_dyn_single_date( nsin_Galil, 'mod_dur', handles.DateNum )); %take the duration of all Galils - since we want to find the longest Galil
[min_val min_loc]=min(abs(Durations-10));               %find the bond with duration as close as possible to 10 years
nsin_Galil10= nsin_Galil(min_loc);   %this is the longest Galil - the closest to 10 years                                                                                          %this is the first index
data_name = {'price_yld','ytm'};
GalilFts = get_multi_dyn( nsin_Galil10 , data_name , 'bond', handles.DateNum, handles.HistLen );
GalilPriceYld=fts2mat(GalilFts.price_yld);
GalilYTM=fts2mat(GalilFts.ytm);
GalilYTM_last=GalilYTM(end);
IndexReturns(:,1)=GalilPriceYld;                                
IndexYTM(1,1) = GalilYTM_last + handles.ExpInflation;
%% find the second multi index - Tel Bond 20
nsin = [707];  %Tel bond 20
tbl_name = 'index';
data_name = {'value_yld','ytm'};
TelBond20Fts = get_multi_dyn( nsin , data_name , tbl_name, handles.DateNum, handles.HistLen);
TelBond20PriceYld=fts2mat(TelBond20Fts.ytm);
TelBond20YTM=fts2mat(TelBond20Fts.value_yld);
TelBond20YTM=IndexYTM(end);
IndexReturns(:,2)=TelBond20PriceYld;
IndexYTM(1,2) = TelBond20YTM + handles.ExpInflation;
%% creat the third multi index - corp bonds, rank BBB+ or below (not including non ranked)
[IndexReturns(:,3) IndexYTM(1,3)] = CreatThirdMultiIndex(handles.HistLen,handles.DateNum,GalilYTM);
%calc index param - beta is a matrix (num of columns = num of multi indices) , and alpha is still a vector
[handles.alpha,handles.beta,handles.Sigma_ei,handles.MultiIndExpCovMat]=...
    CalcMultiIndexParams(handles.ReturnMat,handles.YTM,IndexReturns, IndexYTM, handles.Sigma_eiMethod,handles.HistLen);
handles.ExpCovMat=handles.MultiIndExpCovMat; %use the covariance matrix which was found using the single index model
handles.MultIndVar=diag(handles.MultiIndExpCovMat); %variances of the bonds, according to single index estimation