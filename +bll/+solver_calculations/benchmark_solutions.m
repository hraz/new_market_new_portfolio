function [WantedPortReturns] = benchmark_solutions(PortReturn,PortRisk, benchmark)
%BENCHMARK_SOLUTIONS calculates target returns based on benchmark
%
% [WantedPortReturns] = benchmark_solutions(PortReturn,PortRisk, benchmark)
% - function calculates 5 ytm based target points on the efficient frontier
% based on the benchmark ytm (lowest point) and the point on the frontier
% with the same risk as the benchmark (highest point).
%
%   Input:  PortReturn - Double - array of expected portfolio returns
%
%           PortRisk - Double - array of expected portfolio risks
%
%           benchmark - structure - two fields, YTM - Double - benchmark
%                                                   YTM
%                                               Risk - Double - benchmark
%                                                   risk
%
%   Output: WantedPortReturns - 5 X 1 Double - target YTMs to be solved for
%
%   See also:
%       port_error
%
% Hillel Raz, June 19th, Day.after.Peres'.bday.bash
% Copyright 2013, Bond IT Ltd

import bll.solver_calculations.*;
%% Get corresponding ytm for benchmark
if isempty(PortReturn)
    WantedPortReturns = [];
elseif benchmark.Risk < PortRisk(1)
            error('benchmark_solutions:port_error', 'Risk of benchmark is lower than risk of efficient frontier');
else
    [ytmOutput] = calc_ytm_from_efficient_frontier_and_risk(PortReturn, PortRisk, benchmark.Risk);
    
    %% Get wanted YTM's for benchmark
    
    levelsYTM = (ytmOutput - benchmark.YTM)/4;
    
    %% Solve for each YTM
    WantedPortReturns = zeros(5, 1);
    for i=1:5
        WantedPortReturns(i) = benchmark.YTM +(i-1)*levelsYTM;
    end    
    
end