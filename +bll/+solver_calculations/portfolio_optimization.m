function [PortRisk, PortReturn, PortWts, error, warning, solverOutputDs] = portfolio_optimization(ExpReturn, ExpCovariance, ...
    NumPorts, PortReturn, ConSet, InitWeight, AddTCconstraints, benchmark, varargin) %dummy is intended to make the number of inputs even, as matlab requires in one of his input validation checks
%PORTOPT Portfolios on constrained efficient frontier.
%	Returns portfolios on the mean-variance efficient frontier given asset properties and a user
%	specified set of portfolio constraints (ConSet). Among a collection of NASSETS risky assets,
%	computes a portfolio of asset investment weights which minimize the risk for given values of the
%	expected return.  The portfolio weights satisfy constraints specified in linear inequality
%	equations. See PORTCONS for the generation of portfolio constraints.
%
%		[PortRisk, PortReturn, PortWts] = portopt(ExpReturn, ExpCovariance, ...
%			NumPorts, PortReturn, ConSet, varargin)
%
% Inputs:
%	ExpReturn is a 1xNASSETS vector specifying the expected (mean) return of each asset.
%
%	ExpCovariance is an NASSETSxNASSETS matrix specifying the covariance of the asset returns.
%	ExpCovariance must be symmetric with no negative eigenvalues (positive semi-definite).
%
%	Either NumPorts or PortReturn specifies the set of efficient portfolios computed.
%
%	NumPorts is the number of portfolios generated along the efficient frontier when specific
%	portfolio return values are not requested. The default is 10 portfolios equally spaced between
%	the minimum risk point and the maximum possible return.  Enter NumPorts as an empty matrix [],
%	when specifying PortReturn.
%
%	PortReturn is a vector of length NPORTS containing the target return values along the frontier.
%	If PortReturn is not entered or is empty, NumPorts equally spaced returns between the minimum
%	and maximum possible values will be used.
%
%	ConSet is a matrix of constraints for a portfolio of asset investments.  An eligible 1 by
%	NASSETS vector of asset allocation weights, PortWts, satisfies the inequalities A*PortWts' <= b,
%	where A = ConSet(:,1:end-1) and b = ConSet(:,end).  ConSet should include at least an equation
%	bounding the total value of the portfolio below.
%
%	See PORTCONS for a list of portfolio constraint types and their
%	corresponding financial
%	parameters.
%
%	If the variable ConSet is not specified, a default constraint set will be used.  The default
%	constraints scale the total value of the portfolio to 1, and place the minimum weight of every
%	asset at 0 to prevent short-selling.
% 
%   benchmark - DOUBLE - if non-0, then specifies the benchmark YTM and
%      that the algorithm will solve for similar portfolios.
%
%	Optional input arguments include the following parameter-value pairs.
%	'algorithm'	Either 'lcprog' or 'quadprog' to indicate algorithm to use (default is 'lcprog').
%	'maxiter'	Maximum number of iterations before termination of algorithm (default is 100000).
%	'tiebreak'	Applies to LCP agorithm only. Method to break ties for pivot selection (default is
%				'first'). Options are:
%				'first'		Select pivot with lowest index.
%				'last'		Select pivot with highest index.
%				'random'	Select a pivot at random.
%	'tolcon'	Applies to LCP algorithm only. Tolerance for constraint violations (default is
%				1.0e-6).
%	'tolpiv'	Applies to LCP algorithm only., Pivot value below which a number is considered to be
%				zero (default is 1.0e-9).
%
% Outputs:
%	PortRisk is an NPORTSx1 vector of the standard deviation of return for each portfolio.
%
%	PortReturn is an NPORTSx1 vector of the expected return of each portfolio.
%
%	PortWts is an NPORTSxNASSETS matrix of weights allocated to each asset. Each row represents a
%	different portfolio.
%
% Notes:
%	A plot of the efficient frontier is returned if the function is invoked without output
%	arguments.
%
% See also PORTCONS, PORTSTATS, EWSTATS, FRONTCON

%
% Copyright 1995-2008 The MathWorks, Inc.
% $Revision: 1.23.2.9 $   $ Date: $

%----------------------------------------------------------------
% Input argument validation
%----------------------------------------------------------------
% Check for input errors

import bll.solver_calculations.*;
error=[];
warning=[];
if (nargin < 2)
    error='portfolio_optimization:portopt:missingInputs. You must enter ExpReturn and ExpCovariance.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end
import simulations.scripts.*;

% Make sure that the number of returns entered matches the number of rows/columns in the
% covariance matrix (which represents the number of assets).

ExpReturn = ExpReturn(:);
NASSETS = length(ExpReturn);

[covRows, covCols] = size(ExpCovariance);
if(covRows ~= covCols)
    error='portfolio_optimization:portopt:invalidCovMatrix. The covariance matrix must be NxN, where N = number of assets';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Make sure covariance is positive-semidefinite and symmetric

if norm(ExpCovariance - ExpCovariance',inf) > eps
    warning='portfolio_optimization:portopt:asymmetricCovariance. Non-symmetric covariance matrix will be made symmetric.';
    ExpCovariance = 0.5*(ExpCovariance + ExpCovariance');
end
[L, D] = ldl(ExpCovariance); %#ok
if any(diag(D) < 0)
    warning='portfolio_optimization:portopt:nonposdefCovMatrix. Non-positive-semidefinite covariance input.';
end
clear L
clear D

% Make sure problem is conformable

if size(ExpCovariance, 1) ~= NASSETS
    error='portfolio_optimization:portopt:invalidExpReturns. The number of expected returns does not equal the number of assets.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% process name-value pairs (if any)

if nargin > 7
    if mod(nargin-6,2) ~= 0
        error='portfolio_optimization:portopt:InvalidNameValuePair. Invalid (odd number) of optional name-value pairs.';
        PortRisk=[];
        PortReturn=[];
        PortWts=[];
        return
    end
    
    names = { 'algorithm', 'maxiter', 'tiebreak', 'tolcon', 'tolpiv' };			% names
    values = { 'lcprog', 100000, 'first', 1.0e-6, 1.0e-9 };						% default values
    try
        [algorithm, maxiter, tiebreak, tolcon, tolpiv] = parsepvpairs(names, values, varargin{:});
        
    catch E
        E.throw
    end
else
    algorithm = 'lcprog';
    maxiter = 100000;
    tiebreak = 'first';
    tolcon = 1.0e-6;
    tolpiv = 1.0e-9;
end

% check arguments

if ~isempty(strmatch(lower(algorithm),'lcprog'))
    alg = 1;
elseif ~isempty(strmatch(lower(algorithm),{'qp','quadprog'}))
    alg = 2;
else
    error='portfolio_optimization:portopt:InvalidAlgorithm. Invalid algorithm selected. Choices are ''lcp'',''qp''.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if isempty(strmatch(tiebreak,{'default','first','last','random'}))
    error='portfolio_optimization:portopt:InvalidTieBreak. Invalid choice to break ties in pivot selection. Choices are ''first'',''last'',''random''.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if tolcon < 2*eps
    error='portfolio_optimization:portopt:InvalidTolerance. Unrealistically small tolerance (tolcon) specified.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if tolpiv < 2*eps
    error='portfolio_optimization:portopt:InvalidTolerance. Unrealistically small tolerance (tolpiv) specified.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if maxiter <= 0
    error='portfolio_optimization:portopt:NonPositiveInteger. Maximum number of iterations (maxiter) must be a positive integer.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Determine which optional arguments were entered as non-empty.

if (nargin < 3 || isempty(NumPorts))
    NumPortsEntered = 0;
else
    NumPortsEntered = 1;
    NumPorts = double(NumPorts);
end

if (nargin < 5 || isempty(PortReturn))
    PortReturnEntered = 0;
else
    PortReturnEntered = 1;
end

% When entering the target rate of return (PortReturn), enter NumPorts as an empty matrix.

if (NumPortsEntered == 1) && (PortReturnEntered == 1)
    error='portfolio_optimization:portopt:emptyNumPorts. When entering the target rate of return, PortReturn, enter NumPorts as an empty matrix.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% if 'default' problem, set a flag to trim negative weights to 0 (rmt4)
if (nargin < 6) || isempty(ConSet)
    % A default constraint matrix ConSet will be created if this is not entered.
    ConSet = portcons('default', NASSETS);
    TrimWeights = true;
else
    TrimWeights = false;
end

%----------------------------------------------------------------
% Constraint array construction
%----------------------------------------------------------------
% A = ConSet(:,1:end-1);
% b = ConSet(:,end);
%
% % Call the function EQPARSE in order to find equality equations implied
% % by the linear inequalities A* Wts <= b.
% % The first Neq rows of Aset and Bset will be interpreted as equalitiy
% % equations
% [Aset, Bset, Neq]= eqparse(A,b);
%
% Aeq = []; Beq = [];
% if(Neq > 0)
%    Aeq  = Aset(1:Neq, :);        %equalities constraints
%    Beq  = Bset(1:Neq, :);        %equalities constraints
% end
%
% Aineq = []; Bineq = [];
% if(Neq < size(Aset,1))
%    Aineq = Aset(Neq+1:end, :);   %inequalities constraints
%    Bineq = Bset(Neq+1:end, :);   %inequalities constraints
% end

% Call INEQPARSE to find inequalites, equalites, and bounds implied by
% the linear inequalities in ConSet.

[Aineq, Bineq, Aeq, Beq, LB, UB] = ineqparse(ConSet);

if isempty(LB) || any(~isfinite(LB)) || (~isempty(UB) && any(~isfinite(UB)))
    warning='portfolio_optimization:portopt:switchQPsolver. Unbounded bound constraints. Cannot use lcprog. Switching to quadprog.';
    alg = 2;
end

% If PortReturn has been entered, check them for legality relative to
% constraints. If it has not been entered, construct the default
% return range. Used only by LINPROG and QUADPROG and does not matter if infeasible.
if isempty(InitWeight)  %this is also done in the function that called portoptError. Can be removed from here but is kept for backward compatiable
    W0 = ones(NASSETS, 1)/NASSETS;
else
    W0 = InitWeight;
end

%----------------------------------------------------------------
% Maximum return calculation
%----------------------------------------------------------------

% Find the maximum expected return achievable, given the individual asset
% expected returns and all the other constraints.

% Set the options: ('LargeScale' mode is turned off because it can cause
%                   some warnings to be thrown.)  Used only by LINPROG and QUADPROG below.

F = zeros(NASSETS, 1);

options = optimset('Display','iter','LargeScale','off','MaxIter',maxiter);
sim_num_save = 123456; % Simply given a value for now, later on in simulation_function updated to actual sim_num_save

Output1=[];
ErrorFlag1=-10;
solverOutputDs = dataset({[0],'sim_num_save'},{[0],'call_num'},{[0],'error_flag'},{[0],'iterations'},{[0],'constr_violation'},...
            {cellstr('a'),'algorithm'},{[0],'cg_iterations'},{cellstr('a'),'msg'},...
            {[0],'first_order_opt'},{[0],'first_excess_duration'},{[0],'second_excess_duration'},...
            {[0],'infeasible_non_duration_const_start'},{[0],'infeasible_duration_const_start'},...
            {[0],'infeasible_non_duration_const_finish'},{[0],'infeasible_duration_const_finish'});
[MaxReturnWeights, Fval, ErrorFlag1, Output1] = linprog(-ExpReturn, Aineq, Bineq, Aeq, Beq, LB, UB, W0, options); %#ok
solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 1,ErrorFlag1,Output1,Aineq,Bineq, W0, MaxReturnWeights, options);
%% changed by Hillel - algo keeps running as it can find solutions even if max return weights has no solution.

% if ErrorFlag <= 0
%    error='portfolio_optimization:portopt:noPortfolios. No portfolios satisfy all the input constraints';
%    PortRisk=[];
%   PortReturn=[];
%   PortWts=[];
%   return
% end
mtxZeros = zeros(NASSETS, NASSETS);
Output2=[];
Output3=[];
ErrorFlag2=-10;
ErrorFlag3=-10;
if ErrorFlag1 < 0
     [MaxReturnWeights,  Fval, ErrorFlag2, Output2] = quadprog(mtxZeros, -ExpReturn, Aineq, Bineq, Aeq, Beq, LB, UB, ...
            W0, options); %#ok
     solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 2, ErrorFlag2, Output2, Aineq, Bineq, W0, MaxReturnWeights, options);
end

if ErrorFlag1 < 0 && ErrorFlag2 < 0
       [MaxReturnWeights,  Fval, ErrorFlag3] = qplcprog(mtxZeros, -ExpReturn, Aineq, Bineq, Aeq, Beq, LB, UB, ...
            'maxiter', maxiter, 'tiebreak', tiebreak, 'tolcon', tolcon, 'tolpiv', tolpiv); %#ok
        solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 3, ErrorFlag3, Output3, Aineq, Bineq, F, MaxReturnWeights, options); %no init point in this solver
end
if ~(ErrorFlag3==-10) && (ErrorFlag3<= 0)
    MaxReturnWeights = F;
end
    
MaxReturn = MaxReturnWeights'*ExpReturn;

% Find the minimum variance return.

port_max_duration_changed = 0; %will measure number of times portfolio max/min duration is changed

flag_max = 1;%when this is on, will change the max portfolio duration, when of, the min.

ErrorFlag4 = -10; %set ErrorFlag negative, keep running loop till find solution
ErrorFlag5 = -10;
ErrorFlag6 = -10;
Output4=[];
Output5=[];
Output6=[];
while (ErrorFlag4) <0 && (ErrorFlag5<=0) && (ErrorFlag6<=0) && port_max_duration_changed < 3
    
    if alg == 1        
        [MinVarWeights,  Fval, ErrorFlag4] = qplcprog(ExpCovariance, F, Aineq, Bineq, Aeq, Beq, LB, UB, ...
            'maxiter', maxiter, 'tiebreak', tiebreak, 'tolcon', tolcon, 'tolpiv', tolpiv); %#ok
        solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 4, ErrorFlag4, Output4, Aineq, Bineq, F, MinVarWeights, options); %no init point in this solver
    end    
    if alg~=1 || ErrorFlag4< 0        
        [MinVarWeights,  Fval, ErrorFlag5, Output5] = quadprog(ExpCovariance, F, Aineq, Bineq, Aeq, Beq, LB, UB, ...
            W0, options); %#ok
        alg =1;
        solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 5, ErrorFlag5, Output5, Aineq, Bineq, W0, MinVarWeights, options); 
    end    
    if ErrorFlag4 <= 0 || (~(ErrorFlag5==-10) && ErrorFlag5<=0 )        
        if flag_max %haven't changed the max portfolio duration yet, or last changed the min portfolio duration
            Bsize = size(Bineq, 1);
            Bineq(Bsize -1, 1) = Bineq(Bsize -1, 1) +0.5; %increment max portfolio duration by 0.5 yrs.
            flag_max = 0;
        else %change the min portfolio duration
            Bsize = size(Bineq, 1);
            Bineq(Bsize, 1) = Bineq(Bsize, 1) +0.5; %reduce min portfolio duration by 0.5 yrs.
            flag_max = 1;
        end
        port_max_duration_changed = port_max_duration_changed + 1;
        [MaxReturnWeights, Fval, ErrorFlag6, Output6] = linprog(-ExpReturn, Aineq, Bineq, Aeq, Beq, LB, UB, W0, options); %#ok
        solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 6, ErrorFlag6, Output6, Aineq, Bineq, W0, MaxReturnWeights, options); 
        MaxReturn = MaxReturnWeights'*ExpReturn; %reconfigure max return according to new portfolio max/min        
    end
    
end

if (ErrorFlag4) <0 && (ErrorFlag5<=0) && (ErrorFlag6<=0)
    error='portfolio_optimization:portopt:noMinVarPortfolio. Algo did not find solution.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

MinVarReturn = MinVarWeights'*ExpReturn;

%----------------------------------------------------------------
% Calculate return corresponding to minimum risk (variance)
%----------------------------------------------------------------

if PortReturnEntered
    % check the requested returns against points on the frontier
    % use a small numerical fudge factor and assume returns are postive
    
    if (min(PortReturn) < MinVarReturn*(1 - 1000*eps))
        error=sprintf('portfolio_optimization:portopt:missingMinPortfolios. One or more requested returns are less than the return of the least risky portfolio');% Previously - MinVarReturn printed
        PortRisk=[];
        PortReturn=[];
        PortWts=[];
        return
    end
    if (max(PortReturn) > MaxReturn*(1 + 1000*eps))
        error=sprintf('portfolio_optimization:portopt:missingMaxPortfolios. One or more requested returns are greater than the maximum achievable return'); % Previously - MaxReturn printed
        PortRisk=[];
        PortReturn=[];
        PortWts=[];
        return
    end
    
    NumFrontPoints = length(PortReturn);
    PortfOptResults = zeros(NumFrontPoints, 2 + NASSETS);
    StartPoint = 1;
    EndPoint = NumFrontPoints;
else
    % If NumPorts is not entered, set a default value.
    
    if NumPortsEntered
        NumFrontPoints = NumPorts;
    else
        NumFrontPoints = 10;
    end
    
    MinVarStd = sqrt(MinVarWeights' * ExpCovariance * MinVarWeights);
    
    if MaxReturn > MinVarReturn+10^(-10)		% This is just the algorithm from MATLAB function, LINSPACE
        PortReturn = [MinVarReturn+(0:NumFrontPoints-2)*(MaxReturn-MinVarReturn)/(NumFrontPoints-1) MaxReturn];
    else
        PortReturn = MaxReturn;
        NumFrontPoints = 1;
    end
    
    PortfOptResults = zeros(NumFrontPoints, 2 + NASSETS);
    PortfOptResults(1, :) = [MinVarReturn MinVarStd MinVarWeights(:)'];
    StartPoint = 2;
    EndPoint = NumFrontPoints-1;
end

FrontPointConstraint = -ExpReturn';
Aeq = [FrontPointConstraint; Aeq ];					% Add a new equality constraint
Beq = [0; Beq];										% Add a new equality constraint

W0 = MaxReturnWeights;
Output7=[];
ErrorFlag7=-10;
counter=0;
for Point = StartPoint:EndPoint
    counter=counter+1;
    if AddTCconstraints                             %if Transaction costs constraint are included, the optimizaito problem is a little different, and this amendment guarantees that the solution will be complementry (i.e., either sell weight or buy weight is positive, but not both them)
        Aeq(1,end) = PortReturn(Point);
    else
        Beq(1) = -PortReturn(Point);
    end
    
    if alg == 1
        [Weights, Fval, ErrorFlag7 ] = qplcprog(ExpCovariance, F, Aineq, Bineq, Aeq, Beq, LB, UB, ...
            'maxiter', maxiter, 'tiebreak', tiebreak, 'tolcon', tolcon, 'tolpiv', tolpiv); %#ok
    solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 7+counter-1, ErrorFlag7, Output7, Aineq, Bineq, F, MaxReturnWeights, options); %no init point in this solver
    elseif alg ~= 1 || ErrorFlag7 < 0
        [Weights, Fval, ErrorFlag7, Output7] = quadprog(ExpCovariance, F, Aineq, Bineq, Aeq, Beq, LB, UB, ...
            W0, options); %#ok
        solverOutputDs = save_output_results(solverOutputDs, sim_num_save, 7+counter-1, ErrorFlag7, Output7, Aineq, Bineq, W0, MaxReturnWeights, options);
    end
    
    if ErrorFlag7 <= 0
        PortfOptResults(Point, :) = [Beq(2) nan*ones(1, NASSETS+1)];
    else
        Return = dot(Weights, ExpReturn);
        Std = sqrt(Weights'*ExpCovariance*Weights);
        PortfOptResults(Point, :) = [Return Std Weights(:)'];
    end
    
end

if ~PortReturnEntered
    % The last row corresponds to the point of maximum return, which has already been calculated
    Std = sqrt(MaxReturnWeights'*ExpCovariance*MaxReturnWeights);
    PortfOptResults(end, :) = [MaxReturn, Std, MaxReturnWeights(:)'];
end

%----------------------------------------------------------------
% Validate results and generate output
%----------------------------------------------------------------

ErrorIndex = find(isnan(PortfOptResults(:, 2)));
if ~isempty(ErrorIndex)
    %NumErrors = num2str(length(ErrorIndex));
    NumErrors = length(ErrorIndex);
    warning=sprintf('portfolio_optimization:portopt:InfeasibleProblem. A solution was not feasible for %d expected return(s).',NumErrors);
end

PortReturn = PortfOptResults(:, 1);
PortRisk = PortfOptResults(:, 2);
PortWts = PortfOptResults(:, 3:size(PortfOptResults, 2));

% if lower-bound weights are zero, force all negative weights to be zero
if TrimWeights
    PortWts(PortWts < 0) = 0;
end

%A plot of the efficient frontier is returned if the function is
%invoked without output arguments.

if nargout == 0
    FrontWin = figure;
    
    set(FrontWin,'NumberTitle','off');
    set(FrontWin,'Name','Efficient Frontier');
    set(FrontWin,'Resize','on');
    
    set(FrontWin,'Tag','FrontWin');
    
    plot(PortRisk, PortReturn);
    title('Mean-Variance-Efficient Frontier', 'Color', 'k');
    xlabel('Risk (Standard Deviation)');
    ylabel('Expected Return');
    grid on;
    
    % prevent any output
    clear PortRisk;
end

%----------------------------------------------------------------
% Auxiliary function(s)
%----------------------------------------------------------------

function [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, bin)
%INEQPARSE Find inequalities, equalities, and bounds implied by Ain*x <= bin.
%  Identifies equalities specified as Arow*x <= bval, -Arow*x <= -bval.
%  Parses out duplicate entries and all zero equations.
%  Finds bound constraints among inequalities.
%
%  [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, bin)
%  [A,b,Aeq,beq,LB,UB] = ineqparse([Ain, bin])
%
%  The function does not catch linear combinations of inequalities which
%  together imply an equality constraint.
%
%  See also PORTCONS.
%
%----------------------------------------------------------------------
%
% % Test the logic to parse out inequalites with single-entry equations
% % 1 : redundant equaltiy
% % 2 : equality
% % 3 : redundant upper bound
% % 4 : lower bound
% % 5 : upper bound
% Astart = [1 2 -1 1 3 -1 -2 -4 5 3]'
% Ain = full(sparse(1:length(Astart),abs(Astart),Astart))
% [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, zeros(length(Astart),1))
%
% % Catch rows which are multiples
% m = 1:length(Astart)
% Ain = diag(m)*Ain
% [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, zeros(length(Astart),1))
%
% % Degenerate case with equality, lower, upper bounds
% C = portcons('default',3,'AssetLims',0,[0.5 0.6 0.7],3)
% [A,b,Aeq,beq,LB,UB] = ineqparse(C)
%
% % Case with a general inequality constraint
% C = portcons('default',3,'AssetLims',0,[0.5 0.6 0.7],3, ...
%              'Custom',[0.1 0.2 0.3],0.40)
% [A,b,Aeq,beq,LB,UB] = ineqparse(C)
%

% find usage ineqparse(ConSet)
if nargin==1
    bin = Ain(:,end);
    Ain = Ain(:,1:end-1);
end

[NumEquations, NumVars] = size(Ain);
if any( size(bin)~=[NumEquations, 1] )
    error='portfolio_optimization:portopt:mismatchAandB. Dimensions of A and b are inconsistent';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Pull out degenerate rows
I = all(Ain==0,2);
if(any(I))
    warning='portfolio_optimization:portopt:ConstraintQual. Degenerate rows found in constraint matrix. Eliminating these constraints';
    Ain(I,:) = [];
    bin(I) = [];
end

% Constraint rows
ConRows = [Ain, bin];

% Form numerator and denominator dot products.
%
% row I and row J are the same direction when:
%   rowI*rowJ' == sqrt(rowI*rowI')*sqrt(rowJ*rowJ')
%        numIJ == denIJ
%
% row I and row J are the opposite direction when:
%   rowI*rowJ' == - sqrt(rowI*rowI')*sqrt(rowJ*rowJ')
%        numIJ == - denIJ

% square (rowI*rowJ') but keep the sign
numIJsqrt = ConRows*ConRows';
numIJ = sign(numIJsqrt).*(numIJsqrt.*numIJsqrt);

% form (rowI*rowI') times (rowJ*rowJ')
rowKdot = dot(ConRows, ConRows, 2);
[rowIdot, rowJdot] = meshgrid(rowKdot, rowKdot);
denIJ = rowIdot .* rowJdot;

% record which equations are negations or duplicates
% denIJ is always positive
% take the upper triangular part only
%
% isdupIJ [NumEqs x NumEqs] row I is a positive multiple of row J
% isnegIJ [NumEqs x NumEqs] row I is a negative multiple of row J
reltol = 1000*eps;
isdupIJ = ( denIJ*(1-reltol) <  numIJ ) & (  numIJ < denIJ*(1+reltol) );
isnegIJ = ( denIJ*(1-reltol) < -numIJ ) & ( -numIJ < denIJ*(1+reltol) );

isdupIJ = triu(isdupIJ, 1);
isnegIJ = triu(isnegIJ, 1);

% search through the equations and clean out equalities and duplicates.
% store the equalities separately.
%
% ConEqs  [NumEqs   x NumVars+1] : [Aeq, beq]
% ConRows [NumInEqs x NumVars+1] : [A, b]
ConEqs = zeros(0, NumVars+1);

i=1;
while (i < size(ConRows,1) )
    % find negations and duplicates of this row
    RowIsNeg = isnegIJ(i,:);
    RowIsDup = isdupIJ(i,:);
    
    % negations and duplicates should be removed from the inequality list
    IndRemove = RowIsNeg | RowIsDup;
    
    if any(RowIsNeg)
        % add the row to the equality list
        ConEqs = [ConEqs; ConRows(i,:)]; %#ok
        
        % remove the row from the inequality list along with negs and dups
        IndRemove(i) = 1;
    else
        % equation i has been left in
        i = i + 1;
    end
    
    % remove equations from the inequality list
    ConRows(IndRemove,:) = [];
    isnegIJ = isnegIJ(~IndRemove, ~IndRemove);
    isdupIJ = isdupIJ(~IndRemove, ~IndRemove);
end

% Break up into left and right hand sides
Aeq = ConEqs(:,1:NumVars);
beq = ConEqs(:,NumVars+1);
A = ConRows(:,1:NumVars);
b = ConRows(:,NumVars+1);

% search through the inequalities and find bounds
% SingleValue * x(Ind) <= b(Ind)
%
% IndSingle   [NumInEqs x 1] true if only 1 non-zero value in row of A
% SingleValue [NumInEqs x 1] only valid for IndSingle == 1
%
% VarNum       [NumInEqs x NumVars] column of each entry of A
% SingleVarNum [NumInEqs x 1] column of first non-zero entry in A
%
IndSingle   = sum(A~=0 , 2) == 1;
SingleValue = sum(A    , 2);

IndLower = IndSingle & ( SingleValue < 0 );
IndUpper = IndSingle & ( SingleValue > 0 );

VarNum = (1:NumVars);
VarNum = VarNum(ones(size(A,1),1),:);
VarNum(A==0) = Inf;
SingleVarNum = min(VarNum,[],2);

if any(IndLower)
    LB = -Inf*ones(NumVars,1);
    
    % find the variable and the bound value
    VarNum = SingleVarNum(IndLower);
    BVal = b(IndLower)./SingleValue(IndLower);
    
    % apply the most restrictive bound to each variable
    UniqVar = unique(VarNum);
    if length(UniqVar)==length(VarNum)
        % no variables have multiple parallel bounds
        LB(SingleVarNum(IndLower)) = BVal;
    else
        for Var=UniqVar(:)'
            LB(Var) = max( BVal( VarNum==Var ) );
        end
    end
    
else
    LB = [];
end

if any(IndUpper)
    UB = Inf*ones(NumVars,1);
    
    % find the variable and the bound value
    VarNum = SingleVarNum(IndUpper);
    BVal = b(IndUpper)./SingleValue(IndUpper);
    
    % apply the most restrictive bound to each variable
    UniqVar = unique(VarNum);
    if length(UniqVar)==length(VarNum)
        % no variables have multiple parallel bounds
        UB(SingleVarNum(IndUpper)) = BVal;
    else
        for Var=UniqVar(:)'
            UB(Var) = min( BVal( VarNum==Var ) );
        end
    end
    
else
    UB = [];
end

% remove lower or upper bound inequalities
A(IndSingle,:) = [];
b(IndSingle) = [];


% [EOF]
