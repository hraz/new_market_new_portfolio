function  [Sharpe InfoRatio ReturnMat]=CalcSharpeInfoReturn(price_clean_yld, HistLen, RisklessRate, IndexPriceYld)
%Calculate Shapre  and information ratio of all the bonds that passed the
%filters, using Matlab's built in functions.
import utility.data_conversions.*;

ReturnMat=price_clean_yld';
ReturnMat = ReturnMat(end-HistLen+1:end,:);
RisklessRateDay=Year2Day(RisklessRate);
Sharpe = sharpe(ReturnMat, RisklessRateDay)'; %calc the Sharpe ratio of each bond

%% Check if the history length of ReturnMat is the same as that of the index.
% If not, set the history length to be the minimum of these two values.
HistLenReturnMat = size(ReturnMat, 1);
HistLenIndex = size(IndexPriceYld, 1);
minimumHistoryLength = min(HistLenReturnMat, HistLenIndex);
if HistLenReturnMat > HistLenIndex
    InfoRatio = inforatio(ReturnMat(end-minimumHistoryLength+1:end,:), IndexPriceYld).';
elseif HistLenReturnMat < HistLenIndex
    InfoRatio = inforatio(ReturnMat, IndexPriceYld(end-minimumHistoryLength+1:end)).';
else
    InfoRatio = inforatio(ReturnMat, IndexPriceYld).';
end