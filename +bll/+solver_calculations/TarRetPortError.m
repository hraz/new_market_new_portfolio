function [PortDuration PortReturnTar PortRiskTar PortWts Wts PortReturnYear PortRiskYear MarkerPosition error warning ClosestErrorFlag]=TarRetPortError(YTM,ExpCovMat,NumPorts,ConSet,TarRet,FminConFlag, DurationVector, handles)
% Solves the targer return portfolio optimization probelm,
%and plots the efficient frontier. it returns the weights for all bonds
%that passed the filters, whose weighted average correspond to the targer return of the user
import gui.*;
TarRetDay=Year2Day(TarRet); %translate to daily terms, since the problems is solved in the day domain, and plotted in the annual domain, for convinience (since the Covariance matrix is in daily terms)
Daily_YTM=Year2Day(YTM);                                    %since the data is daily, and so does the covariance matrix, the YTM should also be daily
if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
    tmp = handles.MyBonds;                                  %MyBonds are the bonds in where am i mode
    if ~isempty(tmp)                                        %if we are in where am i mode, init the algorithm with the users weights
        for i=1:length(handles.MyBonds)
            ind(i)=find(extractfield(bond(BondScope),'ID')==handles.MyBonds(i).ID);
        end
        InitWeight(ind) = handles.MyWeights;                %init of weights according to users bonds
    else                                                    %if not in where am i mode
        InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N
    end   
        AddTCConstraint = 0;

    [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat, [], TarRetDay, ConSet, InitWeight, AddTCConstraint); %Optimization
else                                                        %if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    [PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFmincon(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds);                        %Optimization
end

if isempty(PortRisk)  || sum(isnan(PortRisk))>length(PortRisk)/2                                            %there there was no solution or more than half of points unfeasable, don't continue
    PortWts=[];
    RiskyWts=[];
    Wts = [];
    PortRiskYear=[];
    PortReturnYear=[];
    PortReturnOpt=[]; 
    PortRiskOpt = [];
    MarkerPosition = 1;
     TangPortRetYear =[];
     TangPortRiskYear=[];
     PortDuration =[];
     PortReturnTar=[];
     PortRiskTar =[];
     ClosestErrorFlag =[];
     if isempty(PortRisk)
        return
    end
     
  [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat,2, [], ConSet, InitWeight, AddTCConstraint); %Optimization
    PortReturnYear=Day2Year(PortReturn);                        %tranlate returns back to yearly returns, in order to plot in yearly terms
    PortRiskYear=PortRisk*sqrt(365);
    if PortReturn(2)<TarRetDay
        ReqInd=2;
    else
        ReqInd=1;
    end
    ClosestErrorFlag=0;
%     error=102;
else
    ReqInd=1;
    ClosestErrorFlag=0;
end
%% Find out if there were unfeasible points, which are charachterizes by a NaN in their PortRisk, and a PortReturn or one. exclude these points in order to plot what was succesfful, and tell the user that we are showing the closest point to his requst
SolutionPoints = (~isnan(PortRisk));                        %indices of points which had a solution
PortReturn=PortReturn(SolutionPoints);                      %take only the returns of points with a solution
PortRisk=PortRisk(SolutionPoints);                          %take only the risk of points with a solution
PortWts=PortWts(SolutionPoints,:);                          %take only the weights of points with a solution
PortReturnYear=Day2Year(PortReturn);                        %tranlate returns back to yearly returns, in order to plot in yearly terms
PortRiskYear=PortRisk*sqrt(365);                            %translate the risk to annual terms
hold on
plot(PortRiskYear,PortReturnYear*100,'LineWidth',2);        %plot the efficient frontier
hold on
[TarRetVec ReqInd ClosestErrorFlag]=FindClosestTarRet(PortReturn,TarRetDay,NumPorts);           %finds the index in the PortReturn vector, which is closes to the weekly return requested by the user. ReqInd is the required index of the solution, i.e., the index of the solution vector which is closest to the the user request of YTM                           
ReqInd = 1; ClosestErrorFlag = 0;
plot([0 PortRiskYear(ReqInd)],PortReturnYear(ReqInd)*100*ones(1,2),'r--','LineWidth',2)          %plot horizontal red dotted line
plot(PortRiskYear(ReqInd),PortReturnYear(ReqInd)*100,'r*','markersize',16)                       %plot a red star
xlim([PortRiskYear(1)*0.8 PortRiskYear(end)*1.2])
%DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope                         
PortDuration=PortWts(ReqInd,:)*DurationVector;                  %calculate the weighted average of the years to maturity
tit=sprintf('Return: %1.2f%%, Risk: %1.3f, Duration: %1.2f years',PortReturnYear(ReqInd)*100,PortRiskYear(ReqInd),PortDuration);
PortReturnTar =PortReturnYear(ReqInd);
PortRiskTar = PortRiskYear(ReqInd);
AddFigNames(tit,'\sigma','Annual Return [%]',1)
Wts=PortWts(ReqInd,:)';                                     %these are the weights which give us the required target return
MarkerPosition = ReqInd;                                    %this is also the marker position in the upper left bar of the plot
