function [RiskyReturn RiskyRisk RiskyWts error] = portallocError(PortRisk, PortReturn, PortWts, RisklessRate)
%PORTALLOC Capital allocation to efficient frontier portfolios.
%   Given user-specified standard deviations, expected returns, and weights of
%   NPORTS efficient frontier portfolios of risky assets, and the risk-free
%   rate, calculate the optimal risky portfolio and the optimal allocation of
%   funds between that risky portfolio of NASSETS and the risk-free asset.
%
%   [RiskyRisk, RiskyReturn, RiskyWts, RiskyFraction, OverallRisk, ...
%     OverallReturn] = portalloc(PortRisk, PortReturn, PortWts, RisklessRate)
%
%   [RiskyRisk, RiskyReturn, RiskyWts, RiskyFraction, OverallRisk, ...
%     OverallReturn] = portalloc(PortRisk, PortReturn, PortWts, ...
%     RisklessRate, BorrowRate, RiskAversion)
%
% Optional Inputs: BorrowRate, RiskAversion
%
% Inputs:
%   PortRisk - NPORTS by 1 vector of risky asset efficient frontier portfolio
%     standard deviations.
%
%   PortReturn - NPORTS by 1 vector of risky asset efficient frontier portfolio
%     expected returns.
%
%   PortWts - NPORTS by NASSETS matrix of weights allocated to each asset. Each
%     row represents an efficient frontier portfolio of risky assets. The sum
%     of all weights of each portfolio is 1 (sum-to-one budget constraint).
%
%   RisklessRate - Risk free lending rate, expressed as a scalar decimal.
%
% Optional Inputs:
%   
%   BorrowRate - Borrowing rate, expressed as a scalar decimal. If borrowing is
%     not desired, or not an option, set this to NaN. The default is NaN.
%
%   RiskAversion - Scalar coefficient of investor degree of risk aversion
%     applied to a quadratic utility function. The higher this number, the more
%     averse an investor is to risk. Typical risk aversion coefficient range
%     between 2 and 4. The default is 3.
%
% Outputs:
%   RiskyRisk - Standard deviation of the optimal risky portfolio.
%
%   RiskyReturn - Expected return of the optimal risky portfolio.
%
%   RiskyWts - 1 by NASSETS vector of weights allocated to the assets in the
%     optimal risky portfolio. The total of all weights in the portfolio is 1.
%
%   RiskyFraction - Fraction of the complete portfolio (i.e., the overall
%     portfolio including risky and risk-free assets) allocated to the risky
%     portfolio.
%
%   OverallRisk - Standard deviation of the optimal overall portfolio.
%
%   OverallReturn - Expected rate of return of the optimal overall portfolio.
%
% Note:
%   If invoked without any output arguments, a graph of the optimal capital
%   allocation decision is displayed.
%
% See also PORTOPT, FRONTCON, PORTSTATS, EWSTATS
%
% Reference: Bodie, Kane, and Marcus, Investments, 2nd Ed., Chapters 6 & 7.

%  Author(s): M. Reyes-Kattar, 02/15/98
%  Copyright 1995-2003 The MathWorks, Inc.
%  $Revision: 1.13.2.4 $   $ Date: 1998/01/30 13:45:34 $

% Check for input errors
error=[];

if size(PortReturn,1)>=2 % find optimal point only if there are at least two points on the efficient frontier
    % Set fzero options.
    FZoptions = optimset('Display','none');

    RATE = RisklessRate;
    PP   = spline(PortReturn, PortRisk);         % PP = f(x)
    DPP  = ppdiff(PP);

    % Find the risk at the optimal point (tangent point) using fzero.
    % check starting points
    RetMin = max(RisklessRate+eps, min(PortReturn));
    RetMax = max(PortReturn);
    if portalloptpoint(RetMin, PP, DPP, RATE) >= 0
        % min point
        return_p = RetMin;

    elseif portalloptpoint(RetMax, PP, DPP, RATE) <= 0
        % max point
        return_p = RetMax;

    else
        % search along the frontier
        try
            eflag = -2; %Start eflag with error
            [return_p, fval, eflag] = fzero(@portalloptpoint, ...
                [max([PortReturn(1),RisklessRate+eps])  PortReturn(end)], ...
                FZoptions, PP, DPP, RATE);
        catch
            error='Finance:portalloc:TangencyFailure. Unable to compute tangent to the frontier.';
        end

        if (eflag<0)
            error='Finance:portalloc:TangencyFailure. Unable to compute tangent to the frontier.';
        end

    end

    if(isnan(return_p))
        error='Finance:portalloc:OptimalPointFailure. Unable to find optimal point.';
    end

    % Expected Risk of the optimal point.
    risk_p = ppval(PP, return_p);

    RiskyRisk = risk_p;
    RiskyReturn = return_p;

    % We now find the point of tangency between the CAL
    % and the indifference curve.
  %  RiskyFraction = (return_p - RisklessRate)/(RiskAversion*(risk_p^2));
 %   return_c = RisklessRate + RiskyFraction*(return_p - RisklessRate);
  %  risk_c = RiskyFraction*risk_p;
  %  cal  = 'cal1';
    % The values above correspond to CAL1
    
    RiskyWts = interp1(PortReturn, PortWts, RiskyReturn);
    if RiskyReturn > max(PortReturn)
        error = 'RiskLess rate larger than max PortReturn, no OS solution';
    end
else
    RiskyRisk=PortRisk;
    RiskyReturn=PortReturn;
    RiskyWts=PortWts;

end

  

function dpp = ppdiff(pp)
% dpp = ppdiff(pp) differentiate a piecewise polynomial (such as generated
% by pp = spline(x,y);

% JHA 8/26/96

[breaks,coefs,l,k] = unmkpp(pp);

pows = (k-1:-1:1);

dcoefs = coefs(:,1:k-1).*pows(ones(l,1),:);

dpp = mkpp(breaks,dcoefs);


function err = portalltanpoint(x, PP, DPP, AA)
%  This function is called by fzero. This is a private function callable
%  only through the PORTALLOC function.
%  Indifference curve-> Return = U + 0.5A*Risk^2
%  f1(x) = U + 0.5Ax^2 -> indifference curve
%  f2(x) = efficient frontier curve
%  err = d(f1(x))/dx - d(f2(x))/dx

%  Author(s): M. Reyes-Kattar, 02/05/98

err = 1/(AA*ppval(PP,x)) - ppval(DPP,x);


function err = portalloptpoint(x, PP, DPP, RATE)
%  This function is called by fzero. This is a private function callable
%  only through the PORTALLOC function.
%  err = f'(x) - f(x)/(x - RATE)
%  We want to find x, so that err = 0

%  Author(s): M. Reyes-Kattar, 02/05/98

err = ppval(DPP, x) - ppval(PP,x)/(x - RATE);


% [EOF]

