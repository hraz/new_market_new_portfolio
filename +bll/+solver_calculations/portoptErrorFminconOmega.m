function [PortMar, PortOmega, PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFminconOmega(ExpReturn, ExpCovariance, ...
    NumPorts, PortReturn, ConSet,  solver_props, nsin_data, ReturnMat, varargin)


import utility.data_conversions.*;
import bll.solver_calculations.*;

% Check for input errors
error=[];
warning=[];
if (nargin < 2)
    error='portoptErrorFminconOmega:portopt:missingInputs. You must enter ExpReturn and ExpCovariance.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Make sure that the number of returns entered matches the number of rows/columns in the
% covariance matrix (which represents the number of assets).

ExpReturn = ExpReturn(:);
NASSETS = length(ExpReturn);

[covRows, covCols] = size(ExpCovariance);
if(covRows ~= covCols)
    error='portoptErrorFminconOmega:portopt:invalidCovMatrix. The covariance matrix must be NxN, where N = number of assets';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Make sure covariance is positive-semidefinite and symmetric

if norm(ExpCovariance - ExpCovariance',inf) > eps
    warning='portoptErrorFminconOmega:portopt:asymmetricCovariance. Non-symmetric covariance matrix will be made symmetric.';
    ExpCovariance = 0.5*(ExpCovariance + ExpCovariance');
end
[L, D] = ldl(ExpCovariance); %#ok
if any(diag(D) < 0)
    warning='portoptErrorFminconOmega:portopt:nonposdefCovMatrix. Non-positive-semidefinite covariance input.';
end
clear L
clear D

% Make sure problem is conformable

if size(ExpCovariance, 1) ~= NASSETS
    error='portoptErrorFminconOmega:portopt:invalidExpReturns. The number of expected returns does not equal the number of assets.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% process name-value pairs (if any)

% if nargin > 7
%     if mod(nargin-7,2) ~= 0
%         error='portoptErrorFminconOmega:portopt:InvalidNameValuePair. Invalid (odd number) of optional name-value pairs.';
%         PortRisk=[];
%         PortReturn=[];
%         PortWts=[];
%         return
%     end

names = { 'algorithm', 'maxiter', 'tiebreak', 'tolcon', 'tolpiv' };			% names
values = { 'lcprog', 100000, 'first', 1.0e-6, 1.0e-9 };						% default values
try
    [algorithm, maxiter, tiebreak, tolcon, tolpiv] = parsepvpairs(names, values, varargin{:});
    
catch E
    E.throw
end
% else
algorithm = 'lcprog';
maxiter = 150000;
tiebreak = 'first';
tolcon = 1.0e-6;
tolpiv = 1.0e-9;
% end

% check arguments

if ~isempty(strmatch(lower(algorithm),'lcprog'))
    alg = 1;
elseif ~isempty(strmatch(lower(algorithm),{'qp','quadprog'}))
    alg = 2;
else
    error='portoptErrorFminconOmega:portopt:InvalidAlgorithm. Invalid algorithm selected. Choices are ''lcp'',''qp''.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if isempty(strmatch(tiebreak,{'default','first','last','random'}))
    error='portoptErrorFminconOmega:portopt:InvalidTieBreak. Invalid choice to break ties in pivot selection. Choices are ''first'',''last'',''random''.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if tolcon < 2*eps
    error='portoptErrorFminconOmega:portopt:InvalidTolerance. Unrealistically small tolerance (tolcon) specified.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if tolpiv < 2*eps
    error='portoptErrorFminconOmega:portopt:InvalidTolerance. Unrealistically small tolerance (tolpiv) specified.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

if maxiter <= 0
    error='portoptErrorFminconOmega:portopt:NonPositiveInteger. Maximum number of iterations (maxiter) must be a positive integer.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Determine which optional arguments were entered as non-empty.

if (nargin < 3 || isempty(NumPorts))
    NumPortsEntered = 0;
else
    NumPortsEntered = 1;
    NumPorts = double(NumPorts);
end

if (nargin < 4 || isempty(PortReturn))
    PortReturnEntered = 0;
else
    PortReturnEntered = 1;
end

% When entering the target rate of return (PortReturn), enter NumPorts as an empty matrix.

if (NumPortsEntered == 1) && (PortReturnEntered == 1)
    error='portoptErrorFminconOmega:portopt:emptyNumPorts. When entering the target rate of return, PortReturn, enter NumPorts as an empty matrix.';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% if 'default' problem, set a flag to trim negative weights to 0 (rmt4)
if (nargin < 5) || isempty(ConSet)
    % A default constraint matrix ConSet will be created if this is not entered.
    ConSet = portcons('default', NASSETS);
    TrimWeights = true;
else
    TrimWeights = false;
end


[Aineq, Bineq, Aeq, Beq, LB, UB] = ineqparse(ConSet);

if isempty(LB) || any(~isfinite(LB)) || (~isempty(UB) && any(~isfinite(UB)))
    warning='portoptErrorFminconOmega:portopt:switchQPsolver. Unbounded bound constraints. Cannot use lcprog. Switching to quadprog.';
    alg = 2;
end

% If PortReturn has been entered, check them for legality relative to
% constraints. If it has not been entered, construct the default
% return range. Used only by LINPROG and QUADPROG and does not matter if infeasible.

W0 = ones(NASSETS, 1)/NASSETS;
tolcon0=tolcon;


%% --------------------------------------- Find the Maximum ReturnPoint -------------------------------------

% [MaxReturnWeights, Fval] = MaxExpReturn(ExpReturn, ...
%     ConMinNumOfBonds,ConMaxNumOfBonds,ConMinWeight2AlloBonds,ConMaxWeight2NonAlloBonds,...
%     MinNumOfBonds,MaxNumOfBonds,MinWeight2AllocBonds,MaxWeight2NonAllocBonds, Aineq, Bineq, Aeq, Beq, LB, UB, varargin);
%
% MaxReturnWeights = MaxReturnWeights';
% MaxReturn = MaxReturnWeights*ExpReturn;
% OmegaMaxReturn = omega_fun(ReturnMat, -Fval, MaxReturnWeights);
%
% PortWts(1, :) = MaxReturnWeights;
% PortRisk(1) = sqrt(MaxReturnWeights*ExpCovariance*MaxReturnWeights');
% PortReturn(1) = MaxReturnWeights*ExpReturn;
% PortMar(1) = -Fval;
% Data = ReturnMat*MaxReturnWeights';
% PortOmega(1) =  -OmegaMaxReturn;

%% --------------------------------------- End of  Find the Maximum ReturnPoint -------------------------------------





%% --------------------------------------- Find the Maximum Omega Efficient Frontier -------------------------------------
ErrorFlag = -10;
flag_max = 1;%when this is on, will change the max portfolio duration, when of, the min.

port_max_duration_changed = 0;

if solver_props.MaxOmega
    
    while port_max_duration_changed <3 && (ErrorFlag < 0)
        Mar = solver_props.Mar;
        %NumPorts = 4;      %%%For simplicity, make this an even number%%
        z=1;
        NumRuns= 1;
        
        while (ErrorFlag < 0) && (NumRuns < 4) %:NumPorts/2
            
            MarRunning = Mar+(z-1)*Year2Day(0.01);
            omega = @(w)omega_fun(ReturnMat, MarRunning, w);
            %options2 = optimset('MaxFunEvals', 100000, 'Algorithm', 'active-set');
            %options2 = optimset('MaxFunEvals', 100000, 'Algorithm','interior-point');
            options2 = optimset('MaxFunEvals', 100000, 'Algorithm', 'sqp');
            % [Weights, Fval, ErrorFlag, output] = fmincon(@(w)0.5*w'*ExpCovariance*w, W0, Aineq, Bineq, Aeq, Beq, LB, UB, fcon, options2);
            
            %Aeq(size(Aeq,1)+1, :) = ExpReturn;
            %Beq(end+1) = MarRunning;
            
            Aineq(size(Aineq,1)+1, :) = -ExpReturn;
            Bineq(end+1) = -MarRunning;
            
            [Weights, Fval, ErrorFlag, output] = fmincon(omega, W0, Aineq, Bineq, Aeq, Beq, LB, UB, [], options2);
            
            Aineq(size(Aineq,1), :) = [];
            Bineq(end) = [];
            
            %Aeq(size(Aeq,1), :) = [];
            %Beq(end) = [];
            
            if ErrorFlag>0
                %         PortWts(z, :) = Weights';
                %         PortRisk(z) = NaN;
                %         PortReturn(z) = NaN;
                %         PortMar(z) = MarRunning;
                %         Data = ReturnMat*Weights;
                %         PortOmega(z) = NaN;
                %     else
                PortWts(1, :) = Weights';
                PortRisk(1) = sqrt(Weights'*ExpCovariance*Weights);
                PortReturn(1) = Weights'*ExpReturn;
                PortMar(1) = MarRunning;
                Data = ReturnMat*Weights;
                PortOmega(1) =  -Fval;
            end
            z = 1 + (-1)^(NumRuns-1);
            NumRuns = NumRuns + 1;
            
            
            
        end
        
        if flag_max %haven't changed the max portfolio duration yet, or last changed the min portfolio duration
            Bsize = size(Bineq, 1);
            Bineq(Bsize -1, 1) = Bineq(Bsize -1, 1) +0.5; %increment max portfolio duration by 0.5 yrs.
            flag_max = 0;
        else %change the min portfolio duration
            Bsize = size(Bineq, 1);
            Bineq(Bsize, 1) = Bineq(Bsize, 1) +0.5; %reduce min portfolio duration by 0.5 yrs.
            flag_max = 1;
        end
        port_max_duration_changed = port_max_duration_changed + 1;
        
    end
    
    %% if still no solution
    if ErrorFlag<=0 %Changed to <= to 0. Note that if = 0, more iterations may give solution.
        PortWts(z, :) = Weights';
        PortRisk(z) = NaN;
        PortReturn(z) = NaN;
        PortMar(z) = MarRunning;
        Data = ReturnMat*Weights;
        PortOmega(z) = NaN;
        error='portoptErrorFminconOmega:Algo did not find solution';
    else
        
        SolutionPoints = (~isnan(PortRisk));                        %indices of points which had a solution
        PortReturn=PortReturn(SolutionPoints);                      %take only the returns of points with a solution
        PortMar=PortMar(SolutionPoints);
        PortRisk=PortRisk(SolutionPoints);                          %take only the risk of points with a solution
        PortWts=PortWts(SolutionPoints,:); %take only the weights of points with a solution
        PortOmega=PortOmega(SolutionPoints);
    end
    %%Making sure there are at least 10 points in the efficienr frontier%%
    
    index = length(PortReturn);
    Mar = MarRunning;
    
    
elseif solver_props.RiskParity
    nsin_data.indices_groups=CreateGroups(nsin_data); %divide the assets into groups such that the risk parity can equate the variance contribution of each group
    ftarget=@(x)MinRiskContributionVariance(x,ExpCovariance,nsin_data.indices_groups); %this is the risk parity target function
    options = optimset('Algorithm','interior-point','Display','iter-detailed','MaxFunEvals',10000,'TolFun',1e-6,'TolCon',1e-5); %set the tolerances smaller by x10 to increase the probability of convergence. Also, increase the maximum number of function evaluations
    [Weights, Fval, ErrorFlag, output] = fmincon(ftarget, W0, Aineq, Bineq, Aeq, Beq, LB, UB,[],options);
    PortWts(1, :) = Weights';
    PortRisk(1) = sqrt(Weights'*ExpCovariance*Weights);
    PortReturn(1) = Weights'*ExpReturn;
    
    if ErrorFlag<0
        PortWts(1, :) = Weights';
        PortRisk(1) = NaN;
        PortReturn(1) = NaN;
    else
        SolutionPoints = (~isnan(PortRisk));                        %indices of points which had a solution
        PortReturn=PortReturn(SolutionPoints);                      %take only the returns of points with a solution
        PortRisk=PortRisk(SolutionPoints);                          %take only the risk of points with a solution
        PortWts=PortWts(SolutionPoints,:); %take only the weights of points with a solution
    end
    PortMar=[];
    PortOmega=[];
end
% PortWts = flipud(PortWts);
% PortRisk = fliplr(PortRisk);
% PortReturn = fliplr(PortReturn);
% PortMar = fliplr(PortMar);
% %Data;
% PortOmega = fliplr(PortOmega);


% LengthOfAbovePoints = length(PortRisk);
% for z = LengthOfAbovePoints+1:LengthOfAbovePoints+NumPorts/2
%     MarRunning = Mar-((z-LengthOfAbovePoints))*Year2Day(0.05)/(NumPorts/2);
%     omega = @(w)omega_fun(ReturnMat, MarRunning, w);
%     %options2 = optimset('MaxFunEvals', 100000, 'Algorithm', 'active-set');
%     %options2 = optimset('MaxFunEvals', 100000, 'Algorithm','interior-point');
%     options2 = optimset('MaxFunEvals', 100000, 'Algorithm', 'sqp');
%     % [Weights, Fval, ErrorFlag, output] = fmincon(@(w)0.5*w'*ExpCovariance*w, W0, Aineq, Bineq, Aeq, Beq, LB, UB, fcon, options2);
%
%     %Aeq(size(Aeq,1)+1, :) = ExpReturn;
%     %Beq(end+1) = MarRunning;
%
%     Aineq(size(Aineq,1)+1, :) = -ExpReturn;
%     Bineq(end+1) = -MarRunning;
%
%     [Weights, Fval, ErrorFlag, output] = fmincon(omega, W0, Aineq, Bineq, Aeq, Beq, LB, UB, [], options2);
%
%     Aineq(size(Aineq,1), :) = [];
%     Bineq(end) = [];
%
%     %Aeq(size(Aeq,1), :) = [];
%     %Beq(end) = [];
%
%     if ErrorFlag<0
%         PortWts(z, :) = Weights';
%         PortRisk(z) = NaN;
%         PortReturn(z) = NaN;
%         PortMar(z) = MarRunning;
%         Data = ReturnMat*Weights;
%         PortOmega(z) = NaN;
%     else
%         PortWts(z, :) = Weights';
%         PortRisk(z) = sqrt(Weights'*ExpCovariance*Weights);
%         PortReturn(z) = Weights'*ExpReturn;
%         PortMar(z) = MarRunning;
%         Data = ReturnMat*Weights;
%         PortOmega(z) =  -Fval;
%     end
% end


%index2 = 1;
% if NumPorts>=10
%     while (index<10)
%         %for z = index+1:NumPorts
%         MarRunning = Mar-index2*Year2Day(0.05)/NumPorts;
%         omega = @(w)omega_fun(ReturnMat, MarRunning, w);
%         %options2 = optimset('MaxFunEvals', 100000, 'Algorithm', 'active-set');
%         %options2 = optimset('MaxFunEvals', 100000, 'Algorithm','interior-point');
%         options2 = optimset('MaxFunEvals', 100000, 'Algorithm', 'sqp');
%         % [Weights, Fval, ErrorFlag, output] = fmincon(@(w)0.5*w'*ExpCovariance*w, W0, Aineq, Bineq, Aeq, Beq, LB, UB, fcon, options2);
%
%         %         Aeq(size(Aeq,1)+1, :) = ExpReturn;
%         %         Beq(end+1) = MarRunning;
%         %         [Weights, Fval, ErrorFlag, output] = fmincon(omega, W0, Aineq, Bineq, Aeq, Beq, LB, UB, [], options2);
%         %         Aeq(size(Aeq,1), :) = [];
%         %         Beq(end) = [];
%
%
%
%         Aineq(size(Aineq,1)+1, :) = -ExpReturn;
%         Bineq(end+1) = -MarRunning;
%         [Weights, Fval, ErrorFlag, output] = fmincon(omega, W0, Aineq, Bineq, Aeq, Beq, LB, UB, [], options2);
%         Aineq(size(Aineq,1), :) = [];
%         Bineq(end) = [];
%
%         if ErrorFlag<0
%             PortWts(index2, :) = Weights';
%             PortRisk(index2) = NaN;
%             PortReturn(index2) = NaN;
%             PortMar(index2) = MarRunning;
%             Data = ReturnMat*Weights;
%             PortOmega(index2) = NaN;
%             index2 = index2+1;
%         else
%             PortWts(index2, :) = Weights';
%             PortRisk(index2) = sqrt(Weights'*ExpCovariance*Weights);
%             PortReturn(index2) = Weights'*ExpReturn;
%             PortMar(index2) = MarRunning;
%             Data = ReturnMat*Weights;
%             PortOmega(index2) =  -Fval;
%             index = index+1;
%             index2 = index2+1;
%         end
%         %end
%     end
% end


%% --------------------------------------- End Of Find the Maximum Omega Efficient Frontier -------------------------------------


% if lower-bound weights are zero, force all negative weights to be zero
if TrimWeights
    PortWts(PortWts < 0) = 0;
end

%A plot of the efficient frontier is returned if the function is
%invoked without output arguments.

% if nargout == 0
%     FrontWin = figure;
%
%     set(FrontWin,'NumberTitle','off');
%     set(FrontWin,'Name','Efficient Frontier');
%     set(FrontWin,'Resize','on');
%
%     set(FrontWin,'Tag','FrontWin');
%
% %     plot(PortRisk, PortReturn);
% %     title('Mean-Variance-Efficient Frontier', 'Color', 'k');
% %     xlabel('Risk (Standard Deviation)');
% %     ylabel('Expected Return');
% %     grid on;
%
%     figure
%     %plot(PortOmega, PortReturn);
%     plot(PortRisk, PortOmega, 'r.');
%     grid on;
%     figure;
%
%     % prevent any output
%     clear PortRisk;
% end

%----------------------------------------------------------------
% Auxiliary function(s)
%----------------------------------------------------------------

function [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, bin)
%INEQPARSE Find inequalities, equalities, and bounds implied by Ain*x <= bin.
%  Identifies equalities specified as Arow*x <= bval, -Arow*x <= -bval.
%  Parses out duplicate entries and all zero equations.
%  Finds bound constraints among inequalities.
%
%  [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, bin)
%  [A,b,Aeq,beq,LB,UB] = ineqparse([Ain, bin])
%
%  The function does not catch linear combinations of inequalities which
%  together imply an equality constraint.
%
%  See also PORTCONS.
%
%----------------------------------------------------------------------
%
% % Test the logic to parse out inequalites with single-entry equations
% % 1 : redundant equaltiy
% % 2 : equality
% % 3 : redundant upper bound
% % 4 : lower bound
% % 5 : upper bound
% Astart = [1 2 -1 1 3 -1 -2 -4 5 3]'
% Ain = full(sparse(1:length(Astart),abs(Astart),Astart))
% [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, zeros(length(Astart),1))
%
% % Catch rows which are multiples
% m = 1:length(Astart)
% Ain = diag(m)*Ain
% [A,b,Aeq,beq,LB,UB] = ineqparse(Ain, zeros(length(Astart),1))
%
% % Degenerate case with equality, lower, upper bounds
% C = portcons('default',3,'AssetLims',0,[0.5 0.6 0.7],3)
% [A,b,Aeq,beq,LB,UB] = ineqparse(C)
%
% % Case with a general inequality constraint
% C = portcons('default',3,'AssetLims',0,[0.5 0.6 0.7],3, ...
%              'Custom',[0.1 0.2 0.3],0.40)
% [A,b,Aeq,beq,LB,UB] = ineqparse(C)
%

% find usage ineqparse(ConSet)
if nargin==1
    bin = Ain(:,end);
    Ain = Ain(:,1:end-1);
end

[NumEquations, NumVars] = size(Ain);
if any( size(bin)~=[NumEquations, 1] )
    error='portoptErrorFminconOmega:portopt:mismatchAandB. Dimensions of A and b are inconsistent';
    PortRisk=[];
    PortReturn=[];
    PortWts=[];
    return
end

% Pull out degenerate rows
I = all(Ain==0,2);
if(any(I))
    warning='portoptErrorFminconOmega:portopt:ConstraintQual. Degenerate rows found in constraint matrix. Eliminating these constraints';
    Ain(I,:) = [];
    bin(I) = [];
end

% Constraint rows
ConRows = [Ain, bin];

% Form numerator and denominator dot products.
%
% row I and row J are the same direction when:
%   rowI*rowJ' == sqrt(rowI*rowI')*sqrt(rowJ*rowJ')
%        numIJ == denIJ
%
% row I and row J are the opposite direction when:
%   rowI*rowJ' == - sqrt(rowI*rowI')*sqrt(rowJ*rowJ')
%        numIJ == - denIJ

% square (rowI*rowJ') but keep the sign
numIJsqrt = ConRows*ConRows';
numIJ = sign(numIJsqrt).*(numIJsqrt.*numIJsqrt);

% form (rowI*rowI') times (rowJ*rowJ')
rowKdot = dot(ConRows, ConRows, 2);
[rowIdot, rowJdot] = meshgrid(rowKdot, rowKdot);
denIJ = rowIdot .* rowJdot;

% record which equations are negations or duplicates
% denIJ is always positive
% take the upper triangular part only
%
% isdupIJ [NumEqs x NumEqs] row I is a positive multiple of row J
% isnegIJ [NumEqs x NumEqs] row I is a negative multiple of row J
reltol = 1000*eps;
isdupIJ = ( denIJ*(1-reltol) <  numIJ ) & (  numIJ < denIJ*(1+reltol) );
isnegIJ = ( denIJ*(1-reltol) < -numIJ ) & ( -numIJ < denIJ*(1+reltol) );

isdupIJ = triu(isdupIJ, 1);
isnegIJ = triu(isnegIJ, 1);

% search through the equations and clean out equalities and duplicates.
% store the equalities separately.
%
% ConEqs  [NumEqs   x NumVars+1] : [Aeq, beq]
% ConRows [NumInEqs x NumVars+1] : [A, b]
ConEqs = zeros(0, NumVars+1);

i=1;
while (i < size(ConRows,1) )
    % find negations and duplicates of this row
    RowIsNeg = isnegIJ(i,:);
    RowIsDup = isdupIJ(i,:);
    
    % negations and duplicates should be removed from the inequality list
    IndRemove = RowIsNeg | RowIsDup;
    
    if any(RowIsNeg)
        % add the row to the equality list
        ConEqs = [ConEqs; ConRows(i,:)]; %#ok
        
        % remove the row from the inequality list along with negs and dups
        IndRemove(i) = 1;
    else
        % equation i has been left in
        i = i + 1;
    end
    
    % remove equations from the inequality list
    ConRows(IndRemove,:) = [];
    isnegIJ = isnegIJ(~IndRemove, ~IndRemove);
    isdupIJ = isdupIJ(~IndRemove, ~IndRemove);
end

% Break up into left and right hand sides
Aeq = ConEqs(:,1:NumVars);
beq = ConEqs(:,NumVars+1);
A = ConRows(:,1:NumVars);
b = ConRows(:,NumVars+1);

% search through the inequalities and find bounds
% SingleValue * x(Ind) <= b(Ind)
%
% IndSingle   [NumInEqs x 1] true if only 1 non-zero value in row of A
% SingleValue [NumInEqs x 1] only valid for IndSingle == 1
%
% VarNum       [NumInEqs x NumVars] column of each entry of A
% SingleVarNum [NumInEqs x 1] column of first non-zero entry in A
%
IndSingle   = sum(A~=0 , 2) == 1;
SingleValue = sum(A    , 2);

IndLower = IndSingle & ( SingleValue < 0 );
IndUpper = IndSingle & ( SingleValue > 0 );

VarNum = (1:NumVars);
VarNum = VarNum(ones(size(A,1),1),:);
VarNum(A==0) = Inf;
SingleVarNum = min(VarNum,[],2);

if any(IndLower)
    LB = -Inf*ones(NumVars,1);
    
    % find the variable and the bound value
    VarNum = SingleVarNum(IndLower);
    BVal = b(IndLower)./SingleValue(IndLower);
    
    % apply the most restrictive bound to each variable
    UniqVar = unique(VarNum);
    if length(UniqVar)==length(VarNum)
        % no variables have multiple parallel bounds
        LB(SingleVarNum(IndLower)) = BVal;
    else
        for Var=UniqVar(:)'
            LB(Var) = max( BVal( VarNum==Var ) );
        end
    end
    
else
    LB = [];
end

if any(IndUpper)
    UB = Inf*ones(NumVars,1);
    
    % find the variable and the bound value
    VarNum = SingleVarNum(IndUpper);
    BVal = b(IndUpper)./SingleValue(IndUpper);
    
    % apply the most restrictive bound to each variable
    UniqVar = unique(VarNum);
    if length(UniqVar)==length(VarNum)
        % no variables have multiple parallel bounds
        UB(SingleVarNum(IndUpper)) = BVal;
    else
        for Var=UniqVar(:)'
            UB(Var) = min( BVal( VarNum==Var ) );
        end
    end
    
else
    UB = [];
end

% remove lower or upper bound inequalities
A(IndSingle,:) = [];
b(IndSingle) = [];


% [EOF]
