function [PortWts Wts PortReturnYear PortMarYear PortRiskYear PortOmega MarkerPosition PortDuration error warning] = MaxOmegaPortError(ReturnMat, YTM,ExpCovMat,NumPorts,ConSet,FminConFlag,Duration, handles)
% Solves the maximum omega portfolio optimization problem,
% and plots the "efficient frontier" in omega-return space. it returns the allocated weights for all bonds
% that passed the filters, which are needed in order to obtain the minimum
% variance point.
import gui.*;


Daily_YTM=Year2Day(YTM);                                    %since the data is daily, and so does the covariance matrix, the YTM should also be daily
% if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
%     tmp = handles.MyBonds;                                  %MyBonds are the bonds in where am i mode
%     if ~isempty(tmp)                                        %if we are in where am i mode, init the algorithm with the users weights
%         for i=1:length(handles.MyBonds)
%             ind(i)=find(extractfield(bond(BondScope),'ID')==handles.MyBonds(i).ID);
%         end
%         InitWeight(ind) = handles.MyWeights;                %init of weights according to users bonds
%     else                                                    %if not in where am i mode
%         InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N
%     end
%     [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat,NumPorts, [], ConSet, InitWeight); %Optimization
% else                                                        %if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    handles.Mar = Year2Day(handles.TarRet); %minimum acceptable return
    [PortMar, PortOmega PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFminconOmegaNoGui(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds, ReturnMat, handles.Mar);                        %Optimization
%end

if sum(isempty(PortRisk))+sum(isnan(PortRisk))            %if there was no solution
    PortWts=[];
    Wts=[];
    PortRiskYear=[];
    PortReturnYear=[];
    PortDuration = [];
    MarkerPosition = 1;
    PortMarYear= [];
    PortOmega =[];
    return
end
%% Find out if there were unfeasible points, which are charachterizes by a NaN in their PortRisk, and a PortReturn or one. exclude these points in order to plot what was succesfful, and tell the user that we are showing the closest point to his requst
SolutionPoints = (~isnan(PortRisk)&PortMar>0); %indices of points which had a solution
%SolutionPoints = SolutionPoints(SolutionPoints>0);
Solution = find(SolutionPoints > 0, 1); %get first solution

PortReturn=PortReturn(Solution);                      %take only the returns of points with a solution
PortMar=PortMar(Solution);
PortRisk=PortRisk(Solution);                          %take only the risk of points with a solution
PortWts=PortWts(Solution,:); %take only the weights of points with a solution
PortOmega=PortOmega(Solution);
PortReturnYear=Day2Year(PortReturn);                        %translate returns back to yearly returns, in order to plot in yearly terms
PortMarYear = Day2Year(PortMar);
PortRiskYear=PortRisk*sqrt(365);                            %translate the risk to annual terms
PortDuration=Duration'*PortWts(Solution, :)';
FlipCheck = 0;
% if ~isempty(PortReturn)
%     if PortOmega(2)/PortOmega(1)<1
%         PortOmega = fliplr(PortOmega);
%         FlipCheck = 1;
%     end
%     
% %     for a = 1:length(PortOmega)-1
% %         monotonicity(a) = PortOmega(a+1)-PortOmega(a);
% %     end
% %     for a = 1:length(monotonicity)-1
% %         monotonicity1(a) = sign(monotonicity(a+1)/monotonicity(a));
% %     end
% %     ChangeSignOmegaIndex = find(monotonicity1~=monotonicity1(1));
% %     if (~isempty(ChangeSignOmegaIndex))
% %         ChangeSignOmegaIndex = ChangeSignOmegaIndex(1);
% %         PortRiskYear(ChangeSignOmegaIndex+2:end) = [];
% %         PortReturnYear(ChangeSignOmegaIndex+2:end) = [];
% %         PortOmega(ChangeSignOmegaIndex+2:end) = [];
% %         PortWts(ChangeSignOmegaIndex+2:end,:) = [];
% %     end
% %     if FlipCheck==1
% %         PortOmega = fliplr(PortOmega);
% %     end
%     
%     %plot(PortOmega, PortReturnYear*100, 'LineWidth',2);        %plot the efficient frontier
%     plot(PortOmega, PortMarYear*100, 'LineWidth',2);        %plot the efficient frontier
%     hold on;
%     plot([0 PortOmega(1)],PortMarYear(1)*100*ones(1,2),'r--','LineWidth',2)               %plot horizontal red dotted line
%     plot(PortOmega(1),PortMarYear(1)*100,'r*','markersize',16);                           %plot a red star
%     xlim([min(PortOmega)*0.99 max(PortOmega)*1.01])                                           %set limits
%     DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope
%     Duration=PortWts(1,:)*DurationVector;                       %calculate the weighted average of the years to maturity
%     tit=sprintf('Omega: %1.2f, Mar: %1.3f%%, Duration: %1.2f years',PortOmega(1),PortMarYear(1)*100,Duration);  %set title
%     AddFigNames(tit,'\Omega', 'Mar', 1)
% else
%     sprintf('No Solution was Found. Try a lower reqiured return.');
% end
% Wts=PortWts(1,:)';                                          %output weights are those of the minimal variance, i.e., the first point in the solution
% MarkerPosition = 1;                                         %this is the position of the marker bar in the upper left corner of the efficient frontier plot. Since its the minimum variance portfolio, it is the first point, and thus, the marker should be in his first position


%[TarRetVec ReqInd ClosestErrorFlag]=FindClosestTarRet(PortMarYear, handles.TarRet, NumPorts);           %finds the index in the PortReturn vector, which is closes to the weekly return requested by the user. ReqInd is the required index of the solution, i.e., the index of the solution vector which is closest to the the user request of YTM                           
ReqInd = 1;%find(abs(PortMarYear-handles.TarRet) == min(abs(PortMarYear-handles.TarRet)));

Wts=PortWts(Solution,1:handles.NumBonds)';                                     %these are the weights which give us the required target return
MarkerPosition = ReqInd;        % 
% if ~isempty(PortReturn)
%     if PortOmega(2)/PortOmega(1)<1
%         PortOmega = fliplr(PortOmega);
%         FlipCheck = 1;
%     end
 %plot(PortOmega, PortMarYear*100, 'LineWidth',2);        %plot the efficient frontier
  %  hold on;
   % plot([0 PortOmega(ReqInd)],PortMarYear(ReqInd)*100*ones(1,2),'r--','LineWidth',2)               %plot horizontal red dotted line
   % plot(PortOmega(ReqInd),PortMarYear(ReqInd)*100,'r*','markersize',16);                           %plot a red star
   % xlim([min(PortOmega)*0.99 max(PortOmega)*1.01])                                           %set limits
   % DurationVector=handles.Duration;                            %extract the duration of all the bonds in the BondScope
   % Duration=PortWts(1,:)*DurationVector;                       %calculate the weighted average of the years to maturity
   % tit=sprintf('Omega: %1.2f, Mar: %1.3f%%, Duration: %1.2f years',PortOmega(ReqInd),PortMarYear(ReqInd)*100,Duration);  %set title
   % AddFigNames(tit,'\Omega', 'Mar', 1)
%else
%    sprintf('No Solution was Found. Try a lower reqiured return.');
% end
                            %this is also the marker position in the upper left bar of the plot
