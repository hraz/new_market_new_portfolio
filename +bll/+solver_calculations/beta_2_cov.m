function BetaCovMat=beta_2_cov(beta,IndexPriceYld,Sigma_ei,Rm,HistLen)
% This function transforms the beta into covariances, using the assumption 
% cov(i,j)=beta_i*beta_j*VarMar for i!=j
% cov(i,i)=beta_i^2*VarMar + sigma_ei for i=j

NormMktIndex=IndexPriceYld-repmat(Rm,length(IndexPriceYld),1); %now the expectation is 0
VarMarket=diag(NormMktIndex'*NormMktIndex/HistLen); %estimation of the variance of the marketindices. assume there is no coupling between the diff inds - so take only the diag terms
BetaCovMat=beta*diag(VarMarket)*beta.'+diag(Sigma_ei);                    %construct the cov matrix. the variance eNumOfWeeksements (on the diagonaNumOfWeeks) have additionNumOfWeeks internaNumOfWeeks variance, which is not reNumOfWeeksated to beta (non systematic noise)

