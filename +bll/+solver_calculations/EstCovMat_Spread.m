function handles = EstCovMat_Spread(handles)
% Estimate the covaraince matrix based on the spread model
SpreadMat = zeros(handles.HistLen, handles.NumBonds);
TmpScope=1:length(handles.bond);%take all bond scope in next line
for i=1:length(handles.BondScope)
    CurrMaalotRank=handles.bond(handles.BondScope(i)).Maalot; %find curr bond ranks
    CurrMidroogRank=handles.bond(handles.BondScope(i)).Midroog;
    if strcmp(CurrMaalotRank,'Gov')
        CurrType=handles.bond(handles.BondScope(i)).Type;
        CurrBondRankGroupInds = find(strcmp(extractfield(handles.bond,'Type'),CurrType));
    else
        CurrBondRankGroupInds = BondRankFilter(handles.bond,TmpScope,CurrMaalotRank,CurrMaalotRank,CurrMidroogRank,CurrMidroogRank,'or') ; %all of these bonds have the same ranking (either Maalot or Midroog) as the current bond has
    end
    CurrBondRankGroup=handles.bond(CurrBondRankGroupInds);
    Len=extractfield(CurrBondRankGroup,'Length');
    CurrBondRankGroup = CurrBondRankGroup(Len > handles.HistLen);
    NumOfGroupMembers = length(CurrBondRankGroup );
    %get the group memeber YTM and Duration vectors
    GroupVecYTM=extractfield(CurrBondRankGroup,'RelevantVecYTM');
    GroupVecYTM= reshape(GroupVecYTM, [handles.HistLen  NumOfGroupMembers ] );
    GroupVecDuration=extractfield(CurrBondRankGroup,'RelevantVecDuration');
    GroupVecDuration = reshape(GroupVecDuration, [handles.HistLen  NumOfGroupMembers ] );
    for j=1:handles.HistLen
        CurrYTM = handles.bond(handles.BondScope(i)).RelevantVecYTM(j); %TODO: make sure i am taking the date i want, and its not reveresed
        CurrGroupYTM = GroupVecYTM(j,:);
        CurrGroupDurration = GroupVecDuration(j,:);
        [DurEstLS YTMestLS1 b]=LSestimation2(CurrGroupDurration.',CurrGroupYTM.');
        CurrDuration=handles.bond(handles.BondScope(i)).RelevantVecDuration(j); %find curr bond duration
        [min_val min_loc]=min( abs(DurEstLS  -  CurrDuration)); %this is the closest duration
        SpreadMat(j,i) = year2week(CurrYTM -(YTMestLS1(min_loc)));
    end
end
[dummy, handles.SpreadCovMat] = ewstats(SpreadMat);
handles.ExpCovMat=handles.SpreadCovMat; %use the covariance matrix which was found using the single index model
handles.SpreadVar=diag(handles.SpreadCovMat);