clear; clc;

%% Import external packages:
import bll.rp_mdl.*;
import utility.*;
import dal.market.get.dynamic.*;

%%  Build basic allocation table:
settle_date = {'01012006', '01012007', '01012008', '01012009'};
models = {'Markovitz', 'SingleIndex'};
opt_type = {'minvar', 'optimal&target', 'optimal_sharpe', 'target_return'};
main_folder = '+bll/+rp_mdl/+test/';

%% Initial parameters defining:
leverage_risk_premium = 0.015;
horizon = 100; % number of days for historical data (ytm) at t0.
period = 10; % months
sim_friquency = 6; % times in a year
simulation_period = 2; % years
cir_bound = [-0.2, 0.8]; % compounding_interest_rate_bounds

%% Memory allocation for test results:
alloc = cell(length(settle_date), length(models), length(opt_type));
port_hold_return = cell(length(settle_date), length(models), length(opt_type));
group_weight = cell(length(settle_date), length(models), length(opt_type));

risk = cell(length(settle_date), length(models), length(opt_type));
sharp = cell(length(settle_date), length(models), length(opt_type));

%% Simulations:
for s = 1:length(settle_date)
%     first_date = datenum(settle_date{s}, 'ddmmyyyy');
    first_date = datenum('2007-09-28');
    last_date = addtodate(first_date, simulation_period, 'year');
    for m = 1:length(models)
        for op = 1:length(opt_type)
            % Upload test data from mat files:
            load(fullfile(pwd, main_folder, ['+' settle_date{s}], ['MyPort_' models{m} '_' settle_date{s} '_' opt_type{op} '.mat']));

            % Build source allocation table:
            nsin = cell2mat(Results.ResMat(:,strcmpi(Results.ResFields, 'ID'))); 
            
            % Remove Yoav's nsin = 9999999 for 1 year Short T-Bill
            nsin = nsin(1:end-1);
            source_weight = cell2mat(Results.ResMat(:,strcmpi(Results.ResFields, 'Weight')))/100;
            source_weight = source_weight(1:end-1);
            price = get_bond_dyn_single_date(nsin, 'price_dirty', datenum(settle_date{s}, 'ddmmyyyy'));
%             source_count = (weights2holdings(1, source_weight', fts2mat(price)))';
            source_count = 15000*source_weight./fts2mat(price)';

            allocation = dataset(nsin, source_weight, source_count, 'VarNames', {'nsin', 'weight', 'count'});
            alloc{s,m,op} = sortrows(allocation,1);            

            % Get basic data for initial covariance matrix calculation:
            base_data = get_multi_dyn_between_dates( alloc{s,m,op}.nsin, {'ytm'}, 'bond', addtodate(first_date, -horizon, 'day'), first_date );
            asset_data = fts2mat(base_data.ytm(end - 59:end)); % 60 day backward
            
            % Covariance matrix calculation:
            [ cov_mtx ] = nancov(asset_data);         
            
            % Get Risk-Parity allocation for the given assets' list:
            [ alloc{s,m,op}, group_weight{s,m,op} ] = rp_opt( alloc{s,m,op}.nsin, cov_mtx, @rp_group, first_date );
            alloc{s,m,op} = [alloc{s,m,op}, dataset(15000*alloc{s,m,op}.weight./fts2mat(price)', 'VarNames', {'count'})];
            % Performance evaluation:
            checking_allocations = struct('source', allocation, 'rp', alloc{s,m,op});
            [port_hold_return{s,m,op}, risk{s,m,op}, sharp{s,m,op}] = ...
                rp_performance(checking_allocations, first_date, last_date, cir_bound(1), cir_bound(2), sim_friquency);
        end
    end
end

%% Cummulative return graph presentation:
first_row = fts2mat(portfolio_holding_ret(1));
figure(1);
subplot(2,1,1);
plot(portfolio_holding_ret); 
xlabel('Holding date (days)');
ylabel('Portfolio cumulative income on initial investment (NIS)');
title('Performance during given holding period using ''Buy and hold'' management method.');

subplot(2,1,2);
plot(sharpe_hist); 
xlabel('Holding date (days)');
ylabel('Sharpe Ratio growth.');
title('Performance during given holding period using ''Buy and hold'' management method.');
