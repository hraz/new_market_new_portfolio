function ftsPayExDatesMappingOut = update_fts_pay_ex_dates_mapping(ftsPayExDatesMappingIn, newSecurityID, firstDate, lastDate)


%UPDATE_FTS_PAY_EX_DATES_MAPPING updates the fts which maps the pay date
%into ex date for each security (nsin).
%
%   ftsPayExDatesMappingOut =
%   update_fts_pay_ex_dates_mapping(ftsPayExDatesMappingIn,...
%                                                                               newSecurityID, firstDate, lastDate)
%   receives the existing fts which maps the pay dates into ex dates. It
%   also recieves new security id's (nsins) and updtaes the existing fts to
%   hold the information for all the securities.
%	Input:
%       ftsPayExDatesMappingIn � fts of dimensions (nDates)*(nSecurities).
%                                                                          The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | ex_date1            | ex_date2          | ex_date3           |
%       newSecurityID �  vector of new security id's (nsins). If this is
%                                             non-empty, the variable ftsPayExDatesMappingIn should be updated.
%                                             Otherwise the output argument ftsPayExDatesMappingOut will be
%                                             identiacl to ftsPayExDatesMappingIn.
%
%       firstDate - a datenum variable of the date the portfolio was created.
%       lastDate - a datenum variable of the date the portfolio ends.
%   Output:
%       ftsPayExDatesMappingOut � fts similar to ftsPayExDatesMappingIn but
%       updated due to newSecurityID.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%		UPDATE_PRICES, UPDATE_COUPONS, UPDATE_HISTORICAL_ALLOCATION
%
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Hillel Raz, 21/04/13
% Added default for case that incomplete exDates are returned from
% database, i.e. not for all bonds. 

import dal.market.get.cash_flow.*; % Because of: get_bond_cf_dates
import utility.*;

global FTSPAYEXDATEMAPPING;

%% Check input arguments type:
flagType(1) = isa(ftsPayExDatesMappingIn, 'fints');
flagType(2) = isa(newSecurityID, 'double');
flagType(3) = isa(firstDate, 'numeric');
flagType(4) = isa(lastDate, 'numeric');

if sum(flagType) ~= length(flagType)
    error('update_fts_pay_ex_dates_mapping:wrongInput', ...
        'One or more input arguments is not of the right type');
end

% ftsNewPayExDateMapping =  get_bond_cf_dates(firstDate, lastDate, newSecurityID);
% load(fullfile(pwd, '+bll', '+cumulative_return', 'fts_pay_ex_date_mapping.mat'));
ftsPayExDateMapping = FTSPAYEXDATEMAPPING.ftsPayExDateMapping;

 %%%%%%%%%%%%%
% With Caching

dates = ftsPayExDateMapping.dates;
fields = strid2numid(fieldnames(ftsPayExDateMapping));
fields = fields(4: end);
mtxCache = fts2mat(ftsPayExDateMapping);
index = [];
for nNewID = 1:length(newSecurityID)
    spotID = find(fields == newSecurityID(nNewID));
    index = [index spotID];
end
%index = find(fields == newSecurityID);

% Keeping fts with only the relevant dates rather than all of them.
mtxCachingForNewSecuritiesAllDates = mtxCache(:, index);
indexOfRelevantDates = sum(isnan(mtxCachingForNewSecuritiesAllDates), 2);
indexOfRelevantDates = find(indexOfRelevantDates ~= length(newSecurityID));
mtxCachingForNewSecuritiesRelevantDates = mtxCachingForNewSecuritiesAllDates(indexOfRelevantDates, :);
datesRelevant = dates(indexOfRelevantDates);
% ftsNewPayExDateMapping = fints(dates, mtxCache(:, index), numid2strid(newSecurityID));
if ~isempty(datesRelevant)
    ftsNewPayExDateMapping = fints(datesRelevant, mtxCachingForNewSecuritiesRelevantDates, numid2strid(newSecurityID));
else
    ftsNewPayExDateMapping = fints();
end

%%%%%%%%%%%%%

if size(ftsNewPayExDateMapping, 2) ~= numel(newSecurityID) || isempty(ftsNewPayExDateMapping) %Ex dates for not-all nsins came back from DB
    if isempty(ftsNewPayExDateMapping) %None came back
        newFieldName = numid2strid(newSecurityID);
        ftsNewPayExDateMapping = fints(ftsPayExDatesMappingIn.dates, NaN(length(ftsPayExDatesMappingIn), numel(newFieldName)), newFieldName);
    else %Some came back
        nsinFields = fieldnames(ftsNewPayExDateMapping);
        nsinList = strid2numid(nsinFields(4:end));
        nsinLeft = setdiff(newSecurityID, nsinList);
        newFieldName = numid2strid(nsinLeft);
        ftsNewPayExDateMappingLeftBonds = fints(ftsPayExDatesMappingIn.dates, NaN(length(ftsPayExDatesMappingIn), numel(newFieldName)), newFieldName);
        ftsNewPayExDateMapping = merge(ftsNewPayExDateMapping, ftsNewPayExDateMappingLeftBonds);
    end
end

ftsPayExDatesMappingOut = merge(ftsPayExDatesMappingIn, ftsNewPayExDateMapping);
ftsPayExDatesMappingOut.desc = 'ex_day';


