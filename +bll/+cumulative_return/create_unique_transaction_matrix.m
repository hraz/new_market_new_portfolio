function mtxTransactionsUnique = create_unique_transaction_matrix(mtxTransactionsNonUnique)

% CREATE_UNIQUE_TRANSACTION_MATRIX adds together the units whenever two rows (or more) have identical
% dates and security id's.
%
%   mtxTransactionsUnique = create_unique_transaction_matrix(mtxTransactionsNonUnique)
%   receives the existing transaction matrix which might not be unique in the sense that two rows might
%   have identical dates and security id's. If this is the case it merged
%   these rows into one where the units is the sum of the seperate units of
%   each row.

%	Input:
%       mtxTransactionsNonUnique - matrix of dimensions (nTransactions)*(5). It
%                                                                      tells how many units of a given security we buy or sell at a given
%                                                                      date. The format is:
%                                                                      date | security_id | unit | TC | amount |
%                                                                      ---------|-----------------------|----------|-----|--------------|
%   Output:
%       mtxTransactionsUnique - similar matrix to mtxTransactionsNonUnique
%       but with unique rows as specified above.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%
%
% Idan Oren, 30/9/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

%% Check input arguments type:

flagType(1) = isa(mtxTransactionsNonUnique, 'double');
if sum(flagType) ~= length(flagType)
    error('create_unique_transaction_matrix:wrongInput', ...
        'One or more input arguments is not of the right type');
end


%% Check input arguments sizes:

if ~isempty(mtxTransactionsNonUnique)
    nColumns1 = size(mtxTransactionsNonUnique, 2);
else
    nColumns1 = 5;
end
sizes = [nColumns1];
flagSizes = sizes == [5];
if sum(flagSizes) ~= length(flagSizes)
     error('create_unique_transaction_matrix:wrongInput', ...
                'One or more input arguments is not of the right size');
end

nTransactions = size(mtxTransactionsNonUnique, 1);
[~, mLast, nLast] = unique(mtxTransactionsNonUnique(:, 1:2), 'rows');
% [~, mFirst, nFirst] = unique(mtxTransactionsNonUnique(:, 1:2), 'rows', 'first');
units = mtxTransactionsNonUnique(:, 3);
if length(unique(nLast)) ~= length(nLast)
x = [nLast units];
for a = 2:nTransactions
    x(a, 3) = x(a, 2);
    if nLast(a-1) == nLast(a)
        x(a, 3) = x(a-1, 3) + x(a, 2);
    end
end
x(1, 3) = mtxTransactionsNonUnique(1, 3);

mtxTransactionsUnique = mtxTransactionsNonUnique(mLast, :);
mtxTransactionsUnique(:, 3) = x(mLast, 3);
else
    mtxTransactionsUnique = mtxTransactionsNonUnique;
end