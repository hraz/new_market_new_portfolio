function [mtxTransactions, cashPerBondOut] = specific_bond_reinvestment(dailyYtmInterestPerBond,...
    cashPerBond, desiredDate, lastCallDate, cashIn, securityIdCashNotInBank)

%   SPECIFIC_BOND_REINVESTMENT reinvests money with interest that is equal to the ytm of the bond
%   from which the cah came from (coupon or redemption).
%
%   [mtxTransactions, cashPerBondOut] = specific_bond_reinvestment(dailyYtmInterestPerBond,...
%   cashPerBond, desiredDate, lastCallDate, cashIn)
%   recieves cash which came from a specific bond. It then reinvests the
%   cash in a risk-free interest which is equal to that bond's ytm.
%	Input:
%       dailyYtmInterestPerBond - double which is equal to the daily
%       interest rate.
%       cashPerBond - a matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of  PREVIOUS cash from each bond.
%       desiredDate - a datenum variable of the current date.
%       lastCallDate - a datenum variable of the last date the function was called upon.
%        cashIn - A matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of cash from each bond.
%       securityIdCashNotInBank - nsin of money which is set aside without
%   Output:
%        mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%        CashPerBondOut - a matrix similar to cashPerBond but updated due to
%        interest and to new cash flow.
%   Sample:
%			Some example of the function using.
%
%		See Also:
%           REINVESTMENT, PORTFOLIO_REINVESTMENT, RISK_FREE_REINVESTMENT,
%           BENCHMARK_REINVESTMENT
%
% created by Idan Oren, 23/7/13
% Copyright 2013, Bond IT Ltd.

if isrow(dailyYtmInterestPerBond)
    dailyYtmInterestPerBond = dailyYtmInterestPerBond';
end
previousCashAmount = sum(cashPerBond(:, 2));
nDaysOfInterest = desiredDate - lastCallDate;
cashAvailable = sum(((1 + dailyYtmInterestPerBond).^nDaysOfInterest).*cashPerBond(:, 2)) + sum(cashIn(:, 2)); %% we raise the..
% interest to the power of number of days which passed since the
% last call to the function happened.
mtxTransactions = zeros(1, 5);
mtxTransactions(1, 1) = datenum(desiredDate);
mtxTransactions(1, 2) = securityIdCashNotInBank;
mtxTransactions(1, 3) =1;
mtxTransactions(1, 4) = 0; %% How do we find this?
mtxTransactions(1, 5) = cashAvailable - previousCashAmount;
%         cashPerBondOut = cashPerBond;
cashPerBondOut(:, 2) = cashPerBond(:, 2).*(1 + dailyYtmInterestPerBond).^nDaysOfInterest + cashIn(:, 2);
