function [mtxTransactions] = set_transactions_matrix( transactionDate, mtxInputAllocation)
%SET_TRANSACTION_MATRIX creates transaction matrix
%
%   [mtxTransactions] = set_transactions_matrix(vecFinalDatesPerBond, transactionDate, mtxHistoricalAllocationOut)
%   function receives a vector of the final dates for each bond (based on last price), the transaction dates for all bonds
%   a matrix describing the portfolio allocation and returns the matrix of
%   transactions.
%
%     input:  
%             transactionDate - Value - date on which to build the
%             transaction matrix
%
%             mtxInputAllocation - matrix of the allocation
%             received from the calc_total_value function
%
%
%     output: mtxTransactions - 1 X 5 matrix describing the transaction.
%                  mtxTransactions(1, 1) - date, mtxTransactions(1, 2) - security id,
%                  mtxTransactions(1, 3) - units of bond, mtxTransactions(1,4) -
%                  transaction costs, mtxTransactions(1, 5) = units*price
%
%Hillel Raz January 2013
%Copyright, BondIT Ltd

%% Constants used
securityIdCashInBank = 9;
securityIdCashNotInBank = 0;
beginSpot = 0;

%% Check if one of the bonds has reached its maturity
lastDayOfBond = transactionDate == vecFinalDatesPerBond; % If reached final day for bond
if sum(lastDayOfBond)
    %Check to see if the allocation includes money in the bank or cash, in
    %which case allocation is shifted down
    if mtxInputAllocation(1, 1) == securityIdCashInBank || mtxInputAllocation(1, 1) == securityIdCashNotInBank
        beginSpot = beginSpot + 1;
    end
    if  mtxInputAllocation(2, 1) == securityIdCashNotInBank
        beginSpot = beginSpot + 1;
    end
    
    for numBond=1: sum(lastDayOfBond)
        %% update transactions matrix
        indexLastDayOfBond = find(lastDayOfBond == 1);
        mtxTransactions(numBond, 1) = transactionDate;
        mtxTransactions(numBond, 2) = mtxInputAllocation(indexLastDayOfBond(numBond)+beginSpot, 1); % Nsin id
        mtxTransactions(numBond, 3) = mtxInputAllocation(indexLastDayOfBond(numBond)+beginSpot, 2); % Count
        mtxTransactions(numBond ,4) = 0; % Transactions cost
        mtxTransactions(numBond, 5) = 0; % Updated to 0 as bond has died
    end
else %only coupon is paid, no transaction
    mtxTransactions = [];
end



