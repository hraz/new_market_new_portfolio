function [bondWanted bondPrice bondYTM] = find_specific_bond_according_to_ytm(reqDate, constraints, ytmSought)
% FIND_SPECIFIC_BOND_ACCORDING_TO_YTM function finds bond with closest YTM to that entered
%
%  [bondWanted bondPrice] = find_specific_bond_according_to_ytm(reqDate, constraints, ytmSought) - function finds living
%      bond with closest YTM to that received from input and returns its nsin and dirty price
%
%     input: reqDate - Datenum - date required for reinvestment
%
%            constraints - data structure containing filtering constraints
%             required: constraints.sectors.sectors_to_include               - CELLARRAY list of sectors to include in filter
%                         constraints.sectors.sectors_to_include_id - DOUBLE NSectorsX1 list of sectors ID to include in filter
%
%             Optional: constraints.static.types_to_include - can have one of two options:
%                       - BOOLEAN vector of NTypes X 1 stating which of the types should be
%                         included in the static filter
%                       - dataset with the correct format for the bond_ttl_multi
%
%                     constraints.static.corpgovToInclude -
%                       - BOOLEAN vector of 2 X 1 stating which of the corp\gov should be included in the static filter
%
%                     ytmSought - nAssets X 1 Double - ytm sought for bond to be found, entered as a decimal
%
%                     benchmark_ID
%
%     output: bondWanted - nAssets  X 1 Double - nsin of bond with closest YTM to ytmSought
%
%             bondPrice - nAssets X 1 Double - dirty price of bond with closest YTM to ytmSought
%
%              bondYTM - nAssets X 1 Double - YTM of bonds with closest YTM to ytmSought
%     See also:   calc_total_value, reinvestment
%
% Hillel Raz, July 23rd, royal baby day
% Copyright 2013, Bond IT Ltd

import bll.filtering_functions.*;
import dal.market.get.dynamic.*;
import utility.*;
import utility.fts.*;
import dal.market.get.market_indx.*;

global FTSALLPRICE;
global GLOBALFTSAllBONDYTM;
global GLOBALCELLYTM602;
global GLOBALCELLYTM603;
global CELLYTM707;
global CELLYTM708;
global CELLYTM709;


%%%%% Without Caching
%% Filter bonds according to initial constraints
% [filtered ] = filter_function(constraints, reqDate);
% nsins = filtered.nsin_list;
% %% Draw YTM values from database for all filtered bonds
% [ftsYTM] = get_multi_dyn_between_dates(unique(nsins), {'ytm'}, 'bond', reqDate, reqDate);
% YTM = fts2mat(ftsYTM.ytm);
% dateIndex = find(ftsYTM.ytm.dates <= reqDate, 1, 'last');


%%%% With Caching
switch constraints.benchmark_id
    case 601
        % load(fullfile(pwd, '+bll', '+cumulative_return', 'ftsAllBondYTM.mat'));
        %ftsYTM = ftsAllBondYTM;
        dateIndex = find(cell2mat(GLOBALFTSAllBONDYTM(:, 1)) <= reqDate, 1, 'last');
        YTM = cell2mat(GLOBALFTSAllBONDYTM(dateIndex, 3));
        ytmNsin = cell2mat(GLOBALFTSAllBONDYTM(dateIndex, 2));
    case 602
        % load(fullfile(pwd, '+bll', '+cumulative_return', 'fts602YTM.mat'));
        % ftsYTM = fts602YTM;
       dateIndex = find(cell2mat(GLOBALCELLYTM602(:, 1)) <= reqDate, 1, 'last');
        YTM = cell2mat(GLOBALCELLYTM602(dateIndex, 3));
        ytmNsin = cell2mat(GLOBALCELLYTM602(dateIndex, 2));
    case 603
        % load(fullfile(pwd, '+bll', '+cumulative_return', 'fts603YTM.mat'));
        % ftsYTM =fts603YTM;
       dateIndex = find(cell2mat(GLOBALCELLYTM603(:, 1)) <= reqDate, 1, 'last');
        YTM = cell2mat(GLOBALCELLYTM603(dateIndex, 3));
        ytmNsin = cell2mat(GLOBALCELLYTM603(dateIndex, 2));
    case 707
        % load cellYTMs.mat;
        cellYTM707 = CELLYTM707;
        dateIndex = find(cell2mat(cellYTM707(:, 1)) <= reqDate, 1, 'last');
        if ~isempty(dateIndex)
            ytmNsin = cellYTM707{dateIndex, 2};
        else
            error('index 707 has no date which matches the required date');
        end
        YTM = cellYTM707{dateIndex, 3};
    case 708
        % load cellYTMs.mat;
        cellYTM708 = CELLYTM708;
        cellYTM709 = CELLYTM709;
        dateIndex = find(cell2mat(cellYTM708(:, 1)) <= reqDate, 1, 'last');
        if ~isempty(dateIndex)
            ytmNsin = cellYTM708{dateIndex, 2};
        else
            error('index 708 has no date which matches the required date');
        end
        [~, nsinsIndex, ~]  = intersect(ytmNsin, cellYTM709{dateIndex, 2});
        YTM = cellYTM709{dateIndex, 3}(nsinsIndex);
    case 709
        % load cellYTMs.mat;
        cellYTM709 = CELLYTM709;
        dateIndex = find(cell2mat(cellYTM709(:, 1)) <= reqDate, 1, 'last');
        if ~isempty(dateIndex)
            YTM = cellYTM709{dateIndex, 3};
        else
            error('index 709 has no date which matches the required date');
        end
        ytmNsin = cellYTM709{dateIndex, 2};
end

datePrice = find(FTSALLPRICE.price_dirty.dates <= reqDate, 1, 'last');
        price = fts2mat(FTSALLPRICE.price_dirty(datePrice));
        fields = strid2numid(fieldnames(FTSALLPRICE.price_dirty));
        priceNsin =fields(4: end-1);



%% Find bond with closest YTM to ytmSought
numBonds = length(ytmSought);
bondWanted = zeros(numBonds,1);
bondPrice = zeros(numBonds, 1);
bondYTM = zeros(numBonds, 1);

% load(fullfile(pwd, '+bll', '+cumulative_return', 'ftsDirtyPriceAllBonds.mat'));
%index = [];
%for iID = 1:length(ytmSought)
%   spotID = find(fields == bondWanted(iID));
%    index = [index spotID];
%end

for k = 1:numBonds
    [~, indexYTM] = sort(abs(YTM-ytmSought(k)));
    bondWanted(k, 1) = ytmNsin(indexYTM(1));
    bondYTM(k, 1) = YTM(indexYTM(1));  %%%Cha
    %With Caching
    spotID = find(priceNsin == bondWanted(k));
    bondPrice(k, 1) = price(spotID);
    % bondPrice(k, 1) = prices(indexYTM(1));
end

%% Get dirty price - without Caching
% ftsPrice = get_multi_dyn_between_dates(unique(bondWanted), {'price_dirty'}, 'bond', reqDate, reqDate);
% prices = fts2mat(ftsPrice.price_dirty);
% % fieldNames = fieldnames(ftsPrice.price_dirty);
% % nsinsPrice = strid2numid(fieldNames(4:end));
% nsinsPrice = strid2numid(tsnames(ftsPrice.price_dirty));
% for j = 1:numBonds
%     indexFound = find(bondWanted(j) == nsinsPrice);
%     bondPrice(j) = prices(indexFound);
% end




