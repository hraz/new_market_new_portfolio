function ftsCashFlowOut = update_coupons(ftsCashFlow, newSecurityID, firstDate, lastDate)


%UPDATE_COUPONS updates the fts which holds the information of cash flows for each security (nsin).
%			
%   ftsCouponOut = update_coupons(ftsCoupon, newSecurityID, firstDate, lastDate)
%   receives the existing fts which holds cash flows of existing securities. It
%   also recieves new security id's  and updates the cash flows fts to
%   hold the information for all the securities.
%	Input:
%       ftsCoupon � fts of dimensions (nDates)*(nSecurities).
%                                                                          The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | cash_flow1       | cash_flow2     | cash_flow3       |
%       newSecurityID �  vector of new security id's (nsins). If this is
%                                             non-empty, the variable ftsCoupon should be updated.
%                                             Otherwise the output argument ftsCouponOut will be
%                                             identiacl to ftsCoupon.
%
%       firstDate - a datenum variable of the date the portfolio was created.   
%       lastDate - a datenum variable of the date the portfolio ends.   
%   Output:
%       ftsCouponOut � fts similar to ftsCoupon but
%       updated due to newSecurityID.
%   Sample:
%			Some example of the function using.
% 	
%		See Also:
%		UPDATE_PRICES, UPDATE_HISTORICAL_ALLOCATION, UPDATE_FTS_PAY_EX_DATES_MAPPING
%	
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

import simulations.*;
import dal.market.get.cash_flow.*; % Because of: get_cash_flow

%% Check input arguments type:
flagType(1) = isa(ftsCashFlow, 'fints');
flagType(2) = isa(newSecurityID, 'double');
flagType(3) = isa(firstDate, 'numeric');
flagType(4) = isa(lastDate, 'numeric');
if sum(flagType) ~= length(flagType)
     error('update_coupons:wrongInput', ...
                'One or more input arguments is not of the right type');
end



newCouponsFTS = get_cash_flow(firstDate, lastDate, newSecurityID);
ftsCashFlowOut = merge(ftsCashFlow, newCouponsFTS);
ftsCashFlowOut.desc = 'cash_flow';






