function mtxTransactionsPartial=cut_relevant_transaction(mtxTransactionsTotal,previous_date,current_date)

rindx = find((mtxTransactionsTotal(:,1)>previous_date)&(mtxTransactionsTotal(:,1)<=current_date) );
mtxTransactionsPartial=mtxTransactionsTotal(rindx,:);