function [flagMultiRedemptionOut] = update_flag_multiple_redemption(flagMultiRedemption, newSecurityID, desiredDate, nsinList)
%UPDATE_FLAG_MULTIPLE_REDEMPTION updates the multiple redemption flag due
%to new bonds added to the portfolio.
%			
%   flagMultiRedemptionOut = update_flag_multiple_redemption(flagMultiRedemption, newSecurityID, desiredDate, nsinList)
%   receives the existing flag which tells if a specific bond is a multiple redemption bond (1) or not (0)
%  It also recieves new security id's  and updates the flag to
%   hold the information for all the securities.
%	Input:
%       flagMultiRedemption - a logical vector of 1's and zeros indicating
%       which bonds are multiple redemption ones.
%       newSecurityID �  vector of new security id's (nsins). If this is
%                                             non-empty, the variable ftsPrice should be updated.
%                                             Otherwise the output argument flagMultiRedemptionOut will be
%                                             identiacl to flagMultiRedemption.
%
%       desiredDate - a datenum variable of the current date.
%       nsinList - the existing bond list (before we add the
%       newSecurityID).
%   Output:
%       flagMultiRedemptionOut � logical vector similar to flagMultiRedemption but
%       updated due to newSecurityID.
%   Sample:
%			Some example of the function using.
% 	
%		See Also:
%		UPDATE_HISTORICAL_ALLOCATION, UPDATE_FTS_PAY_EX_DATES_MAPPING
%	
% Idan Oren, 19/3/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

import simulations.*;
import dal.market.get.dynamic.*; % Because of: get_multi_dyn_between_dates
import dal.market.get.static.*; % Because of: get_bond_stat

%% Check input arguments type:
flagType(1) = isa(flagMultiRedemption, 'logical');
flagType(2) = isa(newSecurityID, 'double');
flagType(3) = isa(desiredDate, 'numeric');
flagType(3) = isa(nsinList, 'numeric');
if sum(flagType) ~= length(flagType)
     error('update_coupons:wrongInput', ...
                'One or more input arguments is not of the right type');
end


%% Check input arguments sizes:

% if sum(flagSizes) ~= length(flagSizes)
%      error('update_prices:wrongInput', ...
%                 'One or more input arguments is not of the right size');
% end
if isrow(newSecurityID)
    newSecurityID = newSecurityID';
end
if isrow(nsinList)
    nsinList = nsinList';
end
if isrow(flagMultiRedemption)
    flagMultiRedemption = flagMultiRedemption';
end

%%%%%%%%%%%%%%% 
%% To check in future - for now all bonds considered multi redemption
%
% dataList = {'redemption_type'};
% nsinData = get_bond_stat(newSecurityID, desiredDate, dataList);
% x = strcmp(nsinData.redemption_type, 'multi');
x = ones(length(newSecurityID), 1)>0;
if isrow(x)
    x = x';
end
flagMultiRedemptionOut = [flagMultiRedemption; x];
nsinList = [nsinList; newSecurityID];
[~, Y] = sort(nsinList);
flagMultiRedemptionOut = flagMultiRedemptionOut(Y);




