function [ftsPayExDatesMappingOut, totalHoldingValue, holdingValuePerBond, ftsCashFlowOut, ftsPriceOut,mtxAllocation , mtxHistoricalAllocationOut, mtxTransactionsOut, cashHoldingValue, ftsRedemptionOut, ftsSumOfRedemptionOut, flagMultiRedemptionOut, cashPerBond, benchmarkFTS, ytmInterestPerBondOut] =...
    calc_total_value(mtxTransactions, ftsCashFlow, mtxInputAllocation, ftsPrice, portfolioDuration, reinvestmentStrategy,...
    desiredDate, constraints, lastCallDate, firstDate, lastDate, ftsPayExDatesMapping, mtxHistoricalAllocation, ftsRedemption, ftsSumOfRedemption, flagMultiRedemption, ytmInterestPerBond, ttmPerBond, cashPerBond, solution, nsin_data, benchmarkFTS)

%CALC_TOTAL_VALUE calculates the total value of the portfolio at a given
%date.
%
%   [ftsPayExDatesMappingOut, totalHoldingValue, holdingValuePerBond, ftsCouponOut, ftsPriceOut, mtxhistoricalAllocationOut] =...
%   calc_total_value(mtxTransactions, ftsCoupon, mtxInputAllocation, ftsPrice, portfolioDuration, reinvestmentStrategy,...
%   desiredDate, benchmark_id, lastCallDate, firstDate, lastDate, ftsPayExDatesMapping, mtxHistoricalAllocation)
%
%   receives:
%   matrix of transactions, fts of coupons, matrix of current allocation,
%   matrix of historical allocation, fts of prices, fts of mapping of pay dates into ex dates, reinvestment
%   strategy and additional input arguments.
%   It computes the total worth of the portfolio and in addition it reinvests available cash according to a given strategy,
%   updates the coupon fts, the price fts, the allocation matrix, the historical allocation matrix and
%   the mapping between pay and ex dates.
%       Input:
%        mtxTransactions ?  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%                                                             Comment: If units are sold then the units entered are negative.
%       ftsCashFlow ? fts of dimensions (nDates)*(nSecurities).
%                                                                          The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | cash_flow1       | cash_flow2     | cash_flow3       |
%       mtxInputAllocation ?  matrix of dimensions (nSecurities)*(3).
%                                                                            The format is:
%                                                                            security_id's | units | price |
%                                                                            -------------------------|------------|-----------|
%       ftsPrice ? Structure with one field (price_dirty) ftsPrice.price_dirty is an fts of dimensions
%                                                                          (nDates)*(nSecurities). The format is:
%                                                                          date   | security_id1 | security_id2 | security_id3 | ...
%                                                                           ----------|-------------------------|------------------------|-------------------------|
%                                                                           date1 | price1                | price2               | price3                |
%       portfolioDuration - the duration of the portflio.
%       reinvestmentStrategy - a string specifying the reinvestment
%                                                            strategy. For now this is only 'risk-free'.
%       desiredDate - a datenum variable of the current date.
%       constraints - data structure - required field : benchmark_id - nsin of benchmark if needed. At this point it should
%                                      be empty.
%       lastCallDate - a datenum variable of the last date the function was called upon.
%       firstDate - a datenum variable of the date the portfolio was created.
%       lastDate - a datenum variable of the date the portfolio ends.
%       ftsPayExDatesMapping ? fts of dimensions (nDates)*(nSecurities).
%                                                                          The format is:
%                                                                          date             | security_id1 | security_id2 | security_id3 | ...
%                                                                           ------------------|-------------------------|------------------------|-------------------------|
%                                                                           pay_date1 | ex_date1            | ex_date2          | ex_date3           |
%       mtxHistoricalAllocation ? matrix of dimensions (nDates*securities at each date)*(4).
%                                                                          The format is:
%                                                                          date | security_id's | units | price |
%                                                                           --------|--------------------------|------------|-----------|
%       ftsRedemption - fts of redemptions of bonds given in percents.
%       ftsSumOfRedemption - fts of the sum of redemptions of a bond starting today until the bond dies.
%       flagMultiRedemption - a vector of dimensions (nSecurities)*(1) of
%       1's and 0's. 1 is for securities with multi-redemption and 0 for
%       other securities.
%       ytmInterestPerBond - a vector of size (Nnsin)which holds the ytm of each bond at the beginning of the portfolio.
%       CashPerBond - a matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of  PREVIOUS cash from each bond.
%   Output:
%       ftsPayExDatesMappingOut - fts similar to ftsPayExDatesMapping but
%       updated due to new securities which might be purchased during the reinvestment process.
%       totalHoldingValue - a scalar indicating the total worth of the
%       portfolio. This is the main objective of the function!
%       holdingValuePerBond - List of bonds in the portfolio along with price*units for each bond.
%       ftsCashFlowOut - fts similar to ftsCashFlow but
%       updated due to new securities which might be purchased during the reinvestment process.
%       ftsPriceOut - fts similar to ftsPrice but
%       updated due to new securities which might be purchased during the reinvestment process.
%       mtxhistoricalAllocationOut - fts similar to mtxhistoricalAllocation but
%       updated due to the current allocation and the new transactions and
%       reinvestment in the portfolio.
%       cashPerBond - a matrix of size (Nnsin)X(2). The first column is a list of
%                           securities and the second column is the amount of cash from each bond.
%   Sample:
%                       Some example of the function using.
%
%               See Also:
%               MODIFY_ALLOCATION, UPDATE_COUPONS, UPDATE_PRICES,
%               UPDATE_FTS_PAY_EX_DATES_MAPPING, UPDATE_HISTORICAL_ALLOCATION,...
%       REINVESTMENT, CASH_FLOW_FROM_COUPONS_ACCORDING_TO_EX_DATES
%
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Hillel Raz, 28/07/13 Added ytmInterestPerBond as an input parameter, 
% Added as an output - ytmInterestPerBondOut, added && (~strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment'))
% to choice of cashToReinvest, added as an output - ytmInterestPerBondOut



%%

global NBUSINESSDAYS;
import dal.market.get.cash_flow.*;  % Because of: get_cash_flow (in update_prices) and...
%get_bond_cf_dates (in update_fts_pay_ex_dates_mapping)
import dal.market.get.base_data.*; % Because of: da_nominal_yield_between_dates (in modify_allocation)...
% and get_nominal_yield_with_param (in reinvestment)
import dal.market.get.dynamic.*; % Because of: get_multi_dyn_between_dates (in update_prices)
import utility.*; % Because of: strid2numid (in cash_flow_from_coupons_according_to_ex_dates)
import dal.market.get.static.*; % Because of: get_bond_stat (in update_flag_multiple_redemption)
import bll.cumulative_return.*;

% try

%% Check input arguments type:
flagType(1) = isa(mtxTransactions, 'double');
flagType(2) = isa(ftsCashFlow, 'fints');
flagType(3) = isa(mtxInputAllocation, 'double');
flagType(4) = isa(ftsPrice, 'struct');
flagType(5) = isa(portfolioDuration, 'double');
flagType(6) = isa(reinvestmentStrategy, 'char');
flagType(7) = isa(desiredDate, 'numeric');
if ~isempty(constraints.benchmark_id)
    flagType(8) = isa(constraints.benchmark_id, 'double');
else
    flagType(8) = 1;
end
flagType(9) = isa(lastCallDate, 'numeric');
flagType(10) = isa(firstDate, 'numeric');
flagType(11) = isa(lastDate, 'numeric');
flagType(12) = isa(ftsPayExDatesMapping, 'fints');
flagType(13) = isa(mtxHistoricalAllocation, 'double');
flagType(14) = isa(ftsRedemption, 'fints');
flagType(15) = isa(ftsSumOfRedemption, 'fints');
flagType(16) = isa(flagMultiRedemption, 'logical');
flagType(17) = isa(ytmInterestPerBond, 'double');
flagType(18) = isa(cashPerBond, 'double');
if sum(flagType) ~= length(flagType)
    error('reinvestment:wrongInput', ...
        'One or more input arguments is not of the right type');
end
%% Check input arguments sizes:
if ~isempty(mtxTransactions)
    nColumns1 = size(mtxTransactions, 2);
else
    nColumns1 = 5;
end
nColumns2 = size(mtxInputAllocation, 2);
if ~isempty(mtxHistoricalAllocation)
    nColumns3 = size(mtxHistoricalAllocation, 2);
else
    nColumns3 = 4;
end
sizes = [nColumns1 nColumns2 nColumns3];
flagSizes = sizes == [5 3 4];
if sum(flagSizes) ~= length(flagSizes)
    error('reinvestment:wrongInput', ...
        'One or more input arguments is not of the right size');
end


%% Stage One: This part gives the cash from selling bonds

cashFromSellingBonds = 0;
if ~isempty(mtxTransactions)
    transactionCost = mtxTransactions(:, 4);
    amount = mtxTransactions(:, 5);
    x = amount<0;
    cashFromSellingBonds = -sum(amount(x))-sum(transactionCost(x)); % The minus signs are because when we sell a bond the units...
    % and amount come with negative signs (and the correspondin cash is
    % positive). The transaction costs are positive (and in the cash
    % calculation they should be negative).
end


%% Stage Two: Money calculated from Coupons (Cash). Notice that the calculation is carried out before the
%    allocation is (possibly) changed due to multi-redemption.
mtxAllocation = mtxInputAllocation;
%units = mtxAllocation(:,2); %%%% CHECK THIS!!!
% cashFromCoupons = cash_flow_from_coupons(couponFTS, desiredDate, units, reinvestmentStrategy);
% cashFromCoupons = cash_flow_from_coupons_according_to_ex_dates(ftsCoupon, ftsPayExDatesMapping,...
%                                         desiredDate, mtxHistoricalAllocation, reinvestmentStrategy);
cashFromCoupons = cash_flow_from_coupons_according_to_ex_dates(ftsCashFlow, ftsPayExDatesMapping,...
    desiredDate, mtxHistoricalAllocation, reinvestmentStrategy, mtxAllocation);
% if isempty(cashFromCoupons)
%     cashFromCoupons = 0;
% end

%% Stage Three: This part changes allocation due to mutiple redemption (reduces number of units)

% the following lines check if we need to go into
% modify_allocation_due_to_multiple_redemption at all by checking if there are
% any redemptions taking place in desiredDate. If there is a
% redemption, the function is called upon.
indexOfRedemptionDate = find(ftsRedemption.dates == desiredDate);
mtxRedemption = fts2mat(ftsRedemption);
relevantLineInMtxRedemption = mtxRedemption(indexOfRedemptionDate, :);
relevantLineInMtxRedemption(isnan(relevantLineInMtxRedemption)) = 0;
if sum(relevantLineInMtxRedemption) ~= 0
    %[mtxTransactionsOut1, mtxAllocation] = modify_allocation_due_to_multiple_redemption(ftsRedemption, ftsSumOfRedemption, flagMultiRedemption, mtxAllocation, desiredDate);
    [mtxTransactionsOut1, mtxAllocation] = modify_allocation_due_to_multiple_redemption(ftsRedemption, ftsSumOfRedemption, flagMultiRedemption, mtxAllocation, desiredDate, ...
        ftsCashFlow, ftsPayExDatesMapping, mtxHistoricalAllocation);
    
else
    mtxTransactionsOut1 = [];
end

%% Stage Four: Modify allocation due to transactions

if ~isempty(mtxTransactions)
    [mtxAllocation] = modify_allocation(mtxTransactions, mtxInputAllocation, portfolioDuration, lastCallDate, desiredDate, constraints.benchmark_id);
end


%% Stage Five: Reinvestment of money

nDaysToMaturity = lastDate-desiredDate;
if nDaysToMaturity>=365
    timeToMaturity = floor(nDaysToMaturity/365);
else
    timeToMaturity = round(1/12*floor(nDaysToMaturity/30)*10000)/10000;
end
if timeToMaturity == 0
    if ~strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment');
        reinvestmentStrategy = 'none';
    else
        % reinvestmentStrategy = 'none'; %% For Debugging!!!
        reinvestmentStrategy = 'Specific-Bond-Reinvestment'; %***
    end
end
if (~strcmp(reinvestmentStrategy, 'Specific-Bond-Reinvestment'))... %%Redundant. Get rid of this or maybe FIX
&& (~strcmp(reinvestmentStrategy, 'benchmark')) && (~strcmp(reinvestmentStrategy, 'real-specific-bond-reinvestment'))
    cashToReinvest = cashFromSellingBonds+sum(cashFromCoupons(:, 2));
    % cashToReinvest = cashFromCoupons;
else
    cashToReinvest = cashFromCoupons;
end

%%% ADDED FOR DEBUGGING - TO BE USED IN CASE THAT WANT TO RUN FUNCTION
%%% WITOUT REINVESTING

% if cashToReinvest>0
%     if mtxAllocation(1,1) == 0
%         mtxAllocation(1,3) = mtxAllocation(1,3) + cashToReinvest;
%     else
% mtxAllocation2 = zeros(size(mtxAllocation, 1)+1, 3);
% mtxAllocation2(2:end, :) = mtxAllocation;
% mtxAllocation2(1,3) = cashToReinvest;
% mtxAllocation2(1,2) = 1;
% mtxAllocation = mtxAllocation2;
%     end
% end
%     mtxTransactionsOut2 = [];
%     mtxTransactionsOut = [mtxTransactionsOut1; mtxTransactionsOut2];

%%%%%

if size(cashToReinvest, 2) == 2
    flagIsThereMoneyToReinvest = sum(cashToReinvest(:, 2)>0);
else
    flagIsThereMoneyToReinvest = sum(cashToReinvest>0);
end

%if flagIsThereMoneyToReinvest>0  ***

dailyYtmInterestPerBond = (1 + ytmInterestPerBond).^(1/365) - 1;
% ytm = solution.PortWts*nsin_data.YTM(solution.selected_indx);
% dailyYtmInterestPerBond = ((1 + ytm)^(1/(252*solution.PortDuration)) - 1)*ones(length(solution.selected_indx), 1);

%     timeToMaturity = 5; % floor((datenum(lastDate)-datenum(desiredDate))/365); %% Check if floor or round %% FIX THIS LINE
[mtxAllocation, mtxTransactionsOut2, benchmarkFlag, cashPerBond, benchmarkFTS, ytmInterestPerBondOut] = reinvestment(mtxAllocation, reinvestmentStrategy, cashToReinvest,...
    timeToMaturity, lastCallDate, desiredDate, constraints, cashPerBond, dailyYtmInterestPerBond, benchmarkFTS, ytmInterestPerBond, nsin_data.riskFreeInfo);
%else
%  mtxTransactionsOut2 = [];
%end
if (size(mtxTransactionsOut2, 1) == 1) && (mtxTransactionsOut2(1, 5) == 0)
    mtxTransactionsOut2 = [];
end
mtxTransactionsOut = [mtxTransactionsOut1; mtxTransactionsOut2];
if ~isempty(mtxTransactionsOut)
    mtxTransactionsOut = create_unique_transaction_matrix(mtxTransactionsOut); %%% making it unique, i.e. no two rows have both the same date and the same security id.
end

%% Stage Six: Updating th price matrix and the coupon matrix

ftsPriceOut = ftsPrice;
ftsCashFlowOut = ftsCashFlow;
ftsPayExDatesMappingOut  = ftsPayExDatesMapping;
ftsRedemptionOut = ftsRedemption;
ftsSumOfRedemptionOut = ftsSumOfRedemption;
flagMultiRedemptionOut = flagMultiRedemption;
securityIdCashInBank = 9;
securityIdCashNotInBank = 0;
newSecurityID = setdiff(union(mtxInputAllocation(:,1), mtxAllocation(:,1)), intersect(mtxInputAllocation(:, 1), mtxAllocation(:, 1)));
newSecurityID = setdiff(newSecurityID, [securityIdCashInBank securityIdCashNotInBank]);
if ~isempty(newSecurityID)
    if benchmarkFlag==0
        tblName = 'bond';
    else
        tblName = 'index';
    end
    ftsPriceOut = update_prices(ftsPrice, newSecurityID, firstDate, lastDate, [], tblName);
    % ftsCashFlowOut = update_coupons(ftsCashFlow, newSecurityID, firstDate, lastDate);
    [ftsCashFlowOut, ftsRedemptionOut, ftsSumOfRedemptionOut] = update_redemption_and_sum_of_redemption_and_cash_flow(ftsRedemption, ftsSumOfRedemption, ftsCashFlow, newSecurityID, firstDate, lastDate);
    ftsPayExDatesMappingOut = update_fts_pay_ex_dates_mapping(ftsPayExDatesMapping, newSecurityID, firstDate, lastDate);
    couponFields = fieldnames(ftsCashFlow);
    nsinList = strid2numid(couponFields(4:end));
    flagMultiRedemptionOut = update_flag_multiple_redemption(flagMultiRedemption, newSecurityID, desiredDate, nsinList);
end


%% Stage Seven: Computing the value of the portfolio and summing all together.

%mtxAllocation = remove_dead_bonds_from_allocation(mtxAllocation, timeToMaturity, lastCallDate, desiredDate, benchmark_id); 
units = mtxAllocation(:,2);
prices = mtxAllocation(:,3);
portfolio_holding = prices'*units;
isThereCash = find (mtxAllocation(:, 1)==0, 1);
if ~isempty(isThereCash)
    cashHoldingValue =  mtxAllocation(isThereCash, 3); % cashFromSellingBonds + cashFromCoupons;
else
    cashHoldingValue = 0;
end
totalHoldingValue = portfolio_holding;
holdingValuePerBond = zeros(size(mtxAllocation, 1), 2);
holdingValuePerBond(:, 1) = mtxAllocation(:, 1);
holdingValuePerBond(:, 2) = prices.*units;


%% Stage Eight: Updating the historical allocation matrix

mtxHistoricalAllocationOut = update_historical_allocation(mtxHistoricalAllocation, mtxAllocation, desiredDate);

% catch ME
%     err_msg = [ME.message];
%     display(err_msg);
% end
