% clc; clear;
% import dal.market.get.static.*;
% import bll.cumulative_return.*;
% % load 'D:\GitRepo\simulation_code\bll\cumulative_return\tests\solution_test_envelope.mat';
% % load 'D:\GitRepo\simulation_code\bll\cumulative_return\tests\solution_test_envelope2.mat';
% % load 'D:\GitRepo\simulation_code\bll\cumulative_return\tests\solution_test_envelope3.mat';
%  load 'D:\GitRepo\simulation_code\bll\cumulative_return\tests\gov_only_bonds_test.mat'
% %load 'D:\GitRepo\simulation_code\bll\cumulative_return\tests\marko_bonds_test.mat'
% 
% % dateBegPort = 734497; %for first test
% % dateBegPort = 733204; %for second and third tests
%  dateBegPort = 734197; %date for gov test
% %dateBegPort = 734352; %for marko
% benchmarkID = [];
% 
%  data_list = {'redemption_type'}; 
% [ ds ] = get_bond_stat( solution.nsin_sol, dateBegPort, data_list );
%         solution.flagMultiRedemption = strcmp(ds.redemption_type, 'multi');
% solution.dateEndSim = dateBegPort + ceil(365*solution.PortDuration);
% [ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, nsin_data, dateBegPort, solution.dateEndSim, benchmarkID )


import bll.cumulative_return.*;
import dal.market.get.dynamic.*;
import dal.market.get.static.*;
global NBUSINESSDAYS
NBUSINESSDAYS = 252;

benchmarkID = 'AllBonds';
dateBegPort = datenum('17-May-2007');
dateFinalData = dateBegPort + 6*365-100;
%solution.nsin_sol = [1095033]; 
solution.nsin_sol = [1095033, 3960044, 9570235];
solution.count = 100*(ones(length(solution.nsin_sol), 1));
    [ftsPrice] = get_multi_dyn_between_dates(solution.nsin_sol, {'price_dirty', 'ytm', 'ttm'}, 'bond', dateBegPort, dateBegPort);
solution.initial_prices = fts2mat(ftsPrice.price_dirty);
nsin_data.YTM = fts2mat(ftsPrice.ytm);
nsin_data.Duration = fts2mat(ftsPrice.ttm);
solution.selected_indx = find(solution.nsin_sol > 0 );
solution.PortDuration = 2;
solution.ReinvestmentStrategy = 'Specific-Bond-Reinvestment';
solution.NumBondsSol = length(solution.nsin_sol);
 data_list = {'redemption_type'}; %{'nsin', 'name', 'bond_type','linkage_index','coupon_type','sector'};
x= get_bond_stat( solution.nsin_sol, dateBegPort, data_list);
solution.flagMultiRedemption = strcmp(x.redemption_type, 'multi');
[ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, nsin_data, dateBegPort, dateFinalData, benchmarkID )