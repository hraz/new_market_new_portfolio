import bll.cumulative_return.*;
import dal.market.get.dynamic.*;
import dal.market.get.static.*;

global NBUSINESSDAYS
NBUSINESSDAYS = 252;


benchmarkID = 'AllBonds';
dateBegPort = datenum('17-May-2007');
dateFinalData = dateBegPort + 6*365-100;
solution.nsin_sol =[9570235];
solution.count = 100*(ones(length(solution.nsin_sol), 1));
    [ftsPrice] = get_multi_dyn_between_dates(solution.nsin_sol, {'price_dirty', 'ytm', 'ttm'}, 'bond', dateBegPort, dateBegPort);
solution.initial_prices = fts2mat(ftsPrice.price_dirty);
nsin_data.YTM = fts2mat(ftsPrice.ytm);
nsin_data.Duration = fts2mat(ftsPrice.ttm);
solution.selected_indx = find(solution.nsin_sol > 0 );
solution.PortDuration = 6;
solution.ReinvestmentStrategy = 'Specific-Bond-Reinvestment';
solution.NumBondsSol = length(solution.nsin_sol);

 data_list = {'redemption_type'}; %{'nsin', 'name', 'bond_type','linkage_index','coupon_type','sector'};
 x= get_bond_stat( solution.nsin_sol, dateBegPort, data_list);
solution.flagMultiRedemption = strcmp(x.redemption_type, 'multi');

[ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, nsin_data, dateBegPort, dateFinalData, benchmarkID )