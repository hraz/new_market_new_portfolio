clc; clear;

import dal.market.get.dynamic.*;
import dml.*;
import bll.*;

first_date = datenum('2005-01-11');
portfolio_maturity_date = datenum('2011-11-23');

 nsin = [1100940;1089960;1093624;1094523;2600195;1081546;1081777];
data_name = {'price_dirty'};

 tbl_name = 'bond';

 [ ftsPrice ] = get_multi_dyn_between_dates( nsin , data_name , tbl_name, first_date, portfolio_maturity_date );
 
 [vecFinalDatesPerBond] = get_first_date_of_nan_prices(ftsPrice, portfolio_maturity_date);
