function [ ucg ] = fivi( cg, alpha )
%FIVI full information value index approach to unsmooth non-liquid assets' return.
%
%   [ ucg ] = fivi( cg, alpha )
%   In the FIVI model, the first order autoregressive filter that uses volatility of residuals to
%   compute the weights used in calculating the unsmoothed capital rate. 
%   The approach assumes the following:
%       1) The mean is the same for the adjusted (unsmoothed) and unadjusted (smoothed) return
%       series.
%       2) The model parameters are the same over time.
%       3) The unsmoothed capital growth rate, UCG, is calculated as:
%
%                   UCG = [CG(t) - alpha * CG(t-1)] / weight
%
%                   weight = 2 * std( [CG(t) - alpha * CG(t-1)] ) / std( CG(t) )
%           where:
%           CG(t)	- capital growth for time t
%           alpha	- unsmoothing parameter
%           std       - standard deviation
%
%   Input:
%       cg - is an NOBSERVATIONSxMASSETS financial time series specifying some smoothing process.
%
%       alpha - is a scalar value specifying the unsmoothing parameter (0.4 < alpha < 0.6).
%
%   Output:
%       ucg - is (N-1)OBSERVATIONSxMASSETS financial timeseries specifying the unsmoothed process.
%

% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import utility.fts.*;
    
    %%  Input validation:
    error(nargchk(2,2,nargin));
    if isempty(cg)
        error('unsmoothing:fivi:mismatchInput', 'An empty value of the first argument.');
    elseif ~isa(cg, 'fints')
        error('unsmoothing:fivi:wrongInput', 'Wrong type of the first argument.');
    end
    if isempty(alpha)
        error('unsmoothing:fivi:mismatchInput', 'An empty value of the second argument.');
    elseif ~isnumeric(alpha)
        error('unsmoothing:fivi:wrongInput', 'Wrong type of the second argument.');
    end
    
    %% Step #1 (Get data from the cg): 
    names = tsnames(cg);
    dates = cg.dates(2:end);
    cgt = fts2mat(cg(2:end));
    cgt_1 = fts2mat(cg(1:end-1));
    
    %% Step #2 (Calculate denominator):
    sigma_equity = nanstd(cg);
    residuals = (cgt - alpha .* cgt_1);
    sigma_residuals = nanstd(residuals);
    weight = 2 * sigma_residuals / sigma_equity;
    
    %% Step #3 (Unsmooth the given data):
    unsmoothed_data = residuals ./ weight;
    
    %% Step #4 (Create the return fints):
    ucg = fints(dates, unsmoothed_data, names);

end