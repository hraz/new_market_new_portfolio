%% Database output format:
setdbprefs ('DataReturnFormat','dataset');

%% Numerical NULL read format:
setdbprefs ('NullNumberRead', 'NaN');

%% Numerical NULL write format:
setdbprefs('NullNumberWrite', '');

%% Disabling warnings:
warning off;
