function new_save(obj)

import dal.portfolio.set.*;
import utility.portfolio.*;

global PUBLIC_FLAG_DEFAULT
global VIRTUAL_FLAG_DEFAULT
global BASED_ON_FLAG_DEFAULT
global EXP_RETURN_CALC_METHOD;
global EXP_VOL_CALC_METHOD;

if isempty(obj.account_id) && isempty(obj.ftsCumulative_return) && ( obj.last_update==obj.creation_date )
    firstSaveFlag=1;
elseif isempty(obj.account_id) || isempty(obj.ftsCumulative_return) || ( obj.last_update==obj.creation_date )
    error('contradiction in first-save flag')
else
    firstSaveFlag=0;
end

inPortfolioID=obj.account_id;
inName=obj.account_name;
inDescription='';
inCreationDate=obj.creation_date;
inMaturityDate=obj.creation_date+floor(365*obj.model.real_holding_period);%solution.PortDuration;
inPublicFlag=PUBLIC_FLAG_DEFAULT;
inVirtualFlag=VIRTUAL_FLAG_DEFAULT;
inBasedOnFlag=BASED_ON_FLAG_DEFAULT;

[outPortfolioID, outStatus, outException] = set_summary (inPortfolioID, ...
    inName, inDescription, inCreationDate, inMaturityDate,inPublicFlag, ...
    inVirtualFlag, inBasedOnFlag);
if isempty(inPortfolioID)
    inPortfolioID=outPortfolioID;
end

%% allocation and transactions
inDate=obj.last_update;
securityID=obj.allocation.nsin;
unitNum=obj.allocation.count;
actionAmount=obj.allocation.holding;
cashIndx=find(securityID==0);
if ~isempty(cashIndx)
    securityID(cashIndx)=[];
    unitNum(cashIndx)=[];
    actionAmount(cashIndx)=[];
end
inAllocationTbl=dataset(securityID,unitNum);

if firstSaveFlag
    % need to manually add transactions of portfolio creation
    update=inDate*ones(length(securityID),1);
    transactionCost=zeros(length(securityID),1);
    inTransactionData=dataset(update,securityID,unitNum,transactionCost,actionAmount);
else
    % otherwise the transactions are all data needed
    inTransactionData=mat2dataset(obj.mtxTransactionsTotal,'VarNames',{'update','securityID','unitNum','transactionCost','actionAmount'});
end

[outStatus, outException] = set_allocation(inPortfolioID ,inDate, inAllocationTbl, inTransactionData);

%% total value and cash (income status)
if ~isempty(obj.ftsCumulative_return)
    %the above condition should be the same as ~firstSaveFlag
    inDate=obj.ftsCumulative_return.dates;
    holdingValue=fts2mat(obj.ftsCumulative_return);    
    cashAmount=fts2mat(obj.ftsCumulative_return_perBond.id0);
    holdingValue(isnan(holdingValue))=0;
    cashAmount(isnan(cashAmount))=0;
    % note that the above two lines should not be moved outside of this
    % function, meaning the cleaning of NaN's should not be made in the
    % 'original' (obj.ftsCumulative_return_perBond etc) since these NaN's
    % are being used for the per-bond-VaR
    
    inIncomeData=dataset(inDate,holdingValue,cashAmount);
    [outStatus, outException] = set_income_status(inPortfolioID ,inIncomeData);
end

%%

expReturnCalcMethod=EXP_RETURN_CALC_METHOD;
expVolatilityCalcMethod=EXP_VOL_CALC_METHOD;
valueAtRisk=obj.risk.value_at_risk.v_a_r;
cVar=obj.risk.value_at_risk.cond_v_a_r;
inDate=obj.last_update;

if firstSaveFlag
    %the case of saving for the first time
    inPerformanceMeasures=dataset();
    inPerBondVaRStatistics=dataset();
    inBenchmarkStatistic=dataset();
    inExcessStatistic=dataset();
    
    expectedReturn=obj.model.expected_return;
    expectedVolatility=obj.model.expected_volatility;
else
   %the case of saving not the first time
    current_holding_period = (obj.last_update - obj.creation_date)/365; % period in years
    holdingReturn=(obj.cumulative_return.value(end)/obj.cumulative_return.value(1))...
        ^(1/current_holding_period) - 1;
    moneyWeightedReturn=NaN;
    timeWeigthedReturn=NaN;
    volatility=obj.risk.statistic.volatility;
    downsideVolatility=NaN;
    skewness=obj.risk.statistic.skewness;
    kurtosis=obj.risk.statistic.kurtosis;
    alpha=obj.performance.capm.alpha;
    beta=obj.performance.capm.beta;
    uniqueRisk=obj.performance.capm.unique_risk;
    sharpe=obj.performance.fin.sharpe;
    jensen=obj.performance.fin.jensen;
    omega=obj.performance.fin.omega;
    infRatio=obj.performance.fin.inf_ratio;
    rsquareVsBench=obj.performance.rsquare.rsquare_vs_bench;
    rmseVsBench=obj.performance.rsquare.rmse_vs_bench;
    inPerformanceMeasures=dataset(holdingReturn, moneyWeightedReturn, timeWeigthedReturn, ...
        volatility, downsideVolatility, skewness, kurtosis, alpha, beta, uniqueRisk, sharpe, ...
        jensen, omega, infRatio, rsquareVsBench, rmseVsBench);
    
    marginalVaR=obj.risk.value_at_risk.marginal_v_a_r;
    componentVaR=obj.risk.value_at_risk.component_v_a_r;
    inPerBondVaRStatistics=dataset(securityID(1),marginalVaR,componentVaR,'VarNames',{'securityId' ...
        'marginalVaR','componentVaR'});
    % above is a temporary code, just for first debugging. full marginal and
    % component VaR are not saved but they weren't saved in the old pDB as
    % well...
    
    benchmarkID=obj.model.benchmark_id;
    bchHoldingReturn=obj.benchmark.holding_return;
    bchVolatility=obj.benchmark.volatility;
    bchSkewness=obj.benchmark.skewness;
    bchKurtosis=obj.benchmark.kurtosis;
    bchSharpe=obj.benchmark.sharpe;
    bchFieldNames={'benchmarkID' 'holdingReturn' 'volatility' 'skewness' 'kurtosis' 'sharpe'};
    inBenchmarkStatistic=dataset(benchmarkID, bchHoldingReturn, bchVolatility, bchSkewness, ...
        bchKurtosis, bchSharpe, 'VarNames', bchFieldNames);
    
    delta_1=obj.deltas.delta1;
    delta_2=obj.deltas.delta2;
    delta_3=obj.deltas.delta3;
    inExcessStatistic=dataset(delta_1,delta_2,delta_3);
    
    %averageReturn=obj.risk.statistic.port_return;
    % the above value will be assigned after Yigal will add it to the new
    % portfolio DB
    
    expectedReturn=obj.model.expected_return;
    expectedVolatility=obj.model.expected_volatility;
end

inRiskMeasures=dataset(expectedReturn, expectedVolatility, expReturnCalcMethod, ...
    expVolatilityCalcMethod, valueAtRisk, cVar);

[outStatus, outException] = set_statistics(inPortfolioID, inDate, ...
    inRiskMeasures, inPerformanceMeasures, inPerBondVaRStatistics, ...
    inBenchmarkStatistic, inExcessStatistic);

%% constraints

dsStaticConstraints = static_constraints_for_save(obj.static_constraints, obj.model.benchmark_id);

dsDynamicConstraints = dyn_constraints_for_save(obj.dyn_constraints);

dsSectorConstraints = sectors_constraints_for_save(obj.sectors_constraints);

dsRatingConstraints = rating_constraints_for_save(obj.rating_constraints);

basic_constraints.investedAmount=obj.initial_amount;
basic_constraints.port_duration_sought=obj.model.req_holding_horizon;
basic_constraints.histLen=obj.model.reinv_low_bnd; 
basic_constraints.maxWeight=obj.model.max_allocation_weight;
basic_constraints.VaRPercentile=obj.model.reinv_up_bnd;
dsBasicConstraint = basic_constraints_for_save(basic_constraints);

target_constraints.optimization_type = obj.model.optimization_type{1};
target_constraints.target_return = obj.model.expected_return;
dsTargetConstraints = target_constraints_for_save(target_constraints);

model_constraints.model=obj.model.model{1};
model_constraints.optimization='price_clean_yld'; % hard coded, change later ?
model_constraints.reinvestmentStrategy=obj.ReinvestmentStrategy;
dsModelConstraints =  model_constraints_for_save(model_constraints);

transactionCost_constraints=0;
dsTaxConstraints = tax_constraints_for_save(transactionCost_constraints);

