clear ;
clc;

import dml.portfolio.*;
import dal.market.get.dynamic.*;

obj = portfolio();

allocation = dataset(); % nsin|count
first_date = []; % start time period date
last_date = []; % end time period date
ir_lb = []; % low bound of reinvestment interest rate
ir_ub = []; % upper  bound of reinvestment interest rate

[ cumulative_holding_ret, holding_ret, holding_income, stop_point ] = get_bond_holding_return( allocation, first_date, last_date, ir_lb, ir_ub, []); 
asset_data = cumulative_holding_ret;
[ exp_ret, volatility, skew, kurt ] = stat( obj, asset_data );