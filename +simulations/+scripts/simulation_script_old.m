function simulation_script(inString)

global DBSOURCE     %TO COPY
global NBUSINESSDAYS

NBUSINESSDAYS = 252;

import dml.*;
import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;

import simulations.scripts.*;  

import bll.solver_calculations.*;
import dal.portfolio.data_access.*;
dbstop if error

tic;
min_num = 1; max_num = 894;
TIME = nan(max_num-min_num+1,2);
[ sim_num ] = da_get_sim_id(min_num,max_num  );
% sims = [400];
while ~isempty(sim_num)&&~isnan(sim_num)
%     for iSimNum = 1:length(sims)
                        
%     DBSOURCE = 'nosql';
%     t1 = tic;
%     simulation_function(sim_num);
%     TIME(sim_num,1) = toc(t1);
%     sim_num=sims(iSimNum);
    DBSOURCE = 'mysql';
    t2 = tic;
    simulation_function(sim_num);
    TIME(sim_num,2) = toc(t2);

    [ sim_num ] = da_get_sim_id(min_num,max_num  );

end

toc

%  save(fullfile(pwd, 'NoSQLTiming.mat'),'TIME');
