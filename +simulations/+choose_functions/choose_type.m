function [constraints] = choose_type(type_flag, constraints)
%CHOOSE_TYPE - sets constraints to filter via types.
%
%   [constraints] = choose_type(type_flag, constraints)
%   function receives type_flag and the constraints data structure and sets
%   the wanted types accordingly
%
%   input:  constraints - data structure containing user's choices for
%               filtering bonds
%           type_flag - string. options are:
%               'Gov' - only government types
%               'Cor' - only coroporate types
%               'G&C' - all types
%               'No-Link' - both corporate and government bonds without
%               linkage
%
%
%    output: constraints with types' choice updated
%

switch type_flag
    
    case  'Gov'
        % solver_props.UserBondTypes=[0 0 0 0 1 1 1];%all gov
        constraints.static.types_to_include = [0 0 0 0 1 1 0];
        constraints.static.minWeight = [0 0 0 0 0 0 0]';
        constraints.static.maxWeight = [0 0 0 0 1 1 0]';
    case 'Cor'
        % solver_props.UserBondTypes=[1 1 1 1 0 0 0]; %all cor
        constraints.static.types_to_include =[1 1 0 1 0 0 0];
        constraints.static.minWeight = [0 0 0 0 0 0 0]';
        constraints.static.maxWeight = [1 1 0 1 0 0 0]';
    case 'G&C'
        % solver_props.UserBondTypes=[1 1 1 1 1 1 1]; %all
        constraints.static.types_to_include =[1 1 0 1 1 1 0];
        constraints.static.minWeight = [0 0 0 0 0 0 0]';
        constraints.static.maxWeight =[1 1 0 1 1 1 0]';
    case 'No-Link'
        % solver_props.UserBondTypes=[1 1 0 0 1 1 0];
        constraints.static.types_to_include =[1 1 0 0 1 1 0];
        constraints.static.minWeight = [0 0 0 0 0 0 0]';
        constraints.static.maxWeight =[1 1 0 0 1 1 0]';
    otherwise % Default
        constraints.static.types_to_include = [ 1 1 0 1 1 1 0];
        constraints.static.minWeight = [0 0 0 0 0 0 0]';
        constraints.static.maxWeight = [1 1 0 1 1 1 0]';
        
end

