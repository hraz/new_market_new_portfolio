function [solver_props] = choose_frontier_point(point_choice, target_choice, solver_props)

% function receives point_choice and target_choice and chooses the
% corresponding optimization model and possible the wanted target return
% specified in the target choice.
%
% input:
%        point_choice - string.   'MV' - mean variance - second digit = 1
%                               'OS' - optimal sharpe - second digit = 2
%                               'Tar' - target return, specific target
%                                   sought specified in target_choice - second
%                                   digit = 3
%                               'TarOS' - target + optimal sharpe point on
%                                   the efficient frontier - second digit = 4
%                               'Om' - Omega - second digit = 5
%                               'HighMom' - Skewness Kurtosis - second
%                                   digit = 6
%
%
%       target_choice - VALUE specifying target return wanted
%
%       solver_props - structure containing the needed properties for the solver
%
%
% output:
%         solver_props - structure containing updated properties for the solver
%

if ~isempty(target_choice)
    solver_props.TarRet = target_choice;
else
    solver_props.TarRet = [];
end

if solver_props.MaxOmega
    solver_props.frontier_point = 'Om';
    solver_props.NumPorts = 1;
    solver_props.FminConFlag = 1;
else
    switch point_choice
        
        case  'MV'
            solver_props.frontier_point = 'MV';
            solver_props.NumPorts = 2;
            solver_props.TarRet = [];
        case 'OS'
            solver_props.frontier_point = 'OS';
            solver_props.NumPorts = 20;
            solver_props.TarRet = [];
        case 'Tar'
            solver_props.frontier_point ='Tar';
            solver_props.NumPorts = [];
        case 'TarOS'
            solver_props.frontier_point ='TarOS';
            solver_props.NumPorts = 10;
            solver_props.TarRetOS = solver_props.TarRet; %first draw OS curve, then afterwards take target return and seek it
            solver_props.TarRet = [];
        case 'HighMom'
            solver_props.HigherMoments = 1;
      otherwise % Default - MeanVariance point
            solver_props.frontier_point = 'MV';
            solver_props.NumPorts = 2;
            solver_props.TarRet = [];
            
    end
end

