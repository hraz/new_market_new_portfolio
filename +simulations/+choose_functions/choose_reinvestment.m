function [constraints reinvChoice] = choose_reinvestment(reinvestmentMethod, constraints)
%CHOOSE_REINVESTMENT picks the reinvestment strategy to be followed
%
%   [constraints] = choose_reinvestment(reinvestmentMethod, constraints)
%   function receives reinvestment choice and sets the constraints
%   accordingly.
%
%   input:  constraints - data structure containing constraints for filtering
%           purposes
%           reinvestmentMethod - string. possible values:
%           'Specific-Bond-Reinvestment', 'risk-free', more to be added
%
%   output: constraints with:   constraints.ReinvestmentStrategy
%
%           reinvChoice - Scalar - 1 = 'Specific-Bond-Reinvestment'
%                                  2 = 'risk-free'
%                                  3 = 'benchmark'
%                                  7 = 'real-specific-bond-reinvestment': seek the best bond
%                                       based on closest YTM to original bond's YTM
%
% Hillel Raz, July 10th
% Copyright 2013, Bond IT Ltd


if ~isa(reinvestmentMethod, 'char')
    error('choose_reinvestment:wrongInput', ...
        'One or more input arguments is not of the right type');
end

constraints.ReinvestmentStrategy = reinvestmentMethod;

switch reinvestmentMethod
    case 'Specific-Bond-Reinvestment'
        reinvChoice = 1;
    case 'risk-free'
        reinvChoice = 2;
    case 'portfolio'
        reinvChoice = 3;
    case 'benchmark'
        reinvChoice = 4;
    case 'real-specific-bond-reinvestment'
        reinvChoice = 7;
end
