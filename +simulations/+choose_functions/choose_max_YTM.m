function [constraints] = choose_max_YTM(YTM_flag, constraints)
% function receives YTM_flag  and chooses the
% YTM range.
%
% input:
%        YTM_flag - string.
%                              'YTM20' = MaxYTM is 20%.
%                              'YTM40' = MaxYTM is 40%.
%                              'YTM60' = MaxYTM is 60%.
%
% output: handles with maximum YTM chosen.
%

constraints.dynamic.yield.min = 0;
switch YTM_flag
    case  'YTM20'
        constraints.dynamic.yield.max = 20/100;
    case  'YTM40'
        constraints.dynamic.yield.max = 40/100;
    case  'YTM60'
        constraints.dynamic.yield.max = 60/100;
    case  'YTM100'
        constraints.dynamic.yield.max = 100/100;
    case 'YTM300'
        constraints.dynamic.yield.max = 300/100;
    otherwise
        constraints.dynamic.yield.max = 1000; % Default value
        
end
