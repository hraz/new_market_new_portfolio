function [riskFreeVec] = risk_free_for_period(riskFreeInfo, timePeriod, reqDate)
% RISK_FREE_FOR_PERIOD finds the riskfree ytm's for a given period
% 
% [riskFreeVec] = risk_free_for_period(riskFreeInfo, timePeriod, reqDate) - function finds the 
%     appropriate riskfree bpnd (2 months) ytm's for the required date and timePeriod back.
%     
%     Input:  riskFreeInfo - fts - contains all risk free bonds for all dates
%     
%             timePeriod - scalar - period of time to which risk free bonds are compared to
%             
%             reqDate - scalar - date required
%             
%     Output: riskFreeVec - timePeriod X 1 Double - ytms of riskfree bond for timePeriod required
% 
%     see also: simulation_function
%     
% 
% Hillel Raz, August 14th, nothing special. 
% Copyright 2013, Bond IT Ltd

closestDateIndex = find(riskFreeInfo.dates <= reqDate, 1, 'last');
riskFreeMat = fts2mat(riskFreeInfo);
riskFreeVec = riskFreeMat(max(closestDateIndex-timePeriod + 1, 1):closestDateIndex, 2);
