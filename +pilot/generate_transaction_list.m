function generate_transaction_list (in_string)
%% intializing
% import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
% dbstop if error
error_id=0;
out_string=[];

%%  parsing

in_string_split=regexp(in_string, '\','split');
field_name=cell(1,length(in_string_split)-1);
field_data=cell(1,length(in_string_split)-1);
for m=2:length(in_string_split)
    input_temp=regexp(in_string_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
% \security_list:[security1,unit1;security2,unit2;�;security,unitN]
if length(field_data)==4
    allocation_initial=field_data{find(strcmp('security_list1',field_name))};
    A=eval(sprintf('%s',allocation_initial));
    if isempty(A)
        nsins1 = []; 
        units1 = [];
    else
        nsins1 = A(:,1); 
        units1 = A(:,2);
    end
    allocation_final=field_data{find(strcmp('security_list2',field_name))};
    B=eval(sprintf('%s',allocation_final));
    if isempty(B)
        nsins2 = []; 
        units2 = [];
    else
        nsins2 = B(:,1); 
        units2 = B(:,2);
    end
    TC=field_data{find(strcmp('transaction_cost',field_name))};
    req_date=datenum(field_data{find(strcmp('transaction_date',field_name))});
else
    %error in number of inputs
    error_id=999;
end

%% Get my bonds data in the current date
if ~error_id
    combined_alloc=[A;B];
    [all_nsins nsins_indx_2]=unique(combined_alloc(:,1));
    full_alloc1=zeros(size(all_nsins,1),2);
    full_alloc2=zeros(size(all_nsins,1),2);
    full_alloc1(:,1)=all_nsins;
    full_alloc2(:,1)=all_nsins;
    rindx2=find(nsins_indx_2>length(nsins1));
    full_alloc2(rindx2,2)=combined_alloc(nsins_indx_2(rindx2),2);
    [all_nsins nsins_indx_1]=unique(combined_alloc(:,1),'first');
    rindx1=find(nsins_indx_1<=length(nsins1));
    full_alloc1(rindx1,2)=combined_alloc(nsins_indx_1(rindx1),2);

    %sanity check: isequal(full_alloc2(rindx2,1),combined_alloc(nsins_indx_2(rindx2),1))
    full_transactions=zeros(size(full_alloc1));
    full_transactions(:,1)=full_alloc1(:,1);
    full_transactions(:,2)=full_alloc2(:,2)-full_alloc1(:,2);
    non_zero_transactions=full_transactions(find(full_transactions(:,2)),:);
    res_fts=get_bond_dyn_single_date( non_zero_transactions(:,1),'price_dirty',req_date);
    action_amounts=(fts2mat(res_fts)/100).*(non_zero_transactions(:,2))';    
    out_string=strcat(out_string,'\error_message:',num2str(error_id));
    out_string=strcat(out_string,'\transaction_list:[');
    for m=1:size(non_zero_transactions,1)
        out_string=strcat(out_string,datestr(req_date,'yyyy-mm-dd'),',');
        out_string=strcat(out_string,num2str(non_zero_transactions(m,1)),',');
        out_string=strcat(out_string,num2str(non_zero_transactions(m,2)),',');
        out_string=strcat(out_string,num2str(TC),',');
        out_string=strcat(out_string,num2str(action_amounts(m)),';');
    end
    out_string(end)=[];
    out_string=strcat(out_string,']');    
else
    out_string=strcat(out_string,'\error_message:',str2num(error_id));
end

fprintf(1,'%s\n',out_string)
