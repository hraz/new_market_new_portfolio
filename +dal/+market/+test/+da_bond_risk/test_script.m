function [] = test_script( funName, testDesc, expectedError, expectedResult, rdate, nsin )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.get.risk.*;
    import dal.market.test.*;
    
    if isempty(expectedError)
        %% Test execution:
       ds = get_bond_risk(nsin, rdate);


        %% Result analysis:
        if (isequal(ds,expectedResult))
            testResult = 'Success';
        else
            testResult = 'Failure';
        end
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
