%% Import external packages:
import dal.market.test.bond_ttl.*;


%% Test description:
funName = 'bond_ttl';
testDesc = 'Correct arguments. -> result set:';

expectedStatus = 1;
expectedError = '';

 stat_param.class_name = {'NON-GOVERNMENT BOND'};
stat_param.coupon_type = {'Fixed Interest'};
stat_param.linkage = {'CPI'};
stat_param.sector = {'COMMUNICATIONS & MEDIA'};
stat_param.bond_type = {'Corporate Bond'};
stat_param.redemption_error = {'error'};

dyn_param.history_length = {1 1000000};
dyn_param.yield = {0 0.5};
dyn_param.mod_dur = {0 7};
 dyn_param.turnover = {1000000 1.0000e+009};
dyn_param.ttm = {0 7};

rdate = '2012-02-02';
expectedResult = [
1113661
1115211
1115674
1123447
];
%% Test execution:
test_script( funName, testDesc, expectedError, expectedResult, rdate, stat_param, dyn_param );
