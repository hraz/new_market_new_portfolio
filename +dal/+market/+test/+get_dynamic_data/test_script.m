function [] = test_script( funName, testDesc, expectedError, expectedResult, nsin_list, data_name, tbl_name, start_date, end_date, obs_count )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.get.dynamic.*;
    import dal.market.test.*;
    
    if isempty(expectedError)
        %% Test execution:
       res_fts = get_dynamic_data( nsin_list, data_name, tbl_name, start_date, end_date, obs_count );


        %% Result analysis:
        for i = 1:length(data_name)
            if (isequal(res_fts.(data_name{i}),expectedResult.(data_name{i})))
                testResult = 'Success';
            else
                testResult = 'Failure';
                break;
            end
        end
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);
    res_fts = [];

end
