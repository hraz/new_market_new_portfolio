function [ ds ] = get_bond_history_length(  nsin_list,  rdate )
%GET_BOND_HSTORY_LENGTH return dynamic data about required bonds at given date.
%
%   [ ds ] = get_bond_history_length( nsin_list, rdate ) receives bond nsin,
%              and required date, and return all history length of the required bonds untill given 
%              date. 
%
%   Input:
%       nsin_list - is an NASSETSx1 numeric vector specifying the nsin numbers of needed bonds. 
%
%       rdate - is a time value ('yyyy-mm-dd') or a number specifying the required date.
%
%   Output:
%       ds - is an NASSETSxMASSETSxHISTORY_LENGTH data set specifying
%       history length of the required bonds untill given date.
%
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(1, 2, nargin));
        
    if (nargin == 1) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
    end
    
    if ~isnumeric(nsin_list)
        error('dal_market_get_dynamic:da_bond_dyn:wrongInputType', ...
            'Wrong type of nsin_list.');
    end
    if ~isvector(nsin_list)
        error('dal_market_get_dynamic:da_bond_dyn:wrongInputDim', ...
            'Wrong dimentions of nsin_list.');
    end
    
     %% Step #1 (SQL sqlquery for this filter):
    sqlquery = ['CALL get_history_length( ''' rdate ''', ' num2str4sql(nsin_list) ');'];
   
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end



