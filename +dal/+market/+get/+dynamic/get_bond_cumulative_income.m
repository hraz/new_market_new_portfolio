function [ periodical_income, rf_alloc, bench_alloc, stop_point ] = get_bond_cumulative_income(allocation, first_date, calc_date, last_date, ir_lb, ir_ub, sim_freq, benchmark_id)
% function [  periodical_income, stop_point  ] = get_bond_cumulative_income(allocation, first_date, last_date, ir_lb, ir_ub)
%GET_BOND_CUMULATIVE_INCOME returns cumulative income on given asset list in required period.
%
%   [ periodical_income, redemption_income ] = gets_bond_cumulative_income(allocation, first_date, last_date, ir_lb, ir_ub)
%   receives portfolio allocation, start and finish dates for cumulative income calculus, and returns
%   portfolio cumulative income on given assets in required period.
%
%   Input:
%       allocation - is an NASSETSx2 dataset represented portfolio allcation: [nsin, count].
%
%       first_date - is a scalar value specifying the start date for portfolio allocation
%                       performance calculus.
%
%       last_date - is a scalar value specifying the last date for portfolio allocation
%                       performance calculus.
%
%       ir_lb - is a scalar value represented the lower bound of the possible interest rate for
%                       reinvestment of given coupon payments. Default = 0;
%
%       ir_ub - is a scalar value represented the upper bound of the possible interest rate for
%                       reinvestment of given coupon payments. Default = 1;
%
%   Output:
%       periodical_income - is an NOBSxNASSETS+1 matrix specifying given assets cumulative income
%                       in required time period.
%
%       stop_point - is a NPOINTSx1 vector specifying the stop-point date for performance valuation.
%
%   Note:   1.Result fts returns sorted nsin of the given assets, it's needed to be checked in calling
%                  code.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.
    
    %% Import external packages:
    import dal.market.get.base_data.*;
    import dal.market.get.dynamic.*;
    import dal.market.get.cash_flow.*;
    import dal.market.get.static.*;
    import utility.fts.*;
    import utility.dal.*;
    import utility.dataset.*;
    import utility.*;
 
    %% Input validation:
    error(nargchk(6, 8, nargin));
    if ~isnumeric(first_date)
        first_date = datenum(first_date);
    end
    if ~isnumeric(last_date)
        last_date = datenum(last_date);
    end
    if isempty(ir_lb)
        ir_lb = 0;
    end
    if isempty(ir_ub)
        ir_ub = 1;
    end
    if ir_lb > ir_ub
        error('dal_market_get_dynamic:get_bond_cumulative_income:wrongInput', ...
                'Lower bound of interest rate cann''t be greater then upper bound.');
    end
    if nargin < 7 | sim_freq == 0
        sim_freq = [];
    end
    if nargin < 8 | isempty(benchmark_id)
        benchmark_id = 601;
    end
    
    %% Step #1 (Static and dynamic information about given assets including its cash flows):
    allocation = sortrows(allocation);
    given_nsin = allocation.nsin;
    nassets = length(given_nsin);
        
    % Reinvestment allocation simulation:
    rf_alloc = dataset(allocation.nsin, NaN(size(allocation,1),1), NaN(size(allocation,1),1), NaN(size(allocation,1),1), 'VarNames', {'nsin', 'rf_reinv_flag', 'rf_reinv_rate', 'rf_reinv_amount'});
    bench_alloc = dataset(allocation.nsin, NaN(size(allocation,1),1), NaN(size(allocation,1),1), NaN(size(allocation,1),1), 'VarNames', {'nsin', 'bench_reinv_flag', 'bench_reinv_coef', 'bench_reinv_amount'});
    
    if ismember('count', dsnames(allocation))
        count = allocation.count;
    else
        count = [];
    end
    
    % Get needed data:
    static_data = get_bond_stat( given_nsin, first_date, {'last_trading_date', 'bond_class'} );
    [coupon_income] = get_cash_flow_income( first_date, calc_date, given_nsin );
    fts = get_multi_dyn_between_dates( given_nsin, {'price_dirty', 'ytm'}, 'bond', first_date, calc_date ); % add one day at the end of period for non-business government payment

    benchmark_data = get_multi_dyn_between_dates( benchmark_id, {'value'}, 'index', first_date, calc_date );
    benchmark_value = fts2mat(benchmark_data.value,1);

    % Define primary constants:
    pmt_existance = ~isempty(coupon_income);
    if pmt_existance
        pmt_nsin = strid2numid(tsnames(coupon_income));    
    else 
        pmt_nsin = [];
    end
    gov_bond_place = strcmpi('GOVERNMENT BOND', static_data.bond_class);
 
    id_names = tsnames(fts.price_dirty);
    if isempty(id_names)
        periodical_income = fints();
        warning('dal_market_get_dynamic:da_bond_cumulative_return:wrongInput', ...
            'All assets in the given allocation are already not traded at given first date.');
        return;
    elseif length(id_names) ~= length(given_nsin)
        periodical_income = fints();
        warning('dal_market_get_dynamic:da_bond_cumulative_return:wrongInput', ...
            'A part of assets in the given allocation are already not traded at given first sdate.');
        return;
    end
    
    bsns_date = benchmark_value(:,1); % because posible situation when there are no any traded asset untill given check date.
    if pmt_existance
        pmt_date = coupon_income.dates;  
    end
 
%      price_data = fts2mat(fts.price_dirty,1);
    if size(benchmark_value,1) ~= size(fts.price_dirty)
        tmp_price = fints(bsns_date, zeros(length(bsns_date),1));
        merged_price_data = rmfield(merge(tmp_price, fts.price_dirty), 'series1');
        price_data = fts2mat(merged_price_data,1);
        
        tmp_ytm = fints(bsns_date, zeros(length(bsns_date),1));
        merged_ytm_data = rmfield(merge(tmp_ytm, fts.ytm), 'series1');
        ir_data = fts2mat(merged_ytm_data,1);
       
    else
        price_data = fts2mat(fts.price_dirty,1);
        ir_data = fts2mat(fts.ytm,1);
    end
       
    %% Step #2 (Find information about assets with last trading day before given end date and build data_mask):
    in_period_bsns_date = bsns_date(bsns_date <= calc_date(ones(length(bsns_date), 1)));
    first_date = in_period_bsns_date(1); 
    calc_date = in_period_bsns_date(end); % The case of non-trading given calc date.
    ntradingdates = length(in_period_bsns_date);
        
    last_trading_date = datenum(static_data.last_trading_date)';
    in_period_last_trading_date_holder_place = ((first_date(ones(1,nassets)) < last_trading_date) & (last_trading_date <= calc_date(ones(1,nassets))))'; 
            
    if any(in_period_last_trading_date_holder_place)
        % Find benchmark reinvestment asset places and risk free reinvestment asset places:
        reinvestment_time_period = etime(datevec(calc_date(ones(1,nassets))), datevec(last_trading_date)) / (3600*24*365);
        rf_reinvestment_period_place = (reinvestment_time_period> 1/365) & (reinvestment_time_period < 1);
        rf_alloc.rf_reinv_flag = (in_period_last_trading_date_holder_place & rf_reinvestment_period_place);
        
%         rf_alloc.rf_reinv_flag{rf_alloc.rf_reinv_flag == true} = 1;
%         rf_alloc.rf_reinv_flag{rf_alloc.rf_reinv_flag == false} = 0;
        
        % Find business days in required time period that are early last trading dates:
        early_last_trading_date = last_trading_date(rf_alloc.rf_reinv_flag');
        numearly_ltd = length(early_last_trading_date);
    
        % Create required risk free rate periods:
        rf_time_period = reinvestment_time_period(rf_alloc.rf_reinv_flag');
 
        %% Start of the new reinvestment politic. %%%
        
        %%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% (If last trading date is a year before last date of portfolio holding, the all cash from
        %%% this asset will be investment into benchmark - now any ETF on All Bonds TASE Index.)
        % Calculate place of the benchmark reinvested last trading  dates in the list of all last trading
        % dates:
        
        if any(~rf_reinvestment_period_place)
            % Find last trading dates that a year before given last date:
            % Determine the assets with benchmark reinvestment last trading date:
            bench_alloc.bench_reinv_flag = (in_period_last_trading_date_holder_place & ~rf_reinvestment_period_place &  (reinvestment_time_period> 1/365));
            if any(bench_alloc.bench_reinv_flag)
                bench_reinv_last_trading_date = last_trading_date(bench_alloc.bench_reinv_flag');

                % Create benchmark reinvestment asset mask:
                bench_reinv_mask = ...
                    in_period_bsns_date(:, ones(nassets + 1, 1)) == [in_period_bsns_date, last_trading_date(ones(ntradingdates,1), :)];

                % Get benchmark value on the given reinvestment period:
%                 benchmark_data = get_multi_dyn_between_dates( benchmark_id, {'value'}, 'index', min(bench_reinv_last_trading_date), calc_date );
                required_benchmark_data_ind = benchmark_value(:,1) >= min(bench_reinv_last_trading_date)*ones(size(benchmark_value,1), 1);
                benchmark_value = benchmark_value(required_benchmark_data_ind, :);
            end
            %%% End of the new reinvestment politic. %%%
            %%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%
        end
        
        %% Old reinvestment politic (the rest of the cash from dead assets investments into risk free assets):
        if ~isempty(early_last_trading_date)
            % Determine the place of the earler last trading dates in total observation dates:
            early_last_trading_date_place = ismember(in_period_bsns_date, early_last_trading_date);

            % Create early_last_trading_date_mask:
            early_last_trading_date_mask = in_period_bsns_date(:, ones(nassets + 1, 1)) == [in_period_bsns_date, last_trading_date(ones(ntradingdates,1), :)];

            % Look for an appropriate risk free time periods for calculated ones:
            [appropriate_rf_period, ~, prev_i] = unique(approp_rf_prd( rf_time_period' ));

            % Looking for risk free interest rate for the period between given last_trading_date and the end of period:
            rf_period = fts2mat(da_nominal_yield_between_dates( appropriate_rf_period, min(early_last_trading_date), calc_date ), 1);

            % Attach risk free to given assets according to its own last trading date:  
            rf_rate = zeros(length(in_period_bsns_date),nassets+1);
            rf_rate(:,1) = in_period_bsns_date;
            rf_rate(early_last_trading_date_place, find(rf_alloc.rf_reinv_flag) + 1) = rf_period(ismember(rf_period(:,1), early_last_trading_date), prev_i +1);
            rf_rate(~early_last_trading_date_mask) = 0;
        end
    end
 
    %% Step #3 (Merge payment days with historical business days):
    if pmt_existance
        fts = fints(bsns_date, NaN(length(bsns_date),1));
        merged_coupon_income = rmfield(merge(fts, coupon_income), 'series1');
        coupon_income_data = fts2mat(merged_coupon_income,1);
    end
    
    %% Step #4 (Build pad of cash flow payments):
    if pmt_existance
        % Looking for non-business days in payment day list:
        non_bsns_pmt_date = pmt_date(~ismember(pmt_date, bsns_date),1);
        non_bsns_pmt_date_place = ismember(coupon_income_data(:,1), non_bsns_pmt_date);

        % Move coupon payments from non-business day to business day according to follow rule:
        % the payment of government bond will moved to the next near business day,
        % and the payment of non-government bond will moved to the previous business day.
        if any(non_bsns_pmt_date_place)
            non_bsns_pmt_date_row = find(non_bsns_pmt_date_place);
            coupon_income_data(non_bsns_pmt_date_row-1, find(~gov_bond_place)+1) = coupon_income_data(non_bsns_pmt_date_place, find(~gov_bond_place)+1);

            % In case when last payment date is equal to last date of required period this payment will
            % not be moved to the next business date.
            coupon_income_data(non_bsns_pmt_date_row(1:end-1)+1, find(gov_bond_place)+1) = coupon_income_data(non_bsns_pmt_date_row(1:end-1), find(gov_bond_place)+1);
            if coupon_income_data(non_bsns_pmt_date_row(end),1) < in_period_bsns_date(end)
                coupon_income_data(non_bsns_pmt_date_row(end)+1, find(gov_bond_place)+1) = coupon_income_data(non_bsns_pmt_date_row(end), find(gov_bond_place)+1);
            end
        end

        % Remove non-business days and dates after last date with its data from the list of coupon payments:
        coupon_income_data = coupon_income_data(ismember(coupon_income_data(:,1),in_period_bsns_date),:);
    
        % Merge coupon payments of payment assets with no-payment assets:
        pmt_asset_place = find(ismember(given_nsin, pmt_nsin));
        non_pmt_asset_place = find(~ismember(given_nsin, pmt_nsin));

        pad_pmt = NaN(size(coupon_income_data,1), nassets+1);
        pad_pmt(:,[1, pmt_asset_place'+1]) = coupon_income_data; % the list of all possible payments

        %% Step #5 (Prepare base for the compounding interest rate pad and payment date pad): 
        % Determinition of the needed number of layers:
        num_pmts = sum(~isnan(pad_pmt(:,2:end)),1);
        num_pads = max(num_pmts);

        % Building the payment pad:
        pad_pmt = pad_pmt(:,:,ones(num_pads,1));

        % Building the interest rate pad:
        pad_ir = ir_data(ismember(ir_data(:,1),in_period_bsns_date),:,ones(num_pads,1));
        pad_ir(:, non_pmt_asset_place + 1, :) = 0;

        % Building the payment dates pad:
        pmt_date = pad_ir(:,1);
        pad_pmt_time = pmt_date(:,ones(size(pad_pmt,2),1),ones(num_pads,1));
        pad_pmt_time(:, non_pmt_asset_place + 1, :) = 0;

        %% Step #6 (Build payment's pad):
        source_pad_mask = ~isnan(pad_pmt(:,2:end,1));

        % Create payment layered mask:
        pad_pmt_mask = false((size(pad_pmt_time)));
        pad_pmt_mask(:,1,:) = true;

    %     num_res = zeros(num_pads,1); % as debuging control
        for p = 1:num_pads
            for a = 1: size(pad_pmt_mask,2)-1
                first_pmt = find(source_pad_mask(:,a), 1, 'first');
                pad_pmt_mask(first_pmt, a+1 , p) = true; % if first_pmt is empty it is done nothing
                source_pad_mask(first_pmt,a) = false; % if first_pmt is empty it is done nothing
            end
    %         control_res = sum(pad_pmt_mask(:,2:end,p),1); % as debuging control
    %         num_res(p) = max(control_res); % as debuging control
        end

        % Layering of payments, compounding intereset rates and payment date pad:
        pad_pmt(~pad_pmt_mask) = 0;

        % Spreading the data over 
        for p = 1:num_pads
            for a = 1: 1: size(pad_pmt_mask,2) - 1
                pmt_place = find(pad_pmt(:,a+1,p) ~= 0); % the time place of the payment for asset a in layer p
                if ~isempty(pmt_place)
                    pad_pmt(pmt_place+1:end, a+1,p) = pad_pmt(pmt_place, a+1,p); % Spreading the payment over asset a vector untill end of it
                    pad_pmt_time(1:pmt_place-1, a+1, p) = 0;
                    pad_pmt_time(pmt_place:end, a+1, p) = etime(datevec(pad_pmt_time(pmt_place:end, a+1, p)), ...
                                                                                            datevec(pad_pmt_time(pmt_place, a+1, p)*ones(length(pad_pmt_time(pmt_place:end, a+1, p)),1))) ./ ...
                                                                                            (3600*24*365); % Calculate time period from payment date to each date after that

                    % Spreading the interest rate from payment date until end of it according to
                    % appropriate time period (for time period less than 1 year the interest rate equals
                    % to nominal rate to this period:
                    appropriate_asset_ind = find(pad_ir(pmt_place, 2:end, p) > ir_lb & pad_ir(pmt_place, 2:end, p) < ir_ub);
                    if ~isempty(appropriate_asset_ind)
                        if ~isempty(count)
                            % Re- engeeniring of the allocation asset weights in the past:
                            weight = holdings2weights(count(appropriate_asset_ind)', price_data(pmt_place, appropriate_asset_ind), 1)';
                            avg_ir = pad_ir(pmt_place, appropriate_asset_ind + 1, p) * (weight/sum(weight)); % weighted average of appropriate yields
                        else
                            avg_ir = mean(pad_ir(pmt_place, find(pad_ir(pmt_place, 2:end, p) > ir_lb & pad_ir(pmt_place, 2:end, p) < ir_ub) + 1, p));
                        end
                    else
                        req_rf_period = pad_pmt_time(end, a+1, p);
                        [appropriate_rf_period] = unique(approp_rf_prd( req_rf_period' ));
                        
                        %% !!!!!!!!!!!!!!!!REPEATE DB CALL!!!!!!!!!!!!!!!!!!!! %%
                        avg_ir = fts2mat(da_nominal_yield_between_dates( appropriate_rf_period, pad_pmt_time(pmt_place,1,1), pad_pmt_time(pmt_place,1,1) ));
                    end
                    pad_ir(pmt_place+1:end, a+1, p) = avg_ir; 
                    pad_ir(1:pmt_place, a+1, p) = 0;
                end
                pad_ir(isnan(pad_ir(:, a+1, p)), a+1, p) = 0;
            end
        end        

        %% Step #7 (Payment compounding):
        pad_pmt(:,2:end,:) = pad_pmt(:,2:end,:) .* ( (1 + pad_ir(:,2:end,:) ) .^ pad_pmt_time(:,2:end,:) );
        pad_pmt(isnan(pad_pmt)) = 0; % remove NaNs before sum
        pmt = sum(pad_pmt(:,2:end,:),3); % total sum of all compounded payments during given period
    end
     
    %% Step #8 (In period income creating - dirty price & compounded coupon payment accumulation):
    in_period_pmt_date_i = price_data(:,1) <=calc_date(ones(length(price_data(:,1) ),1),1);
    if pmt_existance
        periodical_income = [price_data(in_period_pmt_date_i,1), price_data(in_period_pmt_date_i, 2:end) + pmt(in_period_pmt_date_i, :)];
    else
        periodical_income = price_data(in_period_pmt_date_i, :);
    end
 
    %% Step #9 (Service the assets with an early last trading dates):
    if any(rf_alloc.rf_reinv_flag) && ~isempty(early_last_trading_date)
        rf_in_period_time = periodical_income(:,1);
        rf_in_period_time = rf_in_period_time(:,ones(size(periodical_income,2),1));
 
        % Build compounding process in case of early last trading date vs. given last date:
        regular_last_trading_date_holder_place = find(~rf_alloc.rf_reinv_flag);
        early_last_trading_date_holder_place = find(rf_alloc.rf_reinv_flag);
        rf_in_period_time(:, regular_last_trading_date_holder_place + 1) = 0;
        for a = 1: numearly_ltd
            last_trading_day_i  = find(early_last_trading_date_mask(:,early_last_trading_date_holder_place(a)+1), 1, 'first');
            periodical_income(last_trading_day_i:end,  early_last_trading_date_holder_place(a) + 1) = periodical_income(last_trading_day_i - 1,  early_last_trading_date_holder_place(a) + 1);
            rf_rate(last_trading_day_i + 1:end,  early_last_trading_date_holder_place(a) + 1) = rf_rate(last_trading_day_i ,  early_last_trading_date_holder_place(a) + 1);
    
             % Risk free reinvestment rate and amount (For case of performance checking when all bonds reinvestment into risk free rates):
            rf_alloc.rf_reinv_rate(early_last_trading_date_holder_place(a)) = rf_rate(last_trading_day_i ,  early_last_trading_date_holder_place(a) + 1);
            rf_alloc.rf_reinv_amount(early_last_trading_date_holder_place(a)) = periodical_income(last_trading_day_i - 1,  early_last_trading_date_holder_place(a) + 1) .* count(early_last_trading_date_holder_place(a));
            
        
            rf_in_period_time(last_trading_day_i:end,  early_last_trading_date_holder_place(a) + 1) = etime( ...
                datevec(rf_in_period_time(last_trading_day_i:end,  early_last_trading_date_holder_place(a) + 1)), datevec(rf_in_period_time(last_trading_day_i - 1,  early_last_trading_date_holder_place(a) + 1) .*...
                ones(length(rf_in_period_time(last_trading_day_i:end,  early_last_trading_date_holder_place(a) + 1)),1))) ./ ...
                (3600*24*365);
            rf_in_period_time(1:last_trading_day_i,  early_last_trading_date_holder_place(a) + 1) = 0;
        end
 
        % Build compounding factor:
        early_last_trading_date_place = find(early_last_trading_date_place);
        periodical_income(early_last_trading_date_place + 1:end,  early_last_trading_date_holder_place + 1) = ...
            periodical_income(early_last_trading_date_place + 1:end,  early_last_trading_date_holder_place + 1) .* ...
                    ((1 + rf_rate(early_last_trading_date_place + 1:end,  early_last_trading_date_holder_place + 1)) ...%  .* rf_in_period_time(early_last_trading_date_place+1:end,  early_last_trading_date_holder_place + 1)) .^ ...
                    .^ rf_in_period_time(early_last_trading_date_place + 1:end,  early_last_trading_date_holder_place + 1)); 
                
    end
 
    %% Step #9.1 (Benchmark reinvestment):
    %%% Start of the new reinvestment politic. %%%
    if any(bench_alloc.bench_reinv_flag)
        %%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% (If last trading date is a year before last date of portfolio holding, the all cash from
        %%% this asset will be investment into benchmark - now any ETF on All Bonds TASE Index.)
        bench_reinv_place = find(bench_alloc.bench_reinv_flag);
        for ii = 1:length(bench_reinv_last_trading_date)
            bench_reinv_date_i = find(bench_reinv_mask(:, bench_reinv_place(ii) + 1), 1, 'first'); % the place of the last trading date of current asset on the benchmart reinvestment time vector
            bench_value_i = find(ismember(benchmark_value(:,1), periodical_income(bench_reinv_date_i+1:end,1)));
            
            % Benchmark reinvestment parameters: reinvestment price coefficient and amount:
            bench_alloc.bench_reinv_coef(bench_reinv_place(ii)) = (periodical_income(bench_reinv_date_i, bench_reinv_place(ii) + 1) / benchmark_value(bench_value_i(1),2));
            bench_alloc.bench_reinv_amount(bench_reinv_place(ii)) = periodical_income(bench_reinv_date_i, bench_reinv_place(ii)) .* count(bench_reinv_place(ii));
            
            periodical_income(bench_reinv_date_i + 1:end, bench_reinv_place(ii) + 1) = ...
                 bench_alloc.bench_reinv_coef(bench_reinv_place(ii)) * benchmark_value(bench_value_i, 2);
              
        end
        %%% End of the new reinvestment politic. %%%
        %%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%
    end
        
    %% Step #10 (Build stop-points in the given period according to required frequency):
    if ~isempty(sim_freq)
        stop_point = sim_point( periodical_income(1,1), periodical_income(end,1), sim_freq );
        [ dyn_data ] = get_multi_dyn_between_dates( allocation.nsin, {'coupon_pmt'}, 'bond', first_date, last_date );
        extra_pmt_nsin_place = find(ismember(allocation.nsin, strid2numid(tsnames(dyn_data.coupon_pmt))));
        
        rq_extra_pmt = zeros(size(dyn_data.coupon_pmt,1), size(allocation,1)+1);
        stop_point_place = ismember(dyn_data.coupon_pmt.dates, stop_point );
        
        rq_extra_pmt(stop_point_place,1) = dyn_data.coupon_pmt.dates( stop_point_place );
        rq_extra_pmt(stop_point_place,extra_pmt_nsin_place+1) = fts2mat(dyn_data.coupon_pmt( stop_point_place ));
        rq_extra_pmt(isnan(rq_extra_pmt)) = 0;
        
        income_date_place = find(ismember(periodical_income(:,1), stop_point ));
        periodical_income = periodical_income(:,:,ones(length(income_date_place),1));
        for sp = 1:length(income_date_place)
            periodical_income(income_date_place(sp), 2:end, sp) = periodical_income(income_date_place(sp), 2:end, sp) + rq_extra_pmt(sp,2:end); 
            periodical_income(income_date_place(sp)+1:end, 2:end, sp) = NaN;
        end
        
    else
        stop_point = periodical_income(end,1);
        [ dyn_data ] = get_multi_dyn_between_dates( allocation.nsin, {'coupon_pmt'}, 'bond', calc_date, calc_date ); 
        if ~isempty(dyn_data.coupon_pmt)
            extra_pmt_nsin_place = find(ismember(allocation.nsin, strid2numid(tsnames(dyn_data.coupon_pmt))));

            rq_extra_pmt = zeros(size(dyn_data.coupon_pmt,1), size(allocation,1)+1);
            stop_point_place = ismember(dyn_data.coupon_pmt.dates, stop_point );

            rq_extra_pmt(stop_point_place,1) = dyn_data.coupon_pmt.dates( stop_point_place );
            rq_extra_pmt(stop_point_place,extra_pmt_nsin_place+1) = fts2mat(dyn_data.coupon_pmt( stop_point_place ));
            rq_extra_pmt(isnan(rq_extra_pmt)) = 0;
            periodical_income(end, 2:end) = periodical_income(end, 2:end) + rq_extra_pmt(end,2:end);
        end
    end
    
end
