clear; clc;

%% Import external packages:
import dal.market.get.base_data.*;

%% Basic data:
% interest_period = [(1:1:10)/12, 1, 2, 3,4, 5, 7, 10];
start_date = ('2006-1-02');
end_date = ('2006-1-02');
interest_period = [  1     2     3     4     5];



%% Get historical interest rates;
tic
historic_yield = get_nominal_yield_with_param( interest_period, start_date, end_date )
toc

% figure(1)
% plot(historic_yield);
