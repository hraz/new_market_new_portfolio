function [ ds ] = get_bond_base_static( data_type )
%GET_BOND_BASE_STATIC return all values of given data type.
%
%   [ ds ] = get_bond_base_static( data_type ) return all values of required data type.
%
%   Input:
%       data_type - is a string value specifying the needed data type.
%                           The possible values are:
%                           'class' - it is a asset class possible values.
%                           'sector' - it is an answer on the question:
%                                           "What are the sector values?" 
%                           'bond_type' - it is an answer on the question: "What
%                                           is a kind of bond type?" 
%                           'coupon_type' - it is an answer on the
%                                           question: "What kind of coupon type exist?" 
%                           'linkage_index' - it is an answer on the question:
%                                           "What types of linkage are there?" 
%                           'maalot' - it is an answer on the question:
%                                           "What is a scale of Maalot ranking company?" 
%                           'midroog' - it is an answer on the question:
%                                           "What is a scale of Midroog ranking company?" 
%
%   Output:
%       ds - is an NDATATYPESx1 or NDATATYPESx2 dataset of strings specifying
%                           required data types (2- for rating scales and linkage).
%
% Yigal Ben Tal
% Copyright 2011
  
    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(1,1, nargin));
    if (~ischar(data_type))
        error('dal_market_get_base_data:get_bond_base_static:wrongInput', ...
            'Wrong input data type.');
    end
    
    %% Step #1 (SQL query for this filter):
    sqlquery = 'CALL base_bond_stat_sector();';
    
    %% Step #2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','dataset');
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #4 (Close the opened connection):
    close(conn);
    
end
