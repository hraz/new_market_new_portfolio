clear; clc;

import dal.market.filter.total.*;
import utility.*;
import utility.dal.*;

%% 
rdate = '2013-03-05';
ds = dataset();

% filter 1
class_name = {};
coupon_type = {'Fixed Interest'};
linkage_index = {'CPI'};
sector ={};
bond_type = {'Corporate Bond'};
redemption_error={'no_error'};


%% add to dataset 
ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,redemption_error,ds);

% filter 2
class_name =  {};
coupon_type = {};
linkage_index = {};
sector = {};
bond_type = {};
redemption_error={'no_error'};


ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,redemption_error,ds);

% filter 2
class_name =  {};
coupon_type = {};
linkage_index = {};
sector = {};
bond_type = {};
redemption_sum={'no_error'};


ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,redemption_error,ds);


dyn_param.history_length = {NaN NaN};
dyn_param.yield = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};

% dyn_param.history_length = {60 100000};
% dyn_param.yield = {0 0.5};
% dyn_param.mod_dur = {2 7};
%  dyn_param.turnover = {100000 1.0000e+009};

% dyn_param.history_length = {60 100000};
% dyn_param.ytm = {0 0.5};
% dyn_param.mod_dur = {2 7};
%  dyn_param.turnover = {100000 1.0000e+009};
dyn_param.ttm = {NaN NaN};


% dyn_param.history_length = {NaN NaN};
% dyn_param.yield = {NaN NaN};
% dyn_param.mod_dur = {NaN NaN};
%  dyn_param.turnover = {NaN NaN};

% dyn_param.history_length = {NaN NaN};
% dyn_param.yield = {NaN NaN};
% dyn_param.mod_dur = {NaN NaN};
%  dyn_param.turnover = {NaN NaN};
% dyn_param.ttm = {NaN NaN};


% rdate = '2011-06-06';

% dyn_param.history_length = {NaN NaN};
% dyn_param.yield = {NaN NaN};
% dyn_param.mod_dur = {NaN NaN};
% dyn_param.turnover = {NaN NaN};


tic
   [ struct_nsin ] = bond_ttl_multi( rdate,ds, dyn_param )
%     [ struct_nsin ] = bond_ttl_multi( rdate,ds, dyn_param ,struct_nsin.filter_1);
toc

% tic
%   [ struct_nsin1 ] =  bond_ttl_multi( rdate, ds, dyn_param, struct_nsin.filter_1)
%  toc

% stat_param.class_name = {};
% stat_param.coupon_type = {'Fixed Interest'};
% stat_param.linkage_index = {'CPI'};
% stat_param.sector = {};
% stat_param.bond_type = {'Corporate Bond'};
% 
% % stat_param.class_name = {};
% % stat_param.coupon_type = {'Variable Interest', 'Fixed Interest'};
% % stat_param.linkage = {'CPI', 'NON LINKED'};
% % stat_param.sector = {'COMMUNICATIONS AND MEDIA'};
% % stat_param.bond_type = {'Corporate Bond'};
% % stat_param.class_name = {};
% % stat_param.coupon_type = {'Fixed Interest'};
% % stat_param.linkage_index = {'NON LINKED'};
% % stat_param.sector = {};
% % stat_param.bond_type = {};
% 
% 
% 
% rdate = '2009-06-06';
% % dyn_param.history_length = {NaN NaN};
% % dyn_param.yield = {NaN NaN};
% % dyn_param.mod_dur = {NaN NaN};
% % dyn_param.turnover = {NaN NaN};
% 
% 
% tic
% %  [ nsin ] = bond_ttl( rdate,stat_param,dyn_param )
%   [ nsin ] = bond_ttl( rdate,stat_param, dyn_param );
% toc

