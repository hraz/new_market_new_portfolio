clear; clc;

import dal.market.filter.risk.*;
import dml.*;

rdate = '2011-11-23';

global BASE_DATA;
BASE_DATA = base_data(rdate);

%% Test rating filter according to Maalot rating company:
% scale = 'maalot';
% lb = 'D';
% ub = 'D';

% scale = 'maalot';
% lb = 'C';
% ub = 'B';

%% Test rating filter according to Midroog rating company:
scale = 'midroog';
lb = 'Ca'
ub = 'Caa2'

% scale = 'midroog';
% lb = 'Caa3'
% ub = 'Baa1'

tic
nsin = bond_rank_partial( scale, lb, ub, rdate )
toc