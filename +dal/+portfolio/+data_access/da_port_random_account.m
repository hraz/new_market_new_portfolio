function [ account_id ] = da_port_random_account( current_value, first_date, last_date )
%DA_PORT_RANDOM_ACCOUNT return a random account id according to model and optimization type,
%                                                           if both are null, checking for random model and optimization type.
%
%   [ account_id ] = da_port_random_account( required_holding_period,model, opt_type ) receives model, optimization type
%                               and required_holding_period and returns random account id,
%
%   Input:
%
%       model - is an STRING value specifying on which model to take the account.
%       opt_type -  is an STRING value specifying on which optimization type to take the account.
%       holding_period - the time in years the portfolio was active
%
%   Output:
%       account_id - a random account id according to input or according to random input.
%
%
% 15/08/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;

    %% Input validation:
    error(nargchk(1, 2, nargin));

    %% Step #1 (Build SQL query): 
    value_name = fieldnames(current_value);
    
    sqlquery = 'CALL sp_da_port_random_ids(';
    for i=1:length(value_name)
        sqlquery = [sqlquery, current_value.(value_name{i}), ','];
    end
    sqlquery = [sqlquery, first_date, ', ', last_date, ');'];
            
    %% Step #2 (Open connection):
    setdbprefs ('DataReturnFormat','cellarray');
    conn = mysql_conn('portfolio');
        
    %% Step #3 (Execution of sql function):
    data = fetch(conn,sqlquery);
    
    %% Step #4 (Close the opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');
    
    %% Step #5 (Return parameters initialization):
    account_id = double(cell2mat(data));
    
end
