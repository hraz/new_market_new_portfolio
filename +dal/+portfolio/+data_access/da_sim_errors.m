function [ ds ] = da_sim_errors(  )
%DA_SIM_ERRORS Summary of this function goes here
% 
%    [ err_msg ] = da_sim_errors( input_args )
%
%   Input:
%       sim_id - is a scalar spceifying the simulation number
%
%       obs_date - is a date value ('yyyy-mm-dd') specifying the required date.
%
%
%   Output:
%     err_msg - is a error message about the simulation according to date.
%
%Yaakov Rechtman
% Copyright 2012
%% Import external packages:
    import dal.mysql.connection.*;
    import dal.mysql.data_access.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(0, 0, nargin));   
    
%     if(isempty(sim_id))
%           error('dal_portfolio_data_access:da_sim_errors:wrongInput', ...
%             'account_id can not be empty');
%     end
%      if(isempty(obs_date))
%           error('dal_portfolio_data_access:da_sim_errors:wrongInput', ...
%             'obs_date can not be empty');
%     end
    
    
   
    %% Step #1 (SQL query for this filter):
    sqlquery = ['CALL sp_da_sim_errors();'];
    
    %% Step #2 (Execution of sql function):
    setdbprefs ('DataReturnFormat','cellarray');
    conn = mysql_conn('portfolio');
    
    %% Step # 3 (Execution of built SQL query):
    ds = fetch(conn, sqlquery);
    
    %% Step #4 (Close the opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');    
    
end

