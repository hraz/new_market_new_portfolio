function [ status ] = da_port_status_history(  id_list, acc_or_owner )
 %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(2, 2, nargin));
     
    if ~ismember(acc_or_owner, {'account','owner'})
         error('dal_portfolio_data_access:da_port_status:wrongInput', ...
            'wrong input for acc_or_owner.');
    end
    if(isempty(id_list))
          error('dal_portfolio_data_access:da_port_status:wrongInput', ...
            'id_list can not be empty');
    end
  
    %% Step #1 (SQL Connect to the database):
     if strcmp(acc_or_owner,'account')
    	sqlquery = ['CALL sp_da_port_status_history(' cell2str4sql(id_list) ', null );'];
     else
         sqlquery = ['CALL sp_da_port_status_history(null , ' cell2str4sqlbracket(id_list) ');'];
     end
      %% Step #2 (Execution of built SQL sqlquery):
      setdbprefs ('DataReturnFormat','dataset');
    
        conn = mysql_conn('portfolio');
        curs = exec(conn, sqlquery);
        ds = fetchmulti(curs);

     if ~isempty(ds.data)
          if strcmp(acc_or_owner,'account')
              status = cell2struct( ds.data, strcat('account_',id_list));
          else
            status = cell2struct( ds.data, strcat('owner_',id_list), 2);
          end
    else
       status = [];
    end
       
    %% Step #3 (Close the opened connection):
    close(conn);
    
    
 
end


