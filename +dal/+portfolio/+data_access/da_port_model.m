function [ ds ] = da_port_model( account_id, req_date )
%DA_PORT_MODEL  returns portfolio model definition according to account_id.
%
%   [ ds ] = da_port_model( account_id ) - receives  account_id and returns portfolio model with its
%   optimization type and additional constraints.
%
%   Input:
%       account_id - is an INTEGER value  specifying the account id number.
%
%   Output:
%       ds - is a dataset 1x3 containg all model properties according to account_id.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(2, 2, nargin));
        
     if isempty(account_id)
        error('dal_portfolio_data_access:da_port_model:wrongInputType', ...
            'account_id can not be empty.');
    end
    if ~isnumeric(account_id)
        error('dal_portfolio_data_access:da_port_model:wrongInputType', ...
            'Wrong type of account_id.');
    end
    
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['CALL sp_da_port_model(' num2str(account_id) ', ''' datestr(req_date, 'yyyy-mm-dd')  ''');'];

    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    ds = fetch(conn,sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
end


