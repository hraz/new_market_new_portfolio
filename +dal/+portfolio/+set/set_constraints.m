function [outStatus, outException] = set_constraints(inPortfolioID, inDate, inMarketConstraints, inSpecificConstraints)
%SET_CONSTRAINTS saves full set of portfolio constraints.
%
%   [outStatus, outException] = set_constraints(inPortfolioID, inDate, inMarketConstraints, inSpecificConstraints)
%
%   Input:
%       inPortfolioID - is a scalar value specifying some portfolio ID number.
%       inDate - is a scalar value specifying the date of the constraints creation/changes.
%       inMarketConstraints - is a struct of datasets that are market constraints set. This set constraints includes 
%                                           follow more detailed constraint groups:
%               staticConstraint - is a dataset value represented the list of all static constraints. The dataset has fields 
%                                           like: �constraintID�,�constraintValueID�, �minValue�, �maxValue�.
%                                           Possible values for constraint id represented by function get_constraints_map(�static�), 
%                                           except of 16/17 (included_security or excluded_security), when the possible values are any 
%                                           securityID that may be found using get_asset_summary(.). For exclude min/max value must be zero.
%               dynamicConstraint - is a dataset value represented the list of all relevant dynamic constraints. The dataset 
%                                           has fields like: �constraintID�, �minValue�, �maxValue�.
%                                           Possible values for constraint name represented by procedure get_constraints_map (�dynamic�).
%               sectorConstraint - is a dataset value represented the list of all relevant sector constraints. The dataset 
%                                           has fields like:�sectorID�,�minValue�,�maxValue�.
%                                           Possible values for constraint name represented by procedure get_mapping_list_asset(�sector�).
%               ratingConstraint - is a dataset value represented the list all relevant rating constraints. The dataset has 
%                                           fields like: �maalotMinID�, �maalotMaxID�, �midroogMinID�, �midroogMaxID�, �logicOperand�.
%                                           Possible values for rating scales represented by procedure get_mapping_list_asset(.) using  
%                                           maalot_rating_scale�  or �midroog_rating_scale� input value; the logic operand may get just one 
%                                           of values: �AND� or �OR�. The input with 4 [] values as a rating id numbers will be rejected as a wrong 
%                                           input format! It is a must get more than one rating ID!
% 
%       inSpecificConstraints - is a struct of datasets that are specific constraints set. This set constraints includes 
%                                           follow more detailed constraint groups:
%               basicConstraint - is a dataset value represented the list of all relevant basic portfolio constraints. 
%                                           The dataset has fields like: �constraintID�,�value�.
%                                           Possible values for constraint name represented by procedure get_constraints_map(�basic�).
%               targetConstraint - is a dataset value represented the list of all relevant target portfolio constraints. 
%                                           The dataset has fields like:�constraintID�,�value�.
%                                           Possible values for constraint name represented by procedure get_constraints_map(�target�). In 
%                                           case of constraint name 'opt_sharpe' the value must be empty ([]).
%               modelConstraint - is a dataset value represented the list of all relevant specific portfolio constraints. 
%                                           The dataset has fields like:�modelID�, �optimizationTypeID�,�reinvestmentStrategyID�.
%                                           Possible values for portfolio model, optimization type and reinvestment strategy IDs represented 
%                                           by procedure get_constraints_map(.) using follow inputs: �model�, �optimization�, �reinvestment� .
%               taxConstraint - is a dataset value represented the list of all relevant for this portfolio fees. The dataset 
%                                           has fields like: �taxID�,�feeValue�.
%                                           Possible values for portfolio tax�s ID represented by procedure get_constraints_map(�tax�).
% 
%	Output:
%       outStatus - is a boolean value specifying the success of the operation.
% 
%       outException - is a string value specifying the error message. If operation successed exception will be an empty string.
% 
%	Note: 
%       Default value is []; it means that this type of constraints was not defined at all. 
%       If some constraint was not changed, please copy its previous definition as an input. If some constraint was not defined 
%       at all or removed from the required constraint list, input [] value as a constraint group value.
% 
%	Sample:
%       No constraints: 
%           structMarketConstraints = [];
%           structSpecificConstraints = [];
%           [resStatus, errMessage]  = set_constraints(1, 723400, structMarketConstraints, structSpecificConstraints)
%       Result set: resStatus = 0, errMessage  = �No row was inserted!�.
%       Full list of constraints:
%           structMarketConstraints = struct(�
%                       �staticConstraint�, ...
%                       dataset({[[1;1;2;16], [1;2;1;808365], [0.1;0.05;0.25;0.2], [0.3;0.43;0.33;0.6]], �constraintID�,�constraintValueID�, �minValue�, �maxValue�}), �
%                       �dynamicConstraint�, dataset({[[1;2], [0.10;0.05], [0.15;0.10]], �constraintID�, �minValue�, �maxValue�}), �
%                       �sectorConstraint�, dataset({[[24;12], [0.2;0.10], [0.4;0.25]], �sectorID�, �minValue�, �maxValue�}), � 
%                       �ratingConstraint�, ... 
%                       [dataset({[12, 23, NaN, 22], �maalotMinID�, �maalotMaxID�, �midroogMinID�, �midroogMaxID�}), dataset({{�AND�}, �logicOperand�)] �
%                   );
%           structSpecificConstraints = struct(�
%                   �basicConstraint�, dataset({[[3;2;1], [0.22;0.131;0.214]], �constraintID�, �value�}), �
%                   �targetConstraint�, dataset({[[3;1;2], [0.2;0.15;0.24]], �constraintID�, �value�}), �
%                   �modelConstraint�, dataset({[3, 2, 1], �model�, �optimization�, �reinvestment�}),�
%                   �taxConstraint�, dataset({[5, 2], �taxID�,  �feeValue�})�
%                   );
%           [resStatus, errMessage]  = set_constraints(1, 723400, structMarketConstraints, structSpecificConstraints)
%           Result set: resStatus = 1, errMessage  = ��.
%       Partial list of constraints:
%           structMarketConstraints = struct(�
%                       �staticConstraint�, ...
%                       dataset({[[1;1;2;16], [1;2;1;808365], [0.1;0.05;0.25;0.2], [0.3;0.43;0.33;0.6]],�constraintID�, �constraintValueID�, �minValue�, �maxValue�}), �
%                       �dynamicConstraint�, [], �
%                       �sectorConstraint�, dataset({[[24;12], [0.2;0.10], [0.4;0.25]],�sectorID�, �minValue�, �maxValue�}), � 
%                       �ratingConstraint�, [dataset({[12, 23, NaN, 22], �maalotMinID�, �maalotMaxID�, �midroogMinID�, �midroogMaxID�}), dataset({{�AND�}, �logicOperand�)] �
%                   );
%           structSpecificConstraints = struct(�
%                   �basicConstraint�, dataset({[[3;2;1], [0.22;0.131;0.214]], �constraintID�, �value�}), �
%                   �targetConstraint�, dataset({[[3;1;2], [0.2;0.15;0.24]], �constraintID�, �value�}), �
%                   �modelConstraint�, dataset({[3, 2, 1], �model�, �optimization�, �reinvestment�}),�
%                   �taxConstraint�, dataset({[5, 2], �taxID�,  �feeValue�})�
%                   );
%           [resStatus, errMessage]  = set_constraints(1, 723400, structMarketConstraints, structSpecificConstraints)
%           Result set: resStatus = 1, errMessage  = ��.

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;    

    %% Predefined constants:
    if (nargout > 1)
        outException = '';
    end   
    try
    
        predefinedMarketConstraintName = {'staticConstraint'; 'dynamicConstraint'; 'sectorConstraint'; 'ratingConstraint'};
        marketConstraintsNum = numel(predefinedMarketConstraintName);

        predefinedSpecificConstraintName = {'basicConstraint'; 'targetConstraint'; 'modelConstraint'; 'taxConstraint'};
        specificConstraintsNum = numel(predefinedSpecificConstraintName);

        %% Input validation:
        error(nargchk(4, 4, nargin));
        if (isempty(inPortfolioID) || isnan(inPortfolioID))
            error('dal_portfolio_set:set_constraints:mismatchInput', 'Portfolio ID cannot be NULL.');
        elseif ~isnumeric(inPortfolioID)
            error('dal_portfolio_set:set_constraints:mismatchInput', 'Portfolio ID must be numeric.');
        elseif (inPortfolioID < 0)
            error('dal_portfolio_set:set_constraints:mismatchInput', 'Portfolio ID must have a positive value.');
        else
            inPortfolioID = num2str(inPortfolioID);
        end

        if (isempty(inDate))
            error('dal_portfolio_set:set_constraints:mismatchInput', 'Given time point cannot be an empty.');
        elseif isnumeric(inDate)
            inCalcDate = datestr(inDate, 'yyyy-mm-dd HH:MM:SS');
        else
            error('dal_portfolio_set:set_constraints:mismatchInput', 'inDate must be a numeric value.');
        end

        if isempty(inMarketConstraints)

            inMarketConstraints = [];

        elseif (~isa(inMarketConstraints, 'struct'))
            error('dal_portfolio_set:set_constraints:wrongInput', 'inMarketConstraints must be a struct.');
        else
            marketConstraintName = fieldnames(inMarketConstraints);

            if (numel(marketConstraintName) ~= marketConstraintsNum)
                error('dal_portfolio_set:set_constraints:wrongInput', ['inMarketConstraints must have strictly ',num2str(marketConstraintsNum),' constraint groups.']);
            elseif (~all(strcmpi(marketConstraintName, predefinedMarketConstraintName)))
                error('dal_portfolio_set:set_constraints:wrongInput', 'All field names of inMarketConstraints struct must be from predefined name-set and with predefined order.');
            end

            for i = 1 : marketConstraintsNum
                if (~isempty(inMarketConstraints.(predefinedMarketConstraintName{i}))) && ...
                    (~isa(inMarketConstraints.(predefinedMarketConstraintName{i}), 'dataset'))
                    error('dal_portfolio_set:set_constraints:wrongInput', [predefinedMarketConstraintName{i}, ' must be a dataset.']);
                end
            end

        end

        if isempty(inSpecificConstraints)
            inSpecificConstraints = [];

        elseif (~isa(inSpecificConstraints, 'struct'))
            error('dal_portfolio_set:set_constraints:wrongInput', 'inSpecificConstraints must be a struct.');
        else
           specificConstraintName = fieldnames(inSpecificConstraints);

           if (numel(specificConstraintName) ~= specificConstraintsNum)
                error('dal_portfolio_set:set_constraints:wrongInput', ['inSpecificConstraints must have strictly ',num2str(specificConstraintsNum),' constraint groups.']);
            elseif (~all(strcmpi(specificConstraintName, predefinedSpecificConstraintName)))
                error('dal_portfolio_set:set_constraints:wrongInput', 'All field names of inSpecificConstraints struct must be from predefined name-set and with predefined order.');
           end

            for i = 1 : specificConstraintsNum
                if (~isempty(inSpecificConstraints.(predefinedSpecificConstraintName{i}))) && ...
                    (~isa(inSpecificConstraints.(predefinedSpecificConstraintName{i}), 'dataset'))
                    error('dal_portfolio_set:set_constraints:wrongInput', [predefinedSpecificConstraintName{i}, ' must be a dataset.']);
                end
            end

        end

        %% Step #1 (Build SQL query):
        strMarketConstraintGroup = char(zeros(1,1e4));
        strSpecificConstraintGroup = char(zeros(1,1e4));
        if (~isempty(inMarketConstraints))
            strMarketConstraintGroup = '';
            for i = 1 : marketConstraintsNum
                if (isempty(inMarketConstraints.(predefinedMarketConstraintName{i})))
                    strMarketConstraintGroup = [strMarketConstraintGroup, 'NULL,'];
                elseif strcmpi((predefinedMarketConstraintName{i}), 'ratingConstraint')
                    strMarketConstraintGroup = [strMarketConstraintGroup, '"', ds2str4sql(inMarketConstraints.(predefinedMarketConstraintName{i}), 2), '",'];
                else
                    strMarketConstraintGroup = [strMarketConstraintGroup, '"', ds2str4sql(inMarketConstraints.(predefinedMarketConstraintName{i}), 0), '",'];
                end
            end
            strMarketConstraintGroup =  strMarketConstraintGroup(1:end-1);
        else
            strMarketConstraintGroup = 'NULL, NULL, NULL, NULL';
        end

        if (~isempty(inSpecificConstraints))
            strSpecificConstraintGroup = '';
            for i = 1 : specificConstraintsNum
                if (isempty(inSpecificConstraints.(predefinedSpecificConstraintName{i})))
                    strSpecificConstraintGroup = [strSpecificConstraintGroup, 'NULL,'];
                else
                    strSpecificConstraintGroup = [strSpecificConstraintGroup, '"', ds2str4sql(inSpecificConstraints.(predefinedSpecificConstraintName{i}), 0), '",'];
                end
            end
            strSpecificConstraintGroup = strSpecificConstraintGroup(1:end-1);
        else
            strSpecificConstraintGroup = 'NULL, NULL, NULL, NULL';
        end

        sqlquery = ['CALL set_constraints(', inPortfolioID ', ''', inCalcDate, ''', ',  strMarketConstraintGroup, ',', strSpecificConstraintGroup,');'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        conn = mysql_conn('portfolio');
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):    
        fetch(conn, sqlquery);
        if (nargout > 0)
            outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Portfolio constraints were saved.'];
            end       
        end
        
        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    end
    
setdbprefs ('DataReturnFormat','dataset');
end
