function [outStatus, outException] = set_income_status(inPortfolioID, inIncomeData)
%SET_INCOME_STATUS save portfolio income status.
%
%	[outStatus, outException] = set_income_status(inPortfolioID ,inIncomeData)
%
%   Input:
%       inPortfolioID - is a scalar value specifying the given portfolio ID number. It cannot be an empty ([])! 
%
%       inIncomeData - is a dataset value specifying summary of all transactions were done at given transaction date (inDate). 
%                               The structure must have following fields: ‘holdingValue’, ‘cashAmount’.
%                             Where
%                                       inDate - is a scalar value specifying the current date.
%                                       holdingValue - is a scalar value specifying summary  market value of all transaction that were
%                                                               done at given date (in agorot).
%                                       cashAmount - is a scalar value specifying the total free cash value after all sell/purchase 
%                                                               operations in the given portfolio at transaction date (in agorot).
% 
%   Output:
%       outStatus - is a boolean value specifying the success of the operation.
%       outException - is a string value specifying the error message. If operation successed exception will be an empty string.
%
% Sample:
% inData = {‘2013-02-12 12:31:00’};	
% inIncomeData = [dataset(inData),dataset({[145774.56, 1250.03], ‘holdingValue’, ‘cashAmount’})];
% [resStatus, errMessage] = set_income_status(1, inIncomeData);

% Created by Yigal Ben Tal.
% Date:	27.06.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	___amit__________, at _aug 2013__________. The sense of update: ___setdbprefs, db return empty dataset which create
%     error in the fetch. changed to setdbprefs('cellarray')______________________________.
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;    

    %% Predefined constants:
    if (nargout > 1)
        outException = '';
    end
    try
        predefinedIncomeDataList = {'inDate', 'holdingValue', 'cashAmount'};
        predefinedIncomeDataNum = numel(predefinedIncomeDataList);

        %% Input validation:
        error(nargchk(2, 2, nargin));
        if (isempty(inPortfolioID) || isnan(inPortfolioID))
            inPortfolioID = 'NULL';
        elseif ~isnumeric(inPortfolioID)
            error('dal_portfolio_set:set_income_status:mismatchInput', 'Portfolio ID must be numeric.');
        elseif inPortfolioID < 0
            error('dal_portfolio_set:set_income_status:mismatchInput', 'Portfolio ID must be a positive number.');
        else
            inPortfolioID = num2str(inPortfolioID);
        end

        if (isempty(inIncomeData))
            error('dal_portfolio_set:set_income_status:wrongInput', 'Income data cannot be an empty.');
        elseif (~isa(inIncomeData, 'dataset'))
            error('dal_portfolio_set:set_income_status:wrongInput', 'inIncomeData must be a dataset.');
        else
            inIncomeDataList = dsnames(inIncomeData);
            if numel(inIncomeDataList) ~= predefinedIncomeDataNum
                error('dal_portfolio_set:set_income_status:wrongInput', ...
                    ['inIncomeData must have strictly ', num2str(predefinedIncomeDataNum), ' fields.']);
            elseif (~all(strcmpi(inIncomeDataList, predefinedIncomeDataList)))
                error('dal_portfolio_set:set_income_status:wrongInput', ...
                    'All field names of inIncomeData dataset must be from predefined name-set and with predefined order.');
            end
        end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL set_income_status(', inPortfolioID ', "', ds2str4sql(inIncomeData,1, 'f'), '");'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        conn = mysql_conn('portfolio');
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):        
        fetch(conn, sqlquery);
        if (nargout > 0)
        outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Portfolio income status was saved.'];
            end
        end
        
        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end      
        
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
    
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
        end        

    end
    
setdbprefs ('DataReturnFormat','dataset');
end
