function [outExcessStatistics] = get_excess_statistics(inPortfolioIDList, inFirstDate, inLastDate)
%GET_EXCESS_STATISTICS returns excess statistics during some predefined time period.
%
%   [outExcessStatistics] = get_excess_statistics(inPortfolioIDList, inFirstDate, inLastDate)
%
%	Input:
%        inPortfolioIDList - is a column vector specifying the given portfolio ID number. 
%
%        inFirstDate - is a scalar value specifying the first date in required time period.
%
%        inLastDate - is a scalar value specifying the last date in required time period.
%
% Output:
%        outExcessStatistics - is a dataset specifying statistics on portfolio with the same model characteristics that
%                                   have follow fields: 
%            - portfolioID - is a column vector specifying the given portfolio ID number.
%
%            - calcDate - is a column vector specifying the date of the of statistics measuring.
%
%            - delta_1 - is a column vector specifying the delta between holding return of the portfolio vs. benchmark
%                                   return at the given date. 
%
%            - delta_2 - is a column vector specifying the excess holding return between portfolio and expected return,
%                                   normalized by portfolio�s volatility at the given date. 
%
%            - delta_3 - is a column vector specifying the excess Sharpe ratio of the portfolio above the benchmark�s
%                                   one at the given date. 
%
%            - delta_4 - is a column vector specifying the portfolio Information Ratio at the given date.
% 
% Sample:
%	[outExcessStatistics] = get_excess_statistics([1;2;3;4;5;6;7], 756321, 756843);
% Result set:
%       lastUpdate | portfolioID | delta_1 | delta_2 | delta_3 | delta_4
%        7563321 | 1 | 0.0126 | 0.021 | 0.05 | 0.5
%        7562323 | 2 | 0.0135 | 0.21 | 1 | 2
%        7563332 | 3 | 0.0126 | 0.023 | 0.0545 | 0.5213
%        7562323 | 4 | 0.01565 | 0.217 | 0.231 | 0.346
%        7563385 | 5 | 0.01241 | 0.216 | 0.465 | 0.245

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(3,3,nargin));
    
    if isempty(inPortfolioIDList)
        error('dal_portfolio_get:get_excess_statistics:mismatchInput', 'Portfolio ID list cannot be an empty.');
    else
        inPortfolioIDList = num2str4sql(inPortfolioIDList);
    end
    
    if isempty(inFirstDate)
        inFirstDate = 'NULL';
    elseif isnumeric(inFirstDate)
        inFirstDate = ['"' datestr(inFirstDate, 'yyyy-mm-dd HH:MM:SS') '"'];
    end
    
    if isempty(inLastDate)
        inLastDate = 'NULL';
    elseif isnumeric(inLastDate)
        inLastDate = ['"' datestr(inLastDate, 'yyyy-mm-dd HH:MM:SS') '"'];
    end
    
    %% Create SQL query:
    sqlquery = ['CALL get_excess_statistics(', inPortfolioIDList, ',', inFirstDate, ',', inLastDate ');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try
        outExcessStatistics = fetch(conn, sqlquery);
        if ~isempty(outExcessStatistics)
            outExcessStatistics = set(outExcessStatistics, 'VarNames', {'calcDate', 'delta_1', 'delta_2', 'delta_3', 'delta_4'});
        else
            outExcessStatistics = dataset({cell(1,5), 'calcDate', 'delta_1', 'delta_2', 'delta_3', 'delta_4'});
        end
    catch ME
        outExcessStatistics = dataset(); % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
        error('dal_portfolio_get:get_excess_statistics:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);
    
end
