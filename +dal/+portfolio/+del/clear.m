function [ outStatus, outException] = clear( inPortfolioID )
%CLEAN removes all records from portfolio DB according to given ID.
% 	
%   [ outStatus, outException] = clear( inPortfolioID )
%   removes all records from portfolio database according to the give portfolio ID.
%   In case of empty portfolio ID it cleaned all portfolio DB (be careful)!
% 
% 		Input:
% 			inPortfolioID - is a scalar value specifying which portfolio to be cleaned.
%                                           If it is empty or did not given, it means clean all tables and 
%                                           restart all auto-increment counters.
% 
% 		Output:
%           outStatus - is a boolean value specifying the success of the operation.
%
%           outException - is a string value specifying the error message. If operation 
%                                            successed exception will be an empty string. 
 	
% 		Created by 	Yigal Ben Tal
% 		Date:		14.05.2013
% 		Copyright 2013, BondIT Ltd.	
% 
% 		Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
        error(nargchk(0,1, nargin));

        if (nargin == 0) || isempty(inPortfolioID)
            inPortfolioID = 'NULL';
        elseif ~isnumeric(inPortfolioID)
            error('dal_portfolio_del:clear:mismatchInput', 'inPortfolioID must be numeric.');
        else
            inPortfolioID = num2str(inPortfolioID);
        end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL portfolio.clear(',  inPortfolioID, ');'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        conn = mysql_conn('portfolio');
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):
        res = fetch(conn, sqlquery);
        if (nargout > 0)
            outStatus = true;
            if (nargout > 1)
                outException = [outException, res{:}];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end
        
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end
    end
    
end

