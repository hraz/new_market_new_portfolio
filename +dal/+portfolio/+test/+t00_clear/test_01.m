%% Import tested directory:
import dal.portfolio.test.t00_clear.*;

%% Test description:
funName = 'clear';
testDesc = 'Clear not existed portfolio -> expected error:';

%% Expected results:
expectedStatus = 0;
expectedError = 'Such portfolio ID is not found!';

%% Input definition:
portfolioID = 10;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portfolioID);
