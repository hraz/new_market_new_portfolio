%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Empty name -> expected error:';

expectedStatus = 0;
expectedError = 'Name cannot be NULL or an empty string.';

%% Input definition:
inID = [];
inGroupName = {'model', 'optimization', 'reinvestment', 'tax'};
inName = '';
inDesc =  'Test description.';

%% Test execution:
for i = 1:length(inGroupName)
    test_script( funName, testDesc, expectedStatus, expectedError, inID, inGroupName{i}, inName, inDesc);
end
