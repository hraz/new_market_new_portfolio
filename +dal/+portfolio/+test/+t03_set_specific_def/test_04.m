%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Update existed properties -> expected success:';

expectedStatus = 1;
expectedError = 'Given definition is saved.';

%% Input definition:
global specificDefID
global inSpecID
inSpecID = NaN(4,1);
inGroupName = {'model', 'optimization', 'reinvestment', 'tax'};
inName = 'Test';
inDesc =  'Test description.';

%% Test execution:
for i = 1:length(inGroupName)
    inSpecID(i) = specificDefID;
    test_script( funName, testDesc, expectedStatus, expectedError, inSpecID(i), inGroupName{i}, inName, inDesc);
end
