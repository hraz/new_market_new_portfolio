%% Import external packages:
import dal.portfolio.test.t13_set_constraints.*;

%% Test description:
funName = 'set_constraints';
testDesc = 'Duplicate input for target constraint. -> error message:';

expectedStatus = 0;
expectedError = 'Duplicate entry ''1-2012-03-09 12:12:12-3'' for key ''UK_target_constraints''';

%% Input definition:
inID = 1;
inDate =  datenum('2012-03-09 12:12:12');
inMarketConstraints = struct('staticConstraint', dataset({[14,1,0.1,1.0;15,1,0.2,0.99], 'constraintID', 'constraintValueID', 'minValue', 'maxValue'}), ...
                                            'dynamicConstraint', dataset({[7,0.1,1.0;8,0.2,0.98], 'constraintID', 'minValue', 'maxValue'}), ...
                                            'sectorConstraint', dataset({[603,0.0,1.0], 'sectorID', 'minValue', 'maxValue'}), ...
                                            'ratingConstraint', dataset());
inSpecificConstraints = struct('basicConstraint', dataset({[1,0.06], 'constraintID', 'value'}), ...
                                             'targetConstraint', dataset({[3,0.1;4,0.12;5,0.20], 'constraintID', 'value'}), ...
                                             'modelConstraint', dataset({[1,1,1], 'modelID', 'optimizationTypeID', 'reinvestmentStrategyID'}), ...
                                             'taxConstraint', dataset({[1,0.2], 'taxID','feeValue'}));

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inMarketConstraints, inSpecificConstraints);
