%% Import external packages:
import dal.portfolio.test.t12_get_summary.*;

%% Test description:
funName = 'get_summary';
testDesc = 'NULL as portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID list cannot be an empty.';
expectedResult = dataset();

%% Input definition:
 inPortfolioIDList = [];
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList );
