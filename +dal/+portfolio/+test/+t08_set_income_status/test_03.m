%% Import external packages:
import dal.portfolio.test.t08_set_income_status.*;

%% Test description:
funName = 'set_income_status';
testDesc = 'Wrong input for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Foreign Key violated: Check portfolio ID.';

%% Input definition:
inID = 10;
inData = dataset({[datenum('2012-03-01 12:12:12'),111111,111], 'inDate','holdingValue', 'cashAmount'});
    
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inData);
