%% Import external packages:
import dal.portfolio.test.t01_set_calc_methods.*;

%% Test description:
funName = 'set_calc_methods';
testDesc = 'Update non existed calculation method properties -> expected error:';

expectedStatus = 0;
expectedError = 'Not found such ID.';

%% Input definition:
global inCalcID
inID = inCalcID + 1;
inGroupName = {'exp_return', 'exp_volatility'};
inName = 'Test';
inDesc = 'Test description.';

%% Test execution:
for i = 1: length(inGroupName)
    test_script( funName, [inGroupName{i}, ' :: ', testDesc], expectedStatus, expectedError, inID(i), inGroupName{i}, inName, inDesc);
end
