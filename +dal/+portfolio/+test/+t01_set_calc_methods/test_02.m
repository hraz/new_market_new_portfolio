%% Import external packages:
import dal.portfolio.test.t01_set_calc_methods.*;

%% Test description:
funName = 'set_calc_methods';
testDesc = 'Wrong statistic name -> expected error:';

expectedStatus = 0;
expectedError = 'Wrong statistic name.';

%% Input definition:
inID = [];
inStatisticName = 'exp_var';
inMethod = 'Tmp';
inDesc =  'Tmp';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inStatisticName, inMethod, inDesc);
