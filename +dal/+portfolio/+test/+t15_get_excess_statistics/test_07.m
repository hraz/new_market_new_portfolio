%% Import external packages:
import dal.portfolio.test.t15_get_excess_statistics.*;

%% Test description:
funName = 'get_excess_statistics';
testDesc = 'Not NULL first date-time and NULL as a second one. -> result set:';

expectedStatus = 1;
expectedError = '';
calcDate = {'2012-04-02 12:12:12.0'};
data = dataset({[0.24,0.22,0.16,0.44], 'delta_1','delta_2','delta_3','delta_4'});
expectedResult = [dataset(calcDate),data];

%% Input definition:
 inPortfolioIDList = 1;
 inFirstDate = datenum('2012-03-05 12:12:12'); 
 inLastDate = [];

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList, inFirstDate, inLastDate );
