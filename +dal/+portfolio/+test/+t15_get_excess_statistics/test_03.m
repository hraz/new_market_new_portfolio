%% Import external packages:
import dal.portfolio.test.t15_get_excess_statistics.*;

%% Test description:
funName = 'get_excess_statistics';
testDesc = 'Wrong input for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Such portfolio ID is not found.';
expectedResult = struct('portStatistics', [], 'perBondVaRStatistics', [], 'benchStatistics', []);

%% Input definition:
 inPortfolioIDList = 10;
 inFirstDate = datenum('2010-01-01 12:12:12'); 
 inLastDate = datenum('2013-03-01 12:12:12');

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList, inFirstDate, inLastDate );
