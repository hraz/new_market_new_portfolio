function [] = test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList, inFirstDate, inLastDate )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.get.*;
    import utility.dataset.*;
    import dal.portfolio.test.*;
    
    %% Test execution:
    isIdentical = true;
    errMsg = '';
    try
         [outExcessStatistics] = get_excess_statistics(inPortfolioIDList, inFirstDate, inLastDate);
         isIdentical = isidentic(expectedResult, outExcessStatistics);
         status = true;
    catch ME
        errMsg = ME.message;
        status = false;
    end
    
    %% Result analysis:
    if (status == expectedStatus) & (strcmpi(errMsg, expectedError)) & all(isIdentical)
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
