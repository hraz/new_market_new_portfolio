%% Import external packages:
import dal.portfolio.test.t14_get_constraints.*;

%% Test description:
funName = 'get_constraints';
testDesc = 'Wrong date-time string to number transformation. -> error message:';

expectedStatus = 0;
expectedError = 'There is no any data with given parameters!';
expectedResult = dataset();

%% Input definition:
 inPortfolioID = 1;
 inDate = datenum('2012-22-01 12:12:12'); 

expectedResult.marketConstraint = struct();
expectedResult.specificConstraint = struct();
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate );
