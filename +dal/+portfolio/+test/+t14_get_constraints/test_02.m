%% Import external packages:
import dal.portfolio.test.t14_get_constraints.*;

%% Test description:
funName = 'get_constraints';
testDesc = 'Negative value for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID must be positive value.';
expectedResult = dataset();

%% Input definition:
 inPortfolioID = -1;
 inDate = datenum('2012-03-01 12:12:12'); 

expectedResult.marketConstraint = struct();
expectedResult.specificConstraint = struct();
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate );
