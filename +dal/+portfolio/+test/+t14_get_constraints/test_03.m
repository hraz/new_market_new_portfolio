%% Import external packages:
import dal.portfolio.test.t14_get_constraints.*;

%% Test description:
funName = 'get_constraints';
testDesc = 'Wrong input for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Such portfolio ID is not found.';
expectedResult = dataset();

%% Input definition:
 inPortfolioID = 10;
 inDate = datenum('2012-03-01 12:12:12'); 

expectedResult.marketConstraint = struct();
expectedResult.specificConstraint = struct();
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate );
