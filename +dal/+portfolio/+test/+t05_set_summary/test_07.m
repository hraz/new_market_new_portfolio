%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'New portfolio creation: wrong order of creation date and maturity date -> expected error:';

expectedStatus = 0;
expectedError = 'Wrong creation date/maturity date order!';

%% Input definition:
inID = [];
inName = 'New name';
inDesc =  'New';
inCreationDate = datenum('2012-01-01 12:12:12');
inMaturityDate = inCreationDate - 1;
inPublicFlag = 0;
inVirtualFlag = 1;
inBasedOnFlag = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
