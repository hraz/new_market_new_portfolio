%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'New portfolio creation: valid portfolio summary -> expected success:';

expectedStatus = 1;
expectedError = 'Portfolio summary is saved.';

%% Input definition:
global CREATION;
CREATION = datenum('2012-01-01 12:12:12');
inID = [];
inName = 'New name';
inDesc =  'New';
inCreationDate = CREATION;
inMaturityDate = inCreationDate + 365;
inPublicFlag = 1;
inVirtualFlag = 0;
inBasedOnFlag = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
