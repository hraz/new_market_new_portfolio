%% Import external packages:
import dal.portfolio.test.t09_get_income_status.*;

%% Test description:
funName = 'get_income_status';
testDesc = 'NULL first date-time and not NULL a second one. -> result set:';

expectedStatus = 1;
expectedError = '';
date = {'2013-05-01 11:07:32.0';'2013-05-02 11:08:58.0';'2013-05-03 12:09:25.0'; ...
            '2013-05-03 12:10:32.0'};
expectedResult = [dataset(date), dataset({[300,0,805;310,5,805;305,6,805;300,4,805], ...
                            'holdingValue', 'cashAmount', 'investmentAmount'})];

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = []; 
 inLastDate = datenum('2013-05-05 12:12:12');
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
