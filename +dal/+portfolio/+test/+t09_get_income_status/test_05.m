%% Import external packages:
import dal.portfolio.test.t09_get_income_status.*;

%% Test description:
funName = 'get_income_status';
testDesc = 'Wrong order of the first and second date-times. -> error message:';

expectedStatus = 0;
expectedError = 'Wrong time range!';
date = {'2013-05-01 11:07:32.0';'2013-05-02 11:08:58.0';'2013-05-03 12:09:25.0'; ...
            '2013-05-03 12:10:32.0';'2013-05-07 11:09:59.0';'2013-05-08 18:10:31.0'; ...
            '2013-05-09 18:10:54.0';'2013-05-10 11:11:20.0';'2013-05-13 11:11:38.0'};
expectedResult = [dataset(date), dataset({[300,0,805;310,5,805;305,6,805;300,4,805; ...
                            312,5, 805;310,6,805;312,7,805;313,7,805;315,10,805], ...
                            'holdingValue', 'cashAmount', 'investmentAmount'})];

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = datenum('2013-03-01 12:12:12');
 inLastDate = datenum('2010-01-01 12:12:12'); 
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
