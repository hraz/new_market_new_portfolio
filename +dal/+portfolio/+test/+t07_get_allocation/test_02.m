%% Import external packages:
import dal.portfolio.test.t07_get_allocation.*;

%% Test description:
funName = 'get_allocation';
testDesc = 'Negative value for portfolio ID value. -> error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID must be positive value.';
expectedResult = struct('allocationTbl', [], 'transactionList', []);

%% Input definition:
 inPortfolioID = -1;
 inFirstDate = datenum('2012-01-01 12:12:12'); 
 inLastDate = datenum('2013-03-01 12:12:12');

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
