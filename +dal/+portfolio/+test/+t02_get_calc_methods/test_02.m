%% Import external packages:
import dal.portfolio.test.t02_get_calc_methods.*;

%% Test description:
funName = 'get_calc_methods';
testDesc = 'Empty string as inStatisticsName value. -> error message:';

expectedStatus = 0;
expectedError = 'Required input cannot be NULL or an empty string.';
expectedResult = [];

%% Input definition:
inGroupName = '';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName);
