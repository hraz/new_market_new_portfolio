function [account_id,err] = set_port_static(ds2insert)
%SET_PORT_STATIC add new account to the database
%
%    state =set_port_static(ds2insert)-  recieves ds2insert with static
%                                                           data and inserts to the database
%   Input:
%           ds2insert -  it is a  1X5 dataset specifying static data about the
%                               account -  [account_name,owner_id,manager_id,initial_aount,creation_date].
%                               account_name - the name of the account.
%                               owner_id - the id of the account owner.
%                               manager_id - the id of the manager which is in charge of the account.
%                               creation date - a datetime specifying the creation date of the account
%   Output:
%             account_id - a value specifying the last inserted
%                                   account id.
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%               
%22/04/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    
    import utility.dataset.*;

    %% Input validation:
    error(nargchk(1, 1, nargin));
  
      if(isempty(ds2insert))
          error('dal_portfolio_crud:set_port_static:wrongInput', ...
            'ds2insert can not be empty');
     end
 
   %% Step #1 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');                                                     
   
     conn = mysql_conn('portfolio');
     try
        fastinsert(conn,'port_list' , dsnames(ds2insert), ds2insert)
        sqlquery = 'select LAST_INSERT_ID() as account_id';
        result = fetch(conn,sqlquery);
        account_id = result.account_id;
        err = '';
    catch ME
        account_id = [];
        err = ME.message;
    end
    
    %% Step #2 (Close the opened connection):
    close(conn);  
   
end


