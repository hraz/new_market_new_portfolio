function [err] = set_sim_analysis( ds2insert )
%SET_SIM_ANALYSIS insert solver analisys to the database
%
%   [result,err]  = set_sim_analysis( ds_to_insert )
%
%   Input:
%       Input:
%           ds2insert -  it is a  nX15 dataset specifying data about the solver
%                             sim_num_save - simulation number
%                             call_num -  the number of call that was made
%                             to any of the solver function in the current
%                             sim
%                             error_flag - matlab exit flag (positive - solver was successful, negative - solver faild)
%                             iterations - number of iteration done by the solver
%                             constr_violation - size of constraint violation
%                             algorithm - name of used algorithm in the current solver 
%                             cg_iterations -  total value of PCG iterations
%                             message - exit message
%                             first_order_opt - measure of first order optimality                             
%                             first_excess_duration - unknown,
%                             second_excess_duration - unknown
%                             infeasible_non_duration_const_start - boolean
%                             variable which indicates whether the initial
%                             weights are infeasible in the sense that one
%                             of the non-duration inequality constraints
%                             doesn't hold. 
%                             infeasible_duration_const_start - same as
%                             above, for the duration constraints.
%                             infeasible_non_duration_const_finish - same as
%                             above, for the final weights returned from
%                             the solver
%                             infeasible_duration_const_finish - same as
%                             above, for the final weights from the solver
%                           
%   Output:
%               result - is a boolean value specifying if the insert was
%                           successful
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%
% 20/06/2012
% Yaakov Rechtman BondIT LTD
% Copyright 2013

%% Import external packages:
import dal.mysql.connection.*;
import utility.dal.*;
import utility.dataset.*;

%% Input validation:
%     narginchk(1, 1);
  
      if(isempty(ds2insert))
          error('dal_portfolio_crud:set_sim_analysis:wrongInput', ...
            'ds2insert can not be empty');
     end
 
   %% Step #1 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');                                                     
     conn = mysql_conn('errorDB');
     try
        fastinsert(conn,'sim_analysis ' , dsnames(ds2insert), ds2insert)
       err = '';
    catch ME
        err = ME.message;
    end
    
    %% Step #2 (Close the opened connection):
    close(conn);  
   
end
