function [result,err] = clear_sim_err(  )
%CLEAR_SIM_ERR clears all errors in the portfolio database
%
%   [result,err]  = clear_sim_err( ) - Clears all errors in the database
%
%   Input:
%      none.
%
%   Output:
%            result - is a boolean value specifying if the insert was
%                           successful
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%
% 02/07/2013
% Yaakov Rechtman BondIT
% Copyright 2013

%% Import external packages:
import dal.mysql.connection.*;
import utility.dal.*;
import utility.dataset.*;

%% Input validation:
error(nargchk(0, 0, nargin));



% %       connecting to database
 setdbprefs ('DataReturnFormat','dataset');                                                     
   
conn = mysql_conn('portfolio');
set(conn,'AutoCommit','off');
try
    sqlQuery = 'delete from sim_err where sim_id > 0';
    exec(conn,sqlQuery);
    commit(conn);
    result = true;
    err = '';
catch ME
    result = false;
    rollback(conn);
    err = ME.message;
end

%% Step #3 (Close the opened connection):
close(conn);

end
