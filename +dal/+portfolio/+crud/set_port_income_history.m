function [result,err] = set_port_income_history( account_id , ds_income_hist, is_first )
%SET_PORT_INCOME_HISTORY update portfolio on account_id with the input dataset which
%                                       contains income history for portfolio
%
%   [result,err]  = set_port_income_history(account_id , ds_income_hist,is_first )- update
%                       portfolio income history.
%
%   Input:
%
%       account_id - a value specifying the account to update.
%
%      ds_income_hist - it is 2xN dataset containg observation dates with total value
%
%       is_first - is aboolean value specifying if it is the first insert for acount id
%
%   Output:
%            result - is a boolean value specifying if the insert was
%                           successful
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%
% 14/08/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import utility.dataset.*;

    %% Input validation:
    error(nargchk(3, 3, nargin));

    if isempty(ds_income_hist)
        error('dal_portfolio_crud:set_port_income_history:wrongInput', ...
            'ds_income_hist can not be empty');
    end

    if isempty(is_first)
        error('dal_portfolio_crud:set_port_income_history:wrongInput', ...
            'is_first can not be empty');
    end

    if(isempty(account_id))
        error('dal_portfolio_crud:set_port_income_history:wrongInput', ...
            'account_id can not be empty');
    end
    if ~isnumeric(account_id)
        error('dal_portfolio_crud:set_port_income_history:wrongInput', ...
            'wrong input type for account_id');
    end

    %%       connecting to database
     setdbprefs ('DataReturnFormat','dataset');                                                     
   
    conn = mysql_conn('portfolio');
    set(conn,'AutoCommit','off');
    try

        %    preparing dataset for insert
        %  checking if this is the first insert
        if is_first
            account_id = account_id(ones(size(ds_income_hist),1),1);
            ds_income_hist =  [dataset(account_id) , ds_income_hist ];
            fastinsert(conn,'port_income_history', dsnames( ds_income_hist),  ds_income_hist);
        else
            % if not the first insert, update the first row
            first_obs_date = ds_income_hist{1,1};
            value =  {double(ds_income_hist(1,2))};
            where_query = ['where obs_date = ''',first_obs_date , ''' AND account_id = ', num2str(account_id) ];
            update(conn, 'port_income_history', {'value'}, value,where_query);
     %    insert the rest of the rows
            account_id = account_id(ones(size(ds_income_hist)-1,1),1);
            if~isempty(account_id)
                ds_income_hist =  ds_income_hist(2:end,:);
                ds_income_hist =  [dataset(account_id) , ds_income_hist ];
                fastinsert(conn,'port_income_history', dsnames( ds_income_hist),  ds_income_hist);
            end 
        end
        % %      inserting datasets to the database
        commit(conn);
        result = true;
        err = '';
    catch ME
        rollback(conn);
        result = false;
        err = ME.message;
    end

    %% Step #3 (Close the opened connection):
    close(conn);

end
