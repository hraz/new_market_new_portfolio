   clear; clc;

import dal.portfolio.crud.*;


 account_id = 888;

% total_value = 1000;
% coupon_income = 10;
% interest_earned = 100000;
%  ds_performance = dataset( total_value,  coupon_income,interest_earned);
 obs_date = today;

 nsin = 1010016;
 count = 100;
 ds_allocation = dataset(nsin,count);
 
%  model = {'marko'};
%  optimization_type = {'123'};
%  additional_constraint = {'abc'};
%  ds_model = dataset(model,optimization_type,additional_constraint);
 

 
%  ds_risk = dataset(1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2);
 
%  dyn_param.('performance') = ds_performance;
  dyn_param.('allocation') = ds_allocation;
%    dyn_param.('model') = ds_model;
%     dyn_param.('risk') = ds_risk;
 
 account_name = {'hij'};
owner_id = 4;
manager_id = 1;
initial_amount = 1000;
creation_date = {'2007-01-01'};
ds_stat = dataset(account_name,owner_id,manager_id,creation_date,initial_amount);

obs_date = {'2006-01-03'};
value = 100;
ds_income_hist = dataset(obs_date,value);
obs_date = {'2012-01-03'};
value = 1000;
ds = dataset(obs_date,value);
ds_income_hist = [ds_income_hist;ds];
is_first = false;

 param.static = {ds_stat};
param.income = {ds_income_hist};
tic
[result,err,account_id_new] = set_portfolio(obs_date,account_id,dyn_param,is_first,param)
toc
