function [ target_ds ] = bond_daily( source_ds, old_field_name, new_field_name, all_ind )
%BOND_DAILY create a dataset for bond_daily table.
%
%   [ target_ds ] = bond_daily( source_ds, old_field_name, new_field_name, all_ind )
%   receive source dataset, list of old field names, list of needed field
%   names and all old row indices according to common id of bonds, return
%   the dataset for bond_daily table.
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       old_field_name - it is an 1xMFIELDS cell array specifying the
%                            exist fields.
%
%       new_field_name - it is an 1xKFIELDS cell array specifying the
%                           required fields.
%
%       all_ind - it is an NOBSERVATIONSx1 vector specifying the raw data
%                           rows' indices according to common bond_uuid.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(4, 4, nargin));
    
     if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bond_daily:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~iscellstr(old_field_name)) || (~iscellstr(new_field_name))
        error('dal_txt_rebuild:bond_daily:wrongInput', 'At least one of old or new field names is not a cell of strings.');
    end
    
    if (~isnumeric(all_ind))
        error('dal_txt_rebuild:bond_daily:wrongInput', 'All_ind argument is not numerical vector.');
    end
    
    if (size(source_ds, 1) ~= size(all_ind, 1))
        error('dal_txt_rebuild:bond_daily:wrongInput', 'The dimenion of all_ind argument is not equal to source_db one.');    
    end

    %% Step #1 (Absent data simulation):
    bond_id = uint16(all_ind);
    mod_duration = source_ds.duration;
    eff_duration = source_ds.duration * 0.9;
    cap_listed_4_trade = source_ds.market_cap * 0.9;
    
    %% Step #2 (Creation bond_daily dataset):
    target_ds = horzcat(source_ds(:, old_field_name), dataset(bond_id, ...
                                                                                            mod_duration, ...
                                                                                            eff_duration, ...
                                                                                            cap_listed_4_trade));
    target_ds = target_ds(:, new_field_name);
    
    %% Step #3 (Format dates to numeric format):
    target_ds.date = uint32(datenum(target_ds.date, 'yyyy-mm-dd'));
    
    %% Step #4 (Translate percents to decimal fraction):
    target_ds.value_change = target_ds.value_change / 100;
    target_ds.ytm_bruto = target_ds.ytm_bruto / 100;
    target_ds.ytm_neto = target_ds.ytm_neto / 100;
    
end

