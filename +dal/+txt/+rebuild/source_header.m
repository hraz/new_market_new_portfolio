classdef source_header < hgsetget
    %SOURCE_HEADER_BOND Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        bond_data_format = '%d%s%s%s%s%s%s%s%s%s%s%f%f%d%f%f%f%s%s%f%f%d';
        bond_new_header = {'isin', 'name', 'sector', 'group', 'linkage', 'coupon_type', ...
                                          'issue_date', 'date', 'value', 'value_change', 'turnover', 'ytm_bruto', 'ytm_neto', ...
                                          'duration', 'rank_maalot', 'rank_midroog', 'market_cap', 'convexity', 'issuer_id'};
        bond_issue_header = {'isin', 'name', 'linkage', 'coupon_type', 'issue_date', 'issuer_id'};
        bond_daily_header = {'date', 'value', 'value_change', 'turnover', 'ytm_bruto', 'ytm_neto', 'market_cap', 'convexity'};
        
        bindex_data_format = '%d%s%s%s%f%f%f%f%f';
        bindex_new_header = {'uuid', 'name', 'date', 'value_change', 'value', 'market_cap', 'duration', 'yield'};
        bindex_issue_header = {'uuid', 'name'};
        bindex_daily_header = {'date', 'value_change', 'value', 'market_cap', 'duration', 'yield'};
    end
    
    methods
    end
    
end

