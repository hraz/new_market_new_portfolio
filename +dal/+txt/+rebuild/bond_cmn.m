function [ target_ds, old_last_ind, old_ind, cmn_id ] = bond_cmn( source_ds, unique_field, old_field_name, new_field_name, group_ind, sector_ind )
%BOND_CMN create dataset for bond_cmn table.
%
%   [ target_ds, old_last_ind, old_ind, cmn_id ] = bond_cmn( source_ds, unique_field, old_field_name, new_field_name, group_ind, sector_ind )
%   receive source dataset, information about unique field, list of exist
%   fields, full list of requested fields, and id values of foreign tables like
%   group_id and sector_id; returns dataset for bond_cmn table.
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       unique_field - it is a string value specifying the name of unique
%                            field in target dataset.
%
%       old_field_name - it is an 1xMFIELDS cell array specifying the
%                            exist fields.
%
%       new_field_name - it is an 1xKFIELDS cell array specifying the
%                           required fields.
%
%       group_ind - it is an NGROUPSx1 vector of integers specifying the
%                           bond group id number.
%
%       sector_ind - it is an NSECTORSx1 vector of integers specifying the
%                           bond sector id number.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
%       old_last_ind - it is an MOBSERVATIONSx1 vector specifying the last row
%                           index of each unique value with given field name, so
%                           TARGET_DS = SOURCE_DS(old_last_ind).
%
%       old_ind - it is an NOBSERVATIONSx1 vector specifying indices of unique
%                           values with given field name in source dataset
%                           SOURCE_DS = TARGET(old_ind).
%
%       cmn_id - it is an NASSETSx1 vector specifying the Primary Key of the
%                           bond_cmn table.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(6, 6, nargin));
    
    if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bond_cmn:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~ischar(unique_field))
        error('dal_txt_rebuild:bond_cmn:wrongInput', 'Unique field name is not a string.');
    end
    
    if (~iscellstr(old_field_name)) || (~iscellstr(new_field_name))
        error('dal_txt_rebuild:bond_cmn:wrongInput', 'At least one of old or new field names is not a cell of strings.');
    end
    
    if (~isnumeric(group_ind)) || (~isnumeric(sector_ind))
        error('dal_txt_rebuild:bond_cmn:wrongInput', 'At least one of group_ind or sector_ind arguments is not numerical vector.');
    end
    
    if (size(source_ds, 1) ~= size(group_ind, 1)) || (size(source_ds, 1) ~= size(sector_ind, 1))
        error('dal_txt_rebuild:bond_cmn:wrongInput', 'The dimenion of at least one of group_ind or sector_ind arguments is not equal to source_db one.');    
    end
    
    if (~isempty(source_ds)) && (isempty(strfind(fieldnames(source_ds), unique_field)))
        error('dal_txt_rebuild:bond_cmn:wrongInput', 'There is no such unique field in given dataset.');
    end
    
    %% Step #1 (Create base dataset on base given source data):
    [target_ds, old_last_ind, old_ind] = unique( source_ds(:, old_field_name), unique_field, 'last' );
    Leng = length(old_last_ind);
    
    %% Step #2 (Creating string uuid from numeric):
    target_ds = replacedata(target_ds, strcat({'IL'}, num2str(target_ds.tase_uuid)), 'tase_uuid'); 
    
    %% Step #3 (Format dates in numeric format):
    target_ds = replacedata(target_ds, datenum(target_ds.issue_date, 'yyyy-mm-dd'), 'issue_date'); 

    %% Step #4 (Create like AUTO_INCREMENT index):
    cmn_id = uint16([1 : Leng]');
    
    %% Step #5 (Create symbol field data):
    symbol = strcat({'S'}, target_ds.tase_uuid);
    
    %% Step #6 (Adjust grloup_id by given row indices):
    group_id = uint8(group_ind(old_last_ind));
    
    %% Step #7 (Adjust sector_id by given row indices):
    sector_id = uint8(sector_ind(old_last_ind));
    
    %% Step #8 (Simulate absent common data):
    % Linkage data
    for i = 1 : Leng
        if ~isempty(target_ds.linkage{i})
            base_cpi_curr(i, 1) = rand / 10;
            base_cpi_curr_date(i, 1) = target_ds.issue_date(i);
        else
            target_ds.base_cpi_curr(i, 1) = NaN;
            target_ds.base_cpi_curr_date(i, 1) = NaN;
        end
    end
    
    % Create face values of bonds
    notional_amount = 100*ones(Leng, 1);
    redemption_value =  99 * ones(Leng, 1);
    
    % Create bond common dates
    first_coupon_date = zeros(Leng, 1);
    last_coupon_date = zeros(Leng, 1);
    start_date  = zeros(Leng, 1);
    for i = 1:Leng
        first_coupon_date(i, 1) = uint32(addtodate(double(target_ds.issue_date(i)), 1, 'year'));
        last_coupon_date(i, 1) = uint32(addtodate(double(target_ds.issue_date(i)), round(rand)+1, 'year'));
        start_date(i, 1) = uint32(addtodate(double(target_ds.issue_date(i)), 1, 'day'));    
    end
    maturity_date = last_coupon_date;
    
    % Create last coupon rate
    last_coupon_rate = rand([Leng, 1])/10;
     
     % Create coupon frequency
    coupon_frequency = ones(Leng, 1);

    % Create compounding frequency
    compounding_frequency = ones(Leng, 1);

    % Create discount basis
    discount_basis = 0.5*ones(Leng, 1);

    % End mounth rule
    end_month_rule = uint8(ones(Leng, 1));

    %% Step #9 (Merge given data with simulated):
    target_ds = horzcat(target_ds, dataset(symbol, ...
                                                                       sector_id, ...
                                                                       group_id, ...
                                                                       base_cpi_curr, ...
                                                                       base_cpi_curr_date, ...
                                                                       notional_amount, ...
                                                                       redemption_value, ...
                                                                       first_coupon_date, ...
                                                                       last_coupon_date, ...
                                                                       last_coupon_rate, ...
                                                                       coupon_frequency, ...
                                                                       compounding_frequency, ...
                                                                       discount_basis, ...
                                                                       start_date, ...
                                                                       maturity_date, ...
                                                                       end_month_rule));
                                                                   
    %% Step #10 (Order fields by needed table column order):
    target_ds = target_ds(:, new_field_name);
    
end

