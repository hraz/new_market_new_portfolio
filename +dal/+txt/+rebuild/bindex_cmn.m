function [ target_ds, old_last_ind, old_ind, cmn_id ] = bindex_cmn( source_ds, unique_field, field_name, group_ind )
%BINDEX_CMN create dataset for bond_index_cmn table.
%
%   [ target_ds, old_last_ind, old_ind, cmn_id ] = bond_index_cmn( source_ds, unique_field, old_field_name, new_field_name  )
%   receive source dataset, information about unique field, list of exist
%   fields, full list of requested fields and returns dataset for bond_index_cmn datasete.
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       unique_field - it is a string value specifying the name of unique
%                            field in target dataset.
%
%       field_name - it is an 1xMFIELDS cell array specifying the exist fields.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
%       old_last_ind - it is an MOBSERVATIONSx1 vector specifying the last row
%                           index of each unique value with given field name, so
%                           TARGET_DS = SOURCE_DS(old_last_ind).
%
%       old_ind - it is an NOBSERVATIONSx1 vector specifying indices of unique
%                           values with given field name in source dataset
%                           SOURCE_DS = TARGET(old_ind).
%
%       cmn_id - it is an NASSETSx1 vector specifying the Primary Key of the
%                           bond_index_cmn table.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(4, 4, nargin));
    
    if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bond_index_cmn:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~ischar(unique_field))
        error('dal_txt_rebuild:bond_index_cmn:wrongInput', 'Unique field name is not a string.');
    end
    
    if (~iscellstr(field_name))
        error('dal_txt_rebuild:bond_index_cmn:wrongInput', 'Field_name argument is not a cell of strings.');
    end
       
    if (~isempty(source_ds)) && (isempty(strfind(fieldnames(source_ds), unique_field)))
        error('dal_txt_rebuild:bond_index_cmn:wrongInput', 'There is no such unique field in given dataset.');
    end
    
    %% Step #1 (Create base dataset on base given source data):
    [target_ds, old_last_ind, old_ind] = unique( source_ds(:, field_name), unique_field, 'last' );
    Leng = length(old_last_ind);
    
    %% Step #2 (Index renaming):
    [ target_ds ] = indexrename( target_ds ); 
    
    %% Step #3 (Add foreign key simulation):
    [  target_ds ] = horzcat( target_ds, dataset(group_ind) );
    
    %% Step #3 (Create like AUTO_INCREMENT index):
    cmn_id = uint16([1 : Leng]');
   
end

function [ ds ] = indexrename( ds )

    new_index_name =  {606, 'CPI Linked Non-Government Bonds'; ...
                                     628, 'Currency Linked Non-Government Bonds'; ...
                                     603, 'Non-Government Bonds'; ...
                                     707, 'Tel-Bond 20'; ...
                                     708, 'Tel-Bond 40'; ...
                                     709, 'Tel-Bond 60'; ...
                                     710, 'Tel-Bond Shekel'; ...
                                     601, 'All Bonds'; ...
                                     604, 'CPI Linked Bonds'; ...
                                     626, 'Currency Linked Bonds'; ...
                                     637, 'CPI Linked Gov. Bonds 0-2 Years'; ...
                                     646, 'CPI Linked Gov. Bonds 2-5 Years'; ...
                                     658, 'CPI Linked Gov. Bonds 5-10 Years'; ...
                                     605, 'CPI Linked Government Bonds (Galil)'; ...
                                     627, 'Currency Linked Government Bonds (Gilboa)'; ...
                                     602, 'Government Bonds'; ...
                                     700, 'Non-Linked Fixed-Interest Gov. Bonds (Shahar)'; 
                                     702, 'Non-Linked Fixed-Interest Gov. Bonds 0-2 Years'; ...
                                     703, 'Non-Linked Fixed-Interest Gov. Bonds 2-5 Years'; ...
                                     704, 'Non-Linked Fixed-Interest Gov. Bonds 5+ Years'; ...
                                     701, 'Non-Linked Floating-Interest Gov. Bonds (Gilon)'; ...
                                     705, 'Non-Linked Floating-Interest Gov. Bonds 0-5 Years'; ...
                                     690, 'Non-Linked Government Bonds'; ...
                                     800, 'Short-Term Treasury Bill'; ...
                                     692, 'Redemption - 7 - 10 - Government'; ...
                                     695, 'Linked'; ...
                                     698, 'Linked to CPI - Fixed Interest - Government'; ...
                                     706, 'Redemption 5+ years Non-Linked - Variable Interest - Government'};
                                 
    [~, ind] = ismember([new_index_name{:,1}]', ds.tase_uuid);
    ds.index_name(ind(:)) = new_index_name(:,2);
   
end
