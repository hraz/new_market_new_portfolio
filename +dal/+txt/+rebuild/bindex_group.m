function [ target_ds, group_ind ] = bindex_group(bindex_id)
%BINDEX_GROUP create dataset for bond_index_group table.
%
%   [ target_ds, group_ind ] = bindex_group(bindex_id)
%   receive bond uuid returns dataset for bond_index_group dataset.
%
%   Inputs:
%       bindex_id - it is a NUUIDSx1 vector specifying bond index id numbers. 
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
%       group_ind - it is an NUUIDSx1 vector specifying the number of group
%                           of each index from the given list.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Step #1 (Declaration of bond index group names):
    group_name = {'Corporate'; 'General'; 'Government'};
    
    %% Step #2 (Creation dataset for bond_index_group table):
    target_ds = dataset( group_name );
    
    %% Step #3 (Creation simulation of unique id for bond_index_group):
    Corporate =  [603;606;628;697;707;708;709;710];
    General = [601;604;626;695];
    Government = [602;605;627;637;646;658;690;692;696;698;700;701;702;703;704;705;706; 800];
                                 
    group_ind = zeros(length(target_ds), 1);
        
    [~, corp_ind] = ismember(Corporate, bindex_id);
    group_ind(corp_ind(:)) = 1;
    
    [~, gen_ind] = ismember(General, bindex_id);
    group_ind(gen_ind(:)) = 2;
    
    [~, gov_ind] = ismember(Government, bindex_id);
    group_ind(gov_ind(:)) = 3;

end

