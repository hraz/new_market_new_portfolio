clear all;
clc;

%% Import external packages:
import dal.txt.txtrebuilder.*;

%% Step #1 (Declaration of const data):
data_place = 'D:\Database';
source_path = fullfile(data_place, 'raw_source');
target_path =  fullfile(data_place, 'db_source');

%% Step #2 (Creating text files according to bond database tables):
tic
txtrebuilder(source_path, target_path);
toc