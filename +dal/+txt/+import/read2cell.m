function [ data ] = read2cell( file, permission, machine_format, encoding, data_format, delimiter )
%READ2CELL receives parameters of imported file and the data in it, and return
% headers and raw data.
%
%   [ data ] = read2cell(file, permission, machine_format, encoding, data_format, delimiter) 
%   receives many parameters like: file name, how it must be opened (permision),
%   in what format (machine format), what kind of encoding of included data, the
%   format of each row and data delimiter, and returns the header line and the imported data.
%   
%   Inputs:
%       file - it is a MATLABPATH relative partial pathname.  If the
%                                file is not found in the current working
%                                directory, FOPEN searches for it on the MATLAB
%                                search path.  On UNIX systems, file may also start 
%                                with a "~/" or a "~username/", which FOPEN
%                                expands to the current user's home directory or
%                                the specified user's home directory, respectively.
%
%       permission - it is the mode specifying data type access like:
%                                       'r'       open file for reading
%                                       'w'       open file for writing; discard existing contents
%                                       'a'       open or create file for writing; append data to end of file
%                                       'r+'	open (do not create) file for reading and writing
%                                       'w+'    open or create file for reading and writing; discard 
%                                                   existing contents
%                                       'a+'    open or create file for reading and writing; append data 
%                                                   to end of file
%                                       'W'       open file for writing without automatic flushing
%                                       'A'       open file for appending without automatic flushing
%
%       machine_format - it is a format in wich the imported data written. It may
%                                             be one of the following strings:
%                                       'native'            or 'n' - local machine format - the default
%                                       'ieee-le'           or 'l' - IEEE floating point with little-endian
%                                                                                    byte ordering
%                                       'ieee-be'           or 'b' - IEEE floating point with big-endian
%                                                                                    byte ordering
%                                       'ieee-le.l64'	or 'a' - IEEE floating point with little-endian
%                                                                                    byte ordering and 64 bit long data type
%                                       'ieee-be.l64'	or 's' - IEEE floating point with big-endian byte
%                                                                                   ordering and 64 bit long data type.
%
%       encoding - it is a string that specifies the character encoding scheme
%                                associated with the file. It must be the empty string ('') or
%                                a name or alias for an encoding scheme. Some examples
%                                are 'UTF-8', 'latin1', 'US-ASCII', and 'Shift_JIS'. For common names 
%                                and aliases, see the Web site:
%                                http://www.iana.org/assignments/character-sets. 
%                                If encoding is unspecified or is the empty string (''),
%                                MATLAB's default encoding scheme is used.
%
%       data_format - it is a string is of the form:  %<WIDTH>.<PREC><SPECIFIER><WIDTH>
%                               is optional and specifies the number of characters in a field
%                               to read. Inserting a * in front of, or instead of, the <WIDTH> 
%                               causes TEXTSCAN to skip the particular field in the input and no 
%                               output cell is created for this conversion. <PREC> only applies
%                               to the family of %f specifiers and states the
%                               number of significant fractional digits to be converted.
%                               <SPECIFIER> must be given and determines the
%                               conversion output type. Supported FORMAT specifiers:
%                                   %n        - read and convert a number to double
%                                   %d        - read and convert a number to int32
%                                   %d8      - read and convert a number to int8
%                                   %d16	- read and convert a number to int16
%                                   %d32	- read and convert a number to int32
%                                   %d64	- read and convert a number to int64 
%                                   %u        - read a number and convert to uint32 
%                                   %u8      - read and convert a number to uint8 
%                                   %u16	- read and convert a number to uint16
%                                   %u32	- read a number and convert to uint32 
%                                   %u64	- read a number and convert to uint64 
%                                   %f        - read a number and convert to double
%                                   %f32	- read a number and convert to single
%                                   %f64	- read a number and convert to double
%                                   %s        - read a string
%                                   %q        - read a (possibly double-quoted) string
%                                   %c        - read a character, including whitespace
%                                   %[...]      - reads characters matching characters between the
%                                                    brackets until first non-matching character. 
%                                                    Use %[]...] to include ] in the set.
%                                   %[^...]    - reads characters not matching characters between the
%                                                    brackets until first matching character.
%                                                    Use %[^]...] to exclude ] in the set.
%
%       delimiter - it is delimiter characters (default is a space character).
%
%   Notes:
%       For more information see fopen, textscan, fgetl and fclose helps.
%
%
%   Outputs:
%       data - it is a structure, each field of which is the inputted data
%                   property that measured through all given observations.
%
% Yigal Ben Tal
% Copyright 2010 - 2011.

    %% Input validation:
    error(nargchk(6, 6, nargin));
    
    InputedArgs = {file, permission, machine_format, encoding, data_format, delimiter};
    if (~all(iscellstr(InputedArgs)))
        error('dal_export_txt:read:mismatchInput', ...
            'There at least one inputted argument is not string.');
    end
    
    %% Step #1 (Open the needed file):
    [fid, msg]= fopen(file, permission, machine_format, encoding);
    if (~isempty(msg)), error('dal_export_txt:read:fopenError', msg); end
    
    %% Step #2 (Read the header line):
    header = textscan(fgetl(fid),'%s','delimiter',delimiter);
    header = header{:};
    
    %% Step #3 (Read the raw data from the file starting in the second row);
    raw_data = textscan(fid, data_format, 'delimiter', delimiter);

    %% Step #4 (Close the opened file):
    fclose(fid); clear fid;
    
    %% Step % 5 (Collecting raw data corresponding to given field header):
    headerL = length(header);
    for i = 1:headerL
        data.( header{i} ) = raw_data{i};
    end
    
end
