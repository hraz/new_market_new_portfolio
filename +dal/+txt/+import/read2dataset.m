function [ ds ] = read2dataset( source_file, data_format, delimiter, hasheader )
%READ2DATASET read data from txt-source_file into dataset object.
%
%   [ ds ] = read2dataset( source_file, data_format, delimiter, hasheader ) 
%   creates a dataset array by reading column-oriented data in a tab-delimited
%   text source_file.  The dataset variables that are created are either double-valued,
%   if the entire column is numeric, or string-valued, i.e. a cell array of
%   strings, if any element in a column is not numeric.  Fields that are empty
%   are converted to either NaN (for a numeric variable) or the empty string
%   (for a string-valued variable).  Insignificant whitespace in the source_file is ignored.
%
%   Inputs:
%       source_file - it is a string value specifying the full path to source txt-source_file
%                that includes needed data.
%
%       data_format - it is a string value specifying the data format as
%                accepted by the textscan function. 
%
%       delimiter - it is a string value specifying the delimiter between
%               columns. It can be any of ' ', '\t', ',', ';', '|' or their
%               corresponding string names 'space', 'tab', 'comma', 'semi', or
%               'bar'.  Default value is '\t'.
%
%       hasheader - it is boolean value (1 or 0) specifying existence of header
%               row in given source_file. Default value is true.
%
%   Outputs:
%       ds - it is a dataset object specifying all imported data from the
%               source_file. See description about DATASET function in Statistic Toolbox.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(2 ,4, nargin));

    if (nargin < 4)
        hasheader = true;
    end
    
    if (nargin < 3)
        delimiter = '\t';
    end
    
    %% Read needed data:
    [ ds ] = dataset('file', source_file, 'format', data_format, 'Delimiter', delimiter, 'ReadVarNames', hasheader);
    
end
