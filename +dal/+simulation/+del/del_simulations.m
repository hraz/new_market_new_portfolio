function [outStatus, outException] = del_simulations( )
%DEL_SIMULATIONS clears all data in the simulation database.
%
%   [outStatus, outException] = del_simulations( )
%   Clears all data in the simulation database.
%
%   Input:
%      None.
%
%   Output:
%           outStatus - is a boolean value specifying the success of the operation.
%
%           outException - is a string value specifying the error message. If operation 
%                                            successed exception will be an empty string. 
%
% Created by: Yigal Ben Tal
%                    at: 08.09.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: ________________, 
%                    at: ________________. 
% The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
        error(nargchk(0,0, nargin));

        %% Step #1 (Build SQL query):
        sqlquery = 'CALL clear_simulation();';

        %% Step #2 (Open the connection):
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_del:del_simulations:connectionFailure', errMsg);
        end
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):
        exec(conn, sqlquery);
        if (nargout > 0)
            outStatus = true;
        end

        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end
    
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end    
    end
       
end
