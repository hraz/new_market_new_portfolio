function [outStatus, outException] = del_portfolio_by_simid_and_time( simIDList, fromTime, toTime )
%DEL_PORTFOLIO_BY_SIMID_AND_TIME clears all predefined portfolios in the given time period.
%
%   [outStatus, outException] = del_portfolio_by_simid_and_time( simIDList, fromTime, toTime )
%   Clears all portfolios, which were created in the given time period.
%
%   Input:
%      simIDList - is a NSIMULATIONIDx1 numerical vector specifying the simulation IDs 
%                                             from which will be deleted porfolios.
%
%       fromTime - is a DATETIME value specifying the start of portfolio creation period.
%
%       toTime - is a DATETIME value specifying the end of portfolio creation period.
%
%   Output:
%           outStatus - is a boolean value specifying the success of the operation.
%
%           outException - is a string value specifying the error message. If operation 
%                                            successed exception will be an empty string. 
%
% Created by: Yigal Ben Tal
%                     at: 08.09.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: ________________, 
%                     at: ________________.
% The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.num2str;

    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
        error(nargchk(3, 3, nargin));

        if isempty (simIDList) || any(isnan(simIDList))
            simIDList = 'NULL';
        elseif ~isnumeric(simIDList)
            error('dal_simulation_del:del_portfolio_by_simid_and_time:mismatchInput',...
                'The given simulation list must includes just numerical values.');
        else
            simIDList = ['"', num2str(simIDList), '"'];
        end
        
        if isempty(fromTime)
            error('dal_simulation_set:del_portfolio_by_simid_and_time:mismatchInput', ...
                'fromTime cannot be an empty.');
        elseif isnumeric(fromTime)
            fromTime = ['"', datestr(fromTime, 'yyyy-mm-dd HH:MM:SS'), '"'];
        end
        
        if isempty(toTime)
            error('dal_simulation_set:del_portfolio_by_simid_and_time:mismatchInput', ...
                'toTime cannot be an empty.');
        elseif isnumeric(toTime)
            toTime = ['"', datestr(toTime, 'yyyy-mm-dd HH:MM:SS'), '"'];
        end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL del_portfolio_by_simid_and_time(',  simIDList, ', ', fromTime, ', ', toTime,  ');'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_del:del_portfolio_by_simid_and_time:connectionFailure', errMsg);
        end
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):
        msg = fetch(conn, sqlquery);
        if (nargout > 0)
            outStatus = true;
            if (nargout > 1)
                outException = msg{:};
            end
        end

        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end
    
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end    
    end
       
end
