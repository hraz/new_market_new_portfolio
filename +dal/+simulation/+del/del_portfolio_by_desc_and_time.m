function [outStatus, outException]  = del_portfolio_by_desc_and_time( portDescList, fromTime, toTime )
%DEL_PORTFOLIO_BY_DESC_AND_TIME clears all predefined portfolios in the given time period.
%
%   [outStatus, outException] = del_portfolio_by_simid_and_time( portDescList, fromTime, toTime )
%   Clears all portfolios, which were created in the given time period.
%
%   Input:
%      portDescList - is a NSIMULATIONIDx1 cell of strings specifying the deleted portfolio 
%                                             descriptions.
%
%       fromTime - is a DATETIME value specifying the start of portfolio creation period.
%
%       toTime - is a DATETIME value specifying the end of portfolio creation period.
%
%   Output:
%           outStatus - is a boolean value specifying the success of the operation.
%
%           outException - is a string value specifying the error message. If operation 
%                                            successed exception will be an empty string. 
%
% Created by: Yigal Ben Tal
%                     at: 08.09.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: ________________, 
%                     at: ________________.
% The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.cell2str4sql;

    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
        error(nargchk(3, 3, nargin));

        if isempty (portDescList)
            portDescList = 'NULL';
        elseif ~iscellstr(portDescList)
            error('dal_simulation_del:del_portfolio_by_simid_and_time:mismatchInput',...
                'The given portfolio description list must be a cell-array of strings.');
        else
            portDescList = ['"', cell2str4sql(portDescList), '"'];
        end
        
        if isempty(fromTime)
            error('dal_simulation_set:del_portfolio_by_simid_and_time:mismatchInput', ...
                'fromTime cannot be an empty.');
        elseif isnumeric(fromTime)
            fromTime = ['"', datestr(fromTime, 'yyyy-mm-dd HH:MM:SS'), '"'];
        end
        
        if isempty(toTime)
            error('dal_simulation_set:del_portfolio_by_simid_and_time:mismatchInput', ...
                'toTime cannot be an empty.');
        elseif isnumeric(toTime)
            toTime = ['"', datestr(toTime, 'yyyy-mm-dd HH:MM:SS'), '"'];
        end

        %% Step #1 (Build SQL query):
        sqlquery = ['CALL del_portfolio_by_simid_and_time(',  portDescList, ', ', fromTime, ', ', toTime,  ');'];

        %% Step #2 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_del:del_portfolio_by_simid_and_time:connectionFailure', errMsg);
        end
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):
        msg = fetch(conn, sqlquery);
        if (nargout > 0)
            outStatus = true;
            if (nargout > 1)
                outException = msg{:};
            end
        end

        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end
    
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');
        end    
    end
       
end
