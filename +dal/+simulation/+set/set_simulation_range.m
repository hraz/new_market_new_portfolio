function [outStatus, outException] = set_simulation_range( numOfSimulations )
%SET_SIMULATION_RANGE define the full list of simulations, which will be run.
%
%   [outStatus, outException] = set_simulation_range( numOfSimulations )
%   This procedure receives the number of required simulations and create 
%   the full list of the simulation ID s from 1 to the given number.
%
%   Input:
%       numOfSimulations - is an INTEGER value specifying the maximal number
%                                                       of required simulation list.
%
%   Output:
%       outStatus - is a BOOLEAN value specifying if the insert was successful.
%
%       outException - is a STRING value specifying error message if the
%                        insert was unsuccessful, null otherwise.
%
% Created by: Yigal Ben Tal,
%                     at: 08.09.2013
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by: _______________________,
%                     at: _______________________.
% The sense of update: ___________________________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    try
        %% Input validation:
        error(nargchk(1, 1, nargin));

        if isempty(numOfSimulations)
            error('dal_simulation_set:set_simulation_range:mismatchInput', ...
                'numOfSimulations cannot be an empty.');
        elseif ~isnumeric(numOfSimulations)
            error('dal_simulation_set:set_simulation_range:wrongFormat', ...
                'numOfSimulations must be an integer number.');
        else
            numOfSimulations = num2str(numOfSimulations);
        end
    
        outException = '';
        
        %% Step #1 (Create sql query):
        sqlquery = ['CALL simulation.set_simulation_range(', numOfSimulations, ');'];
   
        %% Step #2 (Create connection to database):
        setdbprefs ('DataReturnFormat','numeric');
        [conn, outExceptionMsg] = mysql_conn('simulation');
        if ~isempty(outExceptionMsg)
            error('dal_simulation_set:set_simulation_range:connectionFailure', outExceptionMsg);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Insert data into database):
        fetch(conn,sqlquery);
        if (nargout > 0) 
            outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Simulation ID list is created.'];
            end
        end
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
    
    catch ME
        outException = ME.message;
        outStatus = false;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    end
    setdbprefs ('DataReturnFormat','dataset');
end
