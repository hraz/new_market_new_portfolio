function [outStatus, outException] = set_optimization_details( details )
%SET_OPTIMIZATION_DETAILS insert solver analisys to the database
%
%   [outStatus, outException]  = set_optimization_details( details )
%
%   Input:
%       details -  it is a  nX15 dataset specifying data about the solver
%                             simID - is an INTEGER scalar specifying the number of the simulation.
%
%                             portfolioDesc - is a STRING value specifying the simulated portfolio description.
%
%                             callNum -  is an INTEGER value specifying the number of call that was made to any of 
%                                               the solver function in the current portfolio.
%
%                             errFlag - is an INTEGER value specifying the matlab exit flag (positive - solver was 
%                                               successful, negative - solver faild)
%
%                             iterations - is an INTEGER value specifying the number of iteration done by the solver.
%
%                             constrViolation - is a DOUBLE value specifying the size of constraint violation.
%
%                             algo - is a STRING value specifying the name of used algorithm in the current solver.
%
%                             cgIterations -  is a DOUBLE value specifying the total value of PCG iterations.
%
%                             msg - is a STRING value specifying the exit message.
%
%                             firstOrderOpt - is a DOUBLE value specifying the measure of first order optimality.
%
%                             firstExcessDuration - is a DOUBLE value specifying some unknown parameter.
%
%                             secondExcessDuration - is a STRING value specifying some unknown parameter.
%
%                             infeasibleNonDurationConstStart - is a BOOLEAN value specifying which indicates 
%                                               whether the initial weights are infeasible in the sense that one of the 
%                                               non-duration inequality constraints doesn't hold. 
%
%                             infeasibleDurationConstStart - is a BOOLEAN value specifying the same as above, 
%                                               for the duration constraints.
%
%                             infeasibleNonDurationConstFinish - is a BOOLEAN value specifyin the same as above, 
%                                               for the final weights returned from the solver
%
%                             infeasibleDurationConstFinish - is a BOOLEAN value specifying the same as above, 
%                                               for the final weights from the solver
%                           
%	Output:
%       outStatus - is a BOOLEAN value specifying if the insert was successful.
%
%       outException - is a STRING value specifying error message if the insert was unseccessful, 
%                                               null otherwise.
%
% Created by: Yaakov Rechtman,
%                    at: 20.06.2012
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by:	Yigal Ben Tal, 
% at 16.07.2013. 
% The sense of update: remove saved data to independed simulation database.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    import utility.dataset.*;

    %% Input validation: 
    outStatus = true;
     if (nargout > 1)
        outException = '';
     end
    try
        error(nargchk(1, 1, nargin));
        if(isempty(details))
              error('dal_simulation_set:set_optimization_details:wrongInput', ...
                'The input parameter cannot be an empty.');
        end
        if ~isa(details, 'dataset')
            error('dal_simulation_set:set_optimization_details:wrongInput', ...
                'The input parameter must be a dataset.');
        end
        
        %% Step #1 (open connection):
        setdbprefs ('DataReturnFormat','cellarray');
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_set:set_optimization_details:connectionFailure', errMsg);
        else
            set(conn,'AutoCommit','off');
        end
        
        %% Step #2(Build  and execute SQL query):
        for iRow=1:length(details)
            iRowDetails=details(iRow,:);
            sqlquery = ['CALL simulation.set_optimization_details(', num2str(iRowDetails.simId), ',"', ...
                iRowDetails.portfolioDesc{:}, '",', ...
                num2str(iRowDetails.callNum), ',', ...
                num2str(iRowDetails.errFlag), ',', ...
                num2str(iRowDetails.iterations), ',', ...
                num2str(iRowDetails.constrViolation), ',"', ...
                iRowDetails.algo{:}, '",', ...
                num2str(iRowDetails.cgIterations), ',"', ...
                iRowDetails.msg{:}, '",', ...
                num2str(iRowDetails.firstOrderOpt), ',', ...
                num2str(iRowDetails.firstExcessDuration), ',"', ...
                iRowDetails.secondExcessDuration{:}, '",', ...
                num2str(iRowDetails.infeasibleNonDurationConstStart), ',', ...
                num2str(iRowDetails.infeasibleDurationConstStart), ',', ...
                num2str(iRowDetails.infeasibleNonDurationConstFinish), ',', ...
                num2str(iRowDetails.infeasibleDurationConstFinish), ');' ];
            
            %% Step #3(Execution of built SQL sqlquery):
            fetch(conn,sqlquery);
            if (nargout > 0)
                outStatus = true;
                if (nargout > 1)
                    outException = [outException, 'Optimization details are saved.'];
                end
            end
        end
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
        outException = ME.message;
        outStatus = false;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    end
   setdbprefs ('DataReturnFormat','dataset');
end
