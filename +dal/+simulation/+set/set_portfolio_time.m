function [ outStatus, outException ] = set_portfolio_time( simID, portfolioDesc, isStartTime )
%SET_PORTFOLIO_TIME save the finish time of the given portfolio.
%
%   [ outStatus, outException ] = set_portfolio_time( simID, portfolioDesc, isStartTime )
%   saves for the given simulation ID its start or finish time.  Note that
%   this function must first be called for start time (isStartTime is TRUE)
%   before it can be called for finish time (isStartTime is FALSE).
%
%   Input:
%       simID - is an INTEGER value specifying the given simulation ID.
%
%       portfolioDesc - is a STRING value specifying the given portfolio
%                       description.  Note that if isStartTime is FALSE, the portfolioDesc
%                       needs to be exactly what was specified for the given simID when the function was called
%                       when isStartTime was TRUE.
%
%       isStartTime - is a BOOLEAN value specifying the type of the time point. 
%                                               In case of the start point it must be TRUE, in case of the finish point
%                                               it must be FALSE. When the value is TRUE the function additionally saves in addition 
%                                               the current machine name as a runner name. The case of the empty value or
%                                               absent value defaults to FALSE.
%
%   Output:
%       outStatus - is a boolean value specifying if the insert was successful.
% 
%       outException - is a STRING value specifying error message.
%
%   Note: This function must be used in any case - simulation assumed or error occures!

% Created by: Yigal Ben Tal,
%                     at: 08.09.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: ______________
%                at: ______________
% The sense of update: _____________________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.get_computer_name;

    %% Input validation:
    try
        error(nargchk(2, 3, nargin));

        if (isempty(simID))
            error('dal_simulation_set:set_portfolio_time:mismatchInput',...
                'simID cannot be an empty.');
        elseif ~isnumeric(simID)
            error('dal_simulation_set:set_portfolio_time:wrongFormat',...
                'simID must be a numeric value.');
        else
            simID = num2str(simID);
        end

        if isempty(portfolioDesc)
            error('dal_simulation_set:set_portfolio_time:mismatchInput',...
                'portfolioDesc cannot be an empty.');
        elseif ~ischar(portfolioDesc)
            error('dal_simulation_set:set_portfolio_time:wrongFormat',...
                'portfolioDesc must be a string value.');
        else
            portfolioDesc = ['"', portfolioDesc, '"'];
        end
        
        if (nargin < 3) || (isempty(isStartTime))
            isStartTime = false;
        elseif ~islogical(isStartTime)
            error('dal_simulation_set:set_portfolio_time:mismatchInput', ...
                'isStartTime parameter must have just boolean value.');
        else
            runnerName = ['"', get_computer_name(), '"'];    
        end
        
        outException = '';
        currentTime = ['"', datestr(now(), 'yyyy-mm-dd HH:MM:SS'), '"'];
                
        %% Step #1 (Create sql query):
        if isStartTime
            sqlquery = ['CALL simulation.set_portfolio_start_time(', simID, ',', portfolioDesc, ',', currentTime, ',', runnerName, ')';];
        else
            sqlquery = ['CALL simulation.set_portfolio_finish_time(', simID, ',', portfolioDesc, ',', currentTime, ')';];
        end
        
        %% Step #2 (Create connection to database):
        setdbprefs ('DataReturnFormat','cellarray');
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_set:set_portfolio_time:connectionFailure', errMsg);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Insert data into database):
        fetch(conn,sqlquery);
       if (nargout > 0) 
            outStatus = true;
            if (nargout > 1)
                if isStartTime
                    outException = [outException, 'Portfolio start time is saved.'];
                else
                    outException = [outException, 'Portfolio finish time is saved.'];
                end
            end
       end
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
    
    catch ME
        outException = ME.message;
        outStatus = false;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    end
    setdbprefs ('DataReturnFormat','dataset');
end
