function [ sim_id ] = get_sim_id(minID, maxID  )
%GET_SIM_ID get simulation id which has not done yet
%   
%  [ sim_id ] = get_sim_id(minID,maxID  ) - returning simulation id which has not been done yet between max and min sim_id
% 
%    Input:
%           minID - is a NUMERIC value specifying the minimum simulation id on which to get the sim id
% 
%           maxID - is a NUMERIC value specifying the maximum simulation id on which to get the sim id
%     
%   Output:
%              sim_id - is an INTEGER specifying the sim_id which has not been done yet.    
%
% Created by Yaakov Rechtman.
% Date:		10.09.2012
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by: Yigal Ben Tal
% at 14.07.2013. 
% The sense of update: removing data about the simulation into independent database.

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    error(nargchk(2, 2, nargin));   
     
    %% Step #1 (Execution of sql function):
    sqlquery = ['CALL simulation.get_free_sim_id(', num2str(minID), ', ', num2str(maxID) ');'];
    
    %% Step #2 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','numeric');                                                     
   [conn, errMsg] = mysql_conn('simulation');
    if ~isempty(errMsg)
        error('dal_simulation_get:get_free_sim_id:connectionFailure', errMsg);
    end
    
   %% Step #3 (Execution of built SQL sqlquery):
    try
        ds = fetch(conn, sqlquery);
        sim_id = double(ds);
       
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');  
        end
        
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');  
        end 
        error('dal_simulation_get:get_sim_errors:connectionFailure', ME.message);
    end
    
end

