function [ runTimeList ] = get_simulation_run_time( simIDList )
%GET_SIMULATION_RUN_TIME return running time of the given simulations.
%
%   [ runTimeList ] = get_simulation_run_time( simIDList )
%   return the list of running time for the given simulations.
%
%   Input:
%       simIDList - is a NSIMULATIONSx1 vector specifying the simulation ID list.
%                                       In case of the empty input will be return all saved simulations.
%
%   Output:
%       runTimeList - is a DATASET specifying the list of running time for 
%                                       the given simulatios with follow fields:
% 
%           simID - is an INT UNSIGNED value specifying the some of given simulation IDs.
%
%           runTime - is a TIMESTAMP in seconds of the simulation running time.

% Created by: Yigal Ben Tal
%                    at: 08.09.2013
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.num2str;

    try
        %% Input validation:
        error(nargchk(1, 1, nargin));   
        
        if isempty (simIDList) || any(isnan(simIDList))
            simIDList = '"NULL"';
        elseif ~isnumeric(simIDList)
            error('dal_simulation_get:get_simulation_run_time:mismatchInput',...
                'The given simulation list must includes just numerical values.');
        else
            simIDList = ['"', num2str(simIDList), '"'];
        end
        
        %% Step #1 (Create sql query):
        sqlquery = ['CALL simulation.get_simulation_run_time(', simIDList, ')'];

        %% Step #2 (Execution of built SQL query):
        setdbprefs ('DataReturnFormat','dataset');                                                     
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_get:get_simulation_run_time:connectionFailure', errMsg);
        end

        %% Step #3 (Execution of built SQL sqlquery):
        runTimeList = fetch(conn, sqlquery);
       
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
        error('dal_simulation_get:get_simulation_run_time:connectionFailure', ME.message);
    end
    
end