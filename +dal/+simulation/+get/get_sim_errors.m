function [ ds ] = get_sim_errors(errMsg)
%GET_SIM_ERRORS returns a list of simulation errors.
% 
%    [ err_msg ] = get_sim_errors(errMsg)
%
%   Input:
%       errMsg - is an STRING value specifying the error message on which to
%                       get the data. If input is empty the function returns full list of saved data.
%
%   Output:
%       ds - is a dataset specifying the full list of simulation errors.
%
% Created by Yaakov Rechtman.
% Date:		20.06.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: Yigal Ben Tal
% at 14.07.2013. 
% The sense of update: removing data about the simulation into independent database.

%% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    error(nargchk(0, 1, nargin));
    if (nargin == 0) | (isempty(errMsg))
        errMsg = 'NULL';
    end
    
     %% Step #1 (SQL sqlquery for this filter):
     sqlquery = ['CALL simulation.get_errors("', errMsg ,'");' ];

    %% Step #2 (Create a connection):
    setdbprefs ('DataReturnFormat','dataset');
    [conn, errMsg] = mysql_conn('simulation');
    if ~isempty(errMsg)
        error('dal_simulation_get:get_sim_errors:connectionFailure', errMsg);
    end
    
   %% Step #3 (Execution of built SQL sqlquery):
    try
        ds = fetch(conn,sqlquery);
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
        error('dal_simulation_get:get_sim_errors:connectionFailure', ME.message);
    end
         
end

