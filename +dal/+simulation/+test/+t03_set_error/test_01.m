%% Import tested directory:
import dal.simulation.test.t03_set_sim_analysis.*;

%% Test description:
funName = 'set_sim_analysis';
testDesc = 'Empty imput: -> error message:';

%% Expected results:
expectedStatus = 0;
expectedError = 'dsErrSummary cannot be an empty.';

%% Input definition:
obsDate = '2010-07-01 12:12:12';
portfolioID = 1;
dsErrSummary = dataset();

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, dsErrSummary );
