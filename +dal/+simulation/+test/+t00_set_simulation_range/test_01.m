%% Import tested directory:
import dal.simulation.test.t00_set_simulation_range.*;

%% Test description:
funName = 'set_simulation_range';
testDesc = 'Set the simulation range -> empty result set:';

%% Expected results:
expectedStatus = 1;
expectedError = 'Simulation ID list is created.';

%% Input definition:
numOfSimulations = 100;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, numOfSimulations);
