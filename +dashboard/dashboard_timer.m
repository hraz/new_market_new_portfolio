%% There is a script that can refresh shown on the dashboard data periodically.
%
% Amit Godel
% Copyright 2012, BondIT Ltd.

%% Import external packages:
% import dal.txt.import.*;
% import dashboard.*;

%% Upload dashboard configuration and initialization of timer parameters:
 if exist('dashboard.cnf', 'file') 
    cnf = dal.txt.import.read2cell( 'dashboard.cnf' , 'r',  'n', '', '%f%f', '\t' );
    delay_minutes = cnf.delay_period_in_minutes;
    if cnf.num_of_tasks < 0
        ntasks2exe = Inf;
    else
        ntasks2exe = cnf.num_of_tasks;
    end
 else
    % Initialization of timer parameters:
    delay_minutes = 0.5;
    ntasks2exe = Inf;
 end
 delay_sec = 60 * delay_minutes;
 
%% Upload dashboard GUI:
dashboard_gui();

%% Initialization of the dashboard timer:
t = timer('StartDelay', 0, 'Period', delay_sec, 'TasksToExecute', ntasks2exe, 'ExecutionMode', 'fixedRate');

t.StartFcn = { @dashboard.timer_state_disp, 'Running in the background.'};
t.StopFcn = { @dashboard.timer_state_disp, 'Auto refresh of dashboard has ended.'};
t.TimerFcn =  @dashboard.dashboard_refresh;
t.ErrorFcn =   @dashboard.dashboard_refresh;

%% Start automatic refresh process of the shown at the dashboard data:
try
    start(t);
catch ME
    ME.message
end;

%% Remove from the memory the time:
% delete(t)

