function [ handles ] = dashboard_exe( handles )
%DASHBOARD_EXE collects and set to GUI handles all represented on the dashboard data.
%
%   [ handles ] = dashboard_exe( handles, menu_choosed_value )
%   receives dashboard handles, choosed portfolio parameters and returns handles with required data.
%
%   Input:
%       handles - is a list of all dashboard handles.
%
%       menu_choosed_value - is a struct with three fields: model, opt_type and hold_period. Each
%                       of the fields has scalar value represented choosed model, optimiztion type
%                       and holding period length.
%
%   Output:
%       handles - is a list of all dashboard handles with updated data.
%
% Developed by Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dashboard.*;
    import dal.portfolio.data_access.*;
    import dal.market.get.dynamic.*;
    import utility.statistics.*;
    import utility.dataset.*;

%     global TIMING
    
% temporary fix for sim_id bounds.  always erase entries so entries never
% limit calls to db.  will likely be in effect until move to new portfolio
% database.
if get(handles.mdl_clear_sim_range_checkbox,'Value')
    set(handles.mdl_sim_from_edit,'String','')
    set(handles.mdl_sim_to_edit,'String','')
end
    %% Step #1 (Get from GUI all current menu choices):
%     t1 = tic;
    [ valueList, currentValue, timePeriodBound, simBound ] = get_current_menu_value( handles );
%     TIMING(1,:) = {'get_current_menu_value', toc(t1)};
    
    %% Step #2 (Setup model's tab):
%     t2 = tic;
    [handles] = setup_model( handles, valueList, currentValue, timePeriodBound, simBound );
%     TIMING(2, :) = {'setup_model', toc(t2)};
    
    portfolioLastUpdate = get(handles.prt_sim_range_pnl, 'UserData');
    portfolioLastUpdate = portfolioLastUpdate{:};
    if ~isempty(portfolioLastUpdate.sim_id)

        % Step #3 (Get random portfolio ID):
        [ simID ] = random_id( portfolioLastUpdate.sim_id );
        ind = ismember(portfolioLastUpdate.sim_id,simID);
        
        % Step #4 (Check benchmark filter):
%         t3 = tic;
        [ chosenBenchmark ] = set_prt_benchmark_listbox( handles, portfolioLastUpdate.portfolio_id(ind), portfolioLastUpdate.obs_date{ind} );
%         TIMING(3, :) = {'get_current_benchmark_id',toc(t3)};
       
        % Step #5 (Set portfolio tab):
%         t4=tic;
%         setup_portfolio( handles, portfolio_last_update, sim_id, portfolio_last_update.portfolio_id(ind), portfolio_last_update.obs_date{ind}, chosen_benchmark);
        setup_portfolio( handles, portfolioLastUpdate, simID, ind, chosenBenchmark);
%         TIMING(4, :) = {'setup_portfolio', toc(t4)};
    end
    
end

