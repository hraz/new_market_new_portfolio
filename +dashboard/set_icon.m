function [] = set_icon( hObject, iconName )
%SET_ICON Summary of this function goes here
%   Detailed explanation goes here
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

%% Find required uicontrol:
% jButton = java(findjobj(hObject));

%% Load interested icon:
myIcon = fullfile('+dashboard', [iconName, '.ico']);
[cdata, map] = imread(myIcon);

%% Convert white pixels into a transparent background:
map((map(:,1)+map(:,2)+map(:,3) == 3)) = NaN; % map(find(map(:,1)+map(:,2)+map(:,3) == 3)) = NaN;

%% Convert into 3D  RGB space:
cdata = ind2rgb(cdata, map);

%% Add the icon to uicontrol:
set(hObject, 'cdata', cdata, 'tooltip', 'Run');

end

