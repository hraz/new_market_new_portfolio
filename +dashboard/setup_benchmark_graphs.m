function [] = setup_benchmark_graphs( handles )
%SETUP_BENCHMARK_GRAPHS graphs the current portfolio and any selected
%   benchmarks on the same graph.
%
%   [] = graph_multiple_benchmarks( handles ) takes in the GUI handles,
%   graphs the current portfolio as well as any benchmarks selected by the
%   user on the same graph in the portfolio tab.
%
%   Input:
%       handles - struct of all GUI handles.
%
%   Output:
%       none
%
% Eleanor Millman, August 1, 2013
% Copyright 2013, BondIT Ltd.