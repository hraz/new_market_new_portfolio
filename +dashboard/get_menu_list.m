function [ menuList, menuDates ] = get_menu_list( chosenValue )
%GET_MENU_list set on dashboard portfolio parameters random values.
%
%   [ menuList, menuDates ] = get_menu_list( chosenValue )
%   receives dashboard GUI handles set and return its already updated.
%
%   Input:
%       chosenValue - is a struct that includes all menu values .
%
%   Output:
%       menuList - is a struct that includes all menu lists.
%
%       menuDates - is a dataset that includes time period bounds.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.portfolio.data_access.*;
    import utility.dataset.*;
    
    %% Input validation:
    error(nargchk(1,1,nargin));
    
    %% Step #1 (Get primary data from database):
    % This function doesn't exist in the new API, so will be necessary to
    % create using mapping functions
    [ res ] = da_menu_options_new(chosenValue); 

    %% Step #2 (Define menu values):
    menuName = fieldnames(res); 
    for i = 1:length(menuName)
        dataName = (dsnames(res.(menuName{i})));
        menuList.(menuName{i}) = res.(menuName{i}).(dataName{1}); %db model name;
    end
    
    %% Step #6 (Define min and amx possible values for possible time period):
    menuDates = dataset( {'2005-01-01'}, {'2012-02-01'}, 'VarNames', {'start_date', 'end_date'}); % ds_menu_dates;
        
end
   