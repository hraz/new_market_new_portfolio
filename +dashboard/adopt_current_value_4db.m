function [ adopted_current_value, adopted_time_bound, adopted_sim_bound ] = adopt_current_value_4db( value_list, current_value, time_bound, sim_bound )
%ADOPT_CURRENT_VALUE_4DB prepare all filters choices to database acceptable string value.
%
%   [ adopted_current_value, adopted_time_bound, adopted_sim_bound ] = adopt_current_value_4db( value_list, current_value, time_bound, sim_bound )
%   receives lists of all filters' possible values, its choices, time and simulation bounds, and
%   returns list of current values, time and simulation bonds adobted to database input format.
%
%   Input:
%
%   Output:
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.

    %% Import external packages:
    import utility.dal.*;
    
    %% Input validation;
    error(nargchk(4,4,nargin));

    %% Step #1 (Build adobted data filter choices):
     menu_list = fieldnames(current_value);
    for i = 1: length(menu_list)
        if isempty(current_value.(menu_list{i})) | any(strcmpi(current_value.(menu_list{i}), [{'All'}; value_list.(menu_list{i})(1) ]))
            adopted_current_value.(menu_list{i}) = 'NULL';
        elseif (ismember(menu_list{i}, { 'target_return'; 'investment_horizon'; 'benchmark'; 'max_weight'}))
            if isnan(str2double(current_value.(menu_list{i})))
                adopted_current_value.(menu_list{i}) = 'NULL';
            else
                adopted_current_value.(menu_list{i})  = current_value.(menu_list{i});
            end
        else
            adopted_current_value.(menu_list{i})  = char4sql(current_value.(menu_list{i}));
        end        
    end
    
    %% Step #2 (Build adobted time bounds):
    time_name = fieldnames(time_bound);
    for j = 1 : length(time_name)
        if isempty(time_bound.(time_name{j}))
            adopted_time_bound.(time_name{j}) = 'NULL';
        else
            adopted_time_bound.(time_name{j}) = cell2str(time_bound.(time_name{j}));
        end
    end
    
    %% Step #3 (Build adobted simulation bounds):
    sim_bound_name = fieldnames(sim_bound);
    for k = 1: length(sim_bound_name)
        if isempty(sim_bound.(sim_bound_name{k}))
            adopted_sim_bound.(sim_bound_name{k}) = 'NULL';
        else
            adopted_sim_bound.(sim_bound_name{k}) = sim_bound.(sim_bound_name{k});
        end
    end
    
end

