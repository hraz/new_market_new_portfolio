function [ handles ] = setup_portfolio( handles, portfolioSim, simID, ind, benchmarkID)
%SETUP_PORTFOLIO Collects information about a specified portfolio and
%   that information.
%
%   [ handles ] = setup_portfolio( handles, portfolioSim, simID, ind, benchmarkID)
%       gets information about the portfolio specified in the portfolio tab
%       and presents that information in the portfolio tab.
%
%   Input:
%       handles - the struct of GUI handles 
%
%       portfolioSim -  a Nx3 dataset.  The rows are all portfolios and the
%           columns are 'portfolio_id', 'obs_date', and 'sim_id'.
%
%       simID - a double that gives the sim ID of the specified portfolio.
%
%       ind - an Nx1 logical that has the same number of rows as
%           portfolioSim.  It specifies which row in portfolioSim
%           corresponds with the portfolio specified by simID.
%
%       benchmarkID - a double that specifies the benchmark from which the
%           specified portfolio was constructed.
%
%   Output:
%       handles - the struct of GUI handles
%
% Developed by Yigal Ben Tal
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dashboard.*;
    import dal.portfolio.data_access.*;

%     global TIMING
    
    %% Input validation: 
    error(nargchk(5,6,nargin));
    
    portfolioID = portfolioSim.portfolio_id(ind);
    lastUpdate = portfolioSim.obs_date{ind};
    
    firstSim = min(portfolioSim.sim_id);
    lastSim = max(portfolioSim.sim_id);
    
    %% Step #0 (Set simulation range and benchmark graphing listbox): 
%     t12 = tic;
    update_sim_range( handles, firstSim, lastSim );
    update_current_sim_id(handles, simID);
%     TIMING(12, :) = {'Set simulation range', toc(t12)};

    % Set the graphing benchmark listbox to the correct values and
    % select the current benchmark value.
    possibleBenchmarks = get(handles.mdl_benchmark_listbox, 'String');
    currentBenchmarkIndex = find(cell2mat(cellfun(@(x) isequal(x,num2str(benchmarkID)),possibleBenchmarks,'UniformOutput',false)));

    % Need to remove the first element of the list ('Benchmark') and then
    % shift the index by one to reflect this change.
    set(handles.prt_graphed_benchmark_listbox, 'String',possibleBenchmarks(2:end));
    set(handles.prt_graphed_benchmark_listbox, 'Value',currentBenchmarkIndex-1);
    
    %% Step #1 (Get portfolio summary information):
%     t13 = tic;
    [ portSummaryTbl, creationDate ] = get_portfolio_summary( portfolioID, lastUpdate );
%     TIMING(13,:) = {'get_portfolio_summary', toc(t13)};
    
    %% Step #2 (Get portfolio statistics of risk and performance measures):
%     t14 = tic;
    [ portStatisticsTbl ] = get_portfolio_statistics( portfolioID, datenum(creationDate)+1, lastUpdate );
%     TIMING(14,:) = {'get_portfolio_statistics', toc(t14)};

    %% Step #3 (Get cumulative return of portfolio and its given benchmark, and excess return over given benchmark return):
%     t15 = tic;
    [ portCumulativeReturn, benchCumulativeReturn, posExcessRet, negExcessRet ] = get_cumulative_return(portfolioID, benchmarkID, creationDate, lastUpdate );
%     TIMING(15, :) = {'get_cumulative_return', toc(t15)};
    
    %% Step #4 (Show results in portfolio tab):
%     t16 = tic;
    [handles] = show_portfolio_info( handles, portSummaryTbl, portStatisticsTbl, portCumulativeReturn, benchCumulativeReturn, posExcessRet, negExcessRet  );
%     TIMING(16,:)  ={'show_portfolio_info', toc(t16)};
end
 


