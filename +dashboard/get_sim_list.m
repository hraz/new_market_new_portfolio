function [ simList ] = get_sim_list( handles )
%GET_SIM_LIST returns the list of portfolios returned by the database after
%   a constraint search in the model tab.
%   
%   [ simList ] = get_sim_list( handles ) takes as input the GUI handles
%   and returns a list of all portfolios that satisfy constraint criteria
%   specified in the model tab.  The portfolio ID, obs_date and SIM
%   ID are all provided.
%
%   Input:
%       handles - the struct of GUI handles
%
%   Output:
%       simList - 1x1 cell array that contains a Nx3 dataset.  The rows are
%       all portfolios and the columns are 'portfolio_id', 'obs_date', and
%       'sim_id'.
%
% Developed by Yigal Ben Tal
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    simList = get(handles.prt_sim_range_pnl, 'UserData');
    if ~isempty(simList)
        simList = simList{:};
    end

end

