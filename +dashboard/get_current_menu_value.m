function [ listCurrentConstraintPossibilities, currentValue, timePeriodBound, simBound ] = get_current_menu_value( handles, isrnd)
%GET_CURRENT_MENU_VALUE finds the current values selected by the user for
%   each constraint listbox on the dashboard.
%
%   [ listCurrentConstraintPossibilities, currentValue, timePeriodBound,
%   simBound ] = get_current_menu_value( handles, isrnd) receives the
%   handles of the GUI object and returns current values from
%	all constraint lists.
%
% Input:
%       handles - is the dashboard GUI handles.
%
%       isrnd - is a boolean flag specifying whether random updating of the
%       constraint listboxes has been requested. (Default is FALSE).
%
%   Output:
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 22, 2013 (implemented multiple
% selections in the constraint listboxes and updated comments)
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

%% Input validation.
error(nargchk(1, 2, nargin));

if nargin < 2 || isempty(isrnd)
    isrnd = false;
end

%% Get current constraint values.
numFilters = size(handles.filter_list,1);
for i = 1:numFilters
    
    %% Get possible constraints listed within each constraint listbox.
    currentConstraintPossibilities = get(handles.([ 'mdl_' , handles.filter_list{i}, '_listbox']), 'String');
    if ~iscell(currentConstraintPossibilities)
        listCurrentConstraintPossibilities.(handles.filter_list{i}) = {currentConstraintPossibilities};
    else
        listCurrentConstraintPossibilities.(handles.filter_list{i}) = currentConstraintPossibilities;
    end
    
    %% Get the indices of the current selected constraint values.
    if isrnd
        indexCurrentValue = rnd_period( 1, length(list.(handles.filter_list{i})));
    else
        indexCurrentValue = get(handles.([ 'mdl_' , handles.filter_list{i}, '_listbox']), 'Value');
    end
    
    %% Get the current constraint values using their indices.
    currentValue.(handles.filter_list{i}) = listCurrentConstraintPossibilities.(handles.filter_list{i})(indexCurrentValue);
    
end

%% Get time period bounds:
timePeriodBound.from_date = get(handles.mdl_date_from_edit, 'String');
timePeriodBound.to_date = get(handles.mdl_date_to_edit, 'String');

%% Get given simulation range:
simBound.from_sim = get(handles.mdl_sim_from_edit, 'String');
simBound.to_sim = get(handles.mdl_sim_to_edit, 'String');

end

