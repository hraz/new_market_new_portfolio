function namesList = numbers_to_names_reinvestment(numbersList)

%NUMBERS_TO_NAMES_REINVESTMENT changes a list of numbers to a list of
%   names using an internal table.
%
%   *THIS IS A TEMPORARY FUNCTION THAT WILL BECOME UNNECESSARY AFTER THE
%   SWITCH TO THE NEW PORTFOLIO DATABASE*
%
%   namesList = numbers_to_names_reinvestment(numbersList) receives an
%   Nx1 cell array of strings of numbers that correspond with reinvestment
%   options from the database and changes them to a list of names that more
%   accurately describes those reinvestment options.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

REINVESTMENT_TABLE = {'Spec Bond','Risk Free','Portfolio','Benchmark','Bank','None','Real Bond'};

namesList{1} = numbersList{1};

for i = 2 : numel(numbersList)
    namesList{i} = REINVESTMENT_TABLE{str2double(numbersList(i))};
end

% output the required column vector
namesList = namesList';