function handles = update_sim_nums(handles)
% Get the simulation numbers that match the current model and type that
% were chosen

if (handles.Model==1) && (handles.Optimization==1)
    handles.SimNums = [1:10]';
elseif (handles.Model==2) && (handles.Optimization==1)
    handles.SimNums = [11:20]';
elseif (handles.Model==2) && (handles.Optimization==2)
    handles.SimNums = [21:30]';
elseif (handles.Model==2) && (handles.Optimization==3)
    handles.SimNums = [31:40]';
elseif (handles.Model==2) && (handles.Optimization==4)
    handles.SimNums = [41:50]';
else (handles.Model==2) && (handles.Optimization==5)
    handles.SimNums = [51:60]';
end
set(handles.SimNumsG,'String',mat2cell(handles.SimNums,size(handles.SimNums,1), size(handles.SimNums,2)));
tmp = get(handles.SimNumsG,'String');
handles.SelectedSim = str2num(tmp{get(handles.SimNumsG,'Value')});




