function txt = cursor_update_function(empty, eventObj)
%CURSOR_UPDATE_FUNCTION updates the text outputted by the data cursor in
%   the Matlab GUI
%
%   txt = cursor_update_function(eventObj) is set in the data cursor as the
%   property 'UpdateFcn'.
%
%   Inputs:
%       empty - is not used in this function, but is required for the
%           cursor to update correctly (I have no idea why - this function
%           is clearly called with two inputs, but only one is necessary
%           now).
%
%       eventObj - information from the cursor.
%
%   Output:
%       txt - sets the text that the cursor outputs when a user clicks on a point
%           on a GUI plot.
%
% This code is taken from the Matlab help doc "datacursormode".
%
% Modified by Eleanor Millman, August 12, 2013
% Copyright 2013, BondIT Ltd.

pos = get(eventObj,'Position');
txt = {['Date: ',datestr(pos(1))],['Return: ',num2str(pos(2))]};
