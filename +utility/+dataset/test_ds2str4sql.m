clear, clc;

import utility.dataset.*;

securityID = [1234567,1534568,7534568]';
unitNum = [12,15,15]';
dates = datenum({'2013-12-12 14:25:36', '2013-12-12 14:25:39', '2013-12-12 18:29:39'});
transactionCost = [0.25234, 1.22340, 0.2230]';
actionAmount = [345, 195,205]';

ds = dataset(dates, securityID, unitNum, transactionCost, actionAmount);
flag = 1;

[ string ] = ds2str4sql( ds, flag )

