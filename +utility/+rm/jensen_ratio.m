function [ j_alpha ] = jensen_ratio( asset_ret, asset_beta, bench_er, rf )
%JENSEN calculates Jensen alpha ratio.
%
%   [ j_alpha ] = jensen( asset_ret, asset_beta, bench_er, rf )
%   returns Jensen ratio on base asset/portfolio and market data.
%
%   Input:
%       asset_ret - is a scalar value specifying the asset/portfolio return.
%
%       asset_beta - is a scalar value specifying the asset/portfolio beta.
%
%       bench_er - is a scalar value specifying hte market return.
%
%       rf - is a scalar value specifying the risk neutral rate of return.
%
%   Output:
%       j-alpha - is a scalar value specifyin the asset/portfolio Jensen ratio.
%
% Yigal Ben Tal
% Copyright 2012

    %% Input validation:
    error(nargchk(4,4,nargin));
    
    %% Jensen alpha calculation:
    j_alpha = asset_ret - (rf + asset_beta * (bench_er - rf));
    
end

