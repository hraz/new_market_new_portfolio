function [ acceptable_length, non_acceptable_ind ] = acceptable_min(data, infinum )
%ACCEPTABLE_MIN receives history data of all assets and minimum
% acceptable limit, and returns an acceptable minimum history length for given portfolio and a
% vector  of assets indexes with too short history.
%
%    [ acceptable_length, non_acceptable_ind ] = AcceptableMinFind(data,infinum )
%
%   Inputs:
%       data - is an MAXNUMOBSERVATIONSxNASSETS fts specifying the history data of all assets in
%                  given portfolio. 
%
%       infinum - is a scalar scpecifying the infinum of history data lengths.
%
%   Outputs:
%       acceptable_length - is a scalar specifying the acceptable minimal history length based on given timeseries.
%
%       non_acceptable_ind - is an 1xNUMNONACCEPTABLEIND vector specifying the asset indexes which
%                   have too short history. 
%

% Yigal Ben Tal
% Copyright 2010-2012.

    %% Input validation:
    error(nargchk(2,2,nargin));
    if (isempty(data))
        error('utility:acceptable_min:mismatchInput', 'There is an empty data argument.');
    end
%     if (~isa(data, 'fints'))
%         error('utility:acceptable_min:wrongInput', 'Wrong type ofthe data argument.');
%     end
    
    %% acceptable_length calculation:
    [~, NumAssets] = size(data);
    HistLength = zeros(1, NumAssets);
    for i=1:NumAssets
        HistLength(i) = numfinite(data(:, i)); %because data includes NaN values at the empty data places
    end
    clear NumAssets;
    
    non_acceptable_ind = find(HistLength(:) < infinum);
    acceptable_length = min( setxor( HistLength, HistLength(non_acceptable_ind) ) );
    
end
