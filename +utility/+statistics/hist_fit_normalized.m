function [] = hist_fit_normalized(data) 
%HIST_FIT_NORMALIZED provides a normalized histogram plus a normal
%   distribution fit line for a set of data.
%
%   function [] = hist_fit_normalized(data) takes in an array of doubles
%   and creates a normalized bar graph plus a fit line that is a normal
%   distribution with the same mean and standard deviation as the data.
%
%   Input:
%       data - a Nx1 array of doubles    
% 
%   Output:
%       none
%
%   **This code is an almost exact replica of the Matlab function
%   histfit.**
%
%   Eleanor Millman, July 31, 2013   
%   Copyright 2013, BondIT Ltd. 

NBINS = 100;

if ~isvector(data)
   error(message('hist_fit_normalized: Vector Required'));
end

data = data(:);
data(isnan(data)) = [];
n = numel(data);

x = linspace(min(data), max(data), NBINS);
h = hist( data, x );
hh = bar(x, h/sum(h), 'hist');


try
    pd = fitdist(data,'normal');
catch myException
    if isequal(myException.identifier,'stats:ProbDistUnivParam:fit:NRequired')
        % Binomial is not allowed because we have no N parameter
        error(message('stats:histfit:BadDistribution'))
    else
        % Pass along another other errors
        throw(myException)
    end
end

% Find range for plotting
q = icdf(pd,[0.0013499 0.99865]); % three-sigma range for normal distribution
x = linspace(q(1),q(2));
if ~pd.Support.iscontinuous
    % For discrete distribution use only integers
    x = round(x);
    x(diff(x)==0) = [];
end


% Normalize the density to match the total area of the histogram
xd = get(hh,'Xdata');             % Gets the x-data of the bins.
rangex = max(xd(:)) - min(xd(:)); % Finds the range of this data.
binwidth = rangex/NBINS;          % Finds the width of each bin.
area = n * binwidth;
%y = pdf(pd,x);
y = area* pdf(pd,x)/n;

% Overlay the density
np = get(gca,'NextPlot');    
set(gca,'NextPlot','add')
hh1 = plot(x,y,'r-','LineWidth',2);

set(gca,'NextPlot',np) 
