function [h, p] = proportiontest( sampleDist, nullProportion, significance, setPoint )
%PROPORTIONTEST tests the location of a sample distribution compared to a
%   normal distribution.
%
%   [h, p] = proportiontest( sampleDist, nullProportion, significance,
%   setPoint ) receives a set of data, performs a proportional hypothesis
%   test on it, and outputs the resulting p-value as well as an indicator
%   (h) of whether the null hypothesis was rejected at the specified
%   signifigance. The null hypothesis is that sampleDist is a random sample
%   from a normal population where nullProportion of the data is larger
%   than setPoint.  The alternative hypothesis is that sampleDist is a
%   random sample from a population where more than nullProportion of the
%   data is larger than setPoint. Proportiontest is a right one-tailed
%   (one-sided) test.
%   
%   Input:
%       sampleDist - is a Nx1 vector of doubles.  Represents a sample of
%       data.
%
%       nullProportion - a double between 0 and 1 that represents the
%       proportion of values larger than setPoint.  Default is 0.5 unless
%       an input is provided.
%
%       significance - a double between 0 and 1 that represents the desired
%       level of confidence in the test.  The p-value resulting from the
%       test is compared to this value to determine whether to reject or
%       not reject the null hypothesis. For example, a significance of 0.05
%       corresponds to a 95% confidence level.  Default is 0.05 unless an
%       input is provided.
%
%       setPoint - a double that will divide sampleDist data into two
%       groups: one group that contains values less than or equal to
%       setPoint and on group that contains values greater than setPoint.
%       Default is 0 unless an input is provided.
%
%   Output:
%       h - a boolean scalar specifying the proportion test result:
%
%			0 if the null hypothesis cannot be rejected, i.e. the
%			proportion of values in sampleDist larger than setPoint is not
%			significantly larger than nullProportion.
%
%			1 if the null hypothesis can be rejected, i.e. the proportion
%			of values in sampleDist larger than setPoint is significantly
%			larger than nullProportion.
%
%       p - a double with a value between 0 and 1.  Represents a p-value,
%       which quantifies the probability of obtaining a statistics at least
%       as extreme as the one seen in the sample, assuming the null
%       hypothesis is true.  For example, if p = 0.02, there is only a 2%
%       probability of obtaining a test statistic more extreme than the one
%       seen in the sample.  This suggests that the null hypothesis may not
%       be true, depending on the specified level of significance.
%
%
% Eleanor Millman, July 10, 2013 Copyright 2013, Bond IT Ltd

% Input validation.
error(nargchk(1, 4, nargin));

flagType(1) = isa(sampleDist, 'double');

if nargin < 2 || isempty(nullProportion)
    flagType(2) = 1;
    nullProportion = 0.5; % Default null proportion
else  
    flagType(2) = isa(nullProportion, 'double');
    
    if nullProportion > 1 || nullProportion < 0
        error('proportiontest: nullProportion must be between 0 and 1')
    end
end

if nargin < 3 || isempty(significance)
    flagType(3) = 1;
    significance = 0.05; % Standard 95% confidence level.

else
    flagType(3) = isa(significance, 'double');
    
    if significance>1 || significance<0
        error('proportiontest: significance must be between 0 and 1')
    end
end

if nargin < 4 || isempty(setPoint)
    flagType(4) = 1;
    setPoint = 0; % Default setPoint value
else
    flagType(4) = isa(setPoint, 'double');
end

if sum(flagType) ~= length(flagType)
    error('proportiontest: One or more input arguments is not of the right type');
end

if isempty(sampleDist)
    error('proportiontest: No data')
end

% Calculate sampleProportion, the proportion of the data in sampleDist that
% is greater than setPoint
sampleLargerThanSetPoint = find( sampleDist>setPoint );
sampleProportion = length(sampleLargerThanSetPoint)/length(sampleDist);

% Calculate the standard dev of the sample using the standard formula, then
% set it equal to the population standard deviation.
pStdev = sqrt( nullProportion * (1-nullProportion) );

% Standard error, i.e. the standard dev of the sampling distribution.
standardError = pStdev/sqrt(length(sampleDist));

% Calculate the z-score of sampleDist, i.e. normalize the sampleDist
% proportion using the null proportion and the standard error.
zScore = (sampleProportion-nullProportion)/standardError;

% Find the p-value using Matlab's cdf (cumulative distribution function)
% function for a normal distribution.  The command below yields a p-value
% for a right 1-tailed (1-sided) test.
% Use p = 2 * normcdf(-abs(zScore),0,1) obtain a p-value for a 2-tailed test.
p = normcdf(-zScore,0,1);

% Test the null hypothesis.
if p < significance
    h=1;
else
    h=0;
end