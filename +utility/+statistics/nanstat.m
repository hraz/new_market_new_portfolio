function [ exp_ret, volatility, cov ] = nanstat( value )
%NANSTAT calculates expected return and volatility of inputted values.
%
% [ ExReturn, Volatility ] = nanstat( value, Weight ) receives values and its
% weights and returns its expected return and volatility.
%
%   Inputs:
%       value - it is an NvalueSxNASSETS matrix specifying some values (prices or
%                           yields).
%
%   Outputs:
%       exp_ret - it is an 1xNASSETS vector of expected returns of given
%                               values.
%
%       cov - it is an NASSETSxNASSETS matrix specifying the covariance
%                               matrix of the given values.
%
% Yigal Ben Tal.
% Copyright 2011.

    %% Input validation:
    error(nargchk(1, 1, nargin));
    
    if (nargin < 1)
        error('utility_rm:nanstat:mismatchInputs', ... 
            'There is not enough inputted arguments.');
    end
    
    %% Step #1 (Expected return and covariance matrix of assets):
    exp_ret = nanmean(value);
    volatility = nanstd(value);
    cov = nancov(value);
    
end

