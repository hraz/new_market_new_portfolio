function IRRyear=Week2Year(IRRweek)
%This function translates yearly IRR into weekly IRR
IRRyear=((IRRweek+1).^52-1);