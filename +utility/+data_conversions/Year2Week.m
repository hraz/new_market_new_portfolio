function IRRweek=Year2Week(IRRyear)
%This function translates yearly IRR into weekly IRR
IRRweek=(1+IRRyear).^(1/52)-1;         %convert to weekly returns