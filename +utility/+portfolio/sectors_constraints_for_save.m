function outDS = sectors_constraints_for_save(sectors_constraints)

% import dal.data_access.base_data.*;
% list = da_bond_base_static( 'sector'  );
% sector = list.sector;
sector={
    [804]    'BANKS'                                    
    [625]    'BIOMED'                                   
    [616]    'CHEMICAL RUBBER & PLASTIC'                
    [604]    'COMMERCE'                                 
    [601]    'COMMERCIAL BANKS'                         
    [802]    'COMMUNICATIONS & MEDIA'                   
    [607]    'COMPUTERS'                                
    [805]    'ELECTRICAL'                               
    [614]    'ELECTRONICS'                              
    [612]    'FASHION & CLOTHING'                       
    [608]    'FINANCIAL SERVICES'                       
    [611]    'FOOD'                                     
    [622]    'FOREIGN SHARES AND OPTIONS'               
    [  1]    'GOVERNMENT'                               
    [606]    'HOTELS & TOURISM'                         
    [801]    'INDEX PRODUCTS'                           
    [806]    'INSURANCE'                                
    [603]    'INSURANCE COMPANIES'                      
    [619]    'INVESTMENT & HOLDINGS'                    
    [807]    'INVESTMENTS IN HIGH-TECH'                 
    [800]    'ISSUANCES'                                
    [613]    'METAL'                                    
    [618]    'MISCELLANEOUS INDUSTRY'                   
    [602]    'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS'
    [620]    'OIL & GAS EXPLORATION'                    
    [609]    'REAL-ESTATE AND CONSTRUCTION'             
    [605]    'SERVICES'                                 
    [623]    'STRUCTURED BONDS'                         
    [  0]    'UNKNOWN'                                  
    [617]    'WOOD & PAPER' };

sectors_to_include=sectors_constraints.sectors_to_include;
minWeight=sectors_constraints.minWeight;
maxWeight=sectors_constraints.maxWeight;
if size(minWeight,2)~=size(sectors_to_include,1)
    %the case where the min\max constraint is a scalar meaning it's the
    %same for all sectors (algo team's request)    
    minWeight=minWeight*ones(size(sectors_to_include));
end
if size(maxWeight,2)~=size(sectors_to_include,1)
    %the case where the min\max constraint is a scalar meaning it's the
    %same for all sectors (algo team's request)
    maxWeight=maxWeight*ones(size(sectors_to_include));
end

sectorID=cell2mat(sector(:,1));
minValue=zeros(size(sector,1),1);
maxValue=zeros(size(sector,1),1);

for iSector=1:size(sector,1)
    [secInFlag,loc]= ismember(sector{iSector,2},sectors_to_include);
    if secInFlag
        minValue(iSector)=minWeight(loc);
        maxValue(iSector)=maxWeight(loc);
    else        
        maxValue(iSector)=0;
    end
end
outDS=dataset(sectorID,minValue,maxValue);


