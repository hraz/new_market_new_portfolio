function outDS = model_constraints_for_save(model_constraint)

switch model_constraint.model
    case 'Marko'
        modelID = 1;
    case 'SI'
        modelID = 2;
    case 'Omeg'
        modelID = 3;
    case 'MI'
        modelID = 4;
end

switch model_constraint.optimization
    case 'price_clean_yld'
        optimizationTypeID=1;
    otherwise
        optimizationTypeID=1;
end

switch model_constraint.reinvestmentStrategy
    case 'Specific-Bond-Reinvestment'
        reinvestmentID = 1;
    case 'risk-free'
        reinvestmentID = 2;
    case 'portfolio'
        reinvestmentID = 3;
    case 'benchmark'
        reinvestmentID = 4;
    case 'real-specific-bond-reinvestment'
        reinvestmentID = 7;
end

outDS=dataset(modelID,optimizationTypeID,reinvestmentID);
