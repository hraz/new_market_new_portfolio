function outDS = target_constraints_for_save(target_constraint)

% 3 - discrete target return
% 4 - discrete target risk (not used as to Aug 2013)
% 5 - optimal sharpe
% 6 - decimal target return
% 7 - target optimal sharpe
% 8 - minimum variance

switch target_constraint.optimization_type            
    case 'OS'
        constraintID=5;
    case 'Tar'
        constraintID=6;
    case 'TarOS'
        constraintID=7;
    case 'MV'
        constraintID=8;
end

value=target_constraint.target_return;

outDS=dataset(constraintID,value);
