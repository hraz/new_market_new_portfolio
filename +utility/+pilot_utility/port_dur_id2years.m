function [portfolio_duration port_duration_min port_duration_max] = port_dur_id2years(port_dur_id)

switch port_dur_id
    case 1        
        port_duration_min=1;
        port_duration_max=5;
    case 2
        port_duration_min=5;
        port_duration_max=8;
    case 3
        port_duration_min=8;
        port_duration_max=13;
    case 4
        port_duration_min=13;
        port_duration_max=17;
end
portfolio_duration=mean([port_duration_min port_duration_max]);