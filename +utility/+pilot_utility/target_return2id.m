function target_return_id=target_return2id(weighted_YTM,YTM)

min_YTM=min(YTM);
max_YTM=max(YTM);
relative_YTM=(weighted_YTM-min_YTM)/((max_YTM-min_YTM));
target_return_id=floor(relative_YTM*5)/5;

