function full_allocation=create_full_allocation(transactions,all_dates,nsin_list)
import utility.*;

combined_transactions=zeros(length(all_dates),length(nsin_list));
nsin_indx=find_multi_indx(transactions(:,2),nsin_list);
dates_indx=find_multi_indx_interp(transactions(:,1),all_dates);
for m=1:length(transactions)
    combined_transactions(dates_indx(m),nsin_indx(m))=transactions(m,3);
end
full_allocation=cumsum(combined_transactions);
