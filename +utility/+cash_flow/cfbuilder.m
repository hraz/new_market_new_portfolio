function [ cf, nsin, price_dirty ] = cfbuilder( settle, coupon_type, flag )
%CFBUILDER build total cash flow at given settlement date for required bond type.
%
%   [ cf_fts ] = cfbuilder( settle, coupon_type )
%
%   Input:
%       settle - is a scalar value representing time zero in derivation of the zero curve. Normally
%                   this is also the settlement date for the bonds contained in the portfolio from
%                   which the zero curve will be derived.
%
%       coupon_type - is a scalar value specifying the type of the coupon (0-Zero Coupon or Short
%                   Term Treasury Bill, 1-Government Non-Linked Fixed Coupon bond or Shahar)
%
%   Optional Input:
%       flag - is a boolean scalar specifying the needing to add the current dirty price into the
%                   cash flow.
%
%   Output:
%       cf - is a (NPAYMENTS+1)x1 fts representing result cash flow, where the first row
%                   includes negative cash flow based on current dirty price of all bonds.The fts
%                   padded by NaN when there are no payments. 
%
%       nsin - is an NASSETSx1 numeric vector specifying the list of nsin numbers of the assets in
%                   the cf_fts object.

% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import dal.market.filter.static.*;
    import dal.market.get.static.*;
    import dal.market.get.cash_flow.*;
    import dal.market.get.dynamic.*;
    import utility.cash_flow.*;
    import dal.market.get.base_data.*;


    %% Input validation:
    error(nargchk(2, 3, nargin));
    if isempty(settle)
        settle = today;
    else
        settle = finargdate(settle);
    end
    if isempty(coupon_type)
        coupon_type = 0; % zero coupon bonds.
    elseif ~all(ismember(coupon_type, [0,1,2,3]))
        error('bll_term_structure:cfbuilder:wrongInput', ...
            'The type of needed bonds is wrong.');
    end
    if nargin == 3 && isempty(flag)
        error('bll_term_structure:cfbuilder:wrongInput', ...
            'The last optional variable cannot be an empty if it was inputted.');
    end
    
    %% Step #1 (Create needed input parameters for static filter):
    switch coupon_type 
        case 0
            param.class_name = {'GOVERNMENT BOND'};
            param.coupon_type = {'Zero Coupon'};
        case 1
            param.class_name = {'GOVERNMENT BOND'};
            param.coupon_type = {'Fixed Interest'};
            param.linkage_index = {'NON LINKED'};
        case 2
            param.class_name = {'GOVERNMENT BOND'};
            param.coupon_type = {'Fixed Interest'};
            param.linkage_index = {'CPI'};
         case 3
            param.class_name = {'NON-GOVERNMENT BOND'};
            param.coupon_type = {'Fixed Interest'};
            param.linkage_index = {'NON LINKED'};
%              param.linkage_index = {'NON LINKED'};

        otherwise
            error('utility:cfbuilder:wrongInput', ...
            'The type of needed bonds is wrong.');
    end
    
    %% Step #2 (Find the bonds according to constraints):
    [ nsin ] = bond_static_complex( settle, param );
%         [ nsin ] = get_lived_assets(settle);

    %% Step #3 (Get static data about given assets ):
    [ stat_data ] = get_bond_stat( nsin, settle, {'maturity_date'} );

    %% Step #4 (Filter for short ttm assets):
    res_struct = get_multi_dyn_between_dates( nsin , {'ttm','price_dirty'} , 'bond', settle, settle );
%     [ ttm ] = get_bond_dyn_single_date(nsin, 'ttm', settle);
%     [ price_dirty ] = get_bond_dyn_single_date(nsin, 'price_dirty', settle);
%     [ttm , ind] = sort(fts2mat(ttm));
%     nsin = nsin(ind);
%     nsin = nsin(ttm > 1/52); % ttm > 2 weeks
    nsin = nsin(fts2mat(res_struct.ttm) > 1/52); % ttm > 2 weeks
    
    %% Step #5 (Find max maturity date:):
    last_maturity_date = max(datenum(stat_data.maturity_date));

    %% Step #6 (Get cash flow):
    switch coupon_type
        case 0
            cf = get_cash_flow_stbill( settle, last_maturity_date, nsin );
        case 1
            cf = get_cash_flow( settle, last_maturity_date, nsin );
        case 2
%             cf = get_cash_flow( settle, last_maturity_date, nsin );
            cf = da_bond_amrt(nsin, settle );
        case 3
            cf = get_cash_flow( settle, last_maturity_date, nsin );
        otherwise
            error('bll_term_structure:cfbuilder:wrongInput', ...
            'The type of needed bonds is wrong.');
    end
    cf = merge(cf,-res_struct.price_dirty);
end
