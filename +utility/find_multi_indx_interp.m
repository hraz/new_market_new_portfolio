function out_indx=find_multi_indx_interp(partial_list,full_list,varargin)
% finds the indices of array A in interpolated array B (rounded up or down according to 3rd input). 
% 
% out_indx=find_multi_indx(A,B) where A,B are vectors, returns the biggest(smallest) indices out_indx 
% of B such  that B(out_indx)<=A (B(out_indx)>=A). or maybe the other way
% around
% out_indx=find_multi_indx(A,B) where A,B are multi dimesional returns the biggest indices out_indx
%     of B such that Bv(out_indx)<=Av where Av=A(:) and  Bv=B(:). notice that out_indx
%     is a vector
% % 
% Amit Godel 2012
partial_list=partial_list(:);
full_list=full_list(:);
out_indx=nan(size(partial_list));
if isempty(varargin)||strcmp(varargin{1},'up')
    if ~sum(partial_list>full_list(end))
        for m=1:length(partial_list)
            out_indx(m)=find(full_list>=partial_list(m),1,'first');
        end
    else
        error('some elements of A are not in the range of B, in find_multi_indx_interp(A,B)')
    end
elseif strcmp(varargin{1},'down')
    if ~sum(partial_list>full_list(end))
        for m=1:length(partial_list)
            out_indx(m)=find(full_list<=partial_list(m),1,'last');
        end
    else
        error('some elements of A are not in the range of B, in find_multi_indx_interp(A,B)')
    end
else
    error('error in up\down argument, expected ''up'' or ''down''')
end