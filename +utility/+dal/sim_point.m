function [ stop_point ] = sim_point( first_date, last_date, req_freq )
%SIM_POINT calculates stop points in given time period with required frequency.
%
%   [ stop_point ] = sim_point( first_date, last_date, req_freq )
%   receives bounds of the required time period, required frequency and returns list of stop points.
%
%   Input:
%       first_date - is a scalar value specifying the lower bound of the required period.
%
%       last_date -  is a scalar value specifying the upper bound of the required period.
%
%       req_freq - is a scalar value specifying the number of stop-points in the required period.
%
%   Output:
%       stop_point - is a NDATESx1 vector dates that specifying the stop-dates in required period.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Import external packages:
    import dal.market.get.base_data.*;
        
    %% Input validation:
    error(nargchk(3,3,nargin));
    if isempty(first_date) || isempty(last_date) || isempty(req_freq)
        error('utility_dal:sim_point:wrongInput', 'The input values are not been empty.');
    end
    
    if first_date >= last_date
        error('utility_dal:sim_point:wrongInput', 'The first date must be less then last date.');
    end
    
    %% Step #1 (Definition of possible number of points per a year):
    freq_vs_mnth = [1 2 3 4 6 12; 12 6 4 3 2 1]; 
    % The first row specifying number simulations per one year.
    % The second row specifing the minimal number of months for each number of simulations.

    %% Step #2 (Defining simulation dates to determine the portfolio performance):
    sim_period_mnth = freq_vs_mnth(2,req_freq(1,ones(size(freq_vs_mnth,2),1)) == freq_vs_mnth(1,:));

    %% Step #3 (Get business dates between given bounds with required period):
    stop_point = da_get_sim_dates( sim_period_mnth, first_date, last_date );

end
