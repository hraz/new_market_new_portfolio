function  arr  = add2dsforfilterttl( class_name,coupon_type,linkage_index,sector,bond_type,redemption_error ,arr )
%ADD2DSFORFILTERTTL add the given parameters to the givven dataset
%  
%    [ ds ] = add2dsforfilterttl( class_name,coupon_type,linkage_index,sector,bond_type ,ds ) receives a paramters and add them to the
%    dataset

%   Input:
%       class_name - is a cell array of string containg wanted classes
%       coupon_type - is a cell array of string containg wanted coupon types
%       linkage_index - is a cell array of string containg wanted linkage indices.
%       sector - is a cell array of string containg wanted sectors
%       bond_type - is a cell array of string containg wanted bond types.
%       ds - a dataset on which to add the parameters
% 
%   Output:
%       ds - is dataset containg the given parameters . 
%
% Yaakov Rechtman
% 30/06/2012

 %% Import external packages:
    import utility.dal.*;
     import utility.*;
    
%% Input validation:
    error(nargchk(6, 7, nargin));
    
%      transforming cell array into one string
    stat_param.class_name = class_name;
    stat_param.coupon_type = coupon_type;
    stat_param.linkage_index = linkage_index;
    stat_param.sector = sector;
    stat_param.bond_type =bond_type;
    stat_param.redemption_error =redemption_error;
     
     stat_param = set_fltr_ttl_stat_args(stat_param);
       
%     checking if it's the first input
    if( isempty(arr) || nargin == 5)
        a(1) = stat_param;
        arr = a;
    else
        arr(length(arr) + 1) = stat_param;
%         ds_1 = dataset(stat_param);
%         ds = [ds; ds_1];
    end
   
end

