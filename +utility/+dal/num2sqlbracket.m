function [ string ] = num2sqlbracket(numarr)
%num2sqlbracket Convert a 1-D numeric array to a string in MySQL syntax.
%
%   [ string ] = num2sqlbracket(numarr) converts the 1-D matrix numarr to a string value in MySQl syntax
%   Works as corresponding MAT2STR but the result is a string suitable to using in MySQL.
%
%   Example
%       numarr = [234; 230];
%       num2str4sqlbracketbracket(numarr) produces the string ''('234'), ('230')''; its equal to
%                               ('234'), ('230') as result list for MySQL.
%
%   See also MAT2STR, STRREP, num2str4sqlbracket
%
% Yigal Ben Tal
% Copyright 2012

    %% Input validation:
    if nargin~=1
        error('utility_dal:num2str4sqlbracket:Nargin','Takes 1 input argument.');
    end
    
    if ~isnumeric(numarr)
        error('utility_dal:num2str4sqlbracket:Class','Input argument must be numeric matrix.');
    end
    if ~isvector(numarr)
        error('utility_dal:num2str4sqlbracket:OneDInput','Input matrix must be 1-D.');
    end

    %% Step #1 (Matrix rebuild to one string):
    strnumarr = mat2str(numarr);
    if (size(numarr,1) ==1 &&  size(numarr, 2) == 1)
        string = ['( ', strnumarr, ' )' ];
    else
        if (size(numarr,1) > size(numarr, 2))
            string = strrep(strnumarr, ';', '), (');
        else
            string = strrep(strnumarr, ' ', '), (');
        end
        string = strrep(strrep(string, '[', '('), ']', ')');
    end
    
end
