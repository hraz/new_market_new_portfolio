function [ string ] = num2str4sql(numarr)
%NUM2STR4SQL Convert a 1-D numeric array to a string in MySQL syntax.
%
%   [ string ] = num2str4sql(numarr) converts the 1-D matrix numarr to a string value in MySQl syntax
%   Works as corresponding MAT2STR but the result is a string suitable to using in MySQL.
%
%   Example
%       numarr = [234; 230];
%       num2str4sql(numarr) produces the string '''234, 230'''; its equal to
%                               234, 230 as result list for MySQL.
%
%   See also MAT2STR, STRREP
%
% Created by Yigal Ben Tal.
% Date:		
% Copyright 2011-2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    import utility.dal.mat2str;
    
    %% Input validation:
    if nargin~=1
        error('utility_dal:num2str4sql:Nargin','Takes 1 input argument.');
    end
    
    if ~isnumeric(numarr)
        error('utility_dal:num2str4sql:Class','Input argument must be numeric matrix.');
    end

    %% Step #1 (Matrix rebuild to one string):
    strnumarr = mat2str(numarr, 0,'f');
    [n, m] = size(numarr);
    if (n == 1 && m == 1)
        string = ['"', strnumarr, '"'];
    else
        if (n > m)
            strnumarr = strrep(strnumarr, ';', ',');
        end
        string = strrep(strnumarr, ' ', ',');
        string = strrep(strrep(string, '[', '"'), ']', '"');    
    end
end
