function [ sql_str ] = char4sql( in_data )
%CHAR4SQL reformats string to sql string.
%
%   [ sql_str ] = char4sql( in_data )
%   receive string and return suitable for MySQL string.
%
%   Input:
%       in_data - is an any string value in MATLAB format (char).
%
%   Output:
%       sql_str - is a result string value in MySQL format.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(0,1,nargin));
    
    if ~ischar(in_data)
        error('utility_dal:char4sql:wrongInputType', 'There is wrong input type.');
    end
    
    %% Reformat given string:
    sql_str = ['''''''', in_data, ''''''''];
    
end

