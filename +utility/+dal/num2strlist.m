function [ outStrList ] = num2strlist( integerColumn )
%NUM2STRLIST accepts a matrix and reformats it as a list of numerical strings.
%
%   [ outStrList ] = num2strlist( integerColumn ) 
%   Turns an column of integers into the necessary string format
%   to call the SQL database.  
%   For example, [10,20,30] will become '"'10','20','30'"'. 
%
%   Inputs:
%       integerColumn - is an Nx1 vector of integers.
%
%   Outputs:
%       outStrList - is a string representation of the given values.

% Created by: Yigal Ben Tal
%               at: 24.09.2013
% Copyright 2013, BondIT Ltd.
%
% Updated by: _____________,
%                at: _____________.
% Reason: _______________________________________________.

    %% Input validation:
    error(nargchk(1, 1, nargin));

    if (isempty(integerColumn))
        error('utility_dal:num2strlist:mismatchInput', 'Input is empty.');
    elseif (~isinteger(integerColumn))
        error('utility_dal:num2strlist:mismatchInput', ...
            'Given data must be just a vector of integers.');
    end

    %% Reformatting the given array of strings:
    outStrList =  strrep(['"', sprintf('''%u'',',integerColumn), '"'], ',"', '"');
    
end
